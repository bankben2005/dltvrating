<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['api_rating'] = array(
	'update_device_address'=> array(
		'development'=>'http://s3remoterating.development/api/UpdateDeviceAddress',
		'testing'=>'',
		'production'=>'http://sv-rating.dltv.ac.th/rating/api/UpdateDeviceAddress'
	)
);