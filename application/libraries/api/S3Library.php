<?php


class S3Library{

    private $ci;
    private $tvchannellist_url = "";
    private $check_stream_url = "";
    private $curl_authentication_header = array();
    private $current_youtbe_views_data_table = '';
    function __construct() {
        $CI =& get_instance();
        $this->ci = $CI;
        $this->setTVChannelListUrl();
        $this->setCurrentYoutubeViewsTable();
        //print_r($this->tvchannellist_url);exit;
        $this->curl_authentication_header = array(
            'Content-Type: application/json; charset=utf-8',
            'username:PsiCare',
            'password:dq3JTzXfz9eMYvPW',
            'secretkey:4jeMW5 ',
            'function:GetS3TVChannelList'
        );

        //print_r($this->current_youtbe_views_data_table);exit;
       
    }

    public function setLoginWithFacebook($requestCriteria){
        //print_r($requestCriteria);

        if(!property_exists($requestCriteria, 'facebook_id') || property_exists($requestCriteria, 'facebook_id') == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ Facebook ID'
            );
        }


        $query = $this->ci->db->select('*')
        ->from('members')
        ->where('facebook_id',$requestCriteria->facebook_id)
        ->get();

        if($query->num_rows() > 0){
            /* exist */
            $row = $query->row();

            if(!$row->active){
                return array(
                    'status'=>false,
                    'result_code'=>'-009',
                    'result_desc'=>'คุณไม่มีสิทธิ์เข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
                );

            }


                /* if found return all member data */
                $data_return  = $this->getLoginMemberDataByRow($row);
                return array(
                    'status'=>true,
                    'MemberData'=>$data_return,
                    'login_type'=>'member',
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );


        }else{
            /* not exist */
            /* insert to new record in member table */
            $birthday_datetime = new DateTime($requestCriteria->birthday);

            $query = $this->ci->db->insert('members',array(
                'facebook_id'=>$requestCriteria->facebook_id,
                'firstname'=>$requestCriteria->firstname,
                'lastname'=>$requestCriteria->lastname,
                'email'=>$requestCriteria->email,
                'gender'=>ucfirst($requestCriteria->gender),
                'dob'=> $birthday_datetime->format('Y-m-d'),
                'phone'=>$requestCriteria->phone,
                'picture_profile'=>$requestCriteria->picture_profile,
                'created'=>date('Y-m-d H:i:s'),
                'uuid'=>@$requestCriteria->uuid,
                'os'=>@$requestCriteria->os,
                'os_version'=>@$requestCriteria->os_version,
                'device_type'=>@$requestCriteria->device_type

            ));

            $insert_id = $this->ci->db->insert_id();
            $member_data = $this->ci->db->select('*')
            ->from('members')->where('id',$insert_id)->get();

            return array(
                'status'=>true,
                'MemberData'=>$this->getLoginMemberDataByRow($member_data->row()),
                'login_type'=>'member',
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }


    }

    public function setLoginWithLine($requestCriteria){
       //print_r($requestCriteria);
        if(!property_exists($requestCriteria, 'line_id') || property_exists($requestCriteria, 'line_id') == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ Line ID'
            );
        }


        $query = $this->ci->db->select('*')
        ->from('members')
        ->where('line_id',$requestCriteria->line_id)->get();

        if($query->num_rows() > 0){
            /* exist */
            $row = $query->row();

            if(!$row->active){
                return array(
                    'status'=>false,
                    'result_code'=>'-009',
                    'result_desc'=>'คุณไม่มีสิทธิ์เข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
                );
            }

            /* if found return all member data */
            $data_return  = $this->getLoginMemberDataByRow($row);
                return array(
                    'status'=>true,
                    'MemberData'=>$data_return,
                    'login_type'=>'member',
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );


        }else{
            /* not exist */
            /* insert to new record in member table */
            //$birthday = new date('Y-m-d');
            
            $birthday_datetime = new DateTime(@$requestCriteria->birthday);
            

            // $firstname = $requestCriteria->firstname;
            // $lastname = $requestCriteria->lastname;

            // $firstname = (strpos($firstname, '?') !== false)?str_replace('?', '', $firstname):$firstname;

            // $lastname = (strpos($lastname, '?') !== false)?str_replace('?', '', $lastname):$lastname;

            $query = $this->ci->db->insert('members',array(
                'line_id'=>$requestCriteria->line_id,
                'firstname'=>$requestCriteria->firstname,
                'lastname'=>$requestCriteria->lastname,
                'email'=>$requestCriteria->email,
                'gender'=>ucfirst($requestCriteria->gender),
                'dob'=> $birthday_datetime->format('Y-m-d'),
                'phone'=>$requestCriteria->phone,
                'picture_profile'=>$requestCriteria->picture_profile,
                'created'=>date('Y-m-d H:i:s'),
                'uuid'=>@$requestCriteria->uuid,
                'os'=>@$requestCriteria->os,
                'os_version'=>@$requestCriteria->os_version,
                'device_type'=>@$requestCriteria->device_type

            ));

            $insert_id = $this->ci->db->insert_id();
            $member_data = $this->ci->db->select('*')
            ->from('members')->where('id',$insert_id)->get();

            return array(
                'status'=>true,
                'MemberData'=>$this->getLoginMemberDataByRow($member_data->row()),
                'login_type'=>'member',
                'result_code'=>'000',
                'result_desc'=>'Success'
            );


        }
    }

        public function setLoginWithGuest($requestCriteria){
        //print_r($requestCriteria);exit;
        /* check uuid first */

        $guests = $this->ci->db->select('*')
        ->from('guests')
        ->where('uuid',$requestCriteria->uuid)
        ->get();

        if($guests->num_rows() <= 0){
            $this->ci->db->insert('guests',array(
                'uuid'=>$requestCriteria->uuid,
                'os'=>$requestCriteria->os,
                'os_version'=>$requestCriteria->os_version,
                'device_type'=>$requestCriteria->device_type,
                'created'=>date('Y-m-d H:i:s'),
                'ip_address'=>$this->getUserIP()
            ));

            $insert_id = $this->ci->db->insert_id();
            $guests_insert = $this->ci->db->select('*')
            ->from('guests')
            ->where('id',$insert_id)->get();
            return array(
                'status'=>true,
                'MemberData'=>$guests_insert->row_array(),
                'login_type'=>'guest',
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }else{
            $this->ci->db->update('guests',array(
                'os'=>$requestCriteria->os,
                'os_version'=>$requestCriteria->os_version,
                'device_type'=>$requestCriteria->device_type,
                'ip_address'=>$this->getUserIP(),
                'updated'=>date('Y-m-d H:i:s')
            ),array('uuid'=>$requestCriteria->uuid));

            return array(
                'status'=>true,
                'MemberData'=>$guests->row_array(),
                'login_type'=>'guest',
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }

        
    }

    public function setLoginWithApple($requestCriteria){
        //print_r($requestCriteria);
        if(!property_exists($requestCriteria, 'apple_id') || property_exists($requestCriteria, 'apple_id') == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ Apple ID'
            );
        }


        $query = $this->ci->db->select('*')
        ->from('members')
        ->where('apple_id',$requestCriteria->apple_id)->get();

        if($query->num_rows() > 0){
            /* exist */
            $row = $query->row();

            if(!$row->active){
                return array(
                    'status'=>false,
                    'result_code'=>'-009',
                    'result_desc'=>'คุณไม่มีสิทธิ์เข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
                );
            }

            /* if found return all member data */
            $data_return  = $this->getLoginMemberDataByRow($row);
                return array(
                    'status'=>true,
                    'MemberData'=>$data_return,
                    'login_type'=>'member',
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );


        }else{
            /* not exist */
            /* insert to new record in member table */
            //$birthday = new date('Y-m-d');
            
            $birthday_datetime = new DateTime(@$requestCriteria->birthday);
            

            // $firstname = $requestCriteria->firstname;
            // $lastname = $requestCriteria->lastname;

            // $firstname = (strpos($firstname, '?') !== false)?str_replace('?', '', $firstname):$firstname;

            // $lastname = (strpos($lastname, '?') !== false)?str_replace('?', '', $lastname):$lastname;

            $query = $this->ci->db->insert('members',array(
                'apple_id'=>$requestCriteria->apple_id,
                'firstname'=>$requestCriteria->firstname,
                'lastname'=>$requestCriteria->lastname,
                'email'=>$requestCriteria->email,
                'gender'=>ucfirst(@$requestCriteria->gender),
                'dob'=> @$birthday_datetime->format('Y-m-d'),
                'phone'=>@$requestCriteria->phone,
                'picture_profile'=>@$requestCriteria->picture_profile,
                'created'=>date('Y-m-d H:i:s'),
                'uuid'=>@$requestCriteria->uuid,
                'os'=>@$requestCriteria->os,
                'os_version'=>@$requestCriteria->os_version,
                'device_type'=>@$requestCriteria->device_type

            ));

            $insert_id = $this->ci->db->insert_id();
            $member_data = $this->ci->db->select('*')
            ->from('members')->where('id',$insert_id)->get();

            return array(
                'status'=>true,
                'MemberData'=>$this->getLoginMemberDataByRow($member_data->row()),
                'login_type'=>'member',
                'result_code'=>'000',
                'result_desc'=>'Success'
            );


        }

    }

    public function CheckStreamChannel($requestCriteria){
         /* check property exist  */
        
         if(!property_exists($requestCriteria, 'members_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member property'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'login_type')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found login type'
            ];
        }

        $QueryGetStreamLink_IsActive =  $this->ci->db->select('*')
        ->from('vdo_download_api')
        ->where('active',1)
        ->where('deleted_at',null)
        ->get();
        if(!empty($QueryGetStreamLink_IsActive->result())){
            /* get result and send object to frontend */
            $ObjLink = array();
            foreach ($QueryGetStreamLink_IsActive->result() as $key => $value) {
                /* select url link  */
                $this->setCheckStreamChannelURL();
                $value->fileurl = $this->check_stream_url . '/' . $value->filename;     
                //$value->fileurl = json_encode($value->fileurl,JSON_UNESCAPED_SLASHES);
                /* push to object array for send to front */
                array_push($ObjLink,$value);
            }
            /* eof get result and send object to frontend */
            //$ObjLink=  json_encode($ObjLink,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
            
            return array(
                'status' => true,
                'links' => $ObjLink,
                'result_code' => '000',
                'result_desc' => 'success'

            );

        }else{
            return array(
                'status' => false,
                'result_code' => '-001',
                'result_desc' => 'Stream not found , please try again.'

            );
        }


    }

    public function CheckDownloadLink($requestCriteria){
       
        $download_link =   $this->ci->db->select('*')
        ->from('download_history_log')
        ->where("deleted_at",null)
        ->where('active',"1")
        ->limit(1)
        ->get();
        if($download_link->num_rows() > 0){ 
            $result = $download_link->result();
            return array(
                'status' => true,
                'id' => $result[0]->id,
                'links' => $result[0]->links,
                'result_code' => '000',
                'result_desc' => 'success'

            );
        }
  }

  

    public function getAllChannelList($requestCriteria){
        

        // $this->checkReturnImmediatelyChannel($requestCriteria);

        $dataReturn = array();

        if((property_exists($requestCriteria, 'band_type') && $requestCriteria->band_type != "") && (!property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv != true)){
            $qAllChanels = $this->ci->db->select('*,tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description')
            ->from('tvchannels')
            ->where('tvchannels.active',1)            
            ->where('tvchannels.only_internet_tv',0)
            ->where('tvchannels.band_type',$requestCriteria->band_type)
            ->or_where('tvchannels.band_type','ALL')
            ->order_by('tvchannels.mobile_app_ordinal','asc')
            ->order_by('tvchannels.name','asc')
            ->get();
        }else if((property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv) && (!property_exists($requestCriteria, 'dltv_status') || $requestCriteria->dltv_status == false)){


            $qAllChanels = $this->ci->db->select('*,tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description')
            ->from('tvchannels')
            //->where('tvchannels.active',1)
            ->where('tvchannels.mv_status',1)
            ->or_where('tvchannels.direct_streaming_status',1)
            ->order_by('tvchannels.mobile_app_ordinal','asc')
            ->order_by('tvchannels.name','asc')
            ->get();

            // if(isset($_GET['test']) && $_GET['test'] == 'test'){
            //    echo $this->ci->db->last_query();exit;
            // }

            //
        }else if((property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv == true) && (property_exists($requestCriteria, 'dltv_status') && $requestCriteria->dltv_status == true)){

            if(isset($_GET['test']) && $_GET['test'] == 'test'){
                print_r($requestCriteria);exit;
            }
            if(property_exists($requestCriteria, 'band_type') && $requestCriteria->band_type != ''){
                $qAllChanels = $this->ci->db->select('*,tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description')
                ->from('tvchannels')
                ->where('tvchannels.dltv_status',1)
                ->where('tvchannels.active',1)
                ->where('dltv_status',1)
                ->where('tvchannels.band_type',$requestCriteria->band_type)
                ->or_where('tvchannels.band_type','ALL')
                ->order_by('tvchannels.id','asc')
                ->get();

            }else{
                $qAllChanels = $this->ci->db->select('*,tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description')
                ->from('tvchannels')
                ->where('tvchannels.dltv_status',1)
                ->where('tvchannels.active',1)
                ->where('dltv_status',1)
                ->order_by('tvchannels.id','asc')
                ->get();

            }
            

        }else{
            /* get all chanels from TVChanels table */
            $qAllChanels = $this->ci->db->select('*,tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description')
            ->from('tvchannels')
            ->where('tvchannels.active',1)
            ->order_by('tvchannels.mobile_app_ordinal','asc')
            ->order_by('tvchannels.name','asc')
            ->get();
        }

        if($qAllChanels->num_rows() > 0){
            //print_r($qAllChanels->result());
            foreach ($qAllChanels->result() as $key => $value) {
                # code...
                //echo $value->active.'<br>';
                if($value->active){

                    if((!property_exists($requestCriteria, 'internet_tv') || $requestCriteria->internet_tv == false) && $value->only_internet_tv == 0){ // this for satellite without only_internet_tv status
                            $dataPush = array(
                                'channel_id'=>$value->channel_id,
                                'channel_logo'=>$this->getChannelLogo($value->logo),
                                'channel_name' => $value->channel_name,
                                'channel_description' => $value->channel_description,
                                //'channel_ascii_code'=>($value->channel_ascii_code)?$value->channel_ascii_code:"",
                                'channel_ascii_code'=>$this->getChannelAsciiCode(array(
                                    'requestCriteria'=>$requestCriteria,
                                    'channel_data'=>$value
                                )),
                                'band_type'=>$value->band_type,
                                'dltv_status'=>$value->dltv_status
                                //'channel_category'=>$this->getChannelCategory($value->channel_id)
                                // 'channel_category' => $value->category_name,
                                // 'channel_category_description' => $value->category_description
                            );
                            array_push($dataReturn, $dataPush);
                    }else if(property_exists($requestCriteria, 'internet_tv')){ //    this for internet tv
                            $dataPush = array(
                                'channel_id'=>$value->channel_id,
                                'channel_logo'=>$this->getChannelLogo($value->logo),
                                'channel_name' => $value->channel_name,
                                'channel_description' => $value->channel_description,
                                //'channel_ascii_code'=>($value->channel_ascii_code)?$value->channel_ascii_code:"",
                                'channel_ascii_code'=>$this->getChannelAsciiCode(array(
                                    'requestCriteria'=>$requestCriteria,
                                    'channel_data'=>$value
                                )),
                                'band_type'=>$value->band_type,
                                'dltv_status'=>$value->dltv_status
                                // 'channel_category'=>$this->getChannelCategory($value->channel_id)
                                // 'channel_category' => $value->category_name,
                                // 'channel_category_description' => $value->category_description
                            );
                            array_push($dataReturn, $dataPush);

                    }
                }

            }


            /* write json file for each type */

            
                $this->writeJsonFileForEachType([
                    'requestCriteria'=>$requestCriteria,
                    'json_data' => [
                        'status' => true,
                        'totals'=>count($dataReturn),
                        'Channels' => $dataReturn,
                        'result_code' => '000',
                        'result_desc' => 'success'
                    ]
                ]);
            
            

            /* eof write json file for each type */

            return array(
                'status' => true,
                'totals'=>count($dataReturn),
                'Channels' => $dataReturn,
                'result_code' => '000',
                'result_desc' => 'success'

            );
        }else{
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'ไม่พบข้อมูลช่อง'
            );
        }
    }

    public function GetInternetTvChannelList($requestCriteria){
    
        

            // $this->checkReturnImmediatelyChannel($requestCriteria);
    
            $dataReturn = array();
    
           if((property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv) &&  $requestCriteria->internet_tv == true){
    
    
                $qAllChanels = $this->ci->db->select('*,tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description')
                ->from('tvchannels')
                ->where('tvchannels.active',1)
                ->where("(direct_streaming_status='1' OR mv_status='1')")
                ->order_by('tvchannels.mobile_app_ordinal','asc')
                ->order_by('tvchannels.name','asc')
                ->get();

               
                // if(isset($_GET['test']) && $_GET['test'] == 'test'){
                     // echo '<PRE>';
                    // print_r($qAllChanels->result());exit();
                //    echo $this->ci->db->last_query();exit;
                // }
    
            }else{
                /* get all chanels from TVChanels table */
                return array(
                    'status' => false,
                    'result_code' => '-002',
                    'result_desc' => 'Not found channel.'
                );
            }
    
            if($qAllChanels->num_rows() > 0){
                //print_r($qAllChanels->result());
                foreach ($qAllChanels->result() as $key => $value) {
                    # code...
                    //echo $value->active.'<br>';
                    if($value->active){
    
                        if(property_exists($requestCriteria, 'internet_tv')){ //    this for internet tv
                                //if mv_status = 0 then use direct_streaming_link
                                if($value->mv_status == 0){

                                }
                                $dataPush = array(
                                    'channel_id'=>$value->channel_id,
                                    'channel_name' => $value->channel_name,

                                );
                                array_push($dataReturn, $dataPush);
                        }
                    }
    
                }
    
                 //'channel_id'=>$value->channel_id,
                 // 'channel_logo'=>$this->getChannelLogo($value->logo),
                 //'channel_name' => $value->channel_name,
                 // 'channel_description' => $value->channel_description,
                 // 'channel_ascii_code'=>$this->getChannelAsciiCode(array(
                 //     'requestCriteria'=>$requestCriteria,
                 //     'channel_data'=>$value
                 // )),
                 // 'band_type'=>$value->band_type,
                 // 'dltv_status'=>$value->dltv_status
                /* write json file for each type */
    
                
                    $this->writeJsonFileForEachType([
                        'requestCriteria'=>$requestCriteria,
                        'json_data' => [
                            'status' => true,
                            'result_code' => '000',
                            'result_desc' => 'success',
                            'totals'=>count($dataReturn),
                            'Channels' => $dataReturn
                          
                        ]
                    ]);
                
                
    
                /* eof write json file for each type */
    
                return array(
                    'status' => true,
                    'result_code' => '000',
                    'result_desc' => 'success',
                    'totals'=>count($dataReturn),
                    'Channels' => $dataReturn
                 
    
                );
            }else{
                return array(
                    'status' => false,
                    'result_code' => '-002',
                    'result_desc' => 'Not found channel.'
                );
            }
    
    
     
    }

    public function getdltv_channel($id = null,$active = null){
        $channel_arr = array(161,162,163,164,165,166,167,168,169,170,171,172,173,174,175);
        $query = $this->ci->db->select('tvchannels.*,tvchannels.id,tvchannels.name as channel_name, tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description,tvchannels_externallinks.id as external_id,tvchannels_externallinks.url as youtube_url,tvchannels_externallinks.active as youtube_link_status')
        ->from('tvchannels')
        ->join('tvchannels_externallinks','tvchannels.id = tvchannels_externallinks.channel_id','left')
        ->where('tvchannels_externallinks.active' , 1);
        if($id != null)
        {
            $query = $query->where('tvchannels.id',$id);
        }else{
            $query = $query->where_in('tvchannels.id', $channel_arr);
        }
        if($active != null)
        {
            $query = $query->where('tvchannels.active',"1");
        }
        $results = $query->get();
        if($results->num_rows() > 0){
            return $results->result();
        }else{
            return 0;
        }
    }


    public function GetDLTVYoutubeURL($requestCriteria){
    
     
        // $this->checkReturnImmediatelyChannel($requestCriteria);

        $dataReturn = array();

       if(property_exists($requestCriteria, 'channel_id')){

            $channel_id = $requestCriteria->channel_id;
            if($channel_id == "all"){
                $qAllChanels = $this->getdltv_channel(null,null);
            }else{
                $qAllChanels = $this->getdltv_channel($channel_id,null);
            }
            


        }else{
            /* get all chanels from TVChanels table */
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'Not found channel.'
            );
        }
     
        if(count($qAllChanels) > 0){
            //print_r($qAllChanels->result());
            foreach ($qAllChanels as $key => $value) {
                # code...
           
                if($value->youtube_link_status){
                           // echo $value->logo;exit();
                            $dataPush = array(
                                'channel_id'=>$value->channel_id,
                                'channel_name' => $value->channel_name,
                                'channel_description' => $value->channel_description,
                                'channel_logo'=>$this->getChannelLogo($value->logo),
                                'channel_ascii_code'=>($value->channel_ascii_code)?$value->channel_ascii_code:"",
                                'band_type'=>$value->band_type,
                                'youtube_url' => $value->youtube_url,

                            );
                            array_push($dataReturn, $dataPush);
                  
                }

            }

     
            
            

            /* eof write json file for each type */

            return array(
                'status' => true,
                'result_code' => '000',
                'result_desc' => 'success',
                'totals'=>count($dataReturn),
                'Channels' => $dataReturn
             

            );
        }else{
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'Not found channel.'
            );
        }

    }
    public function SetInternetTvChannelClick($requestCriteria){
        
        $qChannels = $this->ci->db->select('*,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description,tvchannels.active as channel_active')
        ->from('tvchannels')
        ->where('tvchannels.active',1)
        ->where('tvchannels.id',$requestCriteria->channel_id)
        ->get();

        if($qChannels->num_rows() > 0){

            /* check if this channels has been disable by active status */
            if(!$qChannels->row()->channel_active){
                return array(
                    'status' => false,
                    'result_code'=>'-009',
                    'result_desc'=>'this channel has been unactive'
                );
            }
            /* eof check channels active */
            

            /* before get channel detail, insert data to tvchannelviews */
            $dataInsertTvChannelClick = array(
                'tvchannels_id'=>$requestCriteria->channel_id,
                'client_ip'=>$this->getUserIP(),
                'members_id'=>$requestCriteria->members_id,
                'created'=>date('Y-m-d H:i:s')
            );


            if($this->ci->db->insert('tvchannelclicks',$dataInsertTvChannelClick)){
                $channel_data = $qChannels->row();



                if(property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type == 'member'){
                    //============= insert to tvaudiencerecent ==============
                    $this->updateTvAudienceView(array('members_id'=>$requestCriteria->members_id,'channel_id'=>$requestCriteria->channel_id));
                    //============= eof insert to tvaudience recent =========
                }else if(property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type == 'guest'){
                    $this->updateTVGuestView(array(
                        'guests_id'=>$requestCriteria->members_id,
                        'channel_id'=>$requestCriteria->channel_id
                    ));
                }else{
                    //============= insert to tvaudiencerecent ==============
                    $this->updateTvAudienceView(array('members_id'=>$requestCriteria->members_id,'channel_id'=>$requestCriteria->channel_id));
                    //============= eof insert to tvaudience recent =========
                }


                //print_r($channel_data);exit;
                //print_r($channel_data);exit;
                $arr_channel = array(
                    'channel_id'=>$channel_data->channel_id,
                    'channel_url'=> $this->getChannelUrl($channel_data), 
                    //'channel_logo'=>$this->getChannelLogo($channel_data->logo),
                    'channel_name' => $channel_data->channel_name,
                    //'channel_description' => $channel_data->channel_description,
                );  

                return array(
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success',
                    'ChannelData'=>$arr_channel,
                    
                );



            }

        }else{
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'Not found channel.'
            );
        }
    }

    public function setChannelClick($requestCriteria){
        
        $qChannels = $this->ci->db->select('*,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description,tvchannels.active as channel_active')
        ->from('tvchannels')
        ->where('tvchannels.active',1)
        ->where('tvchannels.id',$requestCriteria->channel_id)
        ->get();

        if($qChannels->num_rows() > 0){

            /* check if this channels has been disable by active status */
            if(!$qChannels->row()->channel_active){
                return array(
                    'status' => false,
                    'result_code'=>'-009',
                    'result_desc'=>'ช่องรายการดังกล่าวถูกยกเลิกไว้ในขณะนี้'
                );
            }
            /* eof check channels active */


            /* before get channel detail, insert data to tvchannelviews */
            $dataInsertTvChannelClick = array(
                'tvchannels_id'=>$requestCriteria->channel_id,
                'client_ip'=>$this->getUserIP(),
                'members_id'=>$requestCriteria->members_id,
                'created'=>date('Y-m-d H:i:s')
            );


            if($this->ci->db->insert('tvchannelclicks',$dataInsertTvChannelClick)){
                $channel_data = $qChannels->row();



                if(property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type == 'member'){
                    //============= insert to tvaudiencerecent ==============
                    $this->updateTvAudienceView(array('members_id'=>$requestCriteria->members_id,'channel_id'=>$requestCriteria->channel_id));
                    //============= eof insert to tvaudience recent =========
                }else if(property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type == 'guest'){
                    $this->updateTVGuestView(array(
                        'guests_id'=>$requestCriteria->members_id,
                        'channel_id'=>$requestCriteria->channel_id
                    ));
                }else{
                    //============= insert to tvaudiencerecent ==============
                    $this->updateTvAudienceView(array('members_id'=>$requestCriteria->members_id,'channel_id'=>$requestCriteria->channel_id));
                    //============= eof insert to tvaudience recent =========
                }


                //print_r($channel_data);exit;
                //print_r($channel_data);exit;
                $arr_channel = array(
                    'channel_id'=>$channel_data->channel_id,
                    'channel_url'=> $this->getChannelUrl($channel_data), 
                    'channel_logo'=>$this->getChannelLogo($channel_data->logo),
                    'channel_name' => $channel_data->channel_name,
                    'channel_description' => $channel_data->channel_description,
                );  

                return array(
                    'status'=>true,
                    'ChannelData'=>$arr_channel,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );



            }

        }else{
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'ไม่พบข้อมูลช่องนี้ในระบบ'
            );
        }
    }

    public function getChannelRecentView($requestCriteria){
        $arChannels = array();


        if(property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type == 'member'){
            $arChannels = $this->getMemberChannelRecentView($requestCriteria);
        }else if(property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type == 'guest'){
            $arChannels = $this->getGuestChannelRecentView($requestCriteria);
        }else{
            $arChannels = $this->getMemberChannelRecentView($requestCriteria);
        }

        return array(
                'status' => true,
                'RecentChannels'=>$arChannels,
                'result_code'=>'000',
                'result_desc'=>'Success'
        );
        
    }



    public function setChannelRating($requestCriteria){
        //print_r($requestCriteria);exit;
        
    }

    public function setChipCodeLatLon($requestCriteria){
        
        /* check ship code and create if no exist */
        $checkChipCodeExist = $this->ci->db->select('id,chip_code')
        ->from('devices')
        ->where('chip_code',$requestCriteria->chip_code)
        ->get();
        /* */

        /* check update device token by login type and device token from mobile  */
        $this->updateDeviceToken([
            'requestCriteria'=>$requestCriteria
        ]);

        /* eof device token  */

        if($checkChipCodeExist->num_rows() <= 0){
            /* create devices */
            $this->ci->db->insert('devices',array(
                'chip_code'=>$requestCriteria->chip_code,
                'ip_address'=>$this->getUserIP(),
                'latitude'=>$requestCriteria->latitude,
                'longitude'=>$requestCriteria->longitude,
                'created'=>date('Y-m-d H:i:s')
            ));

            $insert_id = $this->ci->db->insert_id();

            /* create member or guest devices */
            $insert_member_or_guest_devices = $this->InsertMemberOrGuestDevices(array(
                'devices_id'=>$insert_id,
                'requestCriteria'=>$requestCriteria
            ));

            if($insert_member_or_guest_devices){
                /* send curl for update devices lat lon and address into ratting*/
                $update_rating_db = $this->curlUpdateDevicesIntoRatingDB($requestCriteria);
                /* eof send curl */

                if($update_rating_db){
                    /* insert into table device_update_to_rating_logs */
                    $this->ci->db->insert('device_update_to_rating_logs',array(
                        'devices_id'=>$insert_id,
                        'chip_code'=>$requestCriteria->chip_code,
                        'log_message'=>json_encode($requestCriteria),
                        'created'=>date('Y-m-d H:i:s')
                    ));
                    /* update recrod */
                    $this->ci->db->update('devices',array(
                        'update_address_to_rating'=>1,
                        'updated'=>date('Y-m-d H:i:s')
                    ),array('id'=>$insert_id));
                }


                return array(
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );

            }else{
                return array(
                'status'=>false,
                'result_code'=>'003',
                'result_desc'=>'Cannot insert some data [member device or guest device]'
                );
            }
        }else{

            /* if exist device in table then check set devices for this member */
            $check_member_guest_devices = $this->CheckMemberOrGuestDevices(array(
                'devices_id'=>$checkChipCodeExist->row()->id,
                'requestCriteria'=>$requestCriteria
            ));
            /* EOF */

            return array(
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );

            
        }



        
        

    }

    public function getChannelListByBandType($requestCriteria){
        //print_r($requestCriteria);



        $dataReturn = array();
        /* get all chanels from TVChanels table */
        $qAllChanels = $this->ci->db->select('*,tvchannels.api_id as chanel_api_id,tvchannels.id as channel_id,tvchannels.name as channel_name,tvchannels.description as channel_description')
        ->from('tvchannels')
        ->where('tvchannels.active',1)
        ->where('band_type',$requestCriteria->band_type)
        ->order_by('tvchannels.channel_number','asc')
        ->get();

        if($qAllChanels->num_rows() > 0){
            //print_r($qAllChanels->result());
            foreach ($qAllChanels->result() as $key => $value) {
                # code...
                $dataPush = array(
                    'channel_id'=>$value->channel_id,
                    'channel_logo'=>$this->getChannelLogo($value->logo),
                    'channel_name' => $value->channel_name,
                    'channel_description' => $value->channel_description,
                    'channel_ascii_code'=>($value->channel_ascii_code)?$value->channel_ascii_code:"",
                    'band_type'=>$value->band_type
                    //'channel_category'=>$this->getChannelCategory($value->channel_id)
                    // 'channel_category' => $value->category_name,
                    // 'channel_category_description' => $value->category_description
                );
                array_push($dataReturn, $dataPush);

            }

            return array(
                'status' => true,
                'totals'=>count($dataReturn),
                'Channels' => $dataReturn,
                'result_code' => '000',
                'result_desc' => 'success'

            );
        }else{
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'ไม่พบข้อมูลช่อง'
            );
        }

    }

    public function getChannelAdvertisementMobilePopup($requestCriteria){
        //print_r($requestCriteria);exit;

        /* check channel by band type and and ascii code */
        
        
        //echo $this->ci->db->last_query();
        $channel_data = $this->getChannelMobilePopupByCriteria($requestCriteria);

        if($channel_data){
            //print_r($channel_data);exit;
            

        }else{
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบช่องรายการ'
            );
        }
        /* eof check channel */
    }

    public function getYoutubeCategory($requestCriteria){
        //print_r($requestCriteria);
        $arrCategory = array();
        $query = $this->ci->db->select('*')
        ->from('youtube_categories')
        ->where('active',1)
        ->order_by('ordinal')
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                array_push($arrCategory, array(
                    'category_id'=>$value->id,
                    'category_ordinal'=>$value->ordinal,
                    'category_name'=>$value->name,
                    'category_cover'=> base_url('tv/uploaded/youtube_category/'.$value->id.'/'.$value->cover_image),
                    'category_description'=>$value->description,
                    'category_search_keyword'=>$value->search_keyword

                ));
            }
        }

        //print_r($arrCategory);
        return array(
            'status'=>true,
            'Categories'=>$arrCategory,
            'YoutubeAPIKeys'=>$this->getYoutubeAPIKeys(),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function setMemberGenderAndBirthday($requestCriteria){
        //print_r($requestCriteria);
        /* check member or guest */
        if(property_exists($requestCriteria, 'login_type')){

            $row = new StdClass();
            $age = (int)date('Y') - (int)$requestCriteria->birthyear;
            if($requestCriteria->login_type == 'member'){
                $qMember = $this->ci->db->select('*')
                ->from('members')
                ->where('id',$requestCriteria->members_id)
                ->get();
               

                if($qMember->num_rows() <= 0){
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'ไม่พบข้อมูลผู้ใช้งาน'
                    );
                }

                /* update member gender and dob and calculate to age */
                
                $this->ci->db->update('members',array(
                    'age'=>$age,
                    'gender'=>$requestCriteria->gender,
                    'dob'=>$requestCriteria->birthyear.'-01-01',
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$requestCriteria->members_id));
                /* eof update member gender and dob and calculate to age */

                $qMemberUpdate = $this->ci->db->select('*')
                ->from('members')
                ->where('id',$requestCriteria->members_id)
                ->get();
                $row = $qMemberUpdate->row();

            }else{ // guest
                $qGuest = $this->ci->db->select('*')
                ->from('guests')
                ->where('id',$requestCriteria->members_id)
                ->get();
                

                if($qGuest->num_rows() <= 0){
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'ไม่พบข้อมูลผู้ใช้งาน'
                    );
                }

                /* update guest gender and dob and calculate to age */
                $this->ci->db->update('guests',array(
                    'age'=>$age,
                    'gender'=>$requestCriteria->gender,
                    'dob'=>$requestCriteria->birthyear.'-01-01',
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$requestCriteria->members_id));
                /* eof update guest gender and dob and calculate to age */

                $qGuestUpdate = $this->ci->db->select('*')
                ->from('guests')
                ->where('id',$requestCriteria->members_id)
                ->get();

                $row = $qGuestUpdate->row();

            }



            return array(
                'status'=>true,
                'MemberData'=> $this->getLoginMemberDataByRow($row),
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }else{
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบข้อมูลรูปแบบการเข้าสู่ระบบ'
            );
        }
    }

    public function setYoutubeAPIKeyOverLimit($requestCriteria){
        set_time_limit(3);


        $this->ci->load->library([
            'user_agent'
        ]);

        //print_r($this->ci->agent->platform());exit;



        // $log_file_path = $this->createLogFilePath('setYoutubeAPIKeyOverLimitLogs');
        // $file_content = date("Y-m-d H:i:s") . ' platform : ' . $this->ci->agent->platform() . "\n";
        // file_put_contents($log_file_path, $file_content, FILE_APPEND);
        // unset($file_content);

        return array(
                'status'=>true,
                'result_code'=>'000',
                'result_desc'=>'Success'
        );

        //echo json_encode($response);
        //print_r($requestCriteria);
        /* check api key and update over_limit_status into 1 */
        $query = $this->ci->db->select('id,api_key,over_limit_status')
        ->from('youtube_api_keys')
        ->where('api_key',$requestCriteria->api_key)
        ->get();
        //echo $this->ci->db->last_query();exit;

        if($query->num_rows() > 0){
            $row = $query->row();
            //print_r($row);
            $this->ci->db->update('youtube_api_keys',array(
                'over_limit_status'=>1,
                'updated'=>date('Y-m-d H:i:s'),
                'last_updated_by'=>'mobile',
                'last_updated_os'=>$this->ci->agent->platform()
            ),array('id'=>$row->id));

            return array(
                'status'=>true,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }else{
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบข้อมูล api key'
            );

        }
        /* eof check api key and update over_limit_status into 1*/
    }


    public function setYoutubeViewLogs($requestCriteria){
        //print_r($requestCriteria);exit;
        /* check create youtube view data by month and year table */
        //print_r($requestCriteria);exit();
        
        $this->checkCreateYoutubeViewsDataTable();
        /* eof check create youtube view data by month and year table */
      
        
    }

    public function SetHistoryLinkLogs($requestCriteria){
        
        /* check property exist  */
        if(!property_exists($requestCriteria, 'members_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member property'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'login_type')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found login type'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'links')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found links'
            ];
        }
         /* check member exist  */
         if(!$this->checkGuestOrMemberExist($requestCriteria)){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member'
            ];

        }
       
         /* check destination link is alive */
         if($requestCriteria->links != ""){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requestCriteria->links);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl_authentication_header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_GET, TRUE);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_exec($ch);
            /* if alive response code choose be 200 or 202*/
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if($code >= 200 && $code <= 302){
                //not use 
                
            }else{
              /*  return [
                    'status' => false,
                    'result_code'=>'-002',
                    'result_desc'=>'destination url down.'
                ];
                */
            }
         }else{
            return [
                'status' => false,
                'result_code'=>'-002',
                'result_desc'=>'Not found links.'
            ];
         }
       
        /* eof check destination link is alive */
         //&& ($code >= 200 && $code <= 302)
        /* insert new record to history link logs */
        if($requestCriteria->members_id != "" && 
           $requestCriteria->login_type != "" &&
           $requestCriteria->links != "" ){

               try { 
                   /** is link already exist on server  */
                        $queryCheckAlreadyExistLink = $this->ci->db->select('*')
                        ->from('history_link_logs')
                        ->where('ref_id',$requestCriteria->members_id)
                        ->where('user_type' ,$requestCriteria->login_type)
                        ->where('links',$requestCriteria->links)
                        ->where('appname',$requestCriteria->appname)
                        ->get();
                        if($queryCheckAlreadyExistLink->num_rows() > 0){
                            // return array(
                            //     'status'=>false,
                            //     'result_code'=>'-003',
                            //     'result_desc'=>'Exist exception : This link already exists on your device.'
                            // );
                            return [
                                'status'=>true,
                                'result_code'=>'000',
                                'result_desc'=>'Success'
                            ];
                        }
                    /* eof link already exist on server   */


                    $data_insert = array(
                            'ref_id' => $requestCriteria->members_id,
                            'user_type' => $requestCriteria->login_type,
                            'links' => $requestCriteria->links,
                            'appname' => $requestCriteria->appname,
                            'created' => date('Y-m-d H:i:s'),
                            'updated' => date('Y-m-d H:i:s')
                    );
                    $this->ci->db->insert('history_link_logs',$data_insert);
                    
                    /* if i can insert into db then return success */
                    return [
                        'status'=>true,
                        'result_code'=>'000',
                        'result_desc'=>'Success'
                    ];
                }catch( Exception $e ) {
                    return array(
                        'status'=>false,
                        'result_code'=>'-004',
                        'result_desc'=>'Something wrong when save data to system.'
                    );
                }
              

           }else{
                return array(
                    'status'=>false,
                    'result_code'=>'-004',
                    'result_desc'=>'Something wrong when save data to system.'
                );
           }
    }

   
    public function GetHistoryLinkLogs($requestCriteria){
       
        /* check property exist  */
        if(!property_exists($requestCriteria, 'members_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member property'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'login_type')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found login type'
            ];
        }

        /* check member exist  */
        if(!$this->checkGuestOrMemberExist($requestCriteria)){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member'
            ];
        }
        
         /* get  history link logs from member or guest */
         if($requestCriteria->members_id != "" && 
            $requestCriteria->login_type != ""){
                $queryHistoryLinkLog = $this->ci->db->select('*')
                ->from('history_link_logs');
                if($requestCriteria->login_type != 'admin'){
                    $queryHistoryLinkLog->where('ref_id',$requestCriteria->members_id);
                }
                $queryHistoryLinkLog=  $queryHistoryLinkLog->get();
                /* if already have link log on server then */
                if($queryHistoryLinkLog->num_rows() > 0){
                    $result_obj = $queryHistoryLinkLog->result_array;
                    /* if success then return code 000 */
                    return [
                        'status'=>true,
                        'result_code'=>'000',
                        'history_log' => $result_obj,
                        'result_desc'=>'Success'
                    ];

                    

                }else{
                /* else */
                    return [
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'Not found data.'
                    ];
                }
         }


        return array(
            'status'=>false,
            'result_code'=>'-004',
            'result_desc'=>'Something wrong when get data on server.'
        );
    }

    public function EditHistoryLinkLogs($requestCriteria){
        /* check property exist  */
        if(!property_exists($requestCriteria, 'members_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member property'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'login_type')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found login type'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'links')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found links'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'link_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found link property.'
            ];
        }
        
         /* check member exist  */
         if(!$this->checkGuestOrMemberExist($requestCriteria)){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member'
            ];

        }

         /* update history link logs from member or guest */
         if($requestCriteria->members_id != "" && 
         $requestCriteria->login_type != "" && 
         $requestCriteria->link_id != "" &&
         $requestCriteria->links != "" ){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $requestCriteria->links);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl_authentication_header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_GET, TRUE);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_exec($ch);
            /* if alive response code choose be 200 or 202*/
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
           
            //if($code >= 200 && $code <= 302){
            try { 
                $updateData = [
                    'links' => $requestCriteria->links,
                    'appname' => $requestCriteria->appname,
                    ];

                $queryHistoryLinkLog = $this->ci->db->select('*')
                ->from('history_link_logs')
                ->where('id',$requestCriteria->link_id)
                ->update('history_link_logs', $updateData); 
                
                /* if i can update then return success */
                return [
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                ];
            }catch( Exception $e ) {
                return array(
                    'status'=>false,
                    'result_code'=>'-004',
                    'result_desc'=>'Something wrong when delete data to system.'
                );
            }
                
            //}
            
            // else{
            //     return [
            //         'status' => false,
            //         'result_code'=>'-002',
            //         'result_desc'=>'destination url down.'
            //     ];
            // }

           
 
         }else{
             return array(
                 'status'=>false,
                 'result_code'=>'-004',
                 'result_desc'=>'Something wrong when delete data on server.'
             );
 
         }


    }
    
    public function DelHistoryLinkLogs($requestCriteria){
        /* check property exist  */
        
        if(!property_exists($requestCriteria, 'members_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member property'
            ];
        }

        /* check property exist  */
        if(!property_exists($requestCriteria, 'login_type')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found login type'
            ];
        }
         /* check property exist  */
        if(!property_exists($requestCriteria, 'link_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found link property.'
            ];
        }

         /* check member exist  */
         if(!$this->checkGuestOrMemberExist($requestCriteria)){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member'
            ];

        }

        /* get  history link logs from member or guest */
        if($requestCriteria->members_id != "" && 
        $requestCriteria->login_type != "" && 
        $requestCriteria->link_id != ""){
            try { 
                $queryHistoryLinkLog = $this->ci->db->select('*')
                ->from('history_link_logs')
                ->where('id',$requestCriteria->link_id)
                ->delete();
                
                /* if i can delete into db then return success */
                return [
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                ];
                /* eof delete into db then return success */
                
            }catch( Exception $e ) {
                return array(
                    'status'=>false,
                    'result_code'=>'-004',
                    'result_desc'=>'Something wrong when delete data to system.'
                );
            }

        }else{
            return array(
                'status'=>false,
                'result_code'=>'-004',
                'result_desc'=>'Something wrong when delete data on server.'
            );

        }


    }


    public function setMemberDeviceToken($requestCriteria){
        /* check property exist  */
        if(!property_exists($requestCriteria, 'members_id')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member property'
            ];
        }

        if(!property_exists($requestCriteria, 'login_type')){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found login type'
            ];
        }
        /* eof check property exist */

        /* check member exist  */
        if(!$this->checkGuestOrMemberExist($requestCriteria)){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found member'
            ];

        }

        $this->updateDeviceToken([
            'requestCriteria'=>$requestCriteria
        ]);

        return [
            'status'=>true,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ];
    }


    private function getLoginMemberDataByRow($row){

        $firstname = $row->firstname;
        $lastname = $row->lastname;

        $firstname = (strpos($firstname, '?') !== false)?str_replace('?', '', $firstname):$firstname;

        $lastname = (strpos($lastname, '?') !== false)?str_replace('?', '', $lastname):$lastname;

        
        return array(
                'id'=>$row->id,
                'firstname'=>$firstname,
                'lastname'=>$lastname,
                'email'=>@$row->email,
                'gender'=>@$row->gender,
                'language'=>@$row->language,
                'age'=>@$row->age,
                'dob'=>@date('d/m/Y',strtotime($row->dob)),
                'phone'=>@$row->phone,
                'picture_profile'=>@$row->picture_profile
        );


    }

    private function curlGetChannelListFromFixitServer(){

                $fields = array(
                    'test' => 'testtest'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->tvchannellist_url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl_authentication_header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                return $response;



    }

    private function setTVChannelListUrl(){
        switch (ENVIRONMENT) {
            case 'development':
                $this->tvchannellist_url = 'http://psi.development/api/PsiCare/GetS3TVChannelList';
            break;
            case 'testing':

            break;
            case 'production':
                $this->tvchannellist_url = 'http://apiservice.psisat.com/api/PsiCare/GetS3TVChannelList';
            break;
            
            default:
                # code...
            break;
        }
    }

    private function setCheckStreamChannelURL(){
        switch (ENVIRONMENT) {
            case 'development':
                $this->check_stream_url = 'http://localhost:80/s3project/tv/assets/json/';
            break;
            case 'testing':
                $this->check_stream_url = 'http://localhost:80/s3project/tv/assets/json/';

            break;
            case 'production':
                $this->check_stream_url = 'http://sv-rating.dltv.ac.th/tv/assets/json/';
            break;
            
            default:
                # code...
            break;
        }
    }

    private function getChannelLogo($image_logo){
        switch (ENVIRONMENT) {
            case 'development':
                //return 'http://psi.development/uploaded/tv/logo/'.$image_logo;
                return 'http://s3remoteservice.development/tv/uploaded/tv/logo/'.$image_logo;
            break;
            case 'testing':

            break;
            case 'production':
                //return 'http://apiservice.psisat.com/uploaded/tv/logo/'.$image_logo;
                return 'http://sv-rating.dltv.ac.th/tv/uploaded/tv/logo/'.$image_logo;
            break;
            
            default:
                # code...
            break;
        }
    }

    private function updateTvAudienceView($data = array()){
        $query = $this->ci->db->select('*')
        ->from('tvaudiencerecentview')
        ->where('members_id',$data['members_id'])
        ->get();

        if($query->num_rows() > 0){
            $row = $query->row();
            $recent_view = json_decode($row->recent_view);

            /* check total recent view */
            $count_recent_view = count($recent_view);

            if($count_recent_view < 12){
                /* check this channel already in recent */
                if(!(in_array($data['channel_id'], $recent_view))){
                    array_push($recent_view, $data['channel_id']);

                    $data_update = array(
                        'recent_view'=>json_encode($recent_view),
                        'updated' => date('Y-m-d H:i:s')
                    );
                    $this->ci->db->update('tvaudiencerecentview',$data_update,array('id'=>$row->id));

                    // print_r($recent_view);
                }
                
            }else if($count_recent_view == 12){
                //$row = $query->row();
                

                $recent_view = json_decode($row->recent_view);

                if(!(in_array($data['channel_id'], $recent_view))){
                    /* first remove first key of array */
                    unset($recent_view[0]);

                    array_push($recent_view, $data['channel_id']);

                    $recent_arrange = array();
                    /* new arrange array*/

                    // print_r($recent_view);
                    foreach($recent_view as $key => $value) {
                        # code...
                        array_push($recent_arrange, $value);
                    }
                    $data_update = array(
                        'recent_view'=>json_encode($recent_arrange),
                        'updated' => date('Y-m-d H:i:s')
                    );
                    $this->ci->db->update('tvaudiencerecentview',$data_update,array('id'=>$row->id));
                }

            }

        }else{

            /* insert new record to tvaudiencerecentview */
            $data_insert = array(
                'members_id' => $data['members_id'],
                'recent_view' => json_encode(array($data['channel_id'])),
                'created' => date('Y-m-d H:i:s')
            );
            $this->ci->db->insert('tvaudiencerecentview',$data_insert);


        }

    }

    private function updateTVGuestView($data = array()){

        $query = $this->ci->db->select('*')
        ->from('tvguestrecentview')
        ->where('guests_id',$data['guests_id'])
        ->get();

        if($query->num_rows() > 0){
            $row = $query->row();
            $recent_view = json_decode($row->recent_view);

            /* check total recent view */
            $count_recent_view = count($recent_view);

            if($count_recent_view < 12){
                /* check this channel already in recent */
                if(!(in_array($data['channel_id'], $recent_view))){
                    array_push($recent_view, $data['channel_id']);

                    $data_update = array(
                        'recent_view'=>json_encode($recent_view),
                        'updated' => date('Y-m-d H:i:s')
                    );
                    $this->ci->db->update('tvguestrecentview',$data_update,array('id'=>$row->id));

                    // print_r($recent_view);
                }
                
            }else if($count_recent_view == 12){
                //$row = $query->row();

                $recent_view = json_decode($row->recent_view);

                if(!(in_array($data['channel_id'], $recent_view))){
                    /* first remove first key of array */
                    unset($recent_view[0]);

                    array_push($recent_view, $data['channel_id']);

                    $recent_arrange = array();
                    /* new arrange array*/

                    // print_r($recent_view);
                    foreach($recent_view as $key => $value) {
                        # code...
                        array_push($recent_arrange, $value);
                    }
                    $data_update = array(
                        'recent_view'=>json_encode($recent_arrange),
                        'updated' => date('Y-m-d H:i:s')
                    );
                    $this->ci->db->update('tvguestrecentview',$data_update,array('id'=>$row->id));
                }

            }

        }else{

            /* insert new record to tvaudiencerecentview */
            $data_insert = array(
                'guests_id' => $data['guests_id'],
                'recent_view' => json_encode(array($data['channel_id'])),
                'created' => date('Y-m-d H:i:s')
            );
            $this->ci->db->insert('tvguestrecentview',$data_insert);


        }
    }

    private function getUserIP(){
                       $client  = @$_SERVER['HTTP_CLIENT_IP'];
                            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                            $remote  = $_SERVER['REMOTE_ADDR'];

                            if(filter_var($client, FILTER_VALIDATE_IP))
                            {
                                $ip = $client;
                            }
                            elseif(filter_var($forward, FILTER_VALIDATE_IP))
                            {
                                $ip = $forward;
                            }
                            else
                            {
                                $ip = $remote;
                            }

                            return $ip;
    }

    private function getMemberChannelRecentView($requestCriteria){
        $arChannels = array();
        $queryRecent = $this->ci->db->select('*')
        ->from('tvaudiencerecentview')
        ->where('members_id',$requestCriteria->members_id)
        ->get();

        $requestCriteria->band_type = (property_exists($requestCriteria, 'band_type'))?$requestCriteria->band_type:'C';

        if($queryRecent->num_rows() > 0){
            $row = $queryRecent->row();

            $recent_view = json_decode($row->recent_view);
            
            krsort($recent_view);
            
            foreach ($recent_view as $key => $value) {
                # code...
                $query = $this->ci->db->select('*')
                ->from('tvchannels')
                ->where('id',$value)
                ->where('active',1)
                ->get();
                //echo $this->ci->db->last_query();
                if($query->num_rows() > 0){
                    $row_channels = $query->row();

                    if($row_channels->band_type == 'ALL' || $row->channels->band_type == $requestCriteria->band_type){

                            $arChannelPush = array(
                                'channel_id' => $row_channels->id,
                                'channel_logo'=> $this->getChannelLogo($row_channels->logo),
                                'channel_name'=>$row_channels->name,
                                'channel_description'=>$row_channels->description,
                                'channel_ascii_code'=>$this->getChannelAsciiCode(array(
                                    'requestCriteria'=>$requestCriteria,
                                    'channel_data'=>$row_channels
                                ))
                            );
                            array_push($arChannels, $arChannelPush);
                    }
                }
            }

            return $arChannels;
            // return array(
            //     'status' => true,
            //     'RecentChannels'=>$arChannels,
            //     'result_code'=>'000',
            //     'result_desc'=>'Success'
            // );

        }else{
            return $arChannels;
            // return array(
            //     'status' => true,
            //     'RecentChannels'=>$arChannels,
            //     'result_code'=>'000',
            //     'result_desc'=>'Success'
            // );
        }

    }

    private function getGuestChannelRecentView($requestCriteria){
        $arChannels = array();
        $queryRecent = $this->ci->db->select('*')
        ->from('tvguestrecentview')
        ->where('guests_id',$requestCriteria->members_id)
        ->get();

        $requestCriteria->band_type = (property_exists($requestCriteria, 'band_type'))?$requestCriteria->band_type:'C';

        if($queryRecent->num_rows() > 0){
            $row = $queryRecent->row();

            $recent_view = json_decode($row->recent_view);
            
            krsort($recent_view);
            
            foreach ($recent_view as $key => $value) {
                # code...
                $query = $this->ci->db->select('*')
                ->from('tvchannels')
                ->where('id',$value)
                ->where('active',1)
                ->get();

                //echo $this->ci->db->last_query();

                if($query->num_rows() > 0){
                    $row_channels = $query->row();


                    if($row_channels->band_type == 'ALL' || $row_channels->band_type == $requestCriteria->band_type){
                        $arChannelPush = array(
                            'channel_id' => $row_channels->id,
                            'channel_logo'=> $this->getChannelLogo($row_channels->logo),
                            'channel_name'=>$row_channels->name,
                            'channel_description'=>$row_channels->description,
                            // 'channel_ascii_code'=>$row_channels->channel_ascii_code
                            'channel_ascii_code'=>$this->getChannelAsciiCode(array(
                                'requestCriteria'=>$requestCriteria,
                                'channel_data'=>$row_channels
                            ))
                        );
                        array_push($arChannels, $arChannelPush);
                    }
                }
            }

            return $arChannels;
            // return array(
            //     'status' => true,
            //     'RecentChannels'=>$arChannels,
            //     'result_code'=>'000',
            //     'result_desc'=>'Success'
            // );

        }else{
            return $arChannels;
            // return array(
            //     'status' => true,
            //     'RecentChannels'=>$arChannels,
            //     'result_code'=>'000',
            //     'result_desc'=>'Success'
            // );
        }
    }

    private function InsertMemberOrGuestDevices($arrData = array()){

        switch ($arrData['requestCriteria']->login_type) {
            case 'member':
                $this->ci->db->insert('member_devices',array(
                    'members_id'=>$arrData['requestCriteria']->members_id,
                    'devices_id'=>$arrData['devices_id'],
                    'chip_code'=>$arrData['requestCriteria']->chip_code,
                    'created'=>date('Y-m-d H:i:s')
                ));
            break;
            case 'guest':
                $this->ci->db->insert('guest_devices',array(
                    'guests_id'=>$arrData['requestCriteria']->members_id,
                    'devices_id'=>$arrData['devices_id'],
                    'chip_code'=>$arrData['requestCriteria']->chip_code,
                    'created'=>date('Y-m-d H:i:s')
                ));
            break;            
            default:
                $this->ci->db->insert('guest_devices',array(
                    'guests_id'=>$arrData['requestCriteria']->members_id,
                    'devices_id'=>$arrData['devices_id'],
                    'chip_code'=>$arrData['requestCriteria']->chip_code,
                    'created'=>date('Y-m-d H:i:s')
                ));
            break;
        }

        return true;

    }

    private function CheckMemberOrGuestDevices($arrData = array()){
        switch ($arrData['requestCriteria']->login_type) {
            case 'member':
                $checkMember = $this->ci->db->select('devices_id,members_id')
                ->from('member_devices')->where('devices_id',$arrData['devices_id'])
                ->where('members_id',$arrData['requestCriteria']->members_id)->get();
                if($checkMember->num_rows() <= 0){
                    $this->ci->db->insert('member_devices',array(
                        'members_id'=>$arrData['requestCriteria']->members_id,
                        'devices_id'=>$arrData['devices_id'],
                        'chip_code'=>$arrData['requestCriteria']->chip_code,
                        'created'=>date('Y-m-d H:i:s')
                    ));
                }
            break;
            case 'guest':
                $checkGuest = $this->ci->db->select('devices_id,guests_id')
                ->from('guest_devices')->where('devices_id',$arrData['devices_id'])
                ->where('guests_id',$arrData['requestCriteria']->members_id)->get();
                if($checkGuest->num_rows() <= 0){
                        $this->ci->db->insert('guest_devices',array(
                            'guests_id'=>$arrData['requestCriteria']->members_id,
                            'devices_id'=>$arrData['devices_id'],
                            'chip_code'=>$arrData['requestCriteria']->chip_code,
                            'created'=>date('Y-m-d H:i:s')
                        ));
                }
            break;            
            default:
                $this->ci->db->insert('guest_devices',array(
                    'guests_id'=>$arrData['requestCriteria']->members_id,
                    'devices_id'=>$arrData['devices_id'],
                    'chip_code'=>$arrData['requestCriteria']->chip_code,
                    'created'=>date('Y-m-d H:i:s')
                ));
            break;
        }

        return true;
    }

    private function updateDeviceToken($data = []){
        $requestCriteria = $data['requestCriteria'];
        switch (trim($requestCriteria->login_type)) {
            case 'member':
                # code...
                $this->ci->db->update('members',[
                    'device_token'=>$requestCriteria->device_token,
                    'updated'=>date('Y-m-d H:i:s')
                ],['id'=>$requestCriteria->members_id]);
            break;
            case 'guest':
                $this->ci->db->update('guests',[
                    'device_token'=>$requestCriteria->device_token,
                    'updated'=>date('Y-m-d H:i:s')
                ],['id'=>$requestCriteria->members_id]);
            break;
            
            default:
                $this->ci->db->update('guests',[
                    'device_token'=>$requestCriteria->device_token,
                    'updated'=>date('Y-m-d H:i:s')
                ],['id'=>$requestCriteria->members_id]);
            break;
        }
    }

    private function curlUpdateDevicesIntoRatingDB($requestCriteria){

        //print_r($requestCriteria);exit;
        $this->ci->load->config('api');
        $request_url = $this->ci->config->item('api_rating')['update_device_address'][ENVIRONMENT];
        //print_r($request_url);exit;

        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $request_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'chip_code' => $requestCriteria->chip_code,
                'ip_address' => $this->getUserIP(),
                // 'ip_address'=>'171.7.247.30',
                'latitude'=>$requestCriteria->latitude,
                'longitude'=>$requestCriteria->longitude
            )
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        //echo $resp;exit;

        $response = json_decode($resp);

        if($response->status){
            return true;
        }else{
            return false;
        }

    }

        private function getChannelUrl($channel_row){
            if($channel_row->mv_status){
                /* get from mv*/

                /* if record already has link temp then check link expired */
                //-- if expired request for new link and if not yet return  link temp
                if($channel_row->mv_streaming_temp){

                    /* check streaming expired */
                    $get_querystring = parse_url($channel_row->mv_streaming_temp, PHP_URL_QUERY);
                    parse_str($get_querystring, $parseQueryString);

                    if(@$parseQueryString['doostreamendtime'] > strtotime(date('Y-m-d H:i:s')) || @$parseQueryString['psiliveendtime'] > strtotime(date('Y-m-d H:i:s'))){
                        return $channel_row->mv_streaming_temp;
                    }else{

                        $link_streaming = file_get_contents($this->getMVChannelLinkByLinkName($channel_row->mv_request_streaming));

                        $link_streaming = str_replace('"', '', $link_streaming); 
                        //echo $link_streaming;exit;
                        $this->ci->db->update('tvchannels',array(
                            'mv_streaming_temp'=>$link_streaming
                        ),array('id'=>$channel_row->id));

                        return $link_streaming;

                    }
                    


                }else{
                    //--- if no mv streaming temp request and then update to record 
                    $link_streaming = file_get_contents($this->getMVChannelLinkByLinkName($channel_row->mv_request_streaming));

                    $link_streaming = str_replace('"', '', $link_streaming); 
                    //echo $link_streaming;exit;
                    $this->ci->db->update('tvchannels',array(
                        'mv_streaming_temp'=>$link_streaming
                    ),array('id'=>$channel_row->id));

                    return $link_streaming;
                }



            }else if($channel_row->direct_streaming_status){

                return $channel_row->direct_streaming_link;
            }else{
                /* get from symphony*/
                // $bitrates = '300';
                // $prefix_tv_server = $this->getPrefixTVServer($channel_row->nodes_host);
                // return $prefix_tv_server.$this->getServerLoadBalance($channel_row->nodes_host).':1935/'.$channel_row->nodes_folder.'/'.$channel_row->streamer.'_'.$bitrates.'/playlist.m3u8';
                return "";
            }

    }

    private function getMVChannelLinkByLinkName($link_name){

        // return 'http://96.30.124.200:99/api/Product?linkname='.$link_name.'&boxuser=ofive';
        return 'http://96.30.124.109:4040/api/Product?linkname='.$link_name.'&boxuser=ofive&hr=1';
    }

    private function getPrefixTVServer($server_name = ""){
        if(!(preg_match("@^http://@i",$server_name)) && !(preg_match("@^https://@i",$server_name))){
            return 'http://';
        }

    }
    private function getServerLoadBalance($nodes_host){
        $html = file_get_contents('http://'.$nodes_host);

        //print_r($html);exit;
        $serverName = "";
        $serverExplode = explode('=', $html);
        $serverName = $serverExplode[1];
        return $serverName;

    }

    private function getChannelAsciiCode($data = array()){
        $requestCriteria = $data['requestCriteria'];
        $channel = $data['channel_data'];


        if(property_exists($requestCriteria, 'band_type') && $requestCriteria->band_type != ''){
            switch ($requestCriteria->band_type) {
                case 'C':
                    # code...
                    return $channel->cband_ordinal;
                break;
                case 'KU':
                    return $channel->kuband_ordinal;
                break;                
                default:
                    return ($channel->channel_ascii_code)?$channel->channel_ascii_code:"";
                break;
            }
        }else{
            return ($channel->channel_ascii_code)?$channel->channel_ascii_code:"";
        }


    }

    private function getChannelMobilePopupByCriteria($requestCriteria){
        $strQuery = "select * from tvchannels";

        switch ($requestCriteria->band_type) {
            case 'C':
                $strQuery .= " where cband_ordinal = '".$requestCriteria->channel_ascii_code."'";
            break;

            case 'KU':
                $strQuery .= " where kuband_ordinal = '".$requestCriteria->channel_ascii_code."'";
            break;
            
            default:
                $strQuery .= " where cband_ordinal = '".$requestCriteria->channel_ascii_code."'";
            break;
        }

        $query = $this->ci->db->query($strQuery);

        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    private function getYoutubeAPIKeys(){
        $arrKey = array();
        $query = $this->ci->db->select('*')
        ->from('youtube_api_keys')
        ->where('over_limit_status',0)
        ->where('active',1)
        ->order_by('NEWID()')
        ->limit(100)
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                array_push($arrKey, $value->api_key);
            }
        }

        return $arrKey;



    }

    private function remove_emoji($string){
        // Match Emoticons
            $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
            $clear_string = preg_replace($regex_emoticons, '', $string);

            // Match Miscellaneous Symbols and Pictographs
            $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
            $clear_string = preg_replace($regex_symbols, '', $clear_string);

            // Match Transport And Map Symbols
            $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
            $clear_string = preg_replace($regex_transport, '', $clear_string);

            // Match Miscellaneous Symbols
            $regex_misc = '/[\x{2600}-\x{26FF}]/u';
            $clear_string = preg_replace($regex_misc, '', $clear_string);

            // Match Dingbats
            $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
            $clear_string = preg_replace($regex_dingbats, '', $clear_string);

            return $clear_string;
    }

    private function checkCreateYoutubeViewsDataTable(){
        $query = $this->ci->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$this->current_youtbe_views_data_table."'");

        //print_r($this->current_youtbe_views_data_table);exit;

        if($query->num_rows() <= 0){
                /* create table for rating data */
                $strQuery = "CREATE TABLE ".$this->current_youtbe_views_data_table." (
                    id int IDENTITY(1,1) PRIMARY KEY,
                    members_id int,
                    login_type varchar(50),
                    devices_id int,
                    chipcode varchar(50),
                    video_id varchar(50),
                    created datetime
                )";

                $this->ci->db->query($strQuery);

        }

    }

    

    private function setCurrentYoutubeViewsTable(){
        $this->current_youtbe_views_data_table = 'youtube_views_data_'.date('Y').'_'.date('n');
    }

    private function createLogFilePath($filename = '') {
        $log_path = './application/logs/youtube_api_logs';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function checkGuestOrMemberExist($requestCriteria){
        $returnStatus = true;
        switch ($requestCriteria->login_type) {
            case 'guest':
                $queryCheck = $this->ci->db->select('id')
                ->from('guests')
                ->where('id',$requestCriteria->members_id)
                ->get();
                if($queryCheck->num_rows() <= 0){
                    $returnStatus = false;
                }

            break;

            case 'member':
                $queryCheck = $this->ci->db->select('id')
                ->from('members')
                ->where('id',$requestCriteria->members_id)
                ->get();
                if($queryCheck->num_rows() <= 0){
                    $returnStatus = false;
                }
            break;
            
            default:
                # code...
            break;
        }

        return $returnStatus;

    }

    private function checkReturnImmediatelyChannel($requestCriteria){

        $file_path = "";

        $current_datetime = new DateTime();

        $minus_datetime = $current_datetime;
        $minus_datetime->sub(new DateInterval('PT1M'));

        /* check file create  */
        if((property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv == true) && (!property_exists($requestCriteria, 'dltv_status') || $requestCriteria->dltv_status == false)){
            $file_path = './assets/json/streaming_tv.json';
        }else if((property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv == true) && (property_exists($requestCriteria, 'dltv_status') && $requestCriteria->dltv_status == true)){
            $file_path = './assets/json/dltv_tv.json';
        }

        if(property_exists($requestCriteria, 'band_type') && $requestCriteria->band_type == 'C'){
            $file_path = './assets/json/satellite_cband.json';
        }

        if(property_exists($requestCriteria, 'band_type') && $requestCriteria->band_type == 'KU'){
            $file_path = './assets/json/satellite_kuband.json';
        }

        $timeFilePath = strtotime(date('Y-m-d H:i:s',filemtime($file_path)));
        $minusTime = strtotime($minus_datetime->format('Y-m-d H:i:s'));

        

        if($timeFilePath >= $minusTime){

            $strJsonFileContent = file_get_contents($file_path);
            echo $strJsonFileContent;exit;
        }else{
            return true;
        }



    }


    private function writeJsonFileForEachType($data = []){

        $requestCriteria = $data['requestCriteria'];

        if((property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv == true) && (property_exists($requestCriteria, 'dltv_status') && $requestCriteria->dltv_status == true)){ 

            $fp = fopen('./assets/json/dltv_tv.json', 'w');
            fwrite($fp, json_encode($data['json_data']));
            fclose($fp);

        }else if((property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv == true) && (!property_exists($requestCriteria, 'dltv_status') || $requestCriteria->dltv_status == false)){

            $fp = fopen('./assets/json/streaming_tv.json', 'w');
            fwrite($fp, json_encode($data['json_data']));
            fclose($fp);

        }

        // if(property_exists($requestCriteria, 'internet_tv') && $requestCriteria->internet_tv == true){
        //     $fp = fopen('./assets/json/streaming_tv.json', 'w');
        //     fwrite($fp, json_encode($data['json_data']));
        //     fclose($fp);
        // }

        if(property_exists($requestCriteria, 'band_type') && $requestCriteria->band_type == 'C'){
            $fp = fopen('./assets/json/satellite_cband.json', 'w');
            fwrite($fp, json_encode($data['json_data']));
            fclose($fp);
        }

        if(property_exists($requestCriteria, 'band_type') && $requestCriteria->band_type == 'KU'){
            $fp = fopen('./assets/json/satellite_kuband.json', 'w');
            fwrite($fp, json_encode($data['json_data']));
            fclose($fp);
        }

    }
      

}