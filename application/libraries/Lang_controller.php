<?php

/**
 * Description of Lang
 *
 * @author songpol
 */
class Lang_controller {

    /**
     *
     * @var Lang
     */
    private static $instance;

    /**
     *
     * @var directory
     */
    private $language_path;

    /**
     * config from config
     *
     * @var string
     */
    private $language;

    /**
     * meta data of language
     *
     * @var type
     */
    private $meta_data = array();

    /**
     * first new object start
     */
    public function __construct() {


        /**
         * init the current language
         */
        $this->language = get_instance()->config->item('language');


        /**
         * init the current language path
         */
        $this->language_path = APPPATH . "language/" . $this->language;


        $this->loadCurrentLanguage();
    }

    /**
     * on load the page
     *
     * load the current language to memory
     */
    private function loadCurrentLanguage() {
        $this->loadLangFolder(glob($this->language_path . "/*"));
    }

    /**
     * load the folder to language namespace
     *
     * recursive if folder found
     *
     * @param directory $targets
     */
    private function loadLangFolder($targets) {
        foreach ($targets as $target) {
            if (is_dir($target)) {
                $this->loadLangFolder(glob($target . "/*"));
            } else {

                /**
                 * clear the varible before include language file
                 */
                unset($lang);

                $namespace = str_replace($this->language_path . "/", "", $target);
                $namespace = preg_replace("/_lang\.php$/", "", $namespace);

                $pathinfo = pathinfo($target);
                if (preg_match("/_lang\.php/", $pathinfo['basename'])) {
                    include($target);
                    if (isset($lang)) {
                        $this->setMetadata($namespace, $lang);
                    } else {
                        $this->setMetadata($namespace, array());
                    }
                }
            }
        }
    }

    /**
     * set text to memory
     *
     * @param string $namespace
     * @param string $text
     */
    private function setMetadata($namespace, $text) {
        if (!array_key_exists($namespace, $this->meta_data)) {
            $this->meta_data[$namespace] = array();
        }

        $this->meta_data[$namespace] = $text;
    }

    /**
     * get text from metadata
     *
     * @param string $namespace
     * @param string $text
     * @return boolean
     */
    private function getMetadata($namespace, $text) {
        /**
         * if new namespace
         */
        if (!array_key_exists($namespace, $this->meta_data)) {
            return FALSE;
        }

        if (!is_array($this->meta_data[$namespace])) {
            return FALSE;
        }

        /**
         * old name space
         */
        $key = htmlentities($text);
        if (array_key_exists($key, $this->meta_data[$namespace])) {
            return $this->meta_data[$namespace][$key];
        } else {
            return FALSE;
        }
    }

    /**
     * save text to metadata
     *
     * @param string $namespace
     * @param string $text
     */
    public function saveMetadata($namespace, $text) {

        $language_file_path = $this->language_path . '/' . $namespace . '_lang.php';



        /**
         * check the folder name space file
         */
        $dirname = pathinfo($language_file_path);
        $dirname = $dirname['dirname'];
        $directories = explode("/", $dirname);
        $check_path = "";
        foreach ($directories as $directory) {
            $check_path = $check_path . $directory . "/";
            if (!file_exists($check_path)) {
                mkdir($check_path, 0777);
                chmod($check_path, 0777);
            }
        }




        /**
         * if this namespace is not exists
         *
         * create it (with open phptag)
         */
        if (!file_exists($language_file_path)) {
            $begin_of_file_text = "<?php\n\n";
            if (!file_put_contents($language_file_path, $begin_of_file_text, FILE_APPEND)) {
                show_error('save language Metadata fail.');
            }
        }


        /**
         * save text to file
         */
        $key = htmlentities($text);
        $append_string = '$lang[\'' . $key . '\'] = "' . htmlentities($text) . '";' . "\n";
        if (!file_put_contents($language_file_path, $append_string, FILE_APPEND)) {
            show_error('save language Metadata fail.');
        }


        $this->setMetadata($namespace, $text);
    }

    /**
     * i will happy when call translate
     *
     * if text in namespace is found return it
     *
     * if text not found save text to namespace file and return original text
     *
     * @param type $namespace
     * @param type $text
     * @return type
     */
    public function translate($namespace, $text) {
        $this->loadCurrentLanguage();
        $retval = $this->getMetadata($namespace, $text);

        if ($retval == FALSE) {
            $this->saveMetadata($namespace, $text);
            $retval = $text;
        }

        return $retval;
    }

    /**
     *
     * @return Lang
     */
    public static function get_instance() {
        if (!self::$instance) {
            self::$instance = new Lang_controller();
        }
        return self::$instance;
    }

}

?>
