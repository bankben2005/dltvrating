<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function CreateAdsJsonFile(){
		if($this->input->post(NULL,FALSE)){

			//print_r($this->input->post());
			$json_txt = $this->input->post('json_txt');

			file_put_contents('./psi/shopping/ads_psi.json', $json_txt);

			//print_r($json_txt);
		}
	}

	public function testGetIpStack(){
		$url = 'http://api.ipstack.com/61.28.213.249?access_key=057832ef8f23277f66b007e22644cac0&format=1';
        // echo $url;exit;
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT=>10000
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        if(curl_exec($curl) === false)
        {
            echo 'Curl error: ' . curl_error($curl);
        }
        else
        {
            echo 'Operation completed without any errors';
        }

        echo $resp;
	}

	public function UpdateTVChannelFromOfive(){
		$post_data = $this->input->post();


		/* Create log file for debug*/
        $log_file_path = $this->createLogFilePath('updateChannelDirect');
        $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($_FILES) . "\n";
        file_put_contents($log_file_path, $file_content, FILE_APPEND);
        unset($file_content);

		$queryCheck = $this->db->select('*')
		->from('tvchannels')
		->where('ofive_map_channel',$post_data['channel_id'])
		->get();

		if($queryCheck->num_rows() > 0){
			/* update tvchannels */
			$this->db->update('tvchannels',array(
				'name'=>$post_data['name'],
				'description'=>$post_data['description'],
				'mv_status'=>$post_data['mv_status'],
				'mv_request_streaming'=>$post_data['mv_request_streaming'],
				'direct_streaming_link'=>$post_data['direct_streaming_link'],
				'direct_streaming_status'=>$post_data['direct_streaming_status'],
				'active'=>$post_data['active']
			),array('id'=>$queryCheck->row()->id));
			/* eof update tvchannels data*/

			/* check upload profile image  */
			if($_FILES['channel_logo']['error'] == 0){
				$this->uploadChannelLogo(array(
						'tvchannels_id'=>$queryCheck->row()->id
				));
			}





		}


		echo json_encode(array(
			'post_data'=>$post_data
		));
	}

	public function getAllChannelList(){
		$query = $this->db->select('*')
		->from('tvchannels')
		->where('active',1)
		->order_by('name','asc')
		->get();

		echo json_encode($query->result_array());
	}

	public function getChannelDataById(){
		$tvchannels_id = $this->input->get('tvchannels_id');
		$band_type = $this->input->get('band_type');

		$queryString = "select * from tvchannels";
		$queryString .= " where id = '".$tvchannels_id."'";

		$query = $this->db->query($queryString);
		//echo $this->db->last_query();

		if($query->num_rows() > 0){

			echo json_encode(array(
				'status'=>true,
				'ChannelData'=>$query->row()
			));
		}else{
			echo json_encode(array(
				'status'=>false
			));
		}



	}

	public function getChannelDataByMergeId(){
		$tvchannels_id = $this->input->get('tvchannels_id');

		$query = $this->db->select('id,logo')
		->from('tvchannels')
		->where('id',$tvchannels_id)
		->get();
		if($query->num_rows() > 0){
			echo json_encode([
				'status'=>true,
				'ChannelData'=>$query->row()
			]);
		}else{
			echo json_encode([
				'status'=>false
			]);
		}
	}

	public function resetYoutubeKeyStatusUrl(){
		// echo json_encode(array(
		// 	'status'=>true
		// ));
		$query = $this->db->update('youtube_api_keys',[
			'over_limit_status'=>0,
			'updated'=>date('Y-m-d H:i:s')
		],['over_limit_status'=>1]);

		if($query){
			echo json_encode([
				'status'=>true
			]);
		}


	}

	public function CheckAPIOverLimited(){


		set_time_limit(0);

		$queryKey = $this->db->select('*')
		->from('youtube_api_keys')
		->where('over_limit_status',0)
		->order_by('NEWID()')
		->limit(3)
		->get();

		//print_r($this->db->last_query());exit;

		$arrResponse = [];
		if($queryKey->num_rows() > 0){

			//print_r($queryKey->result());
			foreach ($queryKey->result() as $key => $value) {
				# code...
				$curl_data = $this->curlGETCheckOverLimited([
					'api_key'=>$value->api_key
				]);

				$resp = json_decode($curl_data);

				// print_r($resp);

				if(property_exists($resp, 'error')){
					//print_r($resp);

					if($resp->error->code == 403 && $resp->error->message == 'Project blocked; abuse detected.'){
							$this->db->update('youtube_api_keys',[
								'over_limit_status'=>1,
								'updated'=>date('Y-m-d H:i:s'),
								'last_updated_by'=>'schedule',
								'last_updated_os'=>'Window Server',
								'active'=>0,
								'remark'=>'has been block'
							],['gmail_account'=>$value->gmail_account]);

					}else if($resp->error->code == 403 && $resp->error->errors[0]->reason == 'dailyLimitExceeded'){

							$this->db->update('youtube_api_keys',[
								'over_limit_status'=>1,
								'updated'=>date('Y-m-d H:i:s'),
								'last_updated_by'=>'schedule',
								'last_updated_os'=>'Window Server'
							],['id'=>$value->id]);
					}else if($resp->error->code == 403 && $resp->error->errors[0]->reason == 'quotaExceeded'){
							$this->db->update('youtube_api_keys',[
								'over_limit_status'=>1,
								'updated'=>date('Y-m-d H:i:s'),
								'last_updated_by'=>'schedule',
								'last_updated_os'=>'Window Server'
							],['id'=>$value->id]);

					}else if($resp->error->code == 400 && $resp->error->errors[0]->reason == 'keyInvalid'){
							$this->db->update('youtube_api_keys',[
								'updated'=>date('Y-m-d H:i:s'),
								'last_updated_by'=>'schedule',
								'last_updated_os'=>'Window Server',
								'active'=>0,
								'remark'=>'key invalid'
							],['id'=>$value->id]);

					}else if($resp->error->code == '403' && $resp->error->errors[0]->reason == 'accessNotConfigured'){
							$this->db->update('youtube_api_keys',[
								'updated'=>date('Y-m-d H:i:s'),
								'last_updated_by'=>'schedule',
								'last_updated_os'=>'Window Server',
								'active'=>0,
								'remark'=>'access not configured'
							],['id'=>$value->id]);

					}else if($resp->error->code == 403){
							/* updated over_limit_status */
							$this->db->update('youtube_api_keys',[
								'over_limit_status'=>1,
								'updated'=>date('Y-m-d H:i:s'),
								'last_updated_by'=>'schedule',
								'last_updated_os'=>'Window Server'
							],['id'=>$value->id]);

							
					}
					//print_r($resp->error->code);
				}


			}
		}

		echo json_encode(array(
			'status'=>true
		));

		//print_r($arrResponse);
	}

	public function getDeviceUserDataByChipCode(){

		$is_exist = 0;
		$arrResponse = [];

		$chip_code = $this->input->get('chip_code');


		/* first check from guest devices */
		$queryGuestDevices = $this->db->select('guest_devices.guests_id,
			guest_devices.chip_code,
			guests.gender,
			guests.age,
			guests.firstname,
			guests.lastname,
			guests.gender,
			guests.age,
			guests.dob,
			guests.os,
			guests.os_version,
			guests.device_type,
			guests.id as users_id')
		->from('guest_devices')
		->join('guests','guest_devices.guests_id = guests.id')
		->where('guest_devices.chip_code',$chip_code)
		->order_by('guests.age','desc')
		->limit(1)
		->get();

		//echo $this->db->last_query();
		/* eof check from guest devices */

		//print_r($arrResponse);exit;

		if($queryGuestDevices->num_rows() > 0){
			$is_exist = 1;
			$arrResponse['type']='guest';
			$arrResponse['Data'] = $queryGuestDevices->row_array();

		}


		/* check from member devices */
		$queryMemberDevices = $this->db->select('member_devices.members_id,
			member_devices.chip_code,
			members.gender,
			members.age,
			members.firstname,
			members.lastname,
			members.gender,
			members.age,
			members.dob,
			members.os,
			members.os_version,
			members.device_type,
			members.id as users_id')
		->from('member_devices')
		->join('members','member_devices.members_id = members.id')
		->where('member_devices.chip_code',$chip_code)
		->order_by('members.age','desc')
		->limit(1)
		->get();

		/* eof check from member devices */

		if($queryMemberDevices->num_rows() > 0){
			$is_exist = 1;	
			$arrResponse['type'] = 'member';
			$arrResponse['Data'] = $queryMemberDevices->row_array();
		}

		echo json_encode([
			'status'=>true,
			'chip_code'=>$this->input->get('chip_code'),
			'is_exist'=>$is_exist,
			'UserData'=>$arrResponse

		]);
		//echo json_encode($arrResponse);
		// print_r($arrResponse);

	}

	public function setIPTVRating(){
		$post_data = $this->input->post();

		/* set current rating_data table */
		$rating_data_table = 'rating_data_'.date('Y').'_'.date('n');
		/* eof set current rating_data table  */


		/* first check ready create table rating data table  */
		$this->checkRatingDataTable([
			'table_name'=>$rating_data_table
		]);
		/* eof first check ready create table rating data table  */


		// get devices_id by chip_code 
		$devices_id = $this->getDevicesIDByChipCode($post_data);

		/* get tvchannels_id by post url  */
		$tvchannels_id = $this->getTVChannelsID([
			'tvchannels_url'=>$post_data['tvchannels_url']
		]);



		/* insert data into table  */
		$this->db->insert($rating_data_table,[
			'devices_id'=>$devices_id,
			'tvchannels_id'=>$tvchannels_id,
			'chip_code'=>$post_data['chip_code'],
			'view_seconds'=>(int)$post_data['view_seconds'],
			'ip_address'=>$post_data['ip_address'],
			'startview_datetime'=>$post_data['created'],
			'created'=>date('Y-m-d H:i:s')
		]);
		/* eof insert data into table  */


		/* insert into rating_data_daily table  */
		$this->insertRatingDataDaily([
			'tvchannels_id'=>$tvchannels_id,
			'date'=>date('Y-m-d'),
			'view_seconds'=>(int)$post_data['view_seconds'],
			'devices_id'=>$devices_id,
			'chip_code'=>$post_data['chip_code']
		]);
		/* eof insert into rating_data_daily table  */

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	}

	


	private function curlGETCheckOverLimited($data = []){
		

		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => 'https://www.googleapis.com/youtube/v3/videos?id=7lCDEYXw3mM&key='.$data['api_key'].'&part=snippet,statistics',
		    CURLOPT_TIMEOUT=>10000
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);

		//echo $resp;
		// Close request to clear up some resources
		curl_close($curl);

		return $resp;

	}

	public function getTotalInternetTVOnline(){

		$current_datetime = new DateTime();
        $minusSixHour = $current_datetime->sub(new DateInterval('PT6H'));


		/* get all member online last 6 hours by channel click*/
		$query = $this->db->select('members_id')
		->from('tvchannelclicks')
		->where('created >=',$minusSixHour->format('Y-m-d H:i:s'))
		->group_by('members_id')
		->get();


		echo json_encode([
			'status'=>true,
			'totals'=>$query->num_rows()
		]);
		//print_r($query->num_rows());exit;

		/* eof get all member online last 6 hours by channel click */

		
	}

	private function checkRatingDataTable($data = []){
		$query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$data['table_name']."'");

        if($query->num_rows() <= 0){
                /* create table for rating data */
                $strQuery = "CREATE TABLE ".$data['table_name']." (
                    id int IDENTITY(1,1) PRIMARY KEY,
                    devices_id int,
                    tvchannels_id int,
                    chip_code varchar(255),                    
                    view_seconds int,
                    ip_address varchar(50),
                    startview_datetime datetime,
                    created datetime
                )";

                $this->db->query($strQuery);

        }
	}

	private function checkAlreadyCreateRatingDataDailyDevicesTable($data = []){
		$query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$data['table_name']."'");

		if($query->num_rows() <= 0){
                /* create table for rating data */
                $strQuery = "CREATE TABLE ".$data['table_name']." (
                    id int IDENTITY(1,1) PRIMARY KEY,
                    rating_data_daily_id int,
                    devices_id int,
                    chip_code varchar(255),                    
                    total_seconds int,
                    created datetime,
                    updated datetime
                )";

                $this->db->query($strQuery);

        }



		
	}

	private function getDevicesIDByChipCode($data = []){

		$query = $this->db->select('id,chip_code')
		->from('devices')
		->where('chip_code',$data['chip_code'])
		->get();

		if($query->num_rows() > 0){
			return $query->row()->id;
		}else{
			/* insert new devices into devices table  */
			$this->db->insert('devices',[
				'chip_code'=>$data['chip_code'],
				'ip_address'=>$data['ip_address'],
				'created'=>date('Y-m-d H:i:s')
			]);
			/* eof insert new devices into devices table  */

			return $this->db->insert_id();
		}


	}

	private function getTVChannelsID($data = []){

		/* check from mv steaming first  */
		$query = $this->db->select('id,mv_streaming_temp')
		->from('tvchannels')
		->like('mv_streaming_temp',$data['tvchannels_url'],'both')
		->get();

		if($query->num_rows() > 0){
			return $query->row()->id;
		}else{

			/* check from direct streaming link  */
			$queryDirect = $this->db->select('id,direct_streaming_link')
			->from('tvchannels')
			->like('mv_streaming_temp',$data['tvchannels_url'],'both')
			->get();
			/* eof check from direct streaming link  */

			if($queryDirect->num_rows() > 0){
				return $queryDirect->row()->id;
			}else{
				return 0;
			}

		}
	}

	private function insertRatingDataDaily($data = []){

		/* rating data daily devices table  */
        $rating_data_daily_devices_table = 'rating_data_daily_devices_'.date('Y').'_'.date('n');
        /* eof rating data daily devices table  */

		/* check already insert rating data daily devices table */
		$this->checkAlreadyCreateRatingDataDailyDevicesTable([
			'table_name'=>$rating_data_daily_devices_table
		]);



		$check_record = $this->db->select('*')
        ->from('rating_data_daily')
        ->where('date',$data['date'])
        ->where('tvchannels_id',$data['tvchannels_id'])
        ->get();

        if($check_record->num_rows() <= 0){

        	/* insert new record into rating_data_daily */
            $this->db->insert('rating_data_daily',array(
                'date'=>$data['date'],
                'tvchannels_id'=>$data['tvchannels_id'],
                'sum_seconds'=>$data['view_seconds'],
                'reach_devices'=>1,
                'created'=>date('Y-m-d H:i:s')
            ));
            /* eof insert new record*/

            $insert_id = $this->db->insert_id();

            /* insert into rating data daily devices */
            $this->db->insert($rating_data_daily_devices_table,array(
                'rating_data_daily_id'=>$insert_id,
                'devices_id'=>$data['devices_id'],
                'chip_code'=>$data['chip_code'],
                'total_seconds'=>$data['view_seconds'],
                'created'=>date('Y-m-d H:i:s')
            ));
            /* eof insert into rating data daily devices */


        }else{
        	/* if already exist row  */
        	$row_data_daily = $check_record->row();

        	/* check and insert device */
            $queryCheckDevice = $this->db->select('id,devices_id,rating_data_daily_id')
            ->from($rating_data_daily_devices_table)
            ->where('devices_id',$data['devices_id'])
            ->where('rating_data_daily_id',$row_data_daily->id)
            ->get();


            if($queryCheckDevice->num_rows() <= 0){ // if not exist 

            	/* insert new and update reach devices for rating data daily */
                $this->db->insert($rating_data_daily_devices_table,array(
                    'rating_data_daily_id'=>$row_data_daily->id,
                    'devices_id'=>$data['devices_id'],
                    'chip_code'=>$data['chip_code'],
                    'total_seconds'=>$data['view_seconds'],
                    'created'=>date('Y-m-d H:i:s'),
                    'updated'=>date('Y-m-d H:i:s')
                ));

                /* update rating data daily */
                $this->db->update('rating_data_daily',array(
                    'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
                    'reach_devices'=>($row_data_daily->reach_devices + 1),
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$row_data_daily->id));
                /* eof update rating data*/


            }else{ // if already exist 

            	/* update rating data daily device update */
                $this->db->update($rating_data_daily_devices_table,array(
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$queryCheckDevice->row()->id));


                /* only sum  view seconds */
                $this->db->update('rating_data_daily',array(
                    'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$row_data_daily->id));


            }

        }

	}

	private function uploadChannelLogo($data = []){

		$tvchannels_id = $data['tvchannels_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('./tv/uploaded/tv/logo/')){
			mkdir('./tv/uploaded/tv/logo',0777,true);
		}
		

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = './tv/uploaded/tv/logo/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '10000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('channel_logo')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');

                // if(file_exists('uploaded/package_image/available/'.$package_id)){
                //         rmdir('uploaded/package_image/available/'.$package_id);
                // }

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

                //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name'];

                // $package = new M_package($package_id);
                // $package->img_package_available = $upload_file_name;
                // $package->img_package_available_size = $_FILES['img_package_available']['size'];
                // $package->save();
			$this->db->update('tvchannels',[
				'logo'=>$upload_file_name,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$tvchannels_id]);
			return true;
		}


	}
	private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = APPPATH.'/logs/updateChannel';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }
}
