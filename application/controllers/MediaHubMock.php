<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
//require_once(APPPATH. "libraries/api/S3Library.php");



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class MediaHubMock extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }


    public function GetAllHilightPrograms_get(){


        $arrHilightProgram = [];
        $defaultTimeSlots = $this->getDefaultTimeSlots();

        array_push($arrHilightProgram, [
            'id'=>1,
            'program_name'=>'เพลิงพรางเทียน',
            'program_cover'=>base_url('assets/media_hub/mockup/01.png'),
            'program_on_air_txt'=>'Every Fri Sat Sun',
            'program_channel'=>'Channel 3 HD',
            'program_duration_txt'=>'20:30 to 22:30',
            'program_rating'=>'3.123',
            'time_slots'=>$defaultTimeSlots

        ]);

        array_push($arrHilightProgram, [
            'id'=>2,
            'program_name'=>'ดวงใจ ในไฟหนาว',
            'program_cover'=>base_url('assets/media_hub/mockup/02.png'),
            'program_on_air_txt'=>'Every Wed Thu',
            'program_channel'=>'Channel 3 HD',
            'program_duration_txt'=>'20:30 to 22:30',
            'program_rating'=>'2.011',
            'time_slots'=>$defaultTimeSlots

        ]);

        array_push($arrHilightProgram, [
            'id'=>3,
            'program_name'=>'Premium blockbuster',
            'program_cover'=>base_url('assets/media_hub/mockup/03.png'),
            'program_on_air_txt'=>'Every Mon - Fri',
            'program_channel'=>'Mono 29',
            'program_duration_txt'=>'18:20 to 21:00',
            'program_rating'=>'1.905',
            'time_slots'=>$defaultTimeSlots

        ]);

        array_push($arrHilightProgram, [
            'id'=>4,
            'program_name'=>'The voice thailand 2019',
            'program_cover'=>base_url('assets/media_hub/mockup/04.png'),
            'program_on_air_txt'=>'Every Mon',
            'program_channel'=>'PPTV HD 36',
            'program_duration_txt'=>'20:15 to 21:15',
            'program_rating'=>'1.227',
            'time_slots'=>$defaultTimeSlots

        ]);

        $return =  [
            'status'=>true,
            'HilightProgram'=>$arrHilightProgram,
            'result_code'=>'000',
            'result_desc'=>'success'
        ];

        echo json_encode($return);exit;

    }

    public function GetDefaultTimeSlots_get(){
        $time_slots = $this->getDefaultTimeSlots();

        echo json_encode([
            'status'=>true,
            'TimeSlots'=>$time_slots,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ]);
    }

    public function GetTop10Channels_get(){
        //echo 'aaaa';exit;
        $content= file_get_contents('http://sv-rating.dltv.ac.th/rating/api/GetChannelHits?band_type=C');

        echo $content;
    }

    public function GetHistoriesReservation_get(){
        $arrHistories = [];

        array_push($arrHistories, [
            'channel'=>'Mono29',
            'date_txt'=>'04 Dec 2019',
            'reservation_lists'=>[
                [
                    'duration_txt'=>'01:30 to 02:40',
                    'program_name'=>'Life below zero',
                    'package_time_txt'=>'1 minute',
                    'package_cost_txt'=>'120,000.00 THB'
                ],
                [
                    'duration_txt'=>'07:30 to 08:30',
                    'program_name'=>'The legend of white snake',
                    'package_time_txt'=>'0.5 minute',
                    'package_cost_txt'=>'12,000.00 THB'
                ],
                [
                    'duration_txt'=>'10:50 to 11:50',
                    'program_name'=>'Macgyver',
                    'package_time_txt'=>'0.5 minute',
                    'package_cost_txt'=>'12,000.00 THB'
                ]
            ]
        ]);

        $return = [
            'status'=>true,
            'ReservationHitories'=>$arrHistories,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ];

        echo json_encode($return);exit;

    }

    public function ViewAdvertisement_get(){
        $arrAdvertisement = [];

        array_push($arrAdvertisement, [
            'chanel_name'=>'Mono 29',
            'advertisement_lists'=>[
                [
                    'duration_txt'=>'01:30 to 02:40',
                    'package_time_txt'=>'1 minute',
                    'package_total_cost'=>'1,231'
                ],
                [
                    'duration_txt'=>'07:30 to 08:30',
                    'package_time_txt'=>'0.5 minute',
                    'package_total_cost'=>'312,342'
                ],
                [
                    'duration_txt'=>'10:50 to 11:50',
                    'package_time_txt'=>'0.5 minute',
                    'package_total_cost'=>'133,329'
                ]
            ]
        ]);

        array_push($arrAdvertisement, [
            'chanel_name'=>'Thairath TV',
            'advertisement_lists'=>[
                [
                    'duration_txt'=>'01:30 to 02:40',
                    'package_time_txt'=>'1 minute',
                    'package_total_cost'=>'1,231'
                ],
                [
                    'duration_txt'=>'07:30 to 08:30',
                    'package_time_txt'=>'0.5 minute',
                    'package_total_cost'=>'312,342'
                ],
                [
                    'duration_txt'=>'10:50 to 11:50',
                    'package_time_txt'=>'0.5 minute',
                    'package_total_cost'=>'133,329'
                ]
            ]
        ]);


        $return = [
            'status'=>true,
            'month_year_txt'=>'Dec 2019',
            'Advertisements'=>$arrAdvertisement,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ];

        echo json_encode($return);exit;
    }

    public function GetFilterTimeSlots_get(){
        $get_data = $this->input->get();

        print_r($get_data);exit;


    }

    private function getDefaultTimeSlots(){
            $time_slots = [
                [
                    'slot_duration_txt'=>'01:30 to 02:40',
                    'slot_duration_start'=>'01:30',
                    'slot_duration_end'=>'02:40',
                    'slot_program'=>'Life below zero',
                    'slot_rating'=>'1.007',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'02:40 to 03:40',
                    'slot_duration_start'=>'02:40',
                    'slot_duration_end'=>'03:40',
                    'slot_program'=>'News rerun',
                    'slot_rating'=>'0.287',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'03:00 to 05:00',
                    'slot_duration_start'=>'03:00',
                    'slot_duration_end'=>'05:00',
                    'slot_program'=>'Welcome World',
                    'slot_rating'=>'0.200',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'05:00 to 05:30',
                    'slot_duration_start'=>'05:00',
                    'slot_duration_end'=>'05:30',
                    'slot_program'=>'World News',
                    'slot_rating'=>'0.211',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'05:30 to 07:30',
                    'slot_duration_start'=>'05:30',
                    'slot_duration_end'=>'07:30',
                    'slot_program'=>'Good morning thailand',
                    'slot_rating'=>'0.211',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'07:30 to 08:30',
                    'slot_duration_start'=>'07:30',
                    'slot_duration_end'=>'08:30',
                    'slot_program'=>'The legend of white snake',
                    'slot_rating'=>'2.691',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'08:30 to 10:50',
                    'slot_duration_start'=>'08:30',
                    'slot_duration_end'=>'10:50',
                    'slot_program'=>'The monkey king',
                    'slot_rating'=>'2.802',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'10:50 to 11:50',
                    'slot_duration_start'=>'10:50',
                    'slot_duration_end'=>'11:50',
                    'slot_program'=>'Macgyver',
                    'slot_rating'=>'2.930',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'11:50 to 14:00',
                    'slot_duration_start'=>'11:50',
                    'slot_duration_end'=>'14:50',
                    'slot_program'=>'Black water',
                    'slot_rating'=>'3.981',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'14:00 to 14:05',
                    'slot_duration_start'=>'14:00',
                    'slot_duration_end'=>'14:05',
                    'slot_program'=>'Entertainment now',
                    'slot_rating'=>'1.394',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'14:05 to 15:10',
                    'slot_duration_start'=>'14:05',
                    'slot_duration_end'=>'15:10',
                    'slot_program'=>'Hawaii five o',
                    'slot_rating'=>'2.831',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'15:10 to 15:30',
                    'slot_duration_start'=>'15:10',
                    'slot_duration_end'=>'15:30',
                    'slot_program'=>'The day news update',
                    'slot_rating'=>'0.129',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ],
                [
                    'slot_duration_txt'=>'15:40 to 17:20',
                    'slot_duration_start'=>'15:40',
                    'slot_duration_end'=>'17:20',
                    'slot_program'=>'Scoopy doo',
                    'slot_rating'=>'2.843',
                    'slot_packages'=>[
                        [
                            'package_time_txt'=>'1 minute',
                            'package_cost_txt'=>'120,000.00 THB',
                            'package_cost'=>'120000'
                        ],
                        [
                            'package_time_txt'=>'0.5 minute',
                            'package_cost_txt'=>'12,000.00 THB',
                            'package_cost'=>'12000'
                        ]
                    ]
                ]
            ];

            return $time_slots;
    }

}