jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="create-job-template-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ticket_id: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                },
                category:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                sub_category:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                }
            }
    });


    jQuery('input[name="choose_technicial"]').click(function(e){
        //console.log('aaaa');
        //console.log(jQuery(this).val());
        if(jQuery(this).val() === 'yes'){
            jQuery('select[name="technicial"]').parent().css({'display':''});
        }else{
            jQuery('select[name="technicial"]').parent().css({'display':'none'});
        }
    });

    jQuery('input[name="psi_assign_technicial"]').click(function(){
        if(jQuery(this).is(':checked')){
            jQuery('input[name="choose_technicial"]').closest('div.form-group').hide();
        }else{
            //console.log('unchecked');
            jQuery('input[name="choose_technicial"]').closest('div.form-group').show();
        }
    });

});