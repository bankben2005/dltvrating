

$( document ).ready(function() {
    $('#select_school').trigger("change");
    setDevicesAjax("select-deivces");
    
});

function setDevicesAjax(id){
 var base_url = $('input[name="base_url"]').val();
 $('#'+ id)
     .selectpicker({
         liveSearch: true
     })
     .ajaxSelectPicker({
       
         ajax: {
             url: base_url + 'api/classroom/getactivedevices',
             data: function () {
                 
                 var params = {
                     q: '{{{q}}}'
                 };
                 // if(gModel.selectedGroup().hasOwnProperty('ContactGroupID')){
                 //     params.GroupID = gModel.selectedGroup().ContactGroupID;
                 // }
                  return params;
             }
         },
         locale: {
             emptyTitle: 'Search for devices...'
         },
         preprocessData: function(data){
             console.log(data);
             var contacts = [];
             if(data.hasOwnProperty('returndata')){
                 var len = data.returndata.length;
              
                 for(var i = 0; i < len; i++){
                     var curr = data.returndata[i];
                     contacts.push(
                         {
                            'value': curr.id,
                            'text': curr.ship_code,
                            'data': {
                                'icon': 'icon-person',
                                'subtext': '(' + curr.created+ ')'
                            },
                             'disabled': false
                         }
                     );
                 }
             }
             return contacts;
         },
         preserveSelected: false
     });
    }
    //1f88b355-00001b3c

    $("form").on('submit', function (e) {
        var base_url = $('input[name="base_url"]').val();
        //ajax call here
        console.log(e);
        var class_name = $('input[name="class_name"]').val();
        var select_school = $('#select_school').val();
        var devices = $('#select-deivces').val();
        var u_id = $('#user_id').val();
        e.preventDefault();
        console.log($(":input").serializeArray());
        new_obj = {}
        var devices = [];
        $.each($(this).serializeArray(), function(i, obj) {
            if(obj.name == 'select-deivces[]'){
                 var objValue = obj.value;
                 if(!devices.includes(objValue)){
                    devices.push(objValue)
                 }
            }
            if(obj.name == "SchoolID"){
                new_obj["SchoolID"] = obj.value; 
            }
            if(obj.name == "class_id"){
                new_obj["class_id"] = obj.value; 
            }
         });
         // RECHECK 
         if(devices.length > 0){
            new_obj["devices"]  = devices;
         }
         new_obj['user_id'] = u_id;

         // CASE A : device move from a -> b
                 $.ajax({
                        url: base_url + 'api/classroom/checkdevicemove',
                        type: "post",
                        data: {
                            'post_data':new_obj
                        },
                        async:true,
                        dataType:'json',
                        success: function (response) {
                            console.log('==response==');
                            console.log(response);
                            if(response.status == false){
                                var message = "";
                                if(response.result_code == '-002'){
                                    if(confirm(response.result_message)){
                                        //CASE B: check device already exist on same class
                                      
                                        ajaxAlreadyExistOnSameClass(new_obj);
                                        e.preventDefault();
                                    }
                                    else{
                                         // if cancle break
                                        e.preventDefault();
                                    }
                                    e.preventDefault();
                                }
                                else if(response.result_code == '000'){
                                    // IF NOT EXIST ON ANOTHER CLASS
                                    //CASE B:
                                    //alert('B');
                                    ajaxAlreadyExistOnSameClass(new_obj);
                                    e.preventDefault();
                                }
                            }else{
                                //alert('C');
                                ajaxAlreadyExistOnSameClass(new_obj);
                                e.preventDefault();
                            }
                            // you will get response from your php page (what you echo or print)                 

                        },
                        error: function (request, status, error) {
                            console.log(request.responseText);
                        }
                    });



                console.log(new_obj);
                //confirm('Chip ID นี้ 1f88b355-00001b3c ถูกเพิ่มอยู่ในห้องอื่นแล้วยังยืนยันจะดำเนินการต่อหรือไม่ ?');  
                e.preventDefault();

                //stop form submission
            
     });

     function ajaxAlreadyExistOnSameClass(new_obj){
         //alert(5);
        var base_url = $('input[name="base_url"]').val();
        $.ajax({
            url: base_url + 'api/classroom/ajaxAlreadyExistOnSameClass',
            type: "post",
            data: {
                'post_data':new_obj
            },
            async:true,
            dataType:'json',
            success: function (response) {
                console.log('==response==');
                console.log(response);
                if(response.result_code == '-002'){
                    if(confirm(response.result_message)){
                        // if ok
                        deviceActionLog(new_obj);
                        
                        //return;
                    }
                    else{
                        //alert('fuck')
                        // if cancle break
                        //e.preventDefault();
                        return;
                    }
                    
                }
                else if(response.result_code == '000'){
                    // IF NOT EXIST ON ANOTHER CLASS
                    //alert(5);
                    deviceActionLog(new_obj);
                    
                    //return;
                }
               
                // you will get response from your php page (what you echo or print)                 

            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
     }


     function deviceActionLog(new_obj){
        var base_url = $('input[name="base_url"]').val();
        $.ajax({
            url: base_url + 'api/classroom/deviceActionLog',
            type: "post",
            data: {
                'post_data':new_obj
            },
            async:true,
            dataType:'json',
            success: function (response) {
                console.log('==response==');
                console.log(response);
                if(response.result_code == '-002'){
                    
                        alert(response.result_message);
                        //e.preventDefault();
                        return;
                   
                   
                }
                else if(response.result_code == '000'){
                    // IF NOT EXIST ON ANOTHER CLASS
                    alert('SUCCESS');
                    window.location.href=base_url + "admin/class_room/editClassroom/" + new_obj["class_id"];
                    
                }
               
                // you will get response from your php page (what you echo or print)                 

            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
     }

     function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }


    