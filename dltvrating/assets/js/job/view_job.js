baguetteBox.run('.tz-gallery');



function initialize_map(){
		var customer_lat_lon = {
			lat:parseFloat(jQuery('input[name="customer_latitude"]').val()),
			lon:parseFloat(jQuery('input[name="customer_longitude"]').val())
		};

		console.log('customer_lat_lon');
		console.log(customer_lat_lon);
		var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon         
                },
                map:map,
                draggable:false
        });

        marker.setMap(map);  

        var iw = new google.maps.InfoWindow({
            content: "<strong>ที่อยู่ลูกค้า</strong> <br><small>"+jQuery('input[name="customer_address"]').val()+"</small>"
        });
        google.maps.event.addListener(marker, "mouseover", function(e) {
            iw.open(map, this);
        });

}

function changeApproveStatus(element){
    var element = jQuery(element);

    var post_data = {
        'approve_status':element.val(),
        'technicial_job_id':element.data('jobid')
    };
    //console.log(post_data);

                    var base_url = jQuery('input[name="base_url"]').val();
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxChangeApproveStatus",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: jQuery('input[name="change_approve_success"]').val(),
                                  text: jQuery('input[name="change_approve_success_txt"]').val(),
                                  icon: "success",
                                });
                            }else{
                                swal({
                                  title: jQuery('input[name="change_approve_error"]').val(),
                                  text: jQuery('input[name="change_approve_error_txt"]').val(),
                                  icon: "error",
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
}