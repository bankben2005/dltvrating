jQuery("#input_excel").fileinput({ 
    language: "th",
    uploadUrl: '',
    uploadAsync: false,
    maxFileSize: 10000,
    allowedFileExtensions: ["xls", "xlsx"],
    uploadExtraData: {
        gallary_id: ""        
    },
    dropZoneEnabled:false,    
    showUpload: false,
    showClose: false,
    showCaption: true,
    showBrowse: true,
    showUpload:false,
    showUploadedThumbs: false,
    showPreview: false
}).on('filebatchuploaderror', function(event, data, msg) {
	console.log('filebatch uploade rror event = '+event+' data = '+data+' msg = '+msg);
}).on('filebatchpreupload',function(event,data,msg){
    console.log('file batchpreupload event = '+event+' data = '+data+' msg = '+msg);
}).on('filebatchuploadcomplete',function(event,data,msg){
    console.log('filebatch upload complete = '+event+' data = '+data+' msg = '+msg);
});


jQuery('#input_excel').on('fileselect', function(event, numFiles, label) {
    jQuery('button.fileinput-upload-button').attr('disabled','disabled');
});

function approveAllJobDone(element){
    var element = jQuery(element);
    var btn_txt = element.html();
    btn_txt += ' <i class="fa fa-spin fa-spinner"></i>';
    element.html('').html(btn_txt);
    element.attr('disabled','disabled');
    //return false;
    //console.log(btn_txt);return false;
    //console.log(element); return false;
    //console.log('abcd');
                    var base_url = jQuery('input[name="base_url"]').val();
                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"job/ajaxApproveAllJobDone",
                          async:true,
                          dataType:'json',
                          data:{},
                          beforeSend: function( xhr ) {
                                  
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                element.removeAttr('disabled');
                                element.find('i.fa').remove();
                                
                                swal({
                                  title: jQuery('input[name="change_approve_success"]').val(),
                                  text: jQuery('input[name="change_approve_all_success"]').val(),
                                  icon: "success",
                                }).then(function(){
                                    location.reload();
                                });
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

}