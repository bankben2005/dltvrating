jQuery(document).ready(function(){
    var base_url = jQuery('input[name="base_url"]').val();
    jQuery('form[name="export-job-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                job_status: {
                    trigger: 'change keyup',
                    // validators: {
                    //     notEmpty: {
                    //         message: ''
                    //     },
                    //     callback:{
                    //         message:'',
                    //         callback: function (value, validator, $field) {
                    //           if(value[0]===''){
                    //             return false;
                    //           }else{
                    //             return true;
                    //           }
                    //         }
                    //     }
                    // }
                }
            },
            onSuccess: function(e, data) {
                var select_job_status = jQuery('select[name="job_status"]').val();

                if(select_job_status[0] === ""){
                    e.preventDefault();

                    swal({
                        title: 'แจ้งเตือน!!',
                        text: 'กรุณาเลือกสถานะงานอย่างน้อย 1 รายการ',
                        icon: "warning",
                    }).then(function(){
                        //location.reload();
                    });


                }
                console.log(select_job_status);
                
                                
                
            },
            onError:function(e){
                e.preventDefault();
                console.log('aaaa');
            }
    });
});

jQuery(function() {
        jQuery('select[name="job_status"]').change(function() {
            jQuery('input[name="hide_job_status"]').val('').val(jQuery(this).val());
        }).multipleSelect({
            width: '100%'
        });

        //jQuery('select[name="job_status"]').click(function() {
            jQuery('input[data-name="selectItemjob_status"][value=""]').parent().remove();
        //});

        

});



jQuery('input[name="duration"]').daterangepicker({
        autoApply: true,
        timePicker: false,
        showDropdowns: true,
        timePickerIncrement: 30,
        maxDate:moment(new Date()),
        locale: {
            format: 'DD-MM-YYYY',
            customRangeLabel: "กำหนดเอง",
            cancelLabel: 'ยกเลิก',
            applyLabel:'ตกลง'
        },
        ranges: {
           'วันนี้': [moment(), moment()],
           'เมื่อวาน': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 วันล่าสุด': [moment().subtract(6, 'days'), moment()],
           '30 วันล่าสุด': [moment().subtract(29, 'days'), moment()],
           'เดือนนี้': [moment().startOf('month'), moment().endOf('month')],
           'เดือนที่แล้ว': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});