function initialize_map(){
		var customer_lat_lon = {
			lat:parseFloat($('input[name="device_latitude"]').val()),
			lon:parseFloat($('input[name="device_longitude"]').val())
		};

		console.log('customer_lat_lon');
		console.log(customer_lat_lon);
		var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon  
                },
                zoom:9,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon         
                },
                map:map,
                draggable:false
        });

        marker.setMap(map);  

        var iw = new google.maps.InfoWindow({
            content: "<strong>บริเวณของกล่อง</strong> <br><small>"+jQuery('input[name="device_address"]').val()+"</small>"
        });
        google.maps.event.addListener(marker, "mouseover", function(e) {
            iw.open(map, this);
        });

}

function updateDeviceAddressByIpAddress(element){
    var base_url = $('input[name="base_url"]').val();
    var element = $(element);
    var post_data = {
        'ip_address':element.data('ip'),
        'device_addresses_id':element.data('deviceaddressesid')
    };

                $.ajax({
                        url: base_url+"admin/device/ajaxUpdateDeviceAddressByIpAddress",
                        type: "post",
                        data: post_data,
                        async:true,
                        dataType:'json',
                        success: function (response) {
                            console.log('==response==');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: "Good job!",
                                  text: "Update task success",
                                  icon: "success",
                                  button: "OK!",
                                }).then(function(){
                                    location.reload();
                                });
                               
                            }
                           // you will get response from your php page (what you echo or print)                 

                        },
                        error: function (request, status, error) {
                            console.log(request.responseText);
                        }


                });
    console.log(post_data);
}


function updateProvinceRegionByZipcode(element){
    var base_url = $('input[name="base_url"]').val();
    var element = $(element);
    var post_data = {
        'zipcode':element.data('zipcode'),
        'device_addresses_id':element.data('deviceaddressesid')
    };

                $.ajax({
                        url: base_url+"admin/device/ajaxUpdateProvinceRegionByZipcdoe",
                        type: "post",
                        data: post_data,
                        async:true,
                        dataType:'json',
                        success: function (response) {
                            console.log('==response==');
                            console.log(response);
                            if(response.status){
                                swal({
                                  title: "Good job!",
                                  text: "Update task success",
                                  icon: "success",
                                  button: "OK!",
                                }).then(function(){
                                    location.reload();
                                });
                               
                            }
                           // you will get response from your php page (what you echo or print)                 

                        },
                        error: function (request, status, error) {
                            console.log(request.responseText);
                        }


                });

}

function changeSelectRegion(element){
    var base_url = $('input[name="base_url"]').val();
    var element = $(element);
    var post_data = {
        'device_addresses_id':element.data('deviceaddressesid'),
        'region_name':element.val()
    };

    //console.log(post_data);
                $.ajax({
                        url: base_url+"admin/device/ajaxUpdateRegionName",
                        type: "post",
                        data: post_data,
                        async:true,
                        dataType:'json',
                        success: function (response) {
                            console.log('==response==');
                            console.log(response);
                            if(response.status){

                                $('select[name="province_region"]').val(response.province_region);
                                // swal({
                                //   title: "Good job!",
                                //   text: "Update task success",
                                //   icon: "success",
                                //   button: "OK!",
                                // }).then(function(){
                                //     location.reload();
                                // });
                               
                            }
                           // you will get response from your php page (what you echo or print)                 

                        },
                        error: function (request, status, error) {
                            console.log(request.responseText);
                        }


                });



}

function changeSelectProvinceRegion(element){
    var base_url = $('input[name="base_url"]').val();
    var element = $(element);
    var post_data = {
        'device_addresses_id':element.data('deviceaddressesid'),
        'province_region':element.val()

    };
    //console.log(post_data);
                $.ajax({
                        url: base_url+"admin/device/ajaxUpdateProvinceRegion",
                        type: "post",
                        data: post_data,
                        async:true,
                        dataType:'json',
                        success: function (response) {
                            console.log('==response==');
                            console.log(response);
                            if(response.status){
                                // swal({
                                //   title: "Good job!",
                                //   text: "Update task success",
                                //   icon: "success",
                                //   button: "OK!",
                                // }).then(function(){
                                //     location.reload();
                                // });
                               
                            }
                           // you will get response from your php page (what you echo or print)                 

                        },
                        error: function (request, status, error) {
                            console.log(request.responseText);
                        }


                });

}