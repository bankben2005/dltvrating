function clickCollapseMenu(element){
	var element = $(element);


	var checkbox = element.find('input[type="checkbox"]');

	if(checkbox.is(':checked')){
		checkbox.prop('checked',false);
	}else{
		checkbox.prop('checked',true);
	}
}

$(document).ready(function(){
	$('a[data-toggle="collapse"]').each(function(index,value){
		var element = $(value);
		var checkbox = element.find('input[type="checkbox"]');

		if(checkbox.is(':checked')){
			$(element.attr('href')).collapse('show');
			// console.log(element.attr('href'));
		}else{
			$(element.attr('href')).collapse('hide');
		}
	});
});