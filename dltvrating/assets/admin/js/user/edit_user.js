$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('input[name="email"]').attr('readonly','readonly');
    $('form[name="create-user-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                user_accesstype_id:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                firstname: {
                    validators: {
                        notEmpty: {
                            message:''
                        }
                    }
                },
                lastname:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                email:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                }
            }
    });

    $('form[name="reset-password-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
                },
                confirm_new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'new_password',
                                message: ''
                        }
                    }
                }

            },
            onSuccess: function(e, data) {
                e.preventDefault();
                var base_url = $('input[name="base_url"]').val();
                $.ajax({
                url: base_url+"admin/user/ajaxSetResetPassword",
                type: "post",
                data: {
                    'new_password':$('input[name="new_password"]').val(),
                    'user_id':$('input[name="user_id"]').val()
                },
                async:true,
                dataType:'json',
                success: function (response) {
                    console.log('==response==');
                    console.log(response);
                    if(response.status){
                        $('#resetPasswordModal').modal('toggle');

                        swal({
                          title: "Good job!",
                          text: "Password has been reseted!",
                          icon: "success",
                        });
                    }
                   // you will get response from your php page (what you echo or print)                 

                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }


                });

            }

    });

    checkOnClickOwnerChannelStatus();
    checkInitSelectShowTotalDevice();

});

function checkOnClickOwnerChannelStatus(){

    /* check initial hide show */
    var owner_channel_status = $('input[name="owner_channel_status"]:checked').val();

    if(owner_channel_status === 'yes'){
        $('div#panel_owner_status').show();
    }
    //console.log(owner_channel_status);
    /* eof check initial hide show */


    $('input[name="owner_channel_status"]').click(function(){
        var status_val = $(this).val();
        if(status_val === 'yes'){
            $('div#panel_owner_status').show();
        }else{
            $('div#panel_owner_status').hide();
        }
    });
}

function checkInitSelectShowTotalDevice(){
    var all_realtime_viewer_status = $('select[name="admin_status"]').val();

    if(all_realtime_viewer_status === '1'){
        $('div#show_total_device').css({'display':''});
    }else{
        $('div#show_total_device').css({'display':'none'});
    }
}

function checkToggleShowTotalDevice(element){
    // $('select[name=')
    var element = $(element);

    if(element.val() === '1'){
        $('div#show_total_device').css({'display':''});
    }else{
        $('div#show_total_device').css({'display':'none'});
    }

    // console.log(element);
}