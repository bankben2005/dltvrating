$(document).ready(function(){

	var base_url = $('input[name="base_url"]').val();


	$('form[name="upload-image-form"]').bootstrapValidator({
		message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

            	duration:{
            		validators:{
                    	notEmpty:{
                            message:''
                        }
                    }
            	},
                ads_image:{
                    validators:{
                    	notEmpty:{
                            message:''
                        },
                        file:{
                            extension: 'jpeg,png,jpg,gif',
	                        type: 'image/jpeg,image/png,image/jpg,image/gif',
	                        maxSize: 2048 * 1024,
	                        message: 'The selected file is not valid'
                        }
                    }
                }

            },
            onSuccess: function(e, data) {
            	//e.preventDefault();
            }

	});

    $('form[name="add-youtube-url"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                start_datetime:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                youtube_url:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        uri:{
                        	message:''
                        }
                    }
                }
            },
            onSuccess: function(e, data) {
                e.preventDefault();
                var base_url = $('input[name="base_url"]').val();
                var ads_campaign_id = $('input[name="hide_ads_campaign_id"]').val();
                var ads_campaign_videos_id = $('input[name="ads_campaign_videos_id"]').val();
                var start_datetime = $('input[name="start_datetime"]').val();
                var youtube_url = $('input[name="youtube_url"]').val();

                var post_data = {
                	'ads_campaign_id':ads_campaign_id,
                	'ads_campaign_videos_id':ads_campaign_videos_id,
                	'start_datetime':start_datetime,
                	'youtube_url':youtube_url
                };
                
                $.ajax({
		                url: base_url+"admin/shopping/ajaxSetAdsVideo",
		                type: "post",
		                data: post_data,
		                async:true,
		                dataType:'json',
		                success: function (response) {
		                    console.log('==response==');
		                    console.log(response);
		                    if(response.status){
		                    	swal({
								  title: "Good job!",
								  text: "Create advertisement video success!",
								  icon: "success",
								  button: "OK!",
								}).then(function(){
									location.reload();
								});
		                       
		                    }
		                   // you will get response from your php page (what you echo or print)                 

		                },
		                error: function (request, status, error) {
		                    console.log(request.responseText);
		                }


                });

                console.log(post_data);

            }
    });




	$('input[name="start_datetime"]').daterangepicker({
			singleDatePicker:true,
            //autoApply:true,
            timePicker: true,
            timePicker24Hour: true,
            //timePickerIncrement: 30,
            //minDate:moment().add(1, 'days'),
            //minDate:$('input[name="hide_start_date"]').val(),
            locale: {
                format: 'DD-MM-YYYY HH:mm'
            }
    }, function(start, end, label) {
      console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
	});


	$('input[name="duration"]').daterangepicker({
            autoApply:true,
            timePicker: true,
            timePicker24Hour: true,
            //timePickerIncrement: 30,
            // startDate:$('input[name="hide_start_date"]').val(),
            //minDate:$('input[name="hide_start_date"]').val(),
            locale: {
                format: 'DD-MM-YYYY HH:mm'
            }
    }, function(start, end, label) {
      console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
	});

    $('input[name="edit_duration"]').daterangepicker({
            autoApply:true,
            timePicker: true,
            timePicker24Hour: true,
            //timePickerIncrement: 30,
            // startDate:$('input[name="hide_start_date"]').val(),
            //minDate:$('input[name="hide_start_date"]').val(),
            locale: {
                format: 'DD-MM-YYYY HH:mm'
            }
    }, function(start, end, label) {
      console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });




	$('a.click_upload_image').click(function(){
		// console.log($(this));
		var position = $(this).data('position');
		$('input[name="hide_position"]').val(position);
		$('input[name="advertisement_campaigns_id"]').val($('input[name="hide_ads_campaign_id"]').val());
	});

});


$('.pop').click(function(){
    var imgurl = $(this).closest('.view').find('img').attr('src');
    // console.log(imgurl.attr('src'));
    $('#imagepreview').attr('src', imgurl); // here asign the image to the modal when the user click the enlarge link
    $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function


});

function editAdsCampaignVideos(element){
	var element = $(element);
	var start_datetime = element.data('startdatetime');
	var youtube_url = element.data('youtubeurl');
	var adcvideoid = element.data('adcvideoid');


	$('#setYoutubeUrl').modal('toggle');
	var form = $('form[name="add-youtube-url"]');
	form.find('input[name="start_datetime"]').val(start_datetime);
	form.find('input[name="youtube_url"]').val(youtube_url);
	form.find('input[name="ads_campaign_videos_id"]').val(adcvideoid);

}


function editAdsCampaignImage(element){
    var base_url = $('input[name="base_url"]').val();
    var element = $(element);
    var form = $('form[name="edit-adsimage-form"]');
    var ads_image_id = element.data('imageid');
    var ads_image_name = element.data('imagename');
    var duration = element.data('duration');

    form.find('input[name="hide_ads_image_id"]').val(ads_image_id);
    form.find('input[name="edit_duration"]').val(duration);
    form.find('img').attr('src',base_url+'uploaded/advertisement/campaign_images/'+ads_image_id+'/'+ads_image_name);
    

}
function deleteAdsCampaignVideos(element){
	var base_url = $('input[name="base_url"]').val();
	var element = $(element);
	var adcvideoid = element.data('adcvideoid');
	//console.log(adcvideoid); return false;

				$.ajax({
		                url: base_url+"admin/shopping/ajaxUnsetAdsVideo",
		                type: "post",
		                data: {'ads_campaign_videos_id':adcvideoid},
		                async:true,
		                dataType:'json',
		                success: function (response) {
		                    console.log('==response==');
		                    console.log(response);
		                    if(response.status){

		                    	swal({
								  title: "Good job!",
								  text: "Delete advertisement video success!",
								  icon: "success",
								  button: "OK!",
								}).then(function(){
									location.reload();
								});


		                       //location.reload();
		                    }
		                   // you will get response from your php page (what you echo or print)                 

		                },
		                error: function (request, status, error) {
		                    console.log(request.responseText);
		                }


                });


}