function createChannelProgram(element){
	var element = $(element);

	console.log(element);
	$('input[name="hide_channels_id"]').val(element.data('channelid'));
	$('#createChannelProgramModal').modal('toggle');

}

function createChannelProgramMultirows(element){
	var element = $(element);



	console.log(element);
	var channels_id = element.data('channelid');
	$('input[name="hide_multirows_channels_id"]').val(channels_id);
	$('#createChannelProgramMultiRowsModal').modal('toggle');
	console.log(channels_id);


}

function editChannelProgram(element){
	var element = $(element);
	var row_data = element.data('rowdata');

	

	var row_date = moment(row_data.date).format('DD/MM/YYYY');
	//console.log();


	// console.log(moment(row_data.start_time,'h:mm').format('h:mm')); return false;

	var form_obj = $('form[name="create-channel-program"]');
	form_obj.find('input[name="hide_channels_id"]').val(row_data.channels_id);
	form_obj.find('input[name="hide_channel_programs_id"]').val(row_data.id);
	form_obj.find('input[name="action"]').val('update');
	form_obj.find('input[name="name"]').val(row_data.name);
	form_obj.find('input[name="description"]').val(row_data.description);
	form_obj.find('input[name="date"]').val(row_date);
	form_obj.find('input[name="start_time"]').val(moment(row_data.start_time,'h:mm').format('H:mm'));
	form_obj.find('input[name="end_time"]').val(moment(row_data.end_time,'h:mm').format('H:mm'));
	form_obj.find('select[name="active"]').val(row_data.active);
	$('#createChannelProgramModal').modal('toggle');
}

function deleteChannelProgram(element){
	var element = $(element);
	var row_data = element.data('rowdata');
	var base_url = $('input[name="base_url"]').val();

	$.ajax({
		url: base_url+"admin/channels/ajaxDeleteChannelProgram",
		type: "post",
		data: {'rowid':row_data.id} ,
		async:true,
		dataType:'json',
		success: function (response) {
			console.log('==response==');
			console.log(response);
			if(response.status){
				// swal({
				// 	title: "Good job!",
				// 	text: "Save data success",
				// 	icon: "success",
				// }).then(function(){

				// 	location.reload();
				// });

					swal({
            			title: "Good job!",
            			text: "Save data success",
            			icon: "success",
            		},function(){
            			location.reload();
            		});

			}
		           // you will get response from your php page (what you echo or print)                 

		       },
		       error: function (request, status, error) {
		       	console.log(request.responseText);
		       }


		   });
}

function deleteChannelProgramBODate(element){
	var element = $(element);
	var row_data = element.data('rowdata');
	var base_url = $('input[name="base_url"]').val();

	console.log(row_data);
	var closest_row = element.closest('tr');

	//console.log(closest_row); return false;

	$.ajax({
		url: base_url+"admin/channels/ajaxDeleteChannelProgramBODate",
		type: "post",
		data: {'rowid':row_data.id} ,
		async:true,
		dataType:'json',
		success: function (response) {
			console.log('==response==');
			console.log(response);
			if(response.status){
				closest_row.remove();

			}
		           // you will get response from your php page (what you echo or print)                 

		       },
		       error: function (request, status, error) {
		       	console.log(request.responseText);
		       }


		   });


}

function addProgramRows(element){
	var element = $(element);
	var base_url = $('input[name="base_url"]').val();

	$.ajax({
		url: base_url+"admin/channels/ajaxAddChannelProgramRows",
		type: "post",
		data: {} ,
		async:true,
		dataType:'json',
		success: function (response) {
			console.log('==response==');
			console.log(response);

			if(response.status){
				$('#row-program').append(response.row_data);
			}

			           // you will get response from your php page (what you echo or print)                 

			       },
			       error: function (request, status, error) {
			       	console.log(request.responseText);
			       }


			   });


}

function removeProgramRows(element){
	var element = $(element);

	var current_row = element.closest('.row');
	current_row.fadeOut(300, function() { $(this).remove(); });


	console.log(current_row);


}

function submitChannelProgramMultirows(e){
	

	var count_null_textbox = 0;

	$('input[name="multirows_name[]"]').each(function(index,value){
		if($(value).val() === ''){
			count_null_textbox += 1;
		}
	});

	if($('input[name="multirows_name[]"]').length == count_null_textbox){
		e.preventDefault();
		$('input[name="multirows_name[]"]')[0].focus();
	}else{
		e.preventDefault();
		var form_data = $('form[name="create-channel-multirows-program"]').serialize();
		var base_url = $('input[name="base_url"]').val();

		

		var btn_submit = $('form[name="create-channel-multirows-program"]').find('button[type="submit"]');

		$.ajax({
			url: base_url+"admin/channels/ajaxSetChannelProgramMultirows",
			type: "post",
			data: form_data ,
			async:true,
			dataType:'json',
			beforeSend:function(){
						
						var btn_submit_txt = btn_submit.html();

						btn_submit_txt += ' <i class="fa fa-spin fa-spinner"></i>';
						btn_submit.html('').html(btn_submit_txt);
						btn_submit.attr('disabled','disabled');
			},
			success: function (response) {
				console.log('==response==');
				console.log(response);

				if(response.status){

					$('#createChannelProgramMultiRowsModal').modal('toggle');
					// swal({
					// 	title: "Good job!",
     //        			text: "Save data success",
     //        			icon: "success",
					// }).then(function(){

					// 	btn_submit.find('i.fa').remove();
					// 	btn_submit.removeAttr('disabled');


					// 	location.reload();
					// }); 

					swal({
            			title: "Good job!",
            			text: "Save data success",
            			icon: "success",
            		},function(){
            						
						btn_submit.find('i.fa').remove();
						btn_submit.removeAttr('disabled');


						location.reload();
            		});

				}
				
			           // you will get response from your php page (what you echo or print)                 

			       },
			       error: function (request, status, error) {
			       	console.log(request.responseText);
			       }


			   });
		
	}

}
$(document).ready(function(){
	$('#start_time').datetimepicker({
		format: 'LT'
	});
	$('#end_time').datetimepicker({
		format: 'LT'
	});

	$('.start_time').datetimepicker({
		format: 'LT'
	});

	$('#dateprogram').datetimepicker({
		format:'L'
	});

	$('#datemultirows').datetimepicker({
		format:'L',
	});

	$('#search_date').datetimepicker({
		format:'L',
	});
// 	$("#datemultirows").on("dp.change", function (e) {
//      console.log(e.date);
// });

// 	$('#datemultirows').datetimepicker().on('dp.change',function(e){
//     console.log(e)
// });

	var base_url = $('input[name="base_url"]').val();
	$('form[name="create-channel-program"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name: {
				validators: {
					notEmpty: {
						message:''
					}
				}
			},
			date_of_week:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			}
		},
		onSuccess: function(e, data) {
			e.preventDefault();
			var form_data = $('form[name="create-channel-program"]').serialize();
			var base_url = $('input[name="base_url"]').val();
            		//console.log(form_data);
            		$.ajax({
            			url: base_url+"admin/channels/ajaxSetChannelProgramBODate",
            			type: "post",
            			data: form_data ,
            			async:true,
            			dataType:'json',
            			success: function (response) {
            				console.log('==response==');
            				console.log(response);
            				if(response.status){
            					$('#createChannelProgramModal').modal('toggle');
    							swal({
            						title: "Good job!",
            						text: "Save data success",
            						icon: "success",
            					},function(){
            						
									location.reload();
            					});

            				}
							           // you will get response from your php page (what you echo or print)                 

							       },
							       error: function (request, status, error) {
							       	console.log(request.responseText);
							       }


							   });
            	}
            });

	checkDefaultSearchByDate();
});


function checkDefaultSearchByDate(){
	if($('input[name="search_by_date"]').is(':checked')){
		$('input[name="search_date"]').removeAttr('disabled');
	}
}

function addDefaultProgramFromDow(element){
	var element = $(element);
	var base_url = $('input[name="base_url"]').val();

	var post_data = {
		'program_date':$("#datemultirows").find("input").val(),
		'channels_id':$('input[name="hide_multirows_channels_id"]').val()
	};
	// console.log($('#datemultirows').viewDate())
	//console.log(post_data);return false;

			$.ajax({
					url: base_url+"admin/channels/ajaxAddDefaultProgramFromDow",
					type: "post",
					data: post_data ,
					async:true,
					dataType:'json',
					beforeSend:function(){
						var btn_content = $('#btnAddDefaultProgramFromDow').html();
						btn_content += ' <i class="fa fa-spin fa-spinner"></i>';
						$('#btnAddDefaultProgramFromDow').html('').html(btn_content);
					},
					success: function (response) {
						console.log('==response==');
						console.log(response);

						if(response.status){
							// swal({
							// 	title: "Good job!",
							// 	text: "Save data success",
							// 	icon: "success",
							// }).then(function(){

							// 	location.reload();
							// });
							$('#row-program').html('').append(response.row_data);
							$('#btnAddDefaultProgramFromDow').find('i.fa-spin').remove();


						}else{
							$('#btnAddDefaultProgramFromDow').find('i.fa-spin').remove();
						}
						
					           // you will get response from your php page (what you echo or print)                 

					       },
					       error: function (request, status, error) {
					       	console.log(request.responseText);
					       }


			});




	
}

// function changeDateMultirows(element){
// 	console.log(element);
// }

// $("#datemultirows").on("dp.change", function (e) {
     
// });

function checkAllEvent(element){
	var element = $(element);
	if(element.is(':checked')){
		$('input[name="check_event[]"]').each(function(index,el){
			//$(el).attr('checked','checked');
			if(!$(el).is(':checked')){
				$(el).prop('checked',true);
			}
		});
	}else{
		$('input[name="check_event[]"]').each(function(index,el){
			$(el).prop('checked',false);
		});
	}
}
function clickCheckEvent(element){
	var element = $(element);

	if(!element.is(':checked')){
		element.prop('checked',false);
	}

	/* check still check all */
	var check_event_length = $('input[name="check_event[]"]').length;
	var check_event_checked_length = $('input[name="check_event[]"]:checked').length;

	if(check_event_length !== check_event_checked_length){
		$('input[name="check_all"]').prop('checked',false);
	}
	/* */

}

function submitActionEventForm(element){
	var element = $(element);

	var action_type = $('select[name="action"]').val();

	if(!action_type){
			// swal({
			// 	title: "Warning!",
			// 	text: "Please select action.",
			// 	icon: "warning",
			// }).then(function(){
			// 	$('select[name="action"]').focus();
			// }); 

			swal({
				title: "Warning!",
				text: "Please select action.",
				icon: "warning",
            },function(){
            						
				$('select[name="action"]').focus();
            });
		return false;
	}

	var count_check_action = $('input[name="check_event[]"]:checked').length;
	if(count_check_action === 0){
		// swal({
		// 		title: "Warning!",
		// 		text: "Please check record at least 1 record for action.",
		// 		icon: "warning",
		// 	}).then(function(){
				
		// 	});
			swal({
				title: "Warning!",
				text: "Please check record at least 1 record for action.",
				icon: "warning",
            },function(){
            						
				//$('select[name="action"]').focus();
            });
		return false;
	}

	/* elf pass condition */
	var arrCheckEvent = [];
	//arrCheckEvent['eventRows'] = [];
	//arrCheckEvent['action'] = $('select[name="action"]').val();


	$('input[name="check_event[]"]:checked').each(function(i,el){
		var el = $(el);
		arrCheckEvent.push({
			'program_id':el.val()			
		});
	});

	var post_data = array2json(arrCheckEvent);

	$('input[name="json_data"]').val(post_data);

	$('form[name="action-event-form"]').attr('action',window.location.href);

	//return false;
	$('form[name="action-event-form"]').submit();

	// var form_html = '<form action="" method="post">';
	// form_html += '<input type="hidden" name="json_data" value="'+post_data+'"';
	// form_html += '</form>';
	// $(form_html).appendTo('body').submit();
	



	/* eof pass condition */
}

function array2json(arr) {
    var parts = [];
    var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');

    for(var key in arr) {
    	var value = arr[key];
        if(typeof value == "object") { //Custom handling for arrays
            if(is_list) parts.push(array2json(value)); /* :RECURSION: */
            else parts.push('"' + key + '":' + array2json(value)); /* :RECURSION: */
            //else parts[key] = array2json(value); /* :RECURSION: */
            
        } else {
            var str = "";
            if(!is_list) str = '"' + key + '":';

            //Custom handling for multiple data types
            if(typeof value == "number") str += value; //Numbers
            else if(value === false) str += 'false'; //The booleans
            else if(value === true) str += 'true';
            else str += '"' + value + '"'; //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Functions?)

            parts.push(str);
        }
    }
    var json = parts.join(",");
    
    if(is_list) return '[' + json + ']';//Return numerical JSON
    return '{' + json + '}';//Return associative JSON
}

function clickCheckSearchByDate(element){
	var element = $(element);
	if(element.is(':checked')){
		$('input[name="search_date"]').removeAttr('disabled');
	}else{
		$('input[name="search_date"]').attr('disabled','disabled');
	}
}

function changeSorting(sort_value){
	if(sort_value !== ''){
		var url = window.location.href,
	    retObject = {},
	    parameters;

	    if (url.indexOf('?') === -1) {
	    	window.location.href = window.location.href+'?sorting='+sort_value;
	        return null;
	    }

	    var url_without_querystring = url.split('?')[0];
	    url = url.split('?')[1];


	    var query_string = url;

	    parameters = url.split('&');

	    for (var i = 0; i < parameters.length; i++) {
	        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
	    }

	    console.log(query_string);

	    if($.isEmptyObject(retObject)){
	    	//console.log(url_without_querystring);
	    	//console.log('empty');
	    	window.location.href = url_without_querystring+'?sorting='+sort_value;
	    }else{
	    	window.location.href = window.location.href+'&sorting='+sort_value;
	    }
	}
}


