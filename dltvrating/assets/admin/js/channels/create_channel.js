$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    var band_type = $('select[name="band_type"]').val();
    $('form[name="create-channel-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                channel_name: {
                    validators: {
                        callback: {
                            message: '',
                            callback: function (value, validator, $field) {
                                if((band_type === 'C' || band_type === 'ALL') && value === ''){
                                    return false;
                                }else{
                                    return true;
                                }
                            }
                        }
                    }
                }
            },
            onSuccess: function(e, data) {
                        //e.preventDefault();
                        //this section before submit
                        //alert('Success');
                        if(!checkAleadyFillChannelComponent()){
                            $('form[name="create-channel-form"]').find('div.alert-warning').css({
                                'border':'1px dashed red'
                            });
                            e.preventDefault();
                            /* some swall for alert*/
                        }
            }
    });


    $('select[name="band_type"]').change(function(){
        var band_type_val = $(this).val();
        switch(band_type_val){
            case 'C':
                $('ul.nav-tabs > li:nth-child(1)').find('a').removeClass('active');
                $('ul.nav-tabs > li:nth-child(2)').find('a').removeClass('active');

                $('ul.nav-tabs > li:nth-child(1)').show().find('a').addClass('active');
                $('ul.nav-tabs > li:nth-child(2)').hide().find('a').removeClass('active');

                /* content tab*/
                $('div#myTabContent').find('div#cband').show().addClass('fade show active');
                $('div#myTabContent').find('div#kuband').hide();

            break;
            case 'KU':
                $('ul.nav-tabs > li:nth-child(1)').find('a').removeClass('active');
                $('ul.nav-tabs > li:nth-child(2)').find('a').removeClass('active');

                $('ul.nav-tabs > li:nth-child(2)').show().find('a').addClass('active');
                $('ul.nav-tabs > li:nth-child(1)').hide().find('a').removeClass('active');

                /* content tab */
                $('div#myTabContent').find('div#kuband').show().addClass('fade show active');
                $('div#myTabContent').find('div#cband').hide();


            break;
            case 'ALL':
                $('ul.nav-tabs > li:nth-child(1)').find('a').removeClass('active');
                $('ul.nav-tabs > li:nth-child(2)').find('a').removeClass('active');

                $('ul.nav-tabs > li:nth-child(1)').show().find('a').addClass('active');
                $('ul.nav-tabs > li:nth-child(2)').show();

                $('div#myTabContent').find('div#kuband').show();
                $('div#myTabContent').find('div#cband').show().addClass('fade show active');
            break;

        }
    });


    /* script for channel extended */
    $('select[name="channel_extended_status"]').change(function(){
        var extended_status = $(this).val();

        if(extended_status === '1'){
            $('div#channel-extended-from').show();
        }else{
            $('div#channel-extended-from').hide();
        }

    });
    /* eof script for channel extended*/


    checkInitialChannelExtendedStatus();




    checkInitialBandTypeAndTab();

});


function checkAleadyFillChannelComponent(){
    var return_status = true;
    band_type_selected = $('select[name="band_type"]').val();
    //console.log(band_type_selected);
    var cband_frq = $('input[name="cband_frq"]').val();
    var cband_sym = $('input[name="cband_sym"]').val();
    var cband_vdo_pid = $('input[name="cband_vdo_pid"]').val();
    var cband_ado_pid = $('input[name="cband_ado_pid"]').val();


    var kuband_frq = $('input[name="kuband_frq"]').val();
    var kuband_sym = $('input[name="kuband_sym"]').val();
    var kuband_vdo_pid = $('input[name="kuband_vdo_pid"]').val();
    var kuband_ado_pid = $('input[name="kuband_ado_pid"]').val();



    switch(band_type_selected){
        case 'C':
            /* check all input fr cband */
            if(cband_frq === '' || cband_sym === '' || cband_vdo_pid === '' || cband_ado_pid === ''){
                swal({
                    title: 'Warning!!!',
                    text: 'Please enter completely cband component',
                    icon: "warning",
                }).then(function(){
                    
                });
                return_status = false;
                
            }

        break;
        case 'KU':
            if(kuband_frq === '' || kuband_sym === '' || kuband_vdo_pid === '' || kuband_ado_pid === ''){
                swal({
                    title: 'Warning!!!',
                    text: 'Please enter completely kuband component',
                    icon: "warning",
                }).then(function(){
                    //return false;
                });
                return_status = false;
                
            }
        break;
        case 'ALL':
            if(cband_frq === '' || cband_sym === '' || cband_vdo_pid === '' || cband_ado_pid === '' || kuband_frq === '' || kuband_sym === '' || kuband_vdo_pid === '' || kuband_ado_pid === ''){
                swal({
                    title: 'Warning!!!',
                    text: 'Please enter completely cuband and kuband component',
                    icon: "warning",
                }).then(function(){
                    //return false;
                });
                return_status = false;
                
            }

        break;
    }

    return return_status;


}


function checkInitialBandTypeAndTab(){
    var band_type_val = $('select[name="band_type"]').val();

    switch(band_type_val){
            case 'C':
                $('ul.nav-tabs > li:nth-child(1)').find('a').removeClass('active');
                $('ul.nav-tabs > li:nth-child(2)').find('a').removeClass('active');

                $('ul.nav-tabs > li:nth-child(1)').show().find('a').addClass('active');
                $('ul.nav-tabs > li:nth-child(2)').hide().find('a').removeClass('active');

                /* content tab*/
                $('div#myTabContent').find('div#cband').show().addClass('fade show active');
                $('div#myTabContent').find('div#kuband').hide();

            break;
            case 'KU':
                $('ul.nav-tabs > li:nth-child(1)').find('a').removeClass('active');
                $('ul.nav-tabs > li:nth-child(2)').find('a').removeClass('active');

                $('ul.nav-tabs > li:nth-child(2)').show().find('a').addClass('active');
                $('ul.nav-tabs > li:nth-child(1)').hide().find('a').removeClass('active');

                /* content tab */
                $('div#myTabContent').find('div#kuband').show().addClass('fade show active');
                $('div#myTabContent').find('div#cband').hide();


            break;
            case 'ALL':
                $('ul.nav-tabs > li:nth-child(1)').find('a').removeClass('active');
                $('ul.nav-tabs > li:nth-child(2)').find('a').removeClass('active');

                $('ul.nav-tabs > li:nth-child(1)').show().find('a').addClass('active');
                $('ul.nav-tabs > li:nth-child(2)').show();

                $('div#myTabContent').find('div#kuband').removeClass('active show');
                $('div#myTabContent').find('div#cband').show().addClass('fade show active');
            break;

    }





}

function checkInitialChannelExtendedStatus(){
    var extended_status = $('select[name="channel_extended_status"]').val();

    if(extended_status === '1'){
        $('div#channel-extended-from').show();
    }else{
        $('div#channel-extended-from').hide();
    }
}