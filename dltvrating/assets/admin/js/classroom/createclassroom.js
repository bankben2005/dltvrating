var base_url = jQuery('input[name="base_url"]').val();
var $input_customer = jQuery("input[name='customer']");
$input_customer.typeahead({
  source:  function (query, process) {
        return jQuery.get(base_url+'api/ajaxActiveDevices', { query: query}, function (data) {
              data = jQuery.parseJSON(data);
              console.log('data');
              console.log(data);
              return process(data);
          });


  },
  updater: function(item){
    // console.log(item);
    jQuery('input[name="customer_id"]').val(item.id);
    setMapMarker(item);

    jQuery('input[name="customer_lat"]').val(item.lat);
    jQuery('input[name="customer_lon"').val(item.lng);
    return jQuery.trim(item.name);
  },
  autoSelect: false

});