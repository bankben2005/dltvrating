<?php
date_default_timezone_set('Asia/Bangkok');
define('ENVIRONMENT','production');
define('secret_key',':}E6Uc)>FD*[q[>T');
class SetChannelAudienceRegionSummaryFOM{
// Create connection

	private $audience_region_summary_url = "";
	function __construct(){
		$this->audience_region_summary_url = $this->setAudienceRegionSummaryUrl();
		//print_r($this->daily_advertisement_url);
	}

	public function setCronAudienceRegionSummaryFOM(){

		$audience_region_summary_url = $this->audience_region_summary_url;

		/* check first day of month */
		$firstday_of_month = new DateTime(date('Y').'-'.date('m').'-01');
		/* eof check first day of month */

		if($firstday_of_month->format('Y-m-d') == date('Y-m-d')){


			/* create log file */
			

			// Get cURL resource
	        $curl = curl_init();
	        // Set some options - we are passing in a useragent too here
	        curl_setopt_array($curl, array(
	            CURLOPT_RETURNTRANSFER => 1,
	            CURLOPT_URL => $audience_region_summary_url.'?date_report='.$firstday_of_month->format('Y-m-d'),
	            CURLOPT_TIMEOUT=>10000
	        ));
	        // Send the request & save response to $resp
	        $resp = curl_exec($curl);

	        $log_file_path = $this->createLogFilePath('AudienceRegionSummaryFom');

			$file_content = date("Y-m-d H:i:s"). 'fom = '.$firstday_of_month->format('Y-m-d'). "\n";
			file_put_contents($log_file_path, $file_content, FILE_APPEND);
			unset($file_content);

	        //echo $resp;
	        // Close request to clear up some resources
	        curl_close($curl);
    	}
				

	}

	private function setAudienceRegionSummaryUrl(){
		switch (ENVIRONMENT) {
			case 'development':
				return 'http://s3remoterating.development/welcome/setChannelAudienceRegionSummary';
			break;
			case 'testing':

			break;
			case 'production':
				return 'http://sv-rating.dltv.ac.th/dltvrating/welcome/setChannelAudienceRegionSummary';
			break;
			
			default:
				return 'http://sv-rating.dltv.ac.th/dltvrating/welcome/setChannelAudienceRegionSummary';
			break;
		}
	}

	private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = '../../application/logs/cron';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }
	

}

$cron = new SetChannelAudienceRegionSummaryFOM();
$cron->setCronAudienceRegionSummaryFOM();
//$cron->setSubscriptionSchedule();
//$cron->checkTransactionSubscription();
//$cron->connClose();

?>