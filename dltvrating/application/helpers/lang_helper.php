<?php

if (!function_exists('__')) {

    /**
     * quick function name for translate the text from view
     *
     * @param string $text
     * @param string $namespace
     * @return string string after translate
     */
    function __($text, $namespace = null) {

        if (is_null($namespace)) {

            /**
             * get the name your self
             *
             * lazy markkkkkkkkkk
             */
            $e = new Exception();
            $function_stack = $e->getTrace();
            $last_function = array_shift($function_stack);

            /*
             * Example of $last_function
             * Array
              (
              [file] => C:\wamp\www\phase3.tripleasia.com\affiliate_system\httpdocs\app_frontend\views\_debug\footer.php
              [line] => 18
              [function] => __
              [args] => Array
              (
              [0] => Total
              )
              )
             */

            $view_path = str_replace('\\', '/', $last_function['file']);
            $view_file = preg_replace("/^(.)*views\//", "", $view_path);
            $view = preg_replace("/\.php$/", "", $view_file);
            $view = strtolower($view);
            $view = preg_replace("/([^a-z0-9\/_])/", "", $view);

            $namespace = $view;
        }

        $lang = Lang_controller::get_instance();
        return $lang->translate($namespace, $text);
    }
}