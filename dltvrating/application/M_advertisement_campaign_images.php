<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_advertisement_campaign_images extends DataMapper {

    //put your code here
    var $table = 'advertisement_campaign_images';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'advertisement_campaigns' => array(
             'class' => 'M_advertisement_campaigns',
               'other_field' => 'advertisement_campaign_images',
               'join_other_as' => 'advertisement_campaigns',
               'join_table' => 'advertisement_campaigns'
           )
   );
    
   //  var $has_many = array(
   //     'systemuser' => array(
   //         'class' => 'M_systemuser',
   //         'other_field' => 'branch',
   //         'join_self_as' => 'BranchCode',
   //         'join_other_as' => 'BranchCode',
   //         'join_table' => 'SystemUser')
   // );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}