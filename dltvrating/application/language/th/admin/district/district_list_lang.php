<?php

$lang['Districts'] = "Districts";
$lang['Create District'] = "Create District";
$lang['District Name'] = "District Name";
$lang['District Name (EN)'] = "District Name (EN)";
$lang['Amphur'] = "Amphur";
$lang['Latitude/Longitude'] = "Latitude/Longitude";
