<?php

$lang['Devices'] = "Devices";
$lang['Chip Code'] = "Chip Code";
$lang['IP Address'] = "IP Address";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Address'] = "Address";
$lang['Province Region'] = "Province Region";
$lang['Country Name'] = "Country Name";
$lang['Region Name'] = "Region Name";
$lang['Article Detail'] = "Article Detail";
$lang['SEARCH'] = "SEARCH";
$lang['Search'] = "Search";
$lang['-- Country Name --'] = "-- Country Name --";
$lang['-- Select Country Name --'] = "-- Select Country Name --";
$lang['Totals'] = "Totals";
$lang['devices'] = "devices";
$lang['Select Province Region'] = "Select Province Region";
$lang['-- Select Province Region --'] = "-- Select Province Region --";
$lang['Distance from center'] = "Distance from center";
$lang['Distance from center (KM)'] = "Distance from center (KM)";
$lang['Submit'] = "Submit";
$lang['-- Select Status--'] = "-- Select Status--";
