<?php

$lang['ADMINISTRATOR SIGN IN'] = "ADMINISTRATOR SIGN IN";
$lang['Email'] = "Email";
$lang['Password'] = "Password";
$lang['Stay Signed in'] = "Stay Signed in";
$lang['Forgot Password ?'] = "Forgot Password ?";
$lang['SIGN IN'] = "SIGN IN";
$lang['EMAIL'] = "EMAIL";
$lang['RESET'] = "RESET";
$lang['Back to Login'] = "Back to Login";
$lang['Access has been disable,Please try again.'] = "Access has been disable,Please try again.";
$lang['Email or password invalid'] = "Email or password invalid";
