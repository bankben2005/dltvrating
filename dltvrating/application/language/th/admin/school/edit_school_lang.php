<?php

$lang['Edit School'] = "Edit School";
$lang['School List'] = "School List";
$lang['Edit School Form'] = "Edit School Form";
$lang['School Name'] = "School Name";
$lang['School affiliates'] = "School affiliates";
$lang['Provinces'] = "Provinces";
$lang['Amphur'] = "Amphur";
$lang['District'] = "District";
$lang['Zipcode'] = "Zipcode";
$lang['Latitude'] = "Latitude";
$lang['Longitude'] = "Longitude";
$lang['Distance from center (KM)'] = "Distance from center (KM)";
$lang['How many hour does it take to the city'] = "How many hour does it take to the city";
$lang['Email'] = "Email";
$lang['Website'] = "Website";
$lang['Telephone'] = "Telephone";
$lang['Fax'] = "Fax";
