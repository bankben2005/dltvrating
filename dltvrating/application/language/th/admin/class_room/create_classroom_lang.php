<?php

$lang['Create Classroom'] = "Create Classroom";
$lang['Province List'] = "Province List";
$lang['Create Classroom Form'] = "Create Classroom Form";
$lang['Classroom'] = "Classroom";
$lang['School'] = "School";
$lang['Devices'] = "Devices";
$lang['Classroom List'] = "Classroom List";
$lang['Device'] = "Device";
