<?php

$lang['Create Advertisement Campaign'] = "Create Advertisement Campaign";
$lang['Channel Campaign List'] = "Channel Campaign List";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['StartTime - EndTime'] = "StartTime - EndTime";
$lang['Active'] = "Active";
$lang['Create advertisement campaign success'] = "Create advertisement campaign success";
$lang['User'] = "User";
$lang['Set Channels'] = "Set Channels";
$lang['Channel Name'] = "Channel Name";
$lang['Status'] = "Status";
$lang['Edit Advertisement Campaign'] = "Edit Advertisement Campaign";
$lang['Image Popup'] = "Image Popup";
$lang['pls_select_file'] = "pls_select_file";
$lang['Popup Image'] = "Popup Image";
$lang['Popup image url'] = "Popup image url";
$lang['Popup image redirect url'] = "Popup image redirect url";
$lang['confirm_delete_image'] = "confirm_delete_image";
$lang['Image Preview'] = "Image Preview";
$lang['Edit advertisement campaign success'] = "Edit advertisement campaign success";
