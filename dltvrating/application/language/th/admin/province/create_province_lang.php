<?php

$lang['Create Province'] = "Create Province";
$lang['Province List'] = "Province List";
$lang['Create Province Form'] = "Create Province Form";
$lang['Edit Province'] = "Edit Province";
$lang['Edit Province Form'] = "Edit Province Form";
$lang['Province Name'] = "Province Name";
$lang['Province Region'] = "Province Region";
$lang['IP Stack province name'] = "IP Stack province name";
$lang['Edit province success'] = "Edit province success";
$lang['Population'] = "Population";
$lang['Latitude'] = "Latitude";
$lang['Longitude'] = "Longitude";
