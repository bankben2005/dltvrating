<?php

$lang['Create Channel Category'] = "Create Channel Category";
$lang['Channel List'] = "Channel List";
$lang['Create Channel'] = "Create Channel";
$lang['Create Channel Form'] = "Create Channel Form";
$lang['Create Channel Category Form'] = "Create Channel Category Form";
$lang['Channel Category List'] = "Channel Category List";
$lang['Category Name'] = "Category Name";
$lang['Category Description'] = "Category Description";
$lang['Create channel category success'] = "Create channel category success";
$lang['Edit Channel Category'] = "Edit Channel Category";
$lang['Edit Channel Category Form'] = "Edit Channel Category Form";
