<?php

$lang['Channels'] = "Channels";
$lang['Create Channel'] = "Create Channel";
$lang['Program Name'] = "Program Name";
$lang['Date of Week'] = "Date of Week";
$lang['Duration time'] = "Duration time";
$lang['Create Channel Program'] = "Create Channel Program";
$lang['Create channel program'] = "Create channel program";
$lang['Description'] = "Description";
$lang['Start Time'] = "Start Time";
$lang['End Time'] = "End Time";
$lang['Sorting'] = "Sorting";
$lang['Time asc'] = "Time asc";
$lang['Time desc'] = "Time desc";
$lang['Action Choosed'] = "Action Choosed";
$lang['Select Action'] = "Select Action";
$lang['Delete'] = "Delete";
$lang['Result(s)'] = "Result(s)";
$lang['record'] = "record";
