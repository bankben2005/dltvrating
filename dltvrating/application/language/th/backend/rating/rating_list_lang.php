<?php

$lang['Ratings'] = "Ratings";
$lang['ShipCode'] = "ShipCode";
$lang['FRQ'] = "FRQ";
$lang['SYM'] = "SYM";
$lang['POL'] = "POL";
$lang['Server Id'] = "Server Id";
$lang['VDO PID'] = "VDO PID";
$lang['ADO PID'] = "ADO PID";
$lang['Duration (Seconds)'] = "Duration (Seconds)";
$lang['Start View'] = "Start View";
$lang['Created'] = "Created";
$lang['ChipCode'] = "ChipCode";
$lang['Service Id'] = "Service Id";
$lang['Channel'] = "Channel";
