<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class S3Module extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    private $current_rating_data_table;
    private $current_rating_data_daily_devices;
    private $current_youtube_view_logs_table;

    public function __construct(){
        parent::__construct();
        //echo 'aaa';exit;

        $this->setCurrentDataTable();
        $this->createMonthYearRatingDataTable();

        $this->current_youtube_view_logs_table = 'youtube_view_logs_'.date('Y').'_'.date('n');

        //print_r($this->current_youtube_view_logs_table);exit;

        // print_r($this->current_rating_data_table);
        // print_r($this->current_rating_data_daily_devices);

        
        //print_r($query);exit;


        //exit;
    }

	public function index(){
		// $this->load->view('welcome_message');
		//echo 'aaaa';exit;
		$userip = $this->getUserIP();
		if($this->input->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->input->post('request');

                // echo $request;exit;
                $ip_address = $this->getUserIP();
                //$ip_address = '14.207.41.209';

                /* make pretty string */
                $rating_string = $request;
                $rating_string = str_replace('START:', '', $rating_string);
                $rating_string = str_replace('END', '', $rating_string);

                /* remove bracket on first character and last character */
                $rating_string = substr($rating_string, 1);
                $rating_string = substr($rating_string, 0,-1);

                $exRatingString = explode(';', $rating_string);
                $arrRating = array();
                $arrRating = array_filter($exRatingString, function($value) { return $value !== ''; });

                $awesomeArray = array();
                $awesomeArray['records'] = array();
                $awesomeArray['ip_address'] = $ip_address;

                $ship_code = "";
                foreach ($arrRating as $key => $value) {
                    # code...
                    //$ship_code = "";
                    $data_value = "";
                    if($key == 0){
                        $exZeroKey = explode(',[', $value);
                        $ship_code = $exZeroKey[0];
                        $data_value = substr($exZeroKey[1], 0,-1);
                        // print_r($exZeroKey);
                        // print_r($data_value);
                        $awesomeArray['ship_code'] = $ship_code;
                    }else{
                        // print_r($value);
                        $sub_value = substr($value, 1);
                        $data_value = substr($sub_value, 0,-1);

                        //print_r($data_value);exit;
                    }
                    //$data_value = '['.$data_value.']';
                    $data_value = $this->getDataValue($data_value);
                    //print_r($data_value);
                    //echo $data_value."<br>";
                    //print_r(json_encode($data_value));
                    array_push($awesomeArray['records'], $data_value);
                }

                //print_r($awesomeArray);exit;

                /* check devices data and zipcode data */
                $devices_id = $this->checkCreateDeviceRecord($awesomeArray);
                /* eof check device data and zipcode data */


//                print_r($awesomeArray);exit;

                /* insert into rating data */
                foreach ($awesomeArray['records'] as $key => $value) {
                    # code...
                    $channels_id = $this->getChannelsId($value);

                    $channels_id = ($channels_id != '0')?$this->checkChennelExtendFrom($channels_id):$channels_id;
                    
                    $this->db->insert($this->current_rating_data_table,array(
                        'devices_id'=>$devices_id,
                        'channels_id'=>$channels_id,
                        'ship_code'=>$awesomeArray['ship_code'],
                        'frq'=>(int)$value['frq'],
                        'sym'=>(int)$value['sym'],
                        'pol'=>$value['pol'],
                        'server_id'=>(int)$value['server_id'],
                        'vdo_pid'=>(int)$value['vdo_pid'],
                        'ado_pid'=>(int)$value['ado_pid'],
                        'view_seconds'=>$value['view_seconds'],
                        'startview_datetime'=>$value['created'],
                        'created'=>date('Y-m-d H:i:s'),
                        'ip_address'=>$ip_address
                    ));


                    /* set rating data daily */
                    if($channels_id){
                            $this->setRatingDataDaily(array(
                                'date'=>date('Y-m-d',strtotime($value['created'])),
                                'devices_id'=>$devices_id,
                                'channels_id'=>$channels_id,
                                'view_seconds'=>$value['view_seconds'],
                                'ship_code'=>$awesomeArray['ship_code']
                            ));
                    }
                    /* eof set rating data daily */
                }
                

                //print_r($awesomeArray);exit;

                 //    $response = array(
                 //    	'status'=>true,
                 //    	'request'=>$request,
                 //    	'userip'=>$userip
                 //    );
                    
                 //    $log_file_path = $this->createLogFilePath('RequestS3Rating');
	                // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($response) . "\n";
	                // file_put_contents($log_file_path, $file_content, FILE_APPEND);
	                // unset($file_content);

                 //    echo json_encode($response);

        }else{
        			$response = array(
        				'status'=>true,
                    	'result_code'=>'error',
        				'result_desc'=>'Not found request',
        				'userip'=>$userip
        			);

        			$log_file_path = $this->createLogFilePath('RequestS3Rating');
	                $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($response) . "\n";
	                file_put_contents($log_file_path, $file_content, FILE_APPEND);
	                unset($file_content);

                    echo json_encode($response);

        }
	}

    public function createMonthYearRatingDataTable(){
        //echo 'aaaa';
        $query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$this->current_rating_data_table."'");

        if($query->num_rows() <= 0){
                /* create table for rating data */
                $strQuery = "CREATE TABLE ".$this->current_rating_data_table." (
                    id int IDENTITY(1,1) PRIMARY KEY,
                    devices_id int,
                    channels_id int,
                    ship_code varchar(255),
                    frq varchar(10),
                    sym varchar(10),
                    pol varchar(1),
                    server_id varchar(10),
                    vdo_pid varchar(10),
                    ado_pid varchar(10),
                    view_seconds int,
                    ip_address varchar(50),
                    startview_datetime datetime,
                    channel_ascii_code int,
                    created datetime
                )";

                $this->db->query($strQuery);

        }

        $queryDailyDevices = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$this->current_rating_data_daily_devices."'");

        if($queryDailyDevices->num_rows() <= 0){

            $strQuery = "CREATE TABLE ".$this->current_rating_data_daily_devices." (
                id int IDENTITY(1,1) PRIMARY KEY,
                rating_data_daily_id int,
                devices_id int,
                ship_code varchar(255),
                total_seconds int,
                created datetime,
                updated datetime
            )";
            $this->db->query($strQuery);
        }

    }

    public function UpdateDeviceAddress(){
        if($this->input->post(NULL,FALSE)){

                $devices = $this->checkCreateDeviceRecordByAPI(array(
                    'ship_code'=>$this->input->post('chip_code'),
                    'ip_address'=>$this->input->post('ip_address'),
                    'latitude'=>$this->input->post('latitude'),
                    'longitude'=>$this->input->post('longitude')
                ));

                echo json_encode(array(
                    'status'=>true
                ));

        }
    }

    public function SetYoutubeViewLogs(){
        $get_data = $this->input->get();
        $this->checkCreateYoutubeViewLogsTable();


        $devices_data = $this->getDevicesDataByChipCode([
            'chipcode'=>$get_data['chipcode']
        ]);

        if(!$devices_data){
            echo json_encode(array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ chipcode ดังกล่าว'
            ));
            exit;
        }
        /* insert data into view logs by month and years */
        $query = $this->db->insert($this->current_youtube_view_logs_table,[
            'members_id'=>$get_data['members_id'],
            'login_type'=>$get_data['login_type'],
            'devices_id'=>@$devices_data->id,
            'chipcode'=>@$devices_data->ship_code,
            'video_id'=>$get_data['video_id'],
            'created'=>date('Y-m-d H:i:s')
        ]);
        /* eof insert data into view logs by month and years*/

        if($query){
            /* check and insert data into youtube view daily devices */
            $queryCheck = $this->db->select('*')
            ->from('youtube_view_daily_devices')
            ->where('date',date('Y-m-d'))
            ->where('devices_id',@$devices_data->id)
            ->get();

            if($queryCheck->num_rows() > 0){
                /* only update times and updated */
                $rowCheck = $queryCheck->row();

                $this->db->update('youtube_view_daily_devices',[
                    'times'=>$rowCheck->times+1,
                    'updated'=>date('Y-m-d H:i:s')
                ],['id'=>$rowCheck->id]);

            }else{
                /* insert new record */
                $this->db->insert('youtube_view_daily_devices',[
                    'date'=>date('Y-m-d'),
                    'devices_id'=>@$devices_data->id,
                    'times'=>1,
                    'created'=>date('Y-m-d H:i:s'),
                    'updated'=>date('Y-m-d H:i:s')
                ]);

            }

            /* eof check */

        }

        echo json_encode(array(
            'status'=>true,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ));
        //print_r($get_data);exit;


    }


	private function createLogFilePath($filename = '') {
        $log_path = './application/logs/requestrating';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function getUserIP(){
                       $client  = @$_SERVER['HTTP_CLIENT_IP'];
                            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                            $remote  = $_SERVER['REMOTE_ADDR'];

                            if(filter_var($client, FILTER_VALIDATE_IP))
                            {
                                $ip = $client;
                            }
                            elseif(filter_var($forward, FILTER_VALIDATE_IP))
                            {
                                $ip = $forward;
                            }
                            else
                            {
                                $ip = $remote;
                            }

                            return $ip;
    }

    private function getDataValue($data_value){
        $exDatavalue = explode(',',$data_value);
        return array(
            'frq'=>$exDatavalue[0],
            'sym'=>$exDatavalue[1],
            'pol'=>$exDatavalue[2],
            'server_id'=>$exDatavalue[3],
            'vdo_pid'=>$exDatavalue[4],
            'ado_pid'=>$exDatavalue[5],
            'view_seconds'=>$exDatavalue[7],
            'created'=>$this->getRatingDateFormat($exDatavalue[6])
        );

        //print_r($exDatavalue);exit;
    }

    private function getRatingDateFormat($date_format){
        // $date_format = str_replace('\', '', $date_format);
        $date_format = stripslashes($date_format);
        $date_format = str_replace('-', ' ', $date_format);
        //print_r($date_format);exit;
        $datetime_return = new DateTime($date_format);
        // print_r($datetime_return);exit;
        return $datetime_return->format('Y-m-d H:i:s');
    }

    private function checkCreateDeviceRecord($arrData){
        
        $queryCheck = $this->db->select('id,ship_code')
        ->from('devices')
        ->where('ship_code',$arrData['ship_code'])
        ->get();
        //echo $this->db->last_query();
        //print_r($queryCheck->row());
        if($queryCheck->num_rows() <= 0){
            /* create new ship code and fill the devices address table */

            $this->db->insert('devices',array(
                'ship_code'=>$arrData['ship_code'],
                'ip_address'=>$arrData['ip_address'],
                'created'=>date('Y-m-d H:i:s')
            ));


            $insert_id = $this->db->insert_id();

            if($insert_id){
                /* insert address for this device */
                $address = $this->getAddressByIpAddress(array(
                    'ip_address'=>$arrData['ip_address']
                ));
                $this->db->insert('device_addresses',array(
                    'devices_id'=>$insert_id,
                    'continent_code'=>$address->continent_code,
                    'continent_name'=>$address->continent_name,
                    'country_code'=>$address->country_code,
                    'country_name'=>$address->country_name,
                    'region_code'=>$address->region_code,
                    'region_name'=>$address->region_name,
                    'city'=>$address->city,
                    'zip'=>$address->zip,
                    'latitude'=>$address->latitude,
                    'longitude'=>$address->longitude,
                    'location'=>json_encode($address->location),
                    'created'=>date('Y-m-d H:i:s')
                ));
            }
            //echo $insert_id;
            return $insert_id;
            
            //print_r($address);
        }else{
            return $queryCheck->row()->id;
        }

    }
    private function checkCreateDeviceRecordByAPI($arrData){
        
        $queryCheck = $this->db->select('id,ship_code')
        ->from('devices')
        ->where('ship_code',$arrData['ship_code'])
        ->get();
        //echo $this->db->last_query();
        //print_r($queryCheck->row());
        if($queryCheck->num_rows() <= 0){
            /* create new ship code and fill the devices address table */

            $this->db->insert('devices',array(
                'ship_code'=>$arrData['ship_code'],
                'ip_address'=>$arrData['ip_address'],
                'created'=>date('Y-m-d H:i:s')
            ));
            //exit;

            $insert_id = $this->db->insert_id();


            if($insert_id){
                /* insert address for this device */
                $address = $this->getAddressByIpAddress(array(
                    'ip_address'=>$arrData['ip_address']
                ));

                //$region_data = $this->filterRegionNameAndGetProvinceRegion($address->region_name);

                // $province_region_name = $this->getRegionNameByZipcode(array(
                //     'zipcode'=>$address->zip
                // ));
                $province_region_name = $this->getProvinceRegionNameByRegionNameAndAPIConfig([
                    'region_name'=>$address->region_name
                ]);

                $this->db->insert('device_addresses',array(
                    'devices_id'=>$insert_id,
                    'continent_code'=>$address->continent_code,
                    'continent_name'=>$address->continent_name,
                    'country_code'=>$address->country_code,
                    'country_name'=>$address->country_name,
                    'region_code'=>$address->region_code,
                    'region_name'=>$address->region_name,
                    'city'=>$address->city,
                    'zip'=>$address->zip,
                    'latitude'=>$arrData['latitude'],
                    'longitude'=>$arrData['longitude'],
                    'location'=>json_encode($address->location),
                    'province_region'=>$province_region_name,
                    'created'=>date('Y-m-d H:i:s'),
                    'update_from_api'=>1
                ));
            }
            //echo $insert_id;
            return $insert_id;
            
            //print_r($address);
        }else{
            //return $queryCheck->row()->id;

            $rowQueryCheck = $queryCheck->row();


            $device_addresses_data = new StdClass();

            $queryDeviceAddresses =$this->db->select('id,devices_id,latitude,longitude')
            ->from('device_addresses')
            ->where('devices_id',$rowQueryCheck->id)
            ->get();

            if($queryDeviceAddresses->num_rows() > 0){
                $device_addresses_data = $queryDeviceAddresses->row();
            }

            /* update device and device addresses */
            $devices_id = $queryCheck->row()->id;

            $this->db->update('devices',array(
                'ip_address'=>$arrData['ip_address'],
                'updated'=>date('Y-m-d H:i:s')
            ),array('id'=>$devices_id));


            $address = $this->getAddressByIpAddress(array(
                    'ip_address'=>$arrData['ip_address']
            ));

            // $region_data = $this->filterRegionNameAndGetProvinceRegion($address->region_name);

            // $province_region_name = $this->getRegionNameByZipcode(array(
            //         'zipcode'=>$address->zip
            // ));

            $province_region_name = $this->getProvinceRegionNameByRegionNameAndAPIConfig([
                    'region_name'=>$address->region_name
            ]);

            $this->db->update('device_addresses',array(
                    'continent_code'=>$address->continent_code,
                    'continent_name'=>$address->continent_name,
                    'country_code'=>$address->country_code,
                    'country_name'=>$address->country_name,
                    'region_code'=>$address->region_code,
                    'region_name'=>$address->region_name,
                    'city'=>$address->city,
                    'zip'=>$address->zip,
                    'latitude'=>($arrData['latitude'] != '0.000000')?$arrData['latitude']:@$device_addresses_data->latitude,
                    'longitude'=>($arrData['longitude'] != '0.000000')?$arrData['longitude']:@$device_addresses_data->longitude,
                    'location'=>json_encode($address->location),
                    'province_region'=>$province_region_name,
                    'updated'=>date('Y-m-d H:i:s'),
                    'update_from_api'=>1
            ),array('devices_id'=>$devices_id));

            /* eof update device*/
        }
        return true;

    }

    private function getAddressByIpAddress($arrData){
        $this->load->config('api');
        $ipstack_config = $this->config->item('ipstack');
        $api_key = $ipstack_config['api_key'];


        /* $queryApiKey */
        $queryApiKey = $this->db->select('*')
        ->from('ipstack_api')
        ->where('over_limit_status',0)
        ->order_by('NEWID()')
        ->limit(1)
        ->get();

        $api_key = $queryApiKey->row()->api_key;

        $json_address = file_get_contents('http://api.ipstack.com/'.$arrData['ip_address'].'?access_key='.$api_key.'&format=1');

        
        return json_decode($json_address);
    }

    private function getChannelsId($arrData){
        $frq = (int)$arrData['frq'];
        $sym = (int)$arrData['sym'];
        $vdo_pid = (int)$arrData['vdo_pid'];
        $ado_pid = (int)$arrData['ado_pid'];

        //echo $frq."<br>".$sym."<br>".$vdo_pid."<br>".$ado_pid;exit;

        // $query = $this->db->select('id,frq,sym,vdo_pid,ado_pid,service_id')
        // ->from('channels')
        // ->where('frq',$frq)
        // ->where('sym',$sym)
        // ->where('vdo_pid',$vdo_pid)
        // ->where('ado_pid',$ado_pid)->get();

        $query = $this->db->select('id,channels_id,frq,sym,pol,vdo_pid,ado_pid,service_id')
        ->from('channel_components')
        ->where('frq',$frq)
        ->where('sym',$sym)
        ->where('vdo_pid',$vdo_pid)
        ->where('ado_pid',$ado_pid)
        ->get();


        //echo $this->db->last_query();exit;

        if($query->num_rows() > 0){
            $row = $query->row();
            if(!$row->service_id){

                /* update service id into channel_components table */
                $this->db->update('channel_components',array(
                    'service_id'=>(int)$arrData['server_id'],
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$row->id));
                /* eof update service id into channel_components table */

                $this->db->update('channels',array(
                    'service_id'=>(int)$arrData['server_id'],
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$row->channels_id));
            }

            return $row->channels_id;
        }else{

            $queryCheckUpdateLogs = $this->db->select('*')
            ->from('channel_components_update_logs')
            ->where('frq',$frq)
            ->where('sym',$sym)
            ->where('vdo_pid',$vdo_pid)
            ->where('ado_pid',$ado_pid)
            ->get();
            if($queryCheckUpdateLogs->num_rows() > 0){
                $row = $queryCheckUpdateLogs->row();

                if(!$row->service_id){

                    /* update service id into channel_components table */
                    $this->db->update('channel_components',array(
                        'service_id'=>(int)$arrData['server_id'],
                        'updated'=>date('Y-m-d H:i:s')
                    ),array('id'=>$row->channel_components_id));
                    /* eof update service id into channel_components table */

                    $this->db->update('channels',array(
                        'service_id'=>(int)$arrData['server_id'],
                        'updated'=>date('Y-m-d H:i:s')
                    ),array('id'=>$row->channels_id));
                }

                return $row->channels_id;

            }else{
                return 0;
            }
            
        }

    }


    private function setRatingDataDaily($data = array()){

        /* check already record by date */
        $check_record = $this->db->select('*')
        ->from('rating_data_daily')
        ->where('date',$data['date'])
        ->where('channels_id',$data['channels_id'])
        ->get();

        if($check_record->num_rows() <= 0){
            /* insert new record into rating_data_daily */
            $this->db->insert('rating_data_daily',array(
                'date'=>$data['date'],
                'channels_id'=>$data['channels_id'],
                'sum_seconds'=>$data['view_seconds'],
                'reach_devices'=>1,
                'created'=>date('Y-m-d H:i:s')
            ));
            /* eof insert new record*/

            $insert_id = $this->db->insert_id();
            /* insert into rating data daily devices */
            $this->db->insert($this->current_rating_data_daily_devices,array(
                'rating_data_daily_id'=>$insert_id,
                'devices_id'=>$data['devices_id'],
                'ship_code'=>$data['ship_code'],
                'total_seconds'=>$data['view_seconds'],
                'created'=>date('Y-m-d H:i:s')
            ));
            /* eof insert into rating data daily devices */
        }else{
            /* if exist record update into record sum view seconds and check device id */

            $row_data_daily = $check_record->row();

            /* check and insert device */
            $queryCheckDevice = $this->db->select('id','devices_id','rating_data_daily_id')
            ->from($this->current_rating_data_daily_devices)
            ->where('devices_id',$data['devices_id'])
            ->where('rating_data_daily_id',$row_data_daily->id)
            ->get();

            if($queryCheckDevice->num_rows() <= 0){
                /* insert new and update reach devices for rating data daily */
                $this->db->insert($this->current_rating_data_daily_devices,array(
                    'rating_data_daily_id'=>$row_data_daily->id,
                    'devices_id'=>$data['devices_id'],
                    'ship_code'=>$data['ship_code'],
                    'total_seconds'=>$data['view_seconds'],
                    'created'=>date('Y-m-d H:i:s'),
                    'updated'=>date('Y-m-d H:i:s')
                ));


                /* update rating data daily */
                $this->db->update('rating_data_daily',array(
                    'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
                    'reach_devices'=>($row_data_daily->reach_devices + 1),
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$row_data_daily->id));
                /* eof update rating data*/
            }else{

                /* update rating data daily device update */
                $this->db->update($this->current_rating_data_daily_devices,array(
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$queryCheckDevice->row()->id));


                /* only sum  view seconds */
                $this->db->update('rating_data_daily',array(
                    'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$row_data_daily->id));

            }




        }


        /* eof check already record by date*/
    }

    public function testFilterRegionName(){
        $region_data = $this->filterRegionNameAndGetProvinceRegion('Bangkok');
        print_r($region_data);exit;
    }

    public function testRegionNameByZipcode(){
        $zipcode = '86130';

        $province_region = $this->getRegionNameByZipcode(array(
            'zipcode'=>$zipcode
        ));

        print_r($province_region);exit;
    }
    private function filterRegionNameAndGetProvinceRegion($region_name){

        $return_region = $region_name;
        //echo $return_region;
        if (strpos($return_region, 'Changwat ') !== false) {
                //echo 'true';
                $return_region = str_replace('Changwat ', '', $return_region);
        }

        if(strpos($return_region, ' Province') !== false){
                $return_region = str_replace(' Province', '', $return_region);
        }

        $return_region = str_replace(' ', '', $return_region);

        $return_region = strtolower($return_region);

        $return_region = ucfirst($return_region);

        //echo $return_region;exit;
        /* check to province to get province region */

        $query = $this->db->select('*')
        ->from('provinces')
        ->where('province_en_name',$return_region)
        ->get();

        if($query->num_rows() > 0){
            return array(
                'region_name'=>$return_region,
                'province_region'=>$query->row()->province_region
            );
        }else{
            return array(
                'region_name'=>$return_region,
                'province_region'=>''
            );
        }
    }


    private function setCurrentDataTable(){
        $this->current_rating_data_table = 'rating_data_'.date('Y').'_'.date('n');
        $this->current_rating_data_daily_devices = 'rating_data_daily_devices_'.date('Y').'_'.date('n');
    }

    private function getRegionNameByZipcode($data = array()){
        $zipcode = $data['zipcode'];

        $query = $this->db->select('zipcode.provinces_id,zipcode.zipcode,provinces.id,provinces.province_region')
        ->from('zipcode')
        ->join('provinces','provinces.id = zipcode.provinces_id')
        ->where('zipcode.zipcode',$zipcode)
        ->get();

        if($query->num_rows() > 0){
            return $query->row()->province_region;
        }else{
            return '';
        }

    }

    private function checkChennelExtendFrom($channels_id){

        $query = $this->db->select('id,channel_extended_status,channel_extended_from_channels_id')
        ->from('channels')
        ->where('id',$channels_id)
        ->where('channel_extended_status',1)
        ->where('channel_extended_from_channels_id !=',0)
        ->get();
        if($query->num_rows() > 0){
            return $query->row()->channel_extended_from_channels_id;
        }else{
            return $channels_id;
        }
    }

    private function checkCreateYoutubeViewLogsTable(){
        $query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$this->current_youtube_view_logs_table."'");

        //print_r($this->current_youtbe_views_data_table);exit;

        if($query->num_rows() <= 0){
                /* create table for rating data */
                $strQuery = "CREATE TABLE ".$this->current_youtube_view_logs_table." (
                    id int IDENTITY(1,1) PRIMARY KEY,
                    members_id int,
                    login_type varchar(50),
                    devices_id int,
                    chipcode varchar(50),
                    video_id varchar(50),
                    created datetime
                )";

                $this->db->query($strQuery);

        }
    }

    private function getDevicesDataByChipCode($data = []){

        $query = $this->db->select('*')
        ->from('devices')
        ->where('ship_code',$data['chipcode'])
        ->get();

        return $query->row();


    }

    private function getProvinceRegionNameByRegionNameAndAPIConfig($data = []){

        $province_region = "";
        $this->load->config('api');
        $arr_province_region = $this->config->item('get_province_region_by_region_name');

        if($data['region_name']){
            $key = array_search($data['region_name'], array_column($arr_province_region, 'region_name'));

            if(is_numeric($key)){
                    $province_region = $arr_province_region[$key]['province_region'];
            }
        }

        return $province_region;
    }



}
