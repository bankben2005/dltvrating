<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $create_json_file_url = "";
	private $s3_application_conn; 
	private $conn_mysql;
	public function __construct(){
		parent::__construct();

		
		$this->setCreateJsonFileUrl();
		//echo $this->create_json_file_url;
	}
	public function index()
	{
		// task schedule: for testing ( school type summary )
		//$this->setChannelAudienceSchoolTypeSummary();
		// task schedule: for testing ( school class summary )
		//$this->setChannelAudienceSchoolClassSummary();
		


		//task -> device active 
		$this->setOnlineRegionScheduleNewVersion();
	
		$this->load->view('welcome_message');


     
	}

	public function test(){
			
		set_time_limit(0);

		require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
		$requestCriteria = new StdClass();
		$requestCriteria->schedule_task = "true";
		// "user_id":17,
		// "month":1,
		// "year":2021,
		// "channels_type" : "S",
		// "school_id" : "1050130091",
		// "class_id" : 3
		$requestCriteria = new \Stdclass;
		$requestCriteria->user_id = 17;
		$requestCriteria->month ='1';
		$requestCriteria->year ='2021';
		$requestCriteria->channels_type = 'S';
		$requestCriteria->school_id = '1050130091';
		$requestCriteria->class_id  = '3';

		$S3RatingLibrary = new S3RatingLibrary();
		$output = $S3RatingLibrary->ExportDLTVTopView($requestCriteria);
		print_r($output);exit();

	}

	public function setDailyAdvertisementJsonFile(){

		/* check campaign which duration in today */
		$query_check = $this->db->select('*')
		->from('advertisement_campaigns')
		->where('end_datetime >=',date('Y-m-d H:i:s'))
		->where('start_datetime <=',date('Y-m-d H:i:s'))
		->where('active',1)
		->get();

		//echo $query_check->num_rows();exit;

		/* */
		$dataJsonFile = new StdClass();
		$dataJsonFile->ads_start_time = date('d-m-Y').' 00:00';
		$dataJsonFile->ads_end_time = date('d-m-Y').' 23:59';
		$dataJsonFile->ads_list = array();

	// 	"ads_start_time": "09-10-2018 00:00",
	// "ads_end_time": "20-10-2018 23:59",

		if($query_check->num_rows() > 0){
			$arr_adslist = array();
			foreach ($query_check->result() as $key => $value) {
				# code...
				/* get channel data from channel id */
				$channel_data = $this->getChannelDataFromChannelId($value->channels_id);
				//print_r($channel_data);

				/* if channel data has two type cband and also kuband */
				$channel_components = $this->getChannelComponentsByChannelId($value->channels_id);

				if($channel_components){
					foreach ($channel_components->result() as $k => $v) {
							# code...
						$data_push = array(
							"frequency"=>$v->frq,
							"service_id"=>($v->service_id)?$v->service_id:"0",
							"video_pid"=>$v->vdo_pid,
							"audio_pid"=>$v->ado_pid,
							'ad_imgs'=>$this->getAdsImageByAdvertisementCampaign(array(
								'advertisement_campaigns_id'=>$value->id
							)),
							'ad_videos'=>$this->getAdsVideoByAdvertisementCampaign(array(
								'advertisement_campaigns_id'=>$value->id
							))

						);

						array_push($dataJsonFile->ads_list, $data_push);


					}
				}
				/* eof check two band type */
				
				

			}

		}

		$data_json_txt = json_encode($dataJsonFile);

		//echo $data_json_txt;exit;

		//$get_content = file_get_contents($this->create_json_file_url.'?json_txt='.$data_json_txt);

		//echo $get_content;
		//echo json_encode($dataJsonFile);
		$fields = array(
			'json_txt' => $data_json_txt
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->create_json_file_url);
                //curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl_authentication_header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		if($response){
			echo $response;
               //  		$log_file_path = $this->createLogFilePath('Notification Cron');

			            // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($response) . "\n";
			            // file_put_contents($log_file_path, $file_content, FILE_APPEND);
			            // unset($file_content);
		}



	}

	// public function setManualAllChannelHasAdvertisement(){


	// 	$dataJsonFile = new StdClass();
	// 	$dataJsonFile->ads_start_time = date('d-m-Y').' 00:00';
	// 	$dataJsonFile->ads_end_time = date('d-m-Y').' 23:59';
	// 	$dataJsonFile->ads_list = array();


	// 	//$arr_data = array();

	// 	$query = $this->db->select('*')
	// 	->from('channel_components')
	// 	->where('active',1)
	// 	->get();

	// 	foreach ($query->result() as $key => $value) {
	// 		# code...
	// 						$data_push = array(
	// 							"frequency"=>$value->frq,
	// 							"service_id"=>($value->service_id)?$value->service_id:"0",
	// 							"video_pid"=>$value->vdo_pid,
	// 							"audio_pid"=>$value->ado_pid,
	// 							'ad_imgs'=>array(
	// 								array(
	// 									"start_date"=> date('d-m-Y')." 00:00",
	// 									"end_date"=> date('d-m-Y')." 23:59",
	// 									"image_url"=> "http://sv-rating.dltv.ac.th/rating/uploaded/advertisement/campaign_images/23/43970fced02903d68aef6c4e99b1af71.jpg",
	// 									"position"=> "19"
	// 								),

	// 								array(
	// 									"start_date"=> date('d-m-Y')." 00:00",
	// 									"end_date"=> date('d-m-Y')." 23:59",
	// 									"image_url"=> "http://sv-rating.dltv.ac.th/rating/uploaded/advertisement/campaign_images/22/8820188e71be142dfdae0d5e1a7c43a3.jpg",
	// 									"position"=> "20"
	// 								)

	// 							),
	// 							'ad_videos'=>array(

	// 							)

	// 						);

	// 						array_push($dataJsonFile->ads_list, $data_push);

	// 	}

	// 	$data_json_txt = json_encode($dataJsonFile);


	// 			$fields = array(
	// 				'json_txt' => $data_json_txt
	// 			);

	// 			$ch = curl_init();
 //                curl_setopt($ch, CURLOPT_URL, $this->create_json_file_url);
 //                //curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl_authentication_header);
 //                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
 //                curl_setopt($ch, CURLOPT_HEADER, FALSE);
 //                curl_setopt($ch, CURLOPT_POST, TRUE);
 //                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
 //                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

 //                $response = curl_exec($ch);
 //                curl_close($ch);

 //                if($response){
 //                	echo $response;

 //                }
	// }

	public function runMapChannelRating(){
		$query = $this->db->select('*')
		->from('rating_data')
		->where('channels_id','0')
		->limit(10000)
		->get();

		if($query->num_rows() > 0){
			foreach($query->result() as $key => $row){
				$channels_id = $this->getChannelMapChannelIDByRatingData(array(
					'rating_data'=>$row
				));

				$this->db->update('rating_data',array(
					'channels_id'=>$channels_id
				),array('id'=>$row->id));


			}
		}
	}

	public function setDailyChannelRating(){
		$query = $this->db->select('id')
		->from('channels')
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				//print_r($value);
				/* get rating data daily  */
				// $rating = $this->getChannelRatingByChannelId($value->id);
				// $data_rating = $this->getChannelRatingByChannelId($value->id);
				$data_rating = $this->getChannelRatingByChannelIdNewVersion($value->id);

				$rating = $data_rating['rating'];
				$reach_devices = $data_rating['reach_devices'];

				//print_r($data_rating);
				//echo $rating;exit;
				/* insert into channel daily rating */
				$this->updateChannelDailyRating(array(
					'channels_id'=>$value->id,
					'rating'=>$rating,
					'reach_devices'=>$reach_devices
				));
				/* eof insert into channel daily rating */
			}
		}

		echo json_encode(array(
			'status'=>true
		));
	}


	public function reCheckDeviceProvinceRegionByZipcode(){
		set_time_limit(0);

		$queryString = "select * from device_addresses";
		$queryString .= " where (province_region is null or province_region = '')";
		$queryString .= " and zip is not null";
		$queryString .= " and country_name = 'Thailand'";

		

		$queryDeviceAddress = $this->db->query($queryString);
		//echo $this->db->last_query();exit;

		// print_r($queryDeviceAddress->result());exit;

		

		if($queryDeviceAddress->num_rows() > 0){

			// print_r($queryDeviceAddress->result());exit;

			foreach ($queryDeviceAddress->result() as $key => $value) {
				# code...
				//print_r($value->id);
				$queryProvince = $this->db->select('zipcode.zipcode,zipcode.provinces_id,provinces.id,provinces.province_region')
				->from('zipcode')
				->join('provinces','zipcode.provinces_id = provinces.id')
				->where('zipcode.zipcode',$value->zip)
				->limit(1)
				->get();

				if($queryProvince->num_rows() > 0){

					$this->db->update('device_addresses',array(
						'province_region'=>$queryProvince->row()->province_region,
						'updated'=>date('Y-m-d H:i:s')
					),array('id'=>$value->id));

					
				}

			}
		}



		$api_config = $this->load->config('api');
		$no_has_zip_and_has_region_name_case = $this->config->item('no_has_zip_and_has_region_name_case');

		/* check case second */
		$query_second = "select * from device_addresses";
		$query_second .= " where (province_region is null or province_region = '')";
		$query_second .= " and zip is null";
		$query_second .= " and country_name = 'Thailand'";
		$query_second .= " and region_name is not null";
		$query_second .= " and region_name != ''";

		$querySecond = $this->db->query($query_second);

		//print_r($querySecond->result());exit;

		if($querySecond->num_rows() > 0){
			foreach ($querySecond->result() as $key => $value) {
				# code...
				foreach ($no_has_zip_and_has_region_name_case as $k => $v) {
					# code...
					if($value->region_name == $v['region_name']){
						/* update province region */
						$this->db->update('device_addresses',[
							'province_region'=>$v['province_region']
						],['id'=>$value->id]);
						/* end of update province region*/

					}
				}

			}

		}

		


		/* query third  check province region by latitude longitude */
		// $query_third = "select * from device_addresses";
		// $query_third .= " where (province_region is null or province_region = '') and (longitude != '0.000000' and longitude != '0') and country_name = 'Thailand'";

		// $queryThird = $this->db->query($query_third);


		// if($queryThird->num_rows() > 0){
		// 	foreach ($queryThird->result() as $key => $value) {


		// 		$curl = curl_init();
		//         curl_setopt_array($curl, array(
		//             CURLOPT_RETURNTRANSFER => 1,
		//             CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAOQvBZ1CAR1PaAJc4LmfTK2K5Zvdgd4xI&latlng='.$value->latitude.','.$value->longitude.'&sensor=false',
		//             CURLOPT_TIMEOUT=>10000
		//         ));
		//         $resp = curl_exec($curl);

		//         curl_close($curl);

		//         $response = json_decode($resp);

		//         $zipcode = $response->results[2]->address_components[0]->long_name;

		//         $address_info = $this->getAnotherDeviceAddressesInfoByZipcode([
		//         	'zipcode'=>$zipcode
		//         ]);


		//         if($address_info){
		//         	$this->db->update('device_addresses',[
		//         		'region_name'=>$address_info->region_name,
		//         		'city'=>$address_info->city,
		//         		'zip'=>$address_info->zip,
		//         		'province_region'=>$address_info->province_region,
		//         		'updated'=>date('Y-m-d H:i:s')
		//         	],['id'=>$value->id]);	

		//         }


		// 	}


		// }


		/* eof check province region by latitude longitude */





		echo json_encode(array(
			'status'=>true
		));

		
	}

	public function checkKalasin(){
		set_time_limit(0);
		$device_addresses = $this->db->select('*,device_addresses.id as addresses_id')
		->from('device_addresses')
		->join('devices','device_addresses.devices_id = devices.id')
		->where('device_addresses.region_name','Changwat Kalasin')
		->where('device_addresses.zip',NULL)
		->where('device_addresses.country_name','Thailand')
		->not_like('devices.ip_address','192.168')
		->get();

		if($device_addresses->num_rows() > 0){
			

			foreach ($device_addresses->result() as $key => $row) {
					# code...
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => 'http://api.ipstack.com/'.$row->ip_address.'?access_key='.$this->getRandomIpStackApiKey(),
					CURLOPT_TIMEOUT=>10000
				));
				$resp = curl_exec($curl);

				curl_close($curl);

				$response = json_decode($resp);

				        //print_r($response);exit;

				if($response->country_name == 'Thailand' && $response->region_name != '' && $response->zip != ''){
					/* update region_name city zipcode and province region*/
					$this->db->update('device_addresses',[
						'region_name'=>$response->region_name,
						'city'=>$response->city,
						'zip'=>$response->zip,
						'province_region'=>$this->getProvinceRegionByZipCode([
							'zipcode'=>$response->zip
						]),
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$row->id]);


				}else{
					/* update province_region null*/
					$this->db->update('device_addresses',[
						'province_region'=>NULL,
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$row->id]);
				}
			}







		}
	}

	public function CheckDeviceAddressesCaseSensitive(){
		$this->load->config('api');
		$case_sensitive = $this->config->item('device_address_case_sensitive');

		foreach ($case_sensitive as $key => $value) {
			# code...
			$queryCheck = $this->db->select('id,region_name,city,zip,province_region')
			->from('device_addresses')
			->where('region_name',$value['region_name'])
			->where('city',$value['city'])
			->where('zip',$value['zip'])
			->where('province_region !=',$value['change_to'])
			->get();
			if($queryCheck->num_rows() > 0){
				$this->db->update('device_addresses',[
					'province_region'=>$value['change_to'],
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$queryCheck->row()->id]);

			}
		}


		/* case sensitive change province region */
		$change_province_region_case = $this->config->item('change_province_region_case_sensitive');

		foreach ($change_province_region_case as $key => $value) {
			# code...
			$queryCheck = $this->db->select('id,region_name')
			->from('device_addresses')
			->where('region_name',$value['region_name'])
			->get();
			if($queryCheck->num_rows() > 0){
				$this->db->update('device_addresses',[
					'region_name'=>$value['change_to'],
					'updated'=>date('Y-m-d H:i:s')
				],['region_name'=>$value['region_name']]);
			}
		}
		/* eof case sensitive change province region */


		/* case wrong provice region */
		$wrong_province_region = $this->config->item('change_province_region_wrong_case');

		foreach ($wrong_province_region as $key => $value) {
			# code...
			$queryCheck = $this->db->select('id,region_name,province_region')
			->from('device_addresses')
			->where('region_name',$value['region_name'])
			->where('province_region',$value['case_found'])
			->get();

			if($queryCheck->num_rows() > 0){
				$rowFound = $queryCheck->row();
				$this->db->update('device_addresses',[
					'province_region'=>$value['change_to'],
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$rowFound->id]);
			}
		}
		/* eof wrong province region */



		echo json_encode([
			'status'=>true
		]);
	}

	public function setOnlineRegionSchedule(){
		set_time_limit(0);

		require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
		$requestCriteria = new StdClass();
		$requestCriteria->schedule_task = "true";

		$S3RatingLibrary = new S3RatingLibrary();
		$output = $S3RatingLibrary->getAllDeviceOnlineEachRegion($requestCriteria);

		//print_r($output);


		/* get sum all devices */
		$sum_devices = 0;
		foreach ($output['ReturnData'] as $key => $value) {
			# code...
			$sum_devices += (int)$value['sum_all_devices'];
		}
		/* eof sum all devices */



		$json_each_region = json_encode($output);

		/* check insert or update into online_each_region table */
		$check_online_each_region = $this->db->select('id')
		->from('online_each_region')
		->where('created !=','')
		->where('type','satellite')
		->get();
		if($check_online_each_region->num_rows() <= 0){
			/* insert new record */
			$this->db->insert('online_each_region',[
				'response'=>$json_each_region,
				'type'=>'satellite',
				'sum_devices'=>$sum_devices,
				'created'=>date('Y-m-d H:i:s')
			]);
			/* eof insert new record*/
		}else{
			/* only update current record */
			$this->db->update('online_each_region',[
				'response'=>$json_each_region,
				'sum_devices'=>$sum_devices,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$check_online_each_region->row()->id,'type'=>'satellite']);
		}
		/* */



		/* set json data each region by name */
		if(count($output['ReturnData']) > 0){
			foreach ($output['ReturnData'] as $key => $value) {
					# code...
					// print_r($value['province_region']);
				$Criteria = new StdClass();
				$Criteria->schedule_task = "true";
				$Criteria->province_region = $value['province_region'];
				$output_of_region = $S3RatingLibrary->getAllDeviceOnlineEachProvinceByRegionName($Criteria);

				/* check and insert or update into */
				$checkByRegionName = $this->db->select('id,province_region,created')
				->from('online_each_region_by_name')
				->where('province_region',$value['province_region'])
				->where('type','satellite')
				->get();

				if($checkByRegionName->num_rows() <= 0){
					/* insert new record into online_each_region_by_name */
					$this->db->insert('online_each_region_by_name',[
						'response'=>json_encode($output_of_region),
						'province_region'=>$value['province_region'],
						'type'=>'satellite',
						'created'=>date('Y-m-d H:i:s')
					]);
				}else{
					/* update response*/
					$this->db->update('online_each_region_by_name',[
						'response'=>json_encode($output_of_region),
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$checkByRegionName->row()->id,'type'=>'satellite']);
				}
				/* eof check */
			}

		}
		/* eof get json data each region by name*/

		// print_r($output);

		$this->setYotubeOnlineRegionSchedule();


	}
	//Q:1
	// SELECT r1.*,provinces.province_code as dis_provcode FROM (
	// 	select  schools.SchoolID,schools.SchoolName,schools.SchoolTypeID
	// 	,provinces.province_name,schools.ProvinceID
		
	// 	,provinces.province_code,districts.id as distict_id,schools.DistrictID
	// 	,districts.provinces_id as dis_proid,districts.district_name as dis_name from dbo.schools 
	// 	join provinces on provinces.province_code = schools.ProvinceID
	// 	join districts on schools.DistrictID = districts.district_code
	// )r1
	// INNER JOIN provinces 
	// ON r1.dis_proid = provinces.id
	// where r1.ProvinceID != provinces.province_code

	//Q:2
	// select * from dbo.school_amphur where SchoolID = '1092140155'

	//Q:3
	// select * from districts 
	// inner join amphurs on districts.amphurs_id = amphurs.id
	// inner join provinces on districts.provinces_id = provinces.id
	// where districts.district_name like '%ปะเหลียน%'  

	//Q:4
	// update dbo.schools set DistrictID ='900310' where SchoolID ='1090550232'
	

	public function cleanData(){
       	 $school_rel_provice_str = "
			SELECT r1.*,provinces.province_code as dis_provcode FROM (
				select  schools.SchoolID,schools.SchoolName,schools.SchoolTypeID
				,provinces.province_name,schools.ProvinceID
				
				,provinces.province_code,districts.id as distict_id,schools.DistrictID
				,districts.provinces_id as dis_proid,districts.district_name as dis_name from dbo.schools 
				join provinces on provinces.province_code = schools.ProvinceID
				join districts on schools.DistrictID = districts.district_code
			)r1
			INNER JOIN provinces 
			ON r1.dis_proid = provinces.id
			where r1.ProvinceID != provinces.province_code
		
			";
            
         
		 $query_sch_rel_prov = $this->db->query($school_rel_provice_str);
		 $result = $query_sch_rel_prov->result();
		 
		$count = 0;
		$arr = array();
		 foreach($result as $key => $value){
			//Query 2
			$query2 = "	select * from dbo.Sheet1$ where SchoolID = '{$value->SchoolID}'";
			$exe_query2 = $this->db->query($query2);
			$rows_q2 = $exe_query2->row();
			//GET AMPHUR NAME
			$DistrinctID = $rows_q2->DistrinctID;
			// echo $DistrinctID.'||';
			$query3 = "select * from districts 
			inner join amphurs on districts.amphurs_id = amphurs.id
			inner join provinces on districts.provinces_id = provinces.id
			where districts.district_name = '{$DistrinctID}' and provinces.province_code = '{$value->ProvinceID}'";
			$exe_query3 = $this->db->query($query3);
			if($exe_query3->num_rows() > 0 && $exe_query3->num_rows() == 1){
				$rows_q3 = $exe_query3->row();
				//echo $rows_q3->district_code;exit();
				$query4 = "update dbo.schools set DistrictID ='{$rows_q3->district_code}' where SchoolID ='{$value->SchoolID}'";
				
				$this->db->query($query4);
			}else if($exe_query3->num_rows() > 1){


			}

			

				//  $querys = "select * from provinces where provinces.province_code = '".$value->ProvinceID."' ";
				//  $ssss = $this->db->query($querys);
				//  $rows = $ssss->row();
				// // $arr[] = array("province_id" => $rows->id , "province_id_from_db" => $value->dis_proid);

				// $query2 = "select * from districts where districts.district_name  like '%{$value->dis_name}%' and distrincts.provinces_id = '{$rows->id}'";
				// $exe_2 = $this->db->query($query2);
				// $arr[$value->SchoolID]['distrinct_match'] = $exe_2->row()->district_name;
				// //  if(empty($rows->id)){
				// // 	 echo $value->DistrictID;
				// // 	 exit();
				// //  }
				//  $province_id = $rows->id;
				// //echo $province_id.'||<br>';

				// ++ $count;
				
				
				//  if(!in_array($value->DistrictID,$arr) || $count =0){
				//   //$querysss = "update dbo.districts set provinces_id = '".$province_id."' where districts.district_code = '".$value->DistrictID."' ";
				//   //$this->db->query($querysss); 
				  
				// //   $arr[] = $value->DistrictID;
				  
				// }else{

				// }
				  
				  //echo $querysss.'||<br>';
				
			// echo '<PRE>';
			// print_r($querysss);exit();
				  
		 }
		 echo '<PRE>';
		 print_r($arr);exit();
		// echo $count;exit();
		
	}

	public function setOnlineRegionScheduleNewVersion(){
		set_time_limit(0);

		
		//echo 'aaa';exit;

		require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
		$requestCriteria = new StdClass();
		$requestCriteria->schedule_task = "true";

		$S3RatingLibrary = new S3RatingLibrary();
		$output = $S3RatingLibrary->setAllDeviceOnlineEachRegion($requestCriteria);
		// echo '<PRE>';
		// print_r($output);exit();
		$Criteria = new StdClass();
		$Criteria->schedule_task = "true";
		$arr_region = $S3RatingLibrary->setAllDeviceOnlineEachProvinceByRegionName($Criteria);
		
		// echo '<PRE>';
		// print_r($arr_region);exit();
		
		/* new version of online each region */
		$arrSumByProvinceRegion = [];

		$arrSumByProvinceRegion["Bangkok"] = 0;
		$arrSumByProvinceRegion["Central"] = 0;
		$arrSumByProvinceRegion["Northern"] = 0;
		$arrSumByProvinceRegion["NorthEastern"] = 0;
		$arrSumByProvinceRegion["Western"] = 0;
		$arrSumByProvinceRegion["Eastern"] = 0;
		$arrSumByProvinceRegion["Southern"] = 0;



		$mapShortProvinceRegion = [
			'Bangkok'=>'Bangkok',
			'Southern'=>'South',
			'Central'=>'Central',
			'NorthEastern'=>'Northeast',
			'Eastern'=>'East',
			'Northern'=>'North',
			'Western'=>'West'
		];



		$final_total_devices = 0;
		// echo '<PRE>';
		// print_r($arr_region);exit();
		// province > school > device (active)
		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {
					# code...
				if(trim($key) == $arrSumByProvinceRegion[trim($key)]){
					foreach ($value['ReturnData']['devices_by_province'] as $k => $v) {
							# code...
						$arrSumByProvinceRegion[trim($key)] += (int)$v['total_devices'];
							//echo $key.' '.$v['province_region'].' = '.$v['total_devices'].'<br>';
					}
				}

			}

		}

		// echo '<PRE>';
		// print_r($arr_region);exit();

		$online_region_data = [];

		foreach ($arrSumByProvinceRegion as $key => $value) {
				# code...
			array_push($online_region_data, [
				'sum_all_devices'=>$value,
				'count_all_devices'=>$this->countAllDevicesByProvinceRegion([
					'province_region'=>trim($key)
				]),
				'province_region'=>trim($key),
				'name'=>$mapShortProvinceRegion[trim($key)]
			]);
			$final_total_devices += (int)$value;
		}

		if(@$_GET['test'] == 'test'){
				//print_r($online_region_data);exit;
		}

		$json_output = [
			'status'=>true,
			'ReturnData'=>$online_region_data,
			'AllDeviceRegistered'=>$output['AllDeviceRegistered'],
			'result_code'=>'000',
			'result_desc'=>'Success'
		];

		$json_each_region = json_encode($json_output);


		$sum_devices = $final_total_devices;
		

		/* check insert or update into online_each_region table */
		$check_online_each_region = $this->db->select('id')
		->from('online_each_region')
		->where('created !=','')
		->where('type','satellite')
		->get();
		if($check_online_each_region->num_rows() <= 0){
			/* insert new record */


			$this->db->insert('online_each_region',[
				'response'=>$json_each_region,
				'type'=>'satellite',
				'sum_devices'=>$sum_devices,
				'created'=>date('Y-m-d H:i:s')
			]);



			/* eof insert new record*/
		}else{
			/* only update current record */


			$this->db->update('online_each_region',[
				'response'=>$json_each_region,
				'sum_devices'=>$sum_devices,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$check_online_each_region->row()->id,'type'=>'satellite']);



		}
		/* */
		
		/* eof new version of online each region */



		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {

				//echo json_encode($arr_region[$key]);exit;
				# code...
				/* check and insert or update into */
				$checkByRegionName = $this->db->select('id,province_region,created')
				->from('online_each_region_by_name')
				->where('province_region',$key)
				->where('type','satellite')
				->get();

				if($checkByRegionName->num_rows() <= 0){
					/* insert new record into online_each_region_by_name */
					$this->db->insert('online_each_region_by_name',[
						'response'=>json_encode($arr_region[$key]),
						'province_region'=>$value['province_region'],
						'type'=>'satellite',
						'created'=>date('Y-m-d H:i:s')
					]);
				}else{
					/* update response*/
					$this->db->update('online_each_region_by_name',[
						'response'=>json_encode($arr_region[$key]),
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$checkByRegionName->row()->id,'type'=>'satellite']);
				}



			}
		}

		
		$this->setYotubeOnlineRegionScheduleNewVersion();

	}

	public function setOnlineRegionScheduleNewVersionRV1(){
		set_time_limit(0);

		
		//echo 'aaa';exit;

		require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
		$requestCriteria = new StdClass();
		$requestCriteria->schedule_task = "true";

		$S3RatingLibrary = new S3RatingLibrary();
		$output = $S3RatingLibrary->setAllDeviceOnlineEachRegionRV1($requestCriteria);

		
		$Criteria = new StdClass();
		$Criteria->schedule_task = "true";
		$arr_region = $S3RatingLibrary->setAllDeviceOnlineEachProvinceByRegionNameRV1($Criteria);

		/* new version of online each region */
		$arrSumByProvinceRegion = [];

		$arrSumByProvinceRegion["Bangkok"] = 0;
		$arrSumByProvinceRegion["Central"] = 0;
		$arrSumByProvinceRegion["Northern"] = 0;
		$arrSumByProvinceRegion["NorthEastern"] = 0;
		$arrSumByProvinceRegion["Western"] = 0;
		$arrSumByProvinceRegion["Eastern"] = 0;
		$arrSumByProvinceRegion["Southern"] = 0;



		$mapShortProvinceRegion = [
			'Bangkok'=>'Bangkok',
			'Southern'=>'South',
			'Central'=>'Central',
			'NorthEastern'=>'Northeast',
			'Eastern'=>'East',
			'Northern'=>'North',
			'Western'=>'West'
		];



		$final_total_devices = 0;


		if(@$_GET['test'] = 'test'){
				//print_r($arr_region);exit;
		}

		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {
					# code...
				if(trim($key) == $arrSumByProvinceRegion[trim($key)]){
					foreach ($value['ReturnData']['devices_by_province'] as $k => $v) {
							# code...
						$arrSumByProvinceRegion[trim($key)] += (int)$v['total_devices'];
							//echo $key.' '.$v['province_region'].' = '.$v['total_devices'].'<br>';
					}
				}

			}

		}

		$online_region_data = [];


		foreach ($arrSumByProvinceRegion as $key => $value) {
				# code...
			array_push($online_region_data, [
				'sum_all_devices'=>$value,
				'count_all_devices'=>$this->countAllDevicesByProvinceRegionRV1([
					'province_region'=>trim($key)
				]),
				'province_region'=>trim($key),
				'name'=>$mapShortProvinceRegion[trim($key)]
			]);
			$final_total_devices += (int)$value;
		}

		if(@$_GET['test'] == 'test'){
				//print_r($online_region_data);exit;
		}

		$json_output = [
			'status'=>true,
			'ReturnData'=>$online_region_data,
			'AllDeviceRegistered'=>$output['AllDeviceRegistered'],
			'result_code'=>'000',
			'result_desc'=>'Success'
		];

		$json_each_region = json_encode($json_output);


		$sum_devices = $final_total_devices;


		/* check insert or update into online_each_region table */
		$check_online_each_region = $this->db->select('id')
		->from('online_each_region_rv1')
		->where('created !=','')
		->where('type','satellite')
		->get();
		if($check_online_each_region->num_rows() <= 0){
			/* insert new record */


			$this->db->insert('online_each_region_rv1',[
				'response'=>$json_each_region,
				'type'=>'satellite',
				'sum_devices'=>$sum_devices,
				'created'=>date('Y-m-d H:i:s')
			]);



			/* eof insert new record*/
		}else{
			/* only update current record */


			$this->db->update('online_each_region_rv1',[
				'response'=>$json_each_region,
				'sum_devices'=>$sum_devices,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$check_online_each_region->row()->id,'type'=>'satellite']);



		}
		/* */
		
		/* eof new version of online each region */



		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {

				//echo json_encode($arr_region[$key]);exit;
				# code...
				/* check and insert or update into */
				$checkByRegionName = $this->db->select('id,province_region,created')
				->from('online_each_region_by_name_rv1')
				->where('province_region',$key)
				->where('type','satellite')
				->get();

				if($checkByRegionName->num_rows() <= 0){
					/* insert new record into online_each_region_by_name */
					$this->db->insert('online_each_region_by_name_rv1',[
						'response'=>json_encode($arr_region[$key]),
						'province_region'=>@$value['province_region'],
						'type'=>'satellite',
						'created'=>date('Y-m-d H:i:s')
					]);
				}else{
					/* update response*/
					$this->db->update('online_each_region_by_name_rv1',[
						'response'=>json_encode($arr_region[$key]),
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$checkByRegionName->row()->id,'type'=>'satellite']);
				}



			}
		}

		$this->setYotubeOnlineRegionScheduleNewVersionRV1();



	}

	public function setYotubeOnlineRegionSchedule(){

		require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
		$requestCriteria = new StdClass();

		$S3RatingLibrary = new S3RatingLibrary();
		$output = $S3RatingLibrary->getYoutubeAllDeviceOnlineEachRegion($requestCriteria);


		/* get sum all devices */
		$sum_devices = 0;
		foreach ($output['ReturnData'] as $key => $value) {
			# code...
			$sum_devices += (int)$value['sum_all_devices'];
		}
		/* eof sum all devices */

		$json_each_region = json_encode($output);

		/* check insert or update into online_each_region table */
		$check_online_each_region = $this->db->select('id')
		->from('online_each_region')
		->where('created !=','')
		->where('type','youtube')
		->get();
		if($check_online_each_region->num_rows() <= 0){
			/* insert new record */
			$this->db->insert('online_each_region',[
				'response'=>$json_each_region,
				'type'=>'youtube',
				'sum_devices'=>$sum_devices,
				'created'=>date('Y-m-d H:i:s')
			]);
			/* eof insert new record*/
		}else{
			/* only update current record */
			$this->db->update('online_each_region',[
				'response'=>$json_each_region,
				'sum_devices'=>$sum_devices,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$check_online_each_region->row()->id,'type'=>'youtube']);
		}
		/* */

		/* set json data each region by name */
		if(count($output['ReturnData']) > 0){
			foreach ($output['ReturnData'] as $key => $value) {
					# code...
					//print_r($value['province_region']);
				$Criteria = new StdClass();
				$Criteria->province_region = $value['province_region'];
				$output_of_region = $S3RatingLibrary->getYoutubeAllDeviceOnlineEachProvinceByRegionName($Criteria);

				/* check and insert or update into */
				$checkByRegionName = $this->db->select('id,province_region,created')
				->from('online_each_region_by_name')
				->where('province_region',$value['province_region'])
				->where('type','youtube')
				->get();

				if($checkByRegionName->num_rows() <= 0){
					/* insert new record into online_each_region_by_name */
					$this->db->insert('online_each_region_by_name',[
						'response'=>json_encode($output_of_region),
						'province_region'=>$value['province_region'],
						'type'=>'youtube',
						'created'=>date('Y-m-d H:i:s')
					]);
				}else{
					/* update response*/
					$this->db->update('online_each_region_by_name',[
						'response'=>json_encode($output_of_region),
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$checkByRegionName->row()->id,'type'=>'youtube']);
				}
				/* eof check */


			}
		}





	}

	public function setYotubeOnlineRegionScheduleNewVersion(){

		require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
		$requestCriteria = new StdClass();

		$S3RatingLibrary = new S3RatingLibrary();
		$output = $S3RatingLibrary->getYoutubeAllDeviceOnlineEachRegionNewVersion($requestCriteria);


		



		/* province region each region name */
		/* eof province region each region name */
		$Criteria = new StdClass();
		$Criteria->schedule_task = "true";
		$arr_region = $S3RatingLibrary->setYoutubeAllDeviceOnlineEachProvinceByRegionName($Criteria);



		/* new version of online each region */
		$arrSumByProvinceRegion = [];

		$arrSumByProvinceRegion['Bangkok'] = 0;
		$arrSumByProvinceRegion["Central"] = 0;
		$arrSumByProvinceRegion["Northern"] = 0;
		$arrSumByProvinceRegion["NorthEastern"] = 0;
		$arrSumByProvinceRegion["Western"] = 0;
		$arrSumByProvinceRegion["Eastern"] = 0;
		$arrSumByProvinceRegion["Southern"] = 0;



		$mapShortProvinceRegion = [
			'Bangkok'=>'Bangkok',
			'Southern'=>'South',
			'Central'=>'Central',
			'NorthEastern'=>'Northeast',
			'Eastern'=>'East',
			'Northern'=>'North',
			'Western'=>'West'
		];

		$final_total_devices = 0;
		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {
					# code...
				if(trim($key) == $arrSumByProvinceRegion[trim($key)]){
					foreach ($value['ReturnData']['devices_by_province'] as $k => $v) {
							# code...
						$arrSumByProvinceRegion[trim($key)] += (int)$v['total_devices'];
							//echo $key.' '.$v['province_region'].' = '.$v['total_devices'].'<br>';
					}
				}

			}

		}

		$online_region_data = [];

		foreach ($arrSumByProvinceRegion as $key => $value) {
				# code...
			array_push($online_region_data, [
				'sum_all_devices'=>$value,
				'count_all_devices'=>$this->countAllDevicesByProvinceRegion([
					'province_region'=>trim($key)
				]),
				'province_region'=>trim($key),
				'name'=>$mapShortProvinceRegion[trim($key)]
			]);
			$final_total_devices += (int)$value;
		}

		$json_output = [
			'status'=>true,
			'ReturnData'=>$online_region_data,
			'AllDeviceRegistered'=>$output['AllDeviceRegistered'],
			'result_code'=>'000',
			'result_desc'=>'Success'
		];

		$json_each_region = json_encode($json_output);


		$sum_devices = $final_total_devices;

		/* check insert or update into online_each_region table */
		$check_online_each_region = $this->db->select('id')
		->from('online_each_region')
		->where('created !=','')
		->where('type','youtube')
		->get();
		if($check_online_each_region->num_rows() <= 0){
			/* insert new record */

			$this->db->insert('online_each_region',[
				'response'=>$json_each_region,
				'type'=>'youtube',
				'sum_devices'=>$sum_devices,
				'created'=>date('Y-m-d H:i:s')
			]);

			/* eof insert new record*/
		}else{
			/* only update current record */

			$this->db->update('online_each_region',[
				'response'=>$json_each_region,
				'sum_devices'=>$sum_devices,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$check_online_each_region->row()->id,'type'=>'youtube']);

		}
		/* */



		if(@$_GET['test'] == 'test'){
			echo 'here';
			print_r($online_region_data);exit;
		}

		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {

				//echo json_encode($arr_region[$key]);exit;
				# code...
				/* check and insert or update into */
				$checkByRegionName = $this->db->select('id,province_region,created')
				->from('online_each_region_by_name')
				->where('province_region',$key)
				->where('type','youtube')
				->get();

				if($checkByRegionName->num_rows() <= 0){
					/* insert new record into online_each_region_by_name */
					$this->db->insert('online_each_region_by_name',[
						'response'=>json_encode($arr_region[$key]),
						'province_region'=>@$value['province_region'],
						'type'=>'youtube',
						'created'=>date('Y-m-d H:i:s')
					]);
				}else{
					/* update response*/
					$this->db->update('online_each_region_by_name',[
						'response'=>json_encode($arr_region[$key]),
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$checkByRegionName->row()->id,'type'=>'youtube']);
				}
			}
		}




	}

	public function setYotubeOnlineRegionScheduleNewVersionRV1(){

		require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
		$requestCriteria = new StdClass();

		$S3RatingLibrary = new S3RatingLibrary();
		$output = $S3RatingLibrary->getYoutubeAllDeviceOnlineEachRegionNewVersionRV1($requestCriteria);


		



		/* province region each region name */
		/* eof province region each region name */
		$Criteria = new StdClass();
		$Criteria->schedule_task = "true";
		$arr_region = $S3RatingLibrary->setYoutubeAllDeviceOnlineEachProvinceByRegionNameRV1($Criteria);


		/* new version of online each region */
		$arrSumByProvinceRegion = [];

		$arrSumByProvinceRegion["Bangkok"] = 0;
		$arrSumByProvinceRegion["Central"] = 0;
		$arrSumByProvinceRegion["Northern"] = 0;
		$arrSumByProvinceRegion["NorthEastern"] = 0;
		$arrSumByProvinceRegion["Western"] = 0;
		$arrSumByProvinceRegion["Eastern"] = 0;
		$arrSumByProvinceRegion["Southern"] = 0;



		$mapShortProvinceRegion = [
			'Bangkok'=>'Bangkok',
			'Southern'=>'South',
			'Central'=>'Central',
			'NorthEastern'=>'Northeast',
			'Eastern'=>'East',
			'Northern'=>'North',
			'Western'=>'West'
		];

		$final_total_devices = 0;
		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {
					# code...
				if(trim($key) == $arrSumByProvinceRegion[trim($key)]){
					foreach ($value['ReturnData']['devices_by_province'] as $k => $v) {
							# code...
						$arrSumByProvinceRegion[trim($key)] += (int)$v['total_devices'];
							//echo $key.' '.$v['province_region'].' = '.$v['total_devices'].'<br>';
					}
				}

			}

		}

		$online_region_data = [];

		foreach ($arrSumByProvinceRegion as $key => $value) {
				# code...
			array_push($online_region_data, [
				'sum_all_devices'=>$value,
				'count_all_devices'=>$this->countAllDevicesByProvinceRegionRV1([
					'province_region'=>trim($key)
				]),
				'province_region'=>trim($key),
				'name'=>$mapShortProvinceRegion[trim($key)]
			]);
			$final_total_devices += (int)$value;
		}

		$json_output = [
			'status'=>true,
			'ReturnData'=>$online_region_data,
			'AllDeviceRegistered'=>$output['AllDeviceRegistered'],
			'result_code'=>'000',
			'result_desc'=>'Success'
		];

		$json_each_region = json_encode($json_output);


		$sum_devices = $final_total_devices;

		/* check insert or update into online_each_region table */
		$check_online_each_region = $this->db->select('id')
		->from('online_each_region_rv1')
		->where('created !=','')
		->where('type','youtube')
		->get();
		if($check_online_each_region->num_rows() <= 0){
			/* insert new record */

			$this->db->insert('online_each_region_rv1',[
				'response'=>$json_each_region,
				'type'=>'youtube',
				'sum_devices'=>$sum_devices,
				'created'=>date('Y-m-d H:i:s')
			]);

			/* eof insert new record*/
		}else{
			/* only update current record */

			$this->db->update('online_each_region_rv1',[
				'response'=>$json_each_region,
				'sum_devices'=>$sum_devices,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$check_online_each_region->row()->id,'type'=>'youtube']);

		}
		/* */



		if(@$_GET['test'] == 'test'){
			// echo 'here';
			// print_r($online_region_data);exit;
		}

		if(count($arr_region) > 0){
			foreach ($arr_region as $key => $value) {

				//echo json_encode($arr_region[$key]);exit;
				# code...
				/* check and insert or update into */
				$checkByRegionName = $this->db->select('id,province_region,created')
				->from('online_each_region_by_name_rv1')
				->where('province_region',$key)
				->where('type','youtube')
				->get();

				if($checkByRegionName->num_rows() <= 0){
					/* insert new record into online_each_region_by_name */
					$this->db->insert('online_each_region_by_name_rv1',[
						'response'=>json_encode($arr_region[$key]),
						'province_region'=>@$value['province_region'],
						'type'=>'youtube',
						'created'=>date('Y-m-d H:i:s')
					]);
				}else{
					/* update response*/
					$this->db->update('online_each_region_by_name_rv1',[
						'response'=>json_encode($arr_region[$key]),
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$checkByRegionName->row()->id,'type'=>'youtube']);
				}
			}
		}







	}

	

	public function getInterNetTVOnline(){
		
		

	}

	public function setChannelDailySummary(){

		set_time_limit(0);


		/* set channel daily summar for 
		channel_daily_date_summary
		channel_dialy_period_summary
		*/

		$date_report = "";

		$date  = new DateTime();
		$interval = new DateInterval('P1D');
		$date->sub($interval); 

		$date_report = (isset($_GET['date_report']))?$_GET['date_report']:$date->format('Y-m-d');

        //print_r($date_to_report);exit;




		$api_config = $this->load->config('api');

		$arr_twenty_four_hours = $this->config->item('arr_twenty_four_hours');

		//print_r($arr_twenty_four_hours);exit;

		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();

		if($channels->num_rows() > 0){
			foreach ($channels->result() as $key => $value) {
				# code...
				$channels_id = $value->id;
				$arrData = array();

				$channel_daily_rating_id = $this->getChannelDailyRatingIDByChannelsIDAndDate([
					'channels_id'=>$value->id,
					'date'=>$date_report
				]);

				$average_daily_rating = $this->getAverageDailyRating([
					'channel_daily_rating_id'=>$channel_daily_rating_id,
					'date'=>$date_report
				]);


				/* check if not exist not insert  */
				$qCheck = $this->db->select('*')
				->from('channel_daily_date_summary')
				->where('channels_id',$channels_id)
				->where('date',$date_report)
				->get();

				/* eof check if not exist not insert */

				if($qCheck->num_rows() <= 0){
					/* insert into channel_daily_date_summary */
					$this->db->insert('channel_daily_date_summary',[
						'channels_id'=>$channels_id,
						'date'=>$date_report,
						'rating'=>$average_daily_rating,
						'created'=>date('Y-m-d H:i:s')
					]);
					/* eof insert into channel_daily_date_summary */
				}



				foreach ($arr_twenty_four_hours as $k => $v) {
					# code...
					$arrData[$k]['date'] = $date_report;
					$arrData[$k]['time'] = $v['start_time'];
					$arrData[$k]['arr_rating'] = array();

					$query = $this->db->select('id,channel_daily_rating_id,rating,created')
					->from('channel_daily_rating_logs')
					->where('channel_daily_rating_id',$channel_daily_rating_id)
					->where('created >= ',$date_report.' '.$v['start_time'])
					->where('created <= ',$date_report.' '.$v['end_time'])
					->get();

					if($query->num_rows() > 0){
						foreach ($query->result() as $k1 => $v1) {
	                        # code...
							array_push($arrData[$k]['arr_rating'], $v1->rating);

						}
					}


				}


				$arrReturn = array();

				foreach ($arrData as $k2 => $v2) {

					$a = array();
					$average = 0;
					/* average array */

					if(count($v2['arr_rating']) > 0){
						$a = array_filter($v2['arr_rating']);
						$average = array_sum($a)/count($a);
					}

					$qCheckTime = $this->db->select('*')
					->from('channel_daily_period_summary')
					->where('channels_id',$channels_id)
					->where('date',$date_report)
					->where('time',$v2['time'])
					->get();

					if($qCheckTime->num_rows() <= 0){
						$this->db->insert('channel_daily_period_summary',[
							'channels_id'=>$channels_id,
							'date'=>$date_report,
							'time'=>$v2['time'],
							'rating'=>($average)?$average:0,
							'created'=>date('Y-m-d H:i:s')
						]);
					}


			        # code...
			        // $arrReturn[$k2]['date'] = $v2['date'];
			        // $arrReturn[$k2]['time'] = $v2['time'];
			        // $arrReturn[$k2]['rating'] = number_format($average,2);

				}	

    //     		print_r($average_daily_rating);
				// print_r($arrReturn);

			}

		}






	}
	public function setChannelAudienceSchoolTypeSummary($arrg_date_report = null){

		set_time_limit(0);
		
		/** SELECT ALL SCHOOL GROUP */
		
		$arrSchoolGroup = array();
		$schoolGroupQuery = $this->db->select('*')
		->from('school_type')
		->where('active',1)
		->get();
	
		

		if($schoolGroupQuery->num_rows() > 0){
			foreach ($schoolGroupQuery->result() as $kk => $vv) {
				$arrSchoolGroup[] = $vv->id;
			}
		}
	
		/* set channel daily summar for 
		channel_daily_date_summary
		channel_dialy_period_summary
		*/

		$date_report = "";

		$date  = new DateTime();
		$interval = new DateInterval('P1D');
		$date->sub($interval); 

		$date_report = (isset($_GET['date_report']))?$_GET['date_report']:$date->format('Y-m-d');

		if($arrg_date_report){
			$date_report = $arrg_date_report;
		}

		$datetime_report = new DateTime($date_report);

		$dailyDeviceTable = 'rating_data_daily_devices_'.$datetime_report->format('Y').'_'.$datetime_report->format('n');
		
		


		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();
	
		if($channels->num_rows() > 0){
			foreach ($channels->result() as $key => $value) {
				$arrSchoolGroupData = array();
				$schoolGroupQuerys = $this->db->select('*')
				->from('school_type')
				->where('active',1)
				->get();
				/** PREPARE ALL GROUP DATA */
				if($schoolGroupQuerys->num_rows() > 0){
					foreach ($schoolGroupQuerys->result() as $qgK => $qgv) {
						$arrSchoolGroupData[$qgv->id]['id'] = $qgv->id;
						$arrSchoolGroupData[$qgv->id]['name'] = $qgv->SchoolType;
						$arrSchoolGroupData[$qgv->id]['watch_time_array'] = array();
						$arrSchoolGroupData[$qgv->id]['watch_time_summary'] = 0;
						$arrSchoolGroupData[$qgv->id]['devices'] = array();
						$arrSchoolGroupData[$qgv->id]['total_devices'] = 0;
					}
				}
				/** EOF */
			

				$channels_id = $value->id;
				//bak test
				// $query = $this->db->select($dailyDeviceTable.".devices_id,count(".$dailyDeviceTable.".id) as count_devices,SUM(".$dailyDeviceTable.".total_seconds) as sum_seconds")
				// ->from($dailyDeviceTable)
				// ->join('rating_data_daily',$dailyDeviceTable.".rating_data_daily_id = rating_data_daily.id")
				// ->where('rating_data_daily.channels_id',$value->id)
			
				// ->group_by($dailyDeviceTable.'.devices_id')
				// ->get();
			
				
				//bak production
				$query = $this->db->select($dailyDeviceTable.".devices_id,count(".$dailyDeviceTable.".id) as count_devices,SUM(".$dailyDeviceTable.".total_seconds) as sum_seconds")
				->from($dailyDeviceTable)
				->join('rating_data_daily',$dailyDeviceTable.".rating_data_daily_id = rating_data_daily.id")
				->where('rating_data_daily.channels_id',$value->id)
				->where('CONVERT(char(10), '.$dailyDeviceTable.'.created,126) = ',$date_report)
				->group_by($dailyDeviceTable.'.devices_id')
				->get();
				



				if($query->num_rows() > 0){
					foreach ($query->result() as $key => $value) {
							# code...
						/* get province region from device_addresses*/
						$qAddress = $this->db->select('device_addresses.devices_id,schools.SchoolTypeID as school_type_id,schools.SchoolID as SchoolID')
						->from('device_addresses')->where('devices_id',$value->devices_id)
						->join('schools', 'device_addresses.SchoolID = schools.SchoolID', 'left')
						->get();
						
						//echo $this->db->last_query();
						
						if($qAddress->num_rows() > 0){
							$rowAddress = $qAddress->row();
					
							if(in_array($rowAddress->school_type_id, $arrSchoolGroup)){
									// array_push($arrRegionData[$rowAddress->province_region]['devices'], $value->devices_id);

									// array_push($arrRegionData[$rowAddress->province_region]['watch_time_array'], $value->sum_seconds?$value->sum_seconds:0);
								$arrSchoolGroupData[$rowAddress->school_type_id]['total_devices'] += 1;

								$arrSchoolGroupData[$rowAddress->school_type_id]['watch_time_summary'] += $value->sum_seconds;
							}

						}
						
					}
					foreach ($arrSchoolGroupData as $key => $value) {
							# code...
						$arrSchoolGroupData[$key]['rating'] = $this->getSchoolTypeRating(array(
							'total_devices'=>$value['total_devices'],
							'date_report'=>$date_report
						));
						$arrSchoolGroupData[$key]['watch_time']=$this->getSchoolTypeWatchTime($value['watch_time_summary']);
					}
					
				}
		
				if(@$this->input->get('test') == 'test'){
					print_r($arrSchoolGroupData);exit;
				}
				

				foreach ($arrSchoolGroupData as $key => $value) {
						# code...

					/* check exist row */
					$checkExist = $this->db->select('*')
					->from('channel_audience_schooltype_summary')
					->where('date',$date_report)
					->where('channels_id',$channels_id)
					->where('SchoolTypeID',@$value['id'])
					->get();
					/* eof check exist row */

					if($checkExist->num_rows() <= 0){

						/* insert data into channel_audience_region */
						$this->db->insert('channel_audience_schooltype_summary',[
							'date'=>$date_report,
							'channels_id'=>$channels_id,
							'SchoolTypeID'=>@$value['id'],
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'created'=>date('Y-m-d H:i:s')
						]);
						/* eof insert data into channel_audience_region */
					}else{

						$rowExist = $checkExist->row();

						$this->db->update('channel_audience_schooltype_summary',[
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'updated'=>date('Y-m-d H:i:s')
						],['id'=>$rowExist->id]);
					}

				}
				unset($arrSchoolGroupData);

					//print_r($arrRegionData);exit;


					//print_r($query->result());exit;

			}
		}



	}

	public function setChannelAudienceSchoolClassSummary($arrg_date_report = null){
		set_time_limit(0);
		
		/** SELECT ALL SCHOOL GROUP */
		
		$arrdeviceGroup = array();
		$deviceGroupQuery = $this->db->select('devices_group.id as parent_id , devices_group.group_name , devices_group.group_desc , devices_group.active,device_rel_group.device_id as device_id ,device_rel_group.device_group_id as GroupID , device_addresses.*')
		->from('devices_group')
		->join('device_rel_group' , 'devices_group.id = device_rel_group.device_group_id' ,'left')
		->join('device_addresses' , 'device_rel_group.device_id = device_addresses.devices_id' , 'left')
		->where('devices_group.active',1)
		->get();

		
		

		// MAKE GROUP OF DEVICE
		if($deviceGroupQuery->num_rows() > 0){
			foreach ($deviceGroupQuery->result() as $kk => $vv) {
			
				// START DECLARE VARIABLE FOR GROUP 
				$arrdeviceGroup[$vv->parent_id]['group_id'] =  $vv->parent_id;
				$arrdeviceGroup[$vv->parent_id]['group_name'] =  $vv->group_name;
				$arrdeviceGroup[$vv->parent_id]['group_desc'] =  $vv->group_desc;
				$arrdeviceGroup[$vv->parent_id]['watch_time_array'] = array();
				$arrdeviceGroup[$vv->parent_id]['watch_time_summary'] = 0;
				$arrdeviceGroup[$vv->parent_id]['total_devices'] = 0;

				// EOF GROUP
				# Create new array group 
				if(!isset($arrdeviceGroup[$vv->parent_id]['devices'])){
					$arrdeviceGroup[$vv->parent_id]['devices'] = array();
					$arrdeviceGroup[$vv->parent_id]['devices_group'] = array();
				}
				# Create device group 
				if(isset($vv->device_id) && !empty($vv->device_id)){
					# START DECLARE VARIABLE FOR Device
					$arrdeviceGroup[$vv->parent_id]['devices'][$vv->device_id]['device_id'] = $vv->device_id;
					$arrdeviceGroup[$vv->parent_id]['devices'][$vv->device_id]['SchoolID'] = $vv->SchoolID;
					if(!empty($vv->SchoolID)){
						$schools = $this->db->select('*')
						->from('schools')
						->where('SchoolID',$vv->SchoolID)
						->get();
						
						$arrdeviceGroup[$vv->parent_id]['devices'][$vv->device_id]['School_lat'] = $schools->row()->Latitude;
						$arrdeviceGroup[$vv->parent_id]['devices'][$vv->device_id]['School_long'] = $schools->row()->Longitude;
					}
					$arrdeviceGroup[$vv->parent_id]['devices_group'][] = $vv->device_id;
					# EOF Device
				}
			}
		}
		
		$date_report = "";

		$date  = new DateTime();
		$interval = new DateInterval('P1D');
		$date->sub($interval); 

		$date_report = (isset($_GET['date_report']))?$_GET['date_report']:$date->format('Y-m-d');

		if($arrg_date_report){
			$date_report = $arrg_date_report;
		}

		$datetime_report = new DateTime($date_report);

		$dailyDeviceTable = 'rating_data_daily_devices_'.$datetime_report->format('Y').'_'.$datetime_report->format('n');

		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();
		
		if($channels->num_rows() > 0){
			foreach ($channels->result() as $key => $value) {
			
				$arrdeviceGroupData= array();

				$channels_id = $value->id;
				//bak test
				$query = $this->db->select($dailyDeviceTable.".devices_id,count(".$dailyDeviceTable.".id) as count_devices,SUM(".$dailyDeviceTable.".total_seconds) as sum_seconds")
				->from($dailyDeviceTable)
				->join('rating_data_daily',$dailyDeviceTable.".rating_data_daily_id = rating_data_daily.id")
				->where('rating_data_daily.channels_id',$value->id)
				->group_by($dailyDeviceTable.'.devices_id')
				->get();
				
		
				
				//bak production
				// $query = $this->db->select($dailyDeviceTable.".devices_id,count(".$dailyDeviceTable.".id) as count_devices,SUM(".$dailyDeviceTable.".total_seconds) as sum_seconds")
				// ->from($dailyDeviceTable)
				// ->join('rating_data_daily',$dailyDeviceTable.".rating_data_daily_id = rating_data_daily.id")
				// ->where('rating_data_daily.channels_id',$value->id)
				// ->where('CONVERT(char(10), '.$dailyDeviceTable.'.created,126) = ',$date_report)
				// ->group_by($dailyDeviceTable.'.devices_id')
				// ->get();

			
				//exit();
				

				if($query->num_rows() > 0){
					/** ========================
					*  SUMMARY GROUP OF DEVICE
					*/
					$count  = 0;
					$arrdeviceGroupData = $arrdeviceGroup;
					foreach ($query->result() as $key => $value) {
						# code...
						
						foreach($arrdeviceGroup as $dvKey  => $dvVal){
							
							if(in_array($value->devices_id, $arrdeviceGroup[$dvKey]['devices_group'])){
								++ $count;	
								
								$arrdeviceGroupData[$dvKey]['channels_id'] =  $channels_id;
								$arrdeviceGroupData[$dvKey]['total_devices'] += 1;
								$arrdeviceGroupData[$dvKey]['watch_time_summary'] += $value->sum_seconds;
								$arrdeviceGroupData[$dvKey]['date_report'] = $date_report;
								
								// if device has exist on table daily record update
								
								$this->insertOrUpdate_ChannelAudienceGroup($date_report,$channels_id,$dvKey,$value);

								/* eof check exist row */

							}
						}
					}

					
					/**===========================
					 * EOF SUMMARY GROUP OF DEVICE
					  ============================ */
					// clear old result 
					
					foreach ($arrdeviceGroupData as $key => $value) {
							# code...
						$arrdeviceGroupData[$key]['rating'] = $this->getDeviceGroupRating(array(
							'total_devices'=>$value['total_devices'],
							'date_report'=>$date_report
						));
						$arrdeviceGroupData[$key]['watch_time']=$this->getDeviceGroupWatchTime($value['watch_time_summary']);
					}
					
				}
				
				if(@$this->input->get('test') == 'test'){
					print_r($arrdeviceGroupData);exit;
				}
				
				//  echo '<PRE>';
				//  print_r($arrdeviceGroupData);
				foreach ($arrdeviceGroupData as $key => $value) {
						# code...

					/* check exist row */
					$checkExist = $this->db->select('*')
					->from('channel_audience_school_class')
					->where('date',$date_report)
					->where('channels_id',$channels_id)
					->where('GroupID',@$value['group_id'])
					->get();
					/* eof check exist row */

					if($checkExist->num_rows() <= 0){

						/* insert data into channel_audience_region */
						$this->db->insert('channel_audience_school_class',[
							'date'=>$date_report,
							'channels_id'=>$channels_id,
							'GroupID'=>@$value['group_id'],
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'created'=>date('Y-m-d H:i:s')
						]);
						/* eof insert data into channel_audience_region */
					}else{

						$rowExist = $checkExist->row();

						$this->db->update('channel_audience_school_class',[
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'updated'=>date('Y-m-d H:i:s')
						],['id'=>$rowExist->id]);
					}

				}
				unset($arrdeviceGroupData);
		
				//print_r($arrRegionData);exit;
				//print_r($query->result());exit;

			}
		}
		
		

	}


	public function insertOrUpdate_ChannelAudienceGroup($date_report,$channels_id,$dvKey,$value){
		// if device has exist on table daily record update
		
		$checkDeviceRelGroup = $this->db->select('*')
		->from('channel_audience_daily_groupofdevice')
		->where('date',$date_report)
		->where('channels_id',$channels_id)
		->where('GroupID',$dvKey)
		->where('devices_id' ,$value->devices_id)
		->get();
		
		
		if($checkDeviceRelGroup->num_rows() <= 0){
			/* insert data into channel_audience_region */
			$this->db->insert('channel_audience_daily_groupofdevice',[
				'date'=>$date_report,
				'channels_id'=>$channels_id,
				'GroupID'=>$dvKey,
				'devices_id'=>$value->devices_id,
				'total_seconds'=>$value->sum_seconds,
				'created'=>date('Y-m-d H:i:s')
			]);
			/* eof insert data into channel_audience_region */
		}else{
			$rowExist = $checkDeviceRelGroup->row();
			$this->db->update('channel_audience_daily_groupofdevice',[
				'date'=>$date_report,
				'channels_id'=>$channels_id,
				'GroupID'=>$dvKey,
				'devices_id'=>$value->devices_id,
				'total_seconds'=>$value->sum_seconds,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$rowExist->id]);
		}


	}
	public function setChannelAudienceRegionSummary($arrg_date_report = null){

		set_time_limit(0);

		$arrRegion = array(
			'Bangkok',
			'Northern',
			'NorthEastern',
			'Central',
			'Eastern',
			'Southern',
			'Western'
		);



		/* set channel daily summar for 
		channel_daily_date_summary
		channel_dialy_period_summary
		*/

		$date_report = "";

		$date  = new DateTime();
		$interval = new DateInterval('P1D');
		$date->sub($interval); 

		$date_report = (isset($_GET['date_report']))?$_GET['date_report']:$date->format('Y-m-d');

		if($arrg_date_report){
			$date_report = $arrg_date_report;
		}

		$datetime_report = new DateTime($date_report);

		$dailyDeviceTable = 'rating_data_daily_devices_'.$datetime_report->format('Y').'_'.$datetime_report->format('n');


		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();

		if($channels->num_rows() > 0){
			foreach ($channels->result() as $key => $value) {

				$arrRegionData = array(
					'Bangkok' => array(
						'name'=>'Bangkok',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Northern'=>array(
						'name'=>'Northern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'NorthEastern'=>array(
						'name'=>'NorthEastern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Central'=>array(
						'name'=>'Central',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Eastern' => array(
						'name'=>'Eastern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Southern' => array(
						'name'=>'Southern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Western' => array(
						'name'=>'Western',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					)
				);



				$channels_id = $value->id;

				$query = $this->db->select($dailyDeviceTable.".devices_id,count(".$dailyDeviceTable.".id) as count_devices,SUM(".$dailyDeviceTable.".total_seconds) as sum_seconds")
				->from($dailyDeviceTable)
				->join('rating_data_daily',$dailyDeviceTable.".rating_data_daily_id = rating_data_daily.id")
				->where('rating_data_daily.channels_id',$value->id)
				->where('CONVERT(char(10), '.$dailyDeviceTable.'.created,126) = ',$date_report)
				->group_by($dailyDeviceTable.'.devices_id')
				->get();




				if($query->num_rows() > 0){
					foreach ($query->result() as $key => $value) {
			                # code...
						/* get province region from device_addresses*/
						$qAddress = $this->db->select('devices_id,province_region')
						->from('device_addresses')->where('devices_id',$value->devices_id)
						->get();

						if($qAddress->num_rows() > 0){
							$rowAddress = $qAddress->row();

							if(in_array($rowAddress->province_region, $arrRegion)){
			                        // array_push($arrRegionData[$rowAddress->province_region]['devices'], $value->devices_id);

			                        // array_push($arrRegionData[$rowAddress->province_region]['watch_time_array'], $value->sum_seconds?$value->sum_seconds:0);
								$arrRegionData[$rowAddress->province_region]['total_devices'] += 1;

								$arrRegionData[$rowAddress->province_region]['watch_time_summary'] += $value->sum_seconds;
							}

						}
					}






					foreach ($arrRegionData as $key => $value) {
			                # code...
						$arrRegionData[$key]['rating'] = $this->getRegionRating(array(
							'total_devices'=>$value['total_devices'],
							'date_report'=>$date_report
						));
						$arrRegionData[$key]['watch_time']=$this->getRegionWatchTime($value['watch_time_summary']);
					}



			            //print_r($arrRegionData);
				}

				if(@$this->input->get('test') == 'test'){
					print_r($arrRegionData);exit;
				}

			        //print_r($arrRegionData);exit;

				foreach ($arrRegionData as $key => $value) {
			        	# code...

					/* check exist row */
					$checkExist = $this->db->select('*')
					->from('channel_audience_region_summary_new')
					->where('date',$date_report)
					->where('channels_id',$channels_id)
					->where('province_region',@$value['name'])
					->get();
					/* eof check exist row */

					if($checkExist->num_rows() <= 0){

						/* insert data into channel_audience_region */
						$this->db->insert('channel_audience_region_summary_new',[
							'date'=>$date_report,
							'channels_id'=>$channels_id,
							'province_region'=>@$value['name'],
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'created'=>date('Y-m-d H:i:s')
						]);
						/* eof insert data into channel_audience_region */
					}else{

						$rowExist = $checkExist->row();

						$this->db->update('channel_audience_region_summary_new',[
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'updated'=>date('Y-m-d H:i:s')
						],['id'=>$rowExist->id]);
					}

				}
				unset($arrRegionData);

			        //print_r($arrRegionData);exit;


		            //print_r($query->result());exit;

			}
		}



        //print_r($dailyDeviceTable);

        //print_r($date_report);


	}

	public function setPreviousChannelAudienceRegionSummaryByDateAndChannel($arr_filter){
		set_time_limit(0);

		$arrRegion = array(
			'Bangkok',
			'Northern',
			'NorthEastern',
			'Central',
			'Eastern',
			'Southern',
			'Western'
		);


		$date_report = $arr_filter['date_report'];
		$channel_id = $arr_filter['channel_id'];

		$datetime_report = new DateTime($date_report);

		$dailyDeviceTable = 'bk_rating_data_daily_devices_'.$datetime_report->format('Y').'_'.$datetime_report->format('n');


		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('id',$channel_id)
		->where('active',1)
		->where('channel_extended_status',0)
		->get();

		if($channels->num_rows() > 0){
			$channel_data = $channels->row();

			// clear older data in table channel_audience_region_summary_new
			// $this->db->delete('channel_audience_region_summary_new',[

			// ]);

			$arrRegionData = array(
				'Bangkok' => array(
					'name'=>'Bangkok',
					'watch_time_array'=>array(),
					'watch_time_summary'=>0,
					'devices'=>array(),
					'total_devices'=>0
				),
				'Northern'=>array(
					'name'=>'Northern',
					'watch_time_array'=>array(),
					'watch_time_summary'=>0,
					'devices'=>array(),
					'total_devices'=>0
				),
				'NorthEastern'=>array(
					'name'=>'NorthEastern',
					'watch_time_array'=>array(),
					'watch_time_summary'=>0,
					'devices'=>array(),
					'total_devices'=>0
				),
				'Central'=>array(
					'name'=>'Central',
					'watch_time_array'=>array(),
					'watch_time_summary'=>0,
					'devices'=>array(),
					'total_devices'=>0
				),
				'Eastern' => array(
					'name'=>'Eastern',
					'watch_time_array'=>array(),
					'watch_time_summary'=>0,
					'devices'=>array(),
					'total_devices'=>0
				),
				'Southern' => array(
					'name'=>'Southern',
					'watch_time_array'=>array(),
					'watch_time_summary'=>0,
					'devices'=>array(),
					'total_devices'=>0
				),
				'Western' => array(
					'name'=>'Western',
					'watch_time_array'=>array(),
					'watch_time_summary'=>0,
					'devices'=>array(),
					'total_devices'=>0
				)
			);

			$query = $this->db->select($dailyDeviceTable.".devices_id,count(".$dailyDeviceTable.".id) as count_devices,SUM(".$dailyDeviceTable.".total_seconds) as sum_seconds")
			->from($dailyDeviceTable)
			->join('rating_data_daily',$dailyDeviceTable.".rating_data_daily_id = rating_data_daily.id")
			->where('rating_data_daily.channels_id',$channel_data->id)
			->where('CONVERT(char(10), '.$dailyDeviceTable.'.created,126) = ',$date_report)
			->group_by($dailyDeviceTable.'.devices_id')
			->get();

			// print_r($this->db->last_query());

			// print_r($query->result());exit;

			if($query->num_rows() > 0){
				foreach ($query->result() as $key => $value) {
			                # code...
					/* get province region from device_addresses*/
					$qAddress = $this->db->select('devices_id,province_region')
					->from('device_addresses')->where('devices_id',$value->devices_id)
					->get();

					if($qAddress->num_rows() > 0){
						$rowAddress = $qAddress->row();

						if(in_array($rowAddress->province_region, $arrRegion)){
			                        // array_push($arrRegionData[$rowAddress->province_region]['devices'], $value->devices_id);

			                        // array_push($arrRegionData[$rowAddress->province_region]['watch_time_array'], $value->sum_seconds?$value->sum_seconds:0);
							$arrRegionData[$rowAddress->province_region]['total_devices'] += 1;

							$arrRegionData[$rowAddress->province_region]['watch_time_summary'] += $value->sum_seconds;
						}

					}
				}






				foreach ($arrRegionData as $key => $value) {
			                # code...
					$arrRegionData[$key]['rating'] = $this->getRegionRating(array(
						'total_devices'=>$value['total_devices'],
						'date_report'=>$date_report
					));
					$arrRegionData[$key]['watch_time']=$this->getRegionWatchTime($value['watch_time_summary']);
				}



			            //print_r($arrRegionData);
			}

			foreach ($arrRegionData as $key => $value) {
			        	# code...

					/* check exist row */
					$checkExist = $this->db->select('*')
					->from('channel_audience_region_summary_new')
					->where('date',$date_report)
					->where('channels_id',$channel_data->id)
					->where('province_region',@$value['name'])
					->get();
					/* eof check exist row */

					if($checkExist->num_rows() <= 0){

						/* insert data into channel_audience_region */
						$this->db->insert('channel_audience_region_summary_new',[
							'date'=>$date_report,
							'channels_id'=>$channels_id,
							'province_region'=>@$value['name'],
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'created'=>date('Y-m-d H:i:s')
						]);
						/* eof insert data into channel_audience_region */
					}else{

						$rowExist = $checkExist->row();

						$this->db->update('channel_audience_region_summary_new',[
							'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
							'sum_duration'=>@$value['watch_time_summary'],
							'updated'=>date('Y-m-d H:i:s')
						],['id'=>$rowExist->id]);
					}

			}
			unset($arrRegionData);



		}

	}

	public function setChannelAudienceRegionSummaryRV1(){

		set_time_limit(0);

		$arrRegion = array(
			'Bangkok',
			'Northern',
			'NorthEastern',
			'Central',
			'Eastern',
			'Southern',
			'Western'
		);



		/* set channel daily summar for 
		channel_daily_date_summary
		channel_dialy_period_summary
		*/

		$date_report = "";

		$date  = new DateTime();
		$interval = new DateInterval('P1D');
		$date->sub($interval); 

		$date_report = (isset($_GET['date_report']))?$_GET['date_report']:$date->format('Y-m-d');


		$datetime_report = new DateTime($date_report);

		$dailyDeviceTable = 'rating_data_daily_devices_'.$datetime_report->format('Y').'_'.$datetime_report->format('n');


		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();

		if($channels->num_rows() > 0){
			foreach ($channels->result() as $key => $value) {

				$arrRegionData = array(
					'Bangkok'=>array(
						'name'=>'Bangkok',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Northern'=>array(
						'name'=>'Northern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'NorthEastern'=>array(
						'name'=>'NorthEastern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Central'=>array(
						'name'=>'Central',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Eastern' => array(
						'name'=>'Eastern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Southern' => array(
						'name'=>'Southern',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					),
					'Western' => array(
						'name'=>'Western',
						'watch_time_array'=>array(),
						'watch_time_summary'=>0,
						'devices'=>array(),
						'total_devices'=>0
					)
				);


				$channels_id = $value->id;

				$query = $this->db->select($dailyDeviceTable.".devices_id,count(".$dailyDeviceTable.".id) as count_devices,SUM(".$dailyDeviceTable.".total_seconds) as sum_seconds")
				->from($dailyDeviceTable)
				->join('rating_data_daily',$dailyDeviceTable.".rating_data_daily_id = rating_data_daily.id")
				->where('rating_data_daily.channels_id',$value->id)
				->where('CONVERT(char(10), '.$dailyDeviceTable.'.created,126) = ',$date_report)
				->group_by($dailyDeviceTable.'.devices_id')
				->get();

				if($query->num_rows() > 0){
					foreach ($query->result() as $key => $value) {
			                # code...
						/* get province region from device_addresses*/
						$qAddress = $this->db->select('devices_id,province_region')
						->from('device_addresses')->where('devices_id',$value->devices_id)
						->get();

						if($qAddress->num_rows() > 0){
							$rowAddress = $qAddress->row();

							if(in_array($rowAddress->province_region, $arrRegion)){
								array_push($arrRegionData[$rowAddress->province_region]['devices'], $value->devices_id);

								array_push($arrRegionData[$rowAddress->province_region]['watch_time_array'], $value->sum_seconds?$value->sum_seconds:0);
							}

						}
					}

					foreach ($arrRegionData as $key => $value) {
			                # code...
						$arrRegionData[$key]['total_devices'] = count($arrRegionData[$key]['devices']); 
						$arrRegionData[$key]['watch_time_summary'] = (count($arrRegionData[$key]['watch_time_array']) > 0)?array_sum($arrRegionData[$key]['watch_time_array']):0;
					}


					foreach ($arrRegionData as $key => $value) {
			                # code...
						$arrRegionData[$key]['rating'] = $this->getRegionRating(array(
							'total_devices'=>$value['total_devices']
						));
						$arrRegionData[$key]['watch_time']=$this->getRegionWatchTime($value['watch_time_summary']);
					}

			            //print_r($arrRegionData);
			        } // eof if query

			        foreach ($arrRegionData as $key => $value) {
			        	# code...

			        	/* check exist row */
			        	$checkExist = $this->db->select('*')
			        	->from('channel_audience_region_summary_rv1')
			        	->where('date',$date_report)
			        	->where('channels_id',$channels_id)
			        	->where('province_region',@$value['name'])
			        	->get();
			        	/* eof check exist row */

			        	if($checkExist->num_rows() <= 0){

			        		/* insert data into channel_audience_region */
			        		$this->db->insert('channel_audience_region_summary_rv1',[
			        			'date'=>$date_report,
			        			'channels_id'=>$channels_id,
			        			'province_region'=>@$value['name'],
			        			'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
			        			'sum_duration'=>@$value['watch_time_summary'],
			        			'created'=>date('Y-m-d H:i:s')
			        		]);
			        		/* eof insert data into channel_audience_region */
			        	}else{

			        		$rowExist = $checkExist->row();

			        		$this->db->update('channel_audience_region_summary_rv1',[
			        			'rating'=>(@$value['rating'])?(float)@$value['rating']:0,
			        			'sum_duration'=>@$value['watch_time_summary'],
			        			'updated'=>date('Y-m-d H:i:s')
			        		],['id'=>$rowExist->id]);
			        	}

			        }
			        unset($arrRegionData);





			} //eof foreach




		} //eof if condition 







	}



	public function recheckDeviceAddressesWithoutAddress(){

		set_time_limit(0);

		$api_key = "";

		$getIpStackAPIKEy = $this->db->select('*')
		->from('ipstack_api')
		->where('over_limit_status',0)
		->where('active',1)
		->limit(1)
		->get();

		if($getIpStackAPIKEy->num_rows() > 0){
			//print_r($getIpStackAPIKEy->row());
			$api_key = $getIpStackAPIKEy->row()->api_key;

		}

		$query_device_addresses = $this->db->select('*,device_addresses.id as addresses_id')
		->from('device_addresses')
		->join('devices','device_addresses.devices_id = devices.id')
		->where('device_addresses.continent_code',NULL)
		->not_like('devices.ip_address','192.168')
		->get();

		//print_r($query_device_addresses->result());exit;

		if($query_device_addresses->num_rows() > 0){

			foreach ($query_device_addresses->result() as $key => $value) {
				# code...

				//print_r($value);

				$url = 'http://api.ipstack.com/'.$value->ip_address.'?access_key='.$api_key.'&format=1';

				$curl = curl_init();
		        // Set some options - we are passing in a useragent too here
				curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $url,
					CURLOPT_TIMEOUT=>10000
				));
		        // Send the request & save response to $resp
				$resp = curl_exec($curl);

		        //echo $resp;
		        // Close request to clear up some resources
				curl_close($curl);

				$response = json_decode($resp);



				/* update device addresses record */
				$this->db->update('device_addresses',[
					'continent_code'=>$response->continent_code,
					'continent_name'=>$response->continent_name,
					'country_code'=>$response->country_code,
					'country_name'=>$response->country_name,
					'region_code'=>$response->region_code,
					'region_name'=>$response->region_name,
					'city'=>$response->city,
					'zip'=>$response->zip,
					'latitude'=>(!$value->latitude)?$response->latitude:$value->latitude,
					'longitude'=>(!$value->longitude)?$response->longitude:$value->longitude,
					'location'=>json_encode($response->location),
					'province_region'=>($response->country_name == 'Thailand')?$this->getProvinceRegionByZipCode(['zipcode'=>$response->zip]):NULL,
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$value->addresses_id]);


		        	// print_r($value);
		        	// exit;

				/* eof update device addresses record */



		        //print_r($response);exit;

		        //print_r($response);

			}
		}

	}


	public function checkDeviceUsersFromS3Remoteservice(){


		$this->__connectS3Application();

		//exit;

		/* get all devices is has_user_status equal zero */
		$query = $this->db->select('id,ship_code,has_user_status')
		->from('devices')
		->where('has_user_status',0)
		->order_by('NEWID()')
		->limit(500)
		->get();
		/* eof get all devices is has_user_status equal zero */

		if($query->num_rows() > 0){
			$count_nochipcode = 0;

			foreach ($query->result() as $key => $row) {
				# code...
				//print_r($value);
				$arrReturn = [];

				/* get data from s3 application db */
				$arrReturn = $this->getS3ApplicationUserDataByChipCode([
					'chip_code'=>$row->ship_code
				]);

				//print_r($arrReturn);
				//print_r('===================================');
				if($arrReturn['user_login_type'] == 'guest'){
				//	echo "\r\n guest \r\n";
				}else if($arrReturn['user_login_type'] == 'member'){
				//	echo "\r\n member \r\n";
				}
				//print_r($arrReturn);
				//print_r("////////////////////////////////////\r\n");
				
				

				if($arrReturn['has_user_device']){
					// print_r('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB');
					// print_r($arrReturn);
					// print_r("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB\r\n");
					$result_row = $arrReturn['result_row'];

					$queryDevices = $this->db->select('*')
					->from('device_users')
					->where('devices_id',$row->id)
					->where('type',trim($arrReturn['user_login_type']))
					->where('users_id',$result_row->users_id)
					->get();

					//echo $this->db->last_query()."<br>";
					if($queryDevices->num_rows() <= 0){

						$dob = ($result_row->dob || $result_row->dob != '')?$result_row->dob->format('Y-m-d'):'';

						$strInsert = "insert into device_users(
						devices_id,
						chip_code,
						type,
						users_id,
						firstname,
						lastname,
						gender,
						age,
						dob,
						os,
						os_version,
						device_type,
						created
						)values(
						'".$row->id."',
						'".$row->ship_code."',
						'".$arrReturn['user_login_type']."',
						'".$result_row->users_id."',
						'".$result_row->firstname."',
						'".$result_row->lastname."',
						'".$result_row->gender."',
						'".$result_row->age."',
						'".$dob."',
						'".$result_row->os."',
						'".$result_row->os_version."',
						'".$result_row->device_type."',
						'".date('Y-m-d H:i:s')."'
					)";

					$this->db->query($strInsert);

					$this->db->query("update devices set has_user_status = 1 where id = '".$row->id."'");

				}
			}
				// if($arrReturn['has_user_device']){
				// 	$result_row = $arrReturn['result_row'];



				// 	$queryCheck = $this->db->select('devices_id,users_id,type')
				// 		->from('device_users')
				// 		->where('devices_id',$row->id)
				// 		->where('type',trim($result_row->type))
				// 		->where('users_id',$result_row->users_id)
				// 		->get();

				// 	//echo $this->db->last_query();
				// 	if($queryCheck->num_rows() <= 0){
				// 		$user_type = "";
				// 		$user_type = trim($result_row->type);
				// 		//print_r($result_row->dob->format('Y-m-d'));exit;
				// 		$this->db->insert('device_users',[
				// 			'devices_id'=>$row->id,
				// 			'type'=>$user_type,
				// 			'users_id'=>$result_row->users_id,
				// 			'firstname'=>$result_row->firstname,
				// 			'lastname'=>$result_row->lastname,
				// 			'gender'=>$result_row->gender,
				// 			'age'=>$result_row->age,
				// 			'dob'=>($result_row->dob)?$result_row->dob->format('Y-m-d'):NULL,
				// 			'os'=>$result_row->os,
				// 			'os_version'=>$result_row->os_version,
				// 			'device_type'=>$result_row->device_type,
				// 			'created'=>date('Y-m-d H:i:s')
				// 		]);
				// 	}
				// }

		}
	}


}

public function setChannelDailyDevicesSummary(){
	set_time_limit(0);


		/* set channel daily summar for 
		channel_daily_date_summary
		channel_dialy_period_summary
		*/

		$date_report = "";

		$date  = new DateTime();
		$interval = new DateInterval('P1D');
		$date->sub($interval); 

		$date_report = (isset($_GET['date_report']))?$_GET['date_report']:$date->format('Y-m-d');
		$rating_data_daily_devices_table = 'rating_data_daily_devices_'.date('Y',strtotime($date_report)).'_'.date('n',strtotime($date_report));
		$channel_daily_devices_summary_table = 'channel_daily_devices_summary_'.date('Y',strtotime($date_report)).'_'.date('n',strtotime($date_report));

		/* runcheck already create table channel daily devices summary*/
		$this->createChannelDailyDevicesSummaryTable([
			'month'=>date('n',strtotime($date_report)),
			'year'=>date('Y',strtotime($date_report))
		]);




		/* foreach all channels */
        //$query = $this->db->select('id,')
		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();

		if($channels->num_rows() > 0){
			foreach ($channels->result() as $key => $value) {
				//print_r($value);
				$rating_data_daily = $this->db->select('id,date,channels_id')
				->from('rating_data_daily')
				->where('date',$date_report)
				->where('channels_id',$value->id)
				->get();
				if($rating_data_daily->num_rows() > 0){
					//print_r($rating_data_daily->row());
					/* get all device id by rating_data_daily_id*/
					$queryAllChipCode = $this->db->select($rating_data_daily_devices_table.'.devices_id')
					->from($rating_data_daily_devices_table)
					->join('device_users','device_users.devices_id = '.$rating_data_daily_devices_table.'.devices_id')
					->where($rating_data_daily_devices_table.'.rating_data_daily_id',$rating_data_daily->row()->id)
					->group_by($rating_data_daily_devices_table.'.devices_id')
					->get();

					//echo $this->db->last_query();

					$arrRecord = array();
					if($queryAllChipCode->num_rows() > 0){
						//print_r($queryAllChipCode->result());
						foreach ($queryAllChipCode->result() as $k => $v) {
							# code...
							$device_users_data = $this->getDeviceUserDataByDevicesId([
								'devices_id'=>$v->devices_id
							]);
							if($device_users_data){	

								// array_push($arrRecord,[
								// 	'devices_id'=>$v->devices_id,
								// 	'channels_id'=>$value->id,
								// 	'chip_code'=>$device_users_data->chip_code,
								// 	'gender'=>$device_users_data->gender,
								// 	'age'=>$device_users_data->age,
								// 	'os'=>$device_users_data->os,
								// 	'os_version'=>$device_users_data->os_version,
								// 	'device_type'=>$device_users_data->device_type
								// ]);

								/* check already exist record */
								$queryCheckExist = $this->db->select('devices_id,channels_id')
								->from($channel_daily_devices_summary_table)
								->where('devices_id',$v->devices_id)
								->where('channels_id',$value->id)
								->get();
								/* eof check already exist record */

								if($queryCheckExist->num_rows() <= 0){
									$this->db->insert($channel_daily_devices_summary_table,[
										'devices_id'=>$v->devices_id,
										'channels_id'=>$value->id,
										'chip_code'=>$device_users_data->chip_code,
										'gender'=>$device_users_data->gender,
										'age'=>$device_users_data->age,
										'os'=>$device_users_data->os,
										'os_version'=>$device_users_data->os_version,
										'device_type'=>$device_users_data->device_type,
										'created'=>date('Y-m-d H:i:s')
									]);
								}

							}
						}

					}


					
					print_r($arrRecord);
				}
				

			}

		}
	}

	public function moveOutOfRankDevicesToCenter(){

		$this->load->helper([
			'our'
		]);

		$province_id = $this->input->get('province_id');

		if(!$province_id){
			echo json_encode([
				'status'=>false,
				'result_desc'=>'Please input province_id'
			]);exit;
		}


		$query = $this->db->select('TOP (1000) device_addresses.id as device_addresses_id,device_addresses.devices_id,device_addresses.region_name,device_addresses.latitude as address_latitude,device_addresses.longitude as address_longitude,provinces.id,provinces.ipstack_province_name,provinces.latitude as provinces_latitude,provinces.longitude as provinces_longitude')
		->from('device_addresses')
		->join('provinces','provinces.ipstack_province_name = device_addresses.region_name')
		->where('provinces.id',$province_id)
		->where('device_addresses.tmp_latlon_status',0)
		->order_by('NEWID()')
		->get();

		// echo $this->db->last_query();exit;

		
		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$distance = calculateDistance($value->address_latitude,$value->address_longitude,$value->provinces_latitude,$value->provinces_longitude,'K');
				
				$distance = (float)$distance;
				if($distance >= 100){

					// print_r($distance);
					print_r($value);
					/* update into device_addresses table */
					$this->db->update('device_addresses',[
						'tmp_latitude'=>$value->provinces_latitude,
						'tmp_longitude'=>$value->provinces_longitude,
						'tmp_latlon_status'=>1,
						'updated'=>date('Y-m-d H:i:s')
					],['id'=>$value->device_addresses_id]);
					/* eof update into device_addresses table */
				}

			}
		}



		$this->runCheckAccuracyOfDevicesHasConfirmLatitudeLongitude();
		//echo 'aaa';
	}


	public function runRecheckDevicesLatitudeLongitudeZero(){
		$query = $this->db->select('TOP (5) *')
		->from('device_addresses')
		->where('device_addresses.latitude','0.000000')
		->where('device_addresses.longitude','0.000000')
		->order_by('NEWID()')
		->get();

		//echo $this->db->last_query();exit;

		if($query->num_rows() > 0){
			
			foreach ($query->result() as $key => $value) {
				# code...
				$ipstack_api_key = $this->getRandomIpStackApiKey();
				$last_ipaddress = $this->getLastIpAddressByDevicesId([
					'devices_id'=>$value->devices_id
				]);

				if($ipstack_api_key && $last_ipaddress){

					print_r($value);

					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_URL => 'http://api.ipstack.com/'.$last_ipaddress.'?access_key='.$ipstack_api_key,
						CURLOPT_TIMEOUT=>10000
					));
					$resp = curl_exec($curl);

					curl_close($curl);

					$response = json_decode($resp);

					if($response){

						$province_region = "";
						$queryProvinceRegion = $this->db->select('ipstack_province_name,province_region')
						->from('provinces')
						->where('ipstack_province_name',$response->region_name)
						->get();

						if($queryProvinceRegion->num_rows() > 0){
							$province_region = $queryProvinceRegion->row()->province_region;
						}

						/* update device_addresses */

						$this->db->update('device_addresses',[
							'continent_code'=>$response->continent_code,
							'continent_name'=>$response->continent_name,
							'country_code'=>$response->country_code,
							'country_name'=>$response->country_name,
							'region_code'=>$response->region_code,
							'region_name'=>$response->region_name,
							'city'=>$response->city,
							'zip'=>$response->zip,
							'latitude'=>$response->latitude,
							'longitude'=>$response->longitude,
							'location'=>json_encode($response->location),
							'tmp_latitude'=>($response->latitude != '0.000000')?'':$value->tmp_latitude,
							'tmp_longitude'=>($response->longitude != '0.000000')?'':$value->tmp_longitude,
							'tmp_latlon_status'=>($response->latitude != '0.000000' && $response->longitude != '0.000000')?0:$value->tmp_latlon_status,
							'updated'=>date('Y-m-d H:i:s'),
							'province_region'=>$province_region
						],['devices_id'=>$value->devices_id]);


					}


				        // print_r($response);


				}

			}

		}
	}

	public function setChannelShowLatitudeLongitudeJsonByChannelId(){

		if(!isset($_GET['province_id'])){
			echo json_encode([
				'status'=>false,
				'result_desc'=>'Please fill provinces id'
			]);exit;
		}


		if(!isset($_GET['type'])){
			echo json_encode([
				'status'=>false,
				'result_desc'=>'Please request type active or all'
			]);exit;
		}

		$requestCriteria = new StdClass();
		$requestCriteria->province_id = $_GET['province_id'];
		$requestCriteria->type = $_GET['type'];

		//print_r($requestCriteria);

		if(strtolower($requestCriteria->type) == 'all'){
			$query = $this->db->select('device_addresses.devices_id,
				device_addresses.latitude,
				device_addresses.longitude,
				device_addresses.tmp_latitude,
				device_addresses.tmp_longitude,
				device_addresses.tmp_latlon_status')
			->from('device_addresses')
			->join('provinces','device_addresses.region_name = provinces.ipstack_province_name')
			->where('provinces.id',$requestCriteria->province_id)
			->where('device_addresses.latitude <>','0.000000')
			->where('device_addresses.longitude <>','0.000000')
			->where('device_addresses.latitude <>','0')
			->where('device_addresses.longitude <>','0')
			->get();


		}else if(strtolower($requestCriteria->type) == 'active'){
			$current_datetime = new DateTime();
			$current_datetime->sub(new DateInterval('PT5H'));

			$current_rating_data_daily_devices_table = 'rating_data_daily_devices_'.date('Y').'_'.date('n');

			$query = $this->db->select(',
				device_addresses.devices_id,
				device_addresses.latitude,
				device_addresses.longitude,
				device_addresses.tmp_latitude,
				device_addresses.tmp_longitude,
				device_addresses.tmp_latlon_status')
			->from('device_addresses')
			->join('provinces','device_addresses.region_name = provinces.ipstack_province_name')
			->join($current_rating_data_daily_devices_table,$current_rating_data_daily_devices_table.'.devices_id = device_addresses.devices_id')
			->where('provinces.id',$requestCriteria->province_id)
			->where($current_rating_data_daily_devices_table.'.updated >=',$current_datetime->format('Y-m-d H:i:s'))
			->where('device_addresses.latitude <>','0.000000')
			->where('device_addresses.longitude <>','0.000000')
			->where('device_addresses.latitude <>','0')
			->where('device_addresses.longitude <>','0')
			->get();
		}

		//echo $this->db->last_query();exit;

		$arrData = [];

		foreach ($query->result() as $key => $value) {
            # code...
			array_push($arrData, [
				'latitude'=>($value->tmp_latlon_status)?$value->tmp_latitude:$value->latitude,
				'longitude'=>($value->tmp_latlon_status)?$value->tmp_longitude:$value->longitude
			]);
		}

		$json_data = [
			'status'=>true,
			'TotalDevices'=>$query->num_rows(),
			'Devices'=>$arrData,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];



		$fp = fopen('./assets/json/province_'.$requestCriteria->province_id.'_'.$requestCriteria->type.'.json', 'w');
		fwrite($fp, json_encode($json_data));
		fclose($fp);

		echo json_encode([
			'status'=>true,
			'result_code'=>'000'
		]);





	}

	public function shoppingUser(){
		$query = $this->db->select('*,user.id as user_id')
		->from('user')
		->join('channels','user.owner_channel_id = channels.id')
		->where('channels.channel_categories_id <>',12)
		->get();

		echo $this->db->last_query();exit;
		

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$this->db->update('user',[
					'show_hide_channels_ranking'=>0,
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$value->user_id]);	

			}
		}
	}


	public function setUpdateChannelDailyPeriodSummary(){

		$time = (isset($_GET['str_time']))?$_GET['str_time']:((int)date('H')-1).':00';

		//echo $time;

		$on_hour = substr($time, 0,2);

		$start_time = $on_hour.':00';
		$end_time = $on_hour.':59';

		//echo $start_time.'<br>'.$end_time;exit;

		$queryChannel = $this->db->select('id,active')
		->from('channels')
		->where('active',1)
		->get();


		/* fetch all channel */

		$arrChannels = [];
		foreach ($queryChannel->result() as $key => $row) {
			# code...
			/* get channel daily rating id */
			$queryDataDailyRating = $this->db->select('*')
			->from('channel_daily_rating')
			->where('date',date('Y-m-d'))
			->where('channels_id',$row->id)
			->get();

			if($queryDataDailyRating->num_rows() > 0){
				$channel_daily_rating_id = $queryDataDailyRating->row()->id;

				$queryAVG = $this->db->select('AVG(rating) as average_rating')
				->from('channel_daily_rating_logs')
				->where('channel_daily_rating_id',$channel_daily_rating_id)
				->where('created >= ',date('Y-m-d').' '.$start_time)
				->where('created <= ',date('Y-m-d').' '.$end_time)
				->get();
			    // array_push($arrChannels, [
			    // 	'channels_id'=>$row->id,
			    // 	'date'=>date('Y-m-d'),
			    // 	'time'=>$start_time,
			    // 	'rating'=>$queryAVG->row()->average_rating
			    // ]);

				/* check exist and insert  */
				$queryCheckExist = $this->db->select('*')
				->form('channel_daily_period_summary')
				->where('channels_id',$row->id)
				->where('date',date('Y-m-d'))
				->where('time',$start_time)
				->get();

				if($queryCheckExist->num_rows() <= 0){
					$this->db->insert('channel_daily_period_summary',[
						'channels_id'=>$row->id,
						'date'=>date('Y-m-d'),
						'time'=>$start_time,
						'rating'=>$queryAVG->row()->average_rating,
						'created'=>date('Y-m-d H:i:s')
					]);
				}
				/* eof check exist record and insert into database */

			}



		}

		//print_r($arrChannels);exit;

		





		

	}

	public function reGetDeviceLocationByLatitudeLongitude(){
		$latitude = $this->input->get('latitude');
		$longitude = $this->input->get('longitude');


		





	}

	public function dubplicateChannelDailyRatingLogs(){
		$from_datetime_period = @$this->input->get('from_datetime_period');
		$to_date = @$this->input->get('to_date');
		$channels_id = @$this->input->get('channels_id');


		$ex_from = explode(' - ', $from_datetime_period);
		
		$from_startdatetime = $ex_from[0];
		$from_enddatetime = $ex_from[1]; 

		$from_date = new DateTime($ex_from[0]);
		$from_date_txt = $from_date->format('Y-m-d');

		
		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();

		if($channels->num_rows() > 0){
			foreach ($channels->result() as $key => $value) {

				$channels_id = $value->id;

				$rowQuery = $this->db->select('*,channel_daily_rating_logs.created as log_created,channel_daily_rating_logs.rating as rating_logs')
				->from('channel_daily_rating_logs')
				->join('channel_daily_rating','channel_daily_rating_logs.channel_daily_rating_id = channel_daily_rating.id')
				->where('channel_daily_rating_logs.created >=',$from_startdatetime)
				->where('channel_daily_rating_logs.created <=',$from_enddatetime)
				->where('channel_daily_rating.date',$from_date_txt)
				->where('channels_id',$channels_id)
				->get();




				if($rowQuery->num_rows() > 0){
					foreach ($rowQuery->result() as $k => $v) {
					# code...
						$channel_daily_rating_id = $this->getChannelDailyRatingIdByChannelsAndDate([
							'channels_id'=>$channels_id,
							'date'=>$to_date
						]);

					// print_r($v->created);
						$new_created = $to_date.' '.date('H:i',strtotime($v->log_created));


						$arrInsert = [
							'channel_daily_rating_id'=>$channel_daily_rating_id,
							'rating'=>$v->rating_logs,
							'reach_devices'=>$v->reach_devices,
							'created'=>$new_created
						];

					// print_r($arrInsert); 
					// insert into date to 
						$this->db->insert('channel_daily_rating_logs',$arrInsert);

					}
				}
			}
		}




	}

	public function dubplicateChannelDailyRatingLogsWithChannelsId(){
		$from_datetime_period = @$this->input->get('from_datetime_period');
		$to_date = @$this->input->get('to_date');
		$channels_id = @$this->input->get('channels_id');


		$ex_from = explode(' - ', $from_datetime_period);
		
		$from_startdatetime = $ex_from[0];
		$from_enddatetime = $ex_from[1]; 

		$from_date = new DateTime($ex_from[0]);
		$from_date_txt = $from_date->format('Y-m-d');

		$rowQuery = $this->db->select('*,channel_daily_rating_logs.created as log_created,channel_daily_rating_logs.rating as rating_logs')
		->from('channel_daily_rating_logs')
		->join('channel_daily_rating','channel_daily_rating_logs.channel_daily_rating_id = channel_daily_rating.id')
		->where('channel_daily_rating_logs.created >=',$from_startdatetime)
		->where('channel_daily_rating_logs.created <=',$from_enddatetime)
		->where('channel_daily_rating.date',$from_date_txt)
		->where('channels_id',$channels_id)
		->get();




		if($rowQuery->num_rows() > 0){
			foreach ($rowQuery->result() as $k => $v) {
					# code...
				$channel_daily_rating_id = $this->getChannelDailyRatingIdByChannelsAndDate([
					'channels_id'=>$channels_id,
					'date'=>$to_date
				]);

					// print_r($v->created);
				$new_created = $to_date.' '.date('H:i',strtotime($v->log_created));


				$arrInsert = [
					'channel_daily_rating_id'=>$channel_daily_rating_id,
					'rating'=>$v->rating_logs,
					'reach_devices'=>$v->reach_devices,
					'created'=>$new_created
				];

				print_r($arrInsert); 
					// insert into date to 
					// $this->db->insert('channel_daily_rating_logs',$arrInsert);

			}
		}


	}


	public function setMiddleValueGraph(){

		$date_report = @$this->input->get('date_report');

		$center_datetime = @$this->input->get('center_datetime');

		$datetime_middle = new DateTime($date_report.' '.$center_datetime);

		$minus_onehour = new DateTime($date_report.' '.$center_datetime);
		$plus_onehour = new DateTime($date_report.' '.$center_datetime);

		$minus_onehour->sub(new DateInterval('PT1H'));
		$plus_onehour->add(new DateInterval('PT1H'));

		

		$channels = $this->db->select('id,active,channel_extended_status')
		->from('channels')
		->where('active',1)
		->where('channel_extended_status',0)
		->get();

		foreach ($channels->result() as $key => $value) {
			# code...
			$getChannelDailyRatingId = $this->db->select('id,channels_id,date')
			->from('channel_daily_rating')
			->where('channels_id',$value->id)
			->where('date',$date_report)
			->get();

			if($getChannelDailyRatingId->num_rows() > 0){
				$channel_daily_rating_id = $getChannelDailyRatingId->row()->id;

				$avgBefore = $this->db->select('AVG(rating) as avg_rating')
				->from('channel_daily_rating_logs')
				->where('channel_daily_rating_id',$channel_daily_rating_id)
				->where('created >= ',$date_report.' 08:00')
				->where('created <= ',$date_report.' 09:00')
				->get();

				$avgAfter = $this->db->select('AVG(rating) as avg_rating')
				->from('channel_daily_rating_logs')
				->where('channel_daily_rating_id',$channel_daily_rating_id)
				->where('created >= ',$date_report.' 10:00')
				->where('created <= ',$date_report.' 11:00')
				->get();

				$avg_before = $avgBefore->row()->avg_rating;
				$avg_after = $avgAfter->row()->avg_rating;

				$avg = (($avg_before+$avg_after)/2);

				// update all record 9:00 - 10:00 to avg value
			}
		}




	}

	public function runUpdateAllDevicesFromSQLSERVERTOMYSQL(){

		
		$queryDevices = $this->db->select('id,ship_code,active')
		->from('devices')
		->where('active',1)
		->order_by('NEWID()')
		->limit(20000)
		->get();

		$this->connectS3WebserverMYSQLRating();



		foreach ($queryDevices->result() as $key => $row) {
			
			$query_txt = 'select chipcode from devices where chipcode = "'.$row->ship_code.'" and ref_id = "'.$row->id.'"';

			$result = $this->conn_mysql->query($query_txt);

			if($result->num_rows <= 0){ 
				$txtInsert = "insert into devices(
				ref_id,
				chipcode,
				created
				)values(
				'".$row->id."',
				'".$row->ship_code."',
				'".date('Y-m-d H:i:s')."'
			)";

			$this->conn_mysql->query($txtInsert);

		}

	}


}

public function setUpdateTrueSelect(){

	$year = '2020';
	$month = '01'; 


	$channels_id = 519;
	$month_to = '03';

	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$today = strtotime(date("Y-m-d")); 
	for ($i = 0; $i < $num; $i++){ 

		$current_day = $i+1; 

		$current_day = ($current_day < 10)?'0'.$current_day:$current_day;

		$current_datetime = new DateTime($year.'-'.$month.'-'.$current_day); 

		$query = $this->db->select('*')
		->from('channel_daily_rating')
		->where('channels_id',519)
		->where('date',$current_datetime->format('Y-m-d'))
		->get();

		if($query->num_rows() > 0){
			$row = $query->row(); 

			$insert_id = $this->setCreateChannelDailyRatingTemp([
				'datetime_to'=>new DateTime($year.'-'.$month_to.'-'.$current_day),
				'row_data'=>$row,
				'channels_id'=>$channels_id
			]); 




			$arrChannelDailyRatingLogs = $this->getChannelDailyRatingLogsByChannelDailyRating([
				'channel_daily_rating_id'=>$row->id
			]); 


			$this->setCreateChannelDailyRatingLogTemp([
				'channel_daily_rating_id'=>$insert_id,
				'arr_data'=>$arrChannelDailyRatingLogs
			]);

	    		//print_r($arrChannelDailyRatingLogs);
		}

	    	//print_r($current_datetime);

	}


}

public function setUpdateRatingDataDaily(){ 

	$year = '2020';
	$month = '01'; 


	$channels_id = 519;
	$month_to = '03';

	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$today = strtotime(date("Y-m-d")); 
	for ($i = 0; $i < $num; $i++){ 

		$current_day = $i+1; 

		$current_day = ($current_day < 10)?'0'.$current_day:$current_day;

		$current_datetime = new DateTime($year.'-'.$month.'-'.$current_day); 

		$query = $this->db->select('*')
		->from('rating_data_daily')
		->where('channels_id',519)
		->where('date',$current_datetime->format('Y-m-d'))
		->get();

		if($query->num_rows() > 0){
			$row = $query->row(); 

			$datetime_to = $year.'-03-'.$current_day;

			$this->db->insert('rating_data_daily',[
				'date'=>$datetime_to,
				'channels_id'=>519,
				'sum_seconds'=>$row->sum_seconds,
				'reach_devices'=>$row->reach_devices,
				'created'=>date('Y-m-d H:i:s'),
				'updated'=>date('Y-m-d H:i:s')
			]);
		}

	}

}

public function setUpdateChannelDailyDateSummary(){
	$year = '2020';
	$month = '01'; 


	$channels_id = 519;
	$month_to = '03';

	$num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$today = strtotime(date("Y-m-d")); 
	for ($i = 0; $i < $num; $i++){ 

		$current_day = $i+1; 

		$current_day = ($current_day < 10)?'0'.$current_day:$current_day;

		$current_datetime = new DateTime($year.'-'.$month.'-'.$current_day); 

		$query = $this->db->select('*')
		->from('channel_daily_date_summary')
		->where('channels_id',519)
		->where('date',$current_datetime->format('Y-m-d'))
		->get();

		if($query->num_rows() > 0){
			$row = $query->row(); 

	    		//print_r($row);

			$datetime_to = $year.'-03-'.$current_day; 

			$this->db->insert('channel_daily_date_summary',[
				'channels_id'=>519,
				'date'=>$datetime_to,
				'rating'=>$row->rating,
				'created'=>date('Y-m-d H:i:s')
			]);

		}

	}
}

public function updateProvinceRegionToBKDailyDevice(){
	$month = $this->input->get('month');
	$year = $this->input->get('year');

	$bk_table_name = 'bk_rating_data_daily_devices_'.$year.'_'.$month;

	$query = $this->db->select($bk_table_name.'.id as bk_id,'.$bk_table_name.'.province_region, device_addresses.province_region as device_province_region')
	->from($bk_table_name)
	->join('device_addresses','device_addresses.devices_id = '.$bk_table_name.'.devices_id')
	->where($bk_table_name.'.province_region',NULL)
	->limit(100000)
	->get();

	foreach($query->result() as $key => $row){
		// print_r($row);
		$this->db->update($bk_table_name,[
			'province_region'=>$row->device_province_region
		],['id'=>$row->bk_id]);
	}




}

private function getChannelDailyRatingLogsByChannelDailyRating($data = []){ 

	$arrReturn = [];
	$query = $this->db->select('*')
	->from('channel_daily_rating_logs')
	->where('channel_daily_rating_id',$data['channel_daily_rating_id'])
	->get();

	if($query->num_rows() > 0){
		foreach ($query->result() as $key => $value) {
				# code...
			array_push($arrReturn, $value);
		}
	}

	return $arrReturn;


}

private function setCreateChannelDailyRatingTemp($data = []){
	$datetime_to = $data['datetime_to'];
	$row_data = $data['row_data'];



	$this->db->insert('channel_daily_rating',[
		'channels_id'=>$data['channels_id'],
		'date'=>$datetime_to->format('Y-m-d'),
		'rating'=>$row_data->rating,
		'created'=>date('Y-m-d H:i:s'),
		'updated'=>date('Y-m-d H:i:s')
	]);

		//print_r($datetime_to);

	return $this->db->insert_id();
}

private function setCreateChannelDailyRatingLogTemp($data = []){
	$channel_daily_rating_id = $data['channel_daily_rating_id'];
	$arr_data = $data['arr_data'];  


	foreach ($arr_data as $key => $value) {
			# code... 
		$datetime_created = new DateTime($value->created);
		$this->db->insert('channel_daily_rating_logs',[
			'channel_daily_rating_id'=>$channel_daily_rating_id,
			'rating'=>$value->rating,
			'reach_devices'=>$value->reach_devices,
			'log_message'=>$value->log_message,
			'created'=>$datetime_created->format('Y').'-03-'.$datetime_created->format('d').' '.$datetime_created->format('H:i:s')
		]);
	}



}


private function getChannelDataFromChannelId($channel_id = 0){
	$query = $this->db->select('*')
	->from('channels')->where('id',$channel_id)->get();

	return $query->row();

}

private function getAdsImageByAdvertisementCampaign($data = array()){
	$arrReturn = array();

	$textQuery = "select * from advertisement_campaign_images";
	$textQuery .= " where advertisement_campaigns_id = '".$data['advertisement_campaigns_id']."'";
		//$textQuery .= " and start_datetime >= '".date('Y-m-d H:i:s')."'";
		// $textQuery .= " and end_datetime >= '".date('Y-m-d H:i:s')."'";
	$textQuery .= " and active = '1'";

	$query = $this->db->query($textQuery);
		// $query = $this->db->select('*')
		// ->from('advertisement_campaign_images')
		// ->where('advertisement_campaigns_id',$data['advertisement_campaigns_id'])
		// ->where('CONVERT(VARCHAR(10), start_datetime , 112) = ',date('Ymd'))
		// ->where('CONVERT(VARCHAR(10), end_datetime , 112) = ',date('Ymd'))
		// // ->where('start_datetime <=',date('Y-m-d'))
		// // ->where('end_datetime >=',date('Y-m-d'))
		// ->where('active',1)
		// ->get();

		//echo $this->db->last_query();exit;
		//echo $query->num_rows();exit;

	if($query->num_rows() > 0){
		foreach ($query->result() as $key => $value) {
				# code...
			array_push($arrReturn, array(
				"start_date"=>date('d-m-Y H:i',strtotime($value->start_datetime)),
				"end_date"=>date('d-m-Y H:i',strtotime($value->end_datetime)),
				"image_url"=>base_url('uploaded/advertisement/campaign_images/'.$value->id.'/'.$value->image),
				"position"=>$value->position
			));
		}


	}

	return $arrReturn;
}

private function getAdsVideoByAdvertisementCampaign($data = array()){
	$arrReturn = array();

	$query = $this->db->select('*')
	->from('advertisement_campaign_videos')
	->where('advertisement_campaigns_id',$data['advertisement_campaigns_id'])
	->where('CONVERT(VARCHAR(10), start_datetime , 112) = ',date('Ymd'))
	->get();

	if($query->num_rows() > 0){
		foreach ($query->result() as $key => $value) {
				# code...
			array_push($arrReturn, array(
				'start_time'=>date('d-m-Y H:i',strtotime($value->start_datetime)),
				'video_url'=>$value->video_url
			));
		}
	}

	return $arrReturn;

}

private function setCreateJsonFileUrl(){
	switch (ENVIRONMENT) {
		case 'development':
		$this->create_json_file_url = 'http://s3remoteservice.development/welcome/CreateAdsJsonFile';
		break;
		case 'testing':

		break;
		case 'production':
		$this->create_json_file_url = 'http://sv-rating.dltv.ac.th/welcome/CreateAdsJsonFile';
		break;

		default:
		$this->create_json_file_url = 'http://sv-rating.dltv.ac.th/welcome/CreateAdsJsonFile';
		break;
	}
}

private function getChannelMapChannelIDByRatingData($data = array()){
	$rating_data = $data['rating_data'];
	$query = $this->db->select('id,frq,sym,vdo_pid,ado_pid')
	->from('channels')
	->where('frq',$rating_data->frq)
	->where('sym',$rating_data->sym)
	->where('vdo_pid',$rating_data->vdo_pid)
	->where('ado_pid',$rating_data->ado_pid)
	->get();

	if($query->num_rows() > 0){
		return $query->row()->id;
	}else{
		return 0;
	}
}

private function getChannelRatingByChannelId($channels_id){

	/* get all devices */
	$queryAllDevice = $this->db->select('count(id) as all_devices')
	->from('devices')
	->get();

	$count_devices = $queryAllDevice->row()->all_devices;
	/* get data from */
	$reach_devices = $this->getChannelDailyReachDevices($channels_id);

	return [
		'rating'=>number_format(($reach_devices/$count_devices)*100,3),
		'reach_devices'=>$reach_devices
	];


}

private function getChannelRatingByChannelIdNewVersion($channels_id){
	/* get all devices */
	$queryAllDevice = $this->db->select('count(id) as all_devices')
	->from('devices')
	->get();

	$count_devices = $queryAllDevice->row()->all_devices;

	$reach_devices = $this->getChannelReachDevicesNewVersion([
		'channels_id'=>$channels_id
	]);

	return [
		'rating'=>number_format(($reach_devices/$count_devices)*100,3),
		'reach_devices'=>$reach_devices
	];



}
private function getChannelDailyReachDevices($channels_id){

	$query = $this->db->select('*')
	->from('rating_data_daily')
	->where('date',date('Y-m-d'))
	->where('channels_id',$channels_id)
	->get();

	if($query->num_rows() > 0){
		return $query->row()->reach_devices;
	}else{
		return 0;
	}

}

private function updateChannelDailyRating($data = array()){
	/* check already exist record */
	$queryCheck = $this->db->select('*')
	->from('channel_daily_rating')
	->where('channels_id',$data['channels_id'])
	->where('date',date('Y-m-d'))
	->get();

		//echo $this->db->last_query();exit;

	if($queryCheck->num_rows() > 0){
		/* only update rating and update channel daily rating logs */
		$this->db->update('channel_daily_rating',array(
			'rating'=>$data['rating'],
			'updated'=>date('Y-m-d H:i:s')
		),array('id'=>$queryCheck->row()->id));

		/* insert record to channel daily rating logs */
		if($data['rating'] > 0){
			$this->db->insert('channel_daily_rating_logs',array(
				'channel_daily_rating_id'=>$queryCheck->row()->id,
				'rating'=>$data['rating'],
				'reach_devices'=>$data['reach_devices'],
				'created'=>date('Y-m-d H:i:s')
			));
		}
	}else{

		/* insert new record and update channel daily rating logs */

		$this->db->insert('channel_daily_rating',array(
			'channels_id'=>$data['channels_id'],
			'date'=>date('Y-m-d'),
			'rating'=>$data['rating'],
			'created'=>date('Y-m-d H:i:s')
		));

			//echo $this->db->last_query();exit;
		$insert_id = $this->db->insert_id();
		/* insert record to channel daily rating logs */
		if($data['rating'] > 0){
			$this->db->insert('channel_daily_rating_logs',array(
				'channel_daily_rating_id'=>$insert_id,
				'rating'=>$data['rating'],
				'reach_devices'=>$data['reach_devices'],
				'created'=>date('Y-m-d H:i:s')
			));
		}
	}
}

private function getChannelComponentsByChannelId($channels_id){
	$query = $this->db->select('*')
	->from('channel_components')
	->where('channels_id',$channels_id)
	->get();

	if($query->num_rows() > 0){
		return $query;
	}else{
		return false;
	}
}

public function testtest(){
	$json = '{"status":true,"ChannelList":[{"ordinal":1,"channel_id":340,"channel_name":"PSI \u0e2a\u0e32\u0e23\u0e30\u0e14\u0e35","rating":"3.95","watch_time":"819 h 30 m 25 s"},{"ordinal":2,"channel_id":278,"channel_name":"\u0e17\u0e35\u0e27\u0e35\u0e14\u0e35\u0e42\u0e21\u0e42\u0e21\u0e481","rating":"3.38","watch_time":"36 h 22 m 42 s"},{"ordinal":3,"channel_id":264,"channel_name":"\u0e40\u0e27\u0e34\u0e23\u0e4c\u0e04\u0e1e\u0e2d\u0e22\u0e17\u0e4c","rating":"2.66","watch_time":"342 h 23 m 15 s"},{"ordinal":4,"channel_id":277,"channel_name":"\u0e1e\u0e35\u0e1e\u0e35\u0e17\u0e35\u0e27\u0e35","rating":"2.74","watch_time":"47 h 0 m 4 s"},{"ordinal":5,"channel_id":272,"channel_name":"\u0e0a\u0e48\u0e2d\u0e07\u0e27\u0e31\u0e19","rating":"2.48","watch_time":"57 h 42 m 6 s"},{"ordinal":6,"channel_id":270,"channel_name":"\u0e42\u0e21\u0e42\u0e19 29","rating":"2.54","watch_time":"26 h 22 m 28 s"},{"ordinal":7,"channel_id":284,"channel_name":"\u0e42\u0e2d\u0e0a\u0e49\u0e2d\u0e1b\u0e1b\u0e34\u0e49\u0e07","rating":"2.89","watch_time":"90 h 33 m 43 s"},{"ordinal":8,"channel_id":275,"channel_name":"\u0e2d\u0e31\u0e21\u0e23\u0e34\u0e19\u0e17\u0e23\u0e4c\u0e17\u0e35\u0e27\u0e35 ","rating":"2.42","watch_time":"97 h 1 m 17 s"},{"ordinal":9,"channel_id":281,"channel_name":"\u0e17\u0e35\u0e27\u0e35\u0e44\u0e14\u0e40\u0e23\u0e47\u0e042","rating":"2.50","watch_time":"66 h 4 m 59 s"},{"ordinal":10,"channel_id":287,"channel_name":"\u0e44\u0e2e \u0e0a\u0e49\u0e2d\u0e1b\u0e1b\u0e34\u0e49\u0e07","rating":"2.35","watch_time":"90 h 40 m 34 s"},{"ordinal":11,"channel_id":252,"channel_name":"Thai PBS","rating":"2.21","watch_time":"6 h 0 m 31 s"},{"ordinal":12,"channel_id":266,"channel_name":"\u0e08\u0e35\u0e40\u0e2d\u0e47\u0e21\u0e40\u0e2d\u0e47\u0e2125","rating":"2.44","watch_time":"14 h 54 m 16 s"},{"ordinal":13,"channel_id":269,"channel_name":"\u0e0a\u0e48\u0e2d\u0e07 3 SD","rating":"2.37","watch_time":"5 h 18 m 0 s"},{"ordinal":14,"channel_id":341,"channel_name":"ZEE NUNG","rating":"2.29","watch_time":"26 h 0 m 37 s"},{"ordinal":15,"channel_id":321,"channel_name":"Chic Station","rating":"2.14","watch_time":"3 h 26 m 24 s"},{"ordinal":16,"channel_id":390,"channel_name":"NEW TM","rating":"2.14","watch_time":"0 h 2 m 34 s"},{"ordinal":17,"channel_id":257,"channel_name":"TNN24","rating":"2.14","watch_time":"117 h 46 m 13 s"},{"ordinal":18,"channel_id":301,"channel_name":"\u0e16\u0e39\u0e01\u0e14\u0e35 TV_V2H3","rating":"2.14","watch_time":"0 h 6 m 41 s"},{"ordinal":19,"channel_id":327,"channel_name":"HIT SQUARE","rating":"2.13","watch_time":"3 h 38 m 22 s"},{"ordinal":20,"channel_id":260,"channel_name":"\u0e2a\u0e1b\u0e23\u0e34\u0e07\u0e19\u0e34\u0e27\u0e2a\u0e4c","rating":"2.09","watch_time":"0 h 54 m 19 s"}],"result_code":"000","result_desc":"Success"}';

		//print_r(json_decode($json));
	$data_decode = json_decode($json);
	$channel_list = $data_decode->ChannelList;
		// print_r($channel_list);exit;
	usort($channel_list, array($this, "cmp"));
		//usort($your_data, "cmp");
	krsort($channel_list);

	print_r($channel_list);
}

public function recheckDeviceAddressAllNull(){
	set_time_limit(0);

	$api_key = "";

	$getIpStackAPIKEy = $this->db->select('*')
	->from('ipstack_api')
	->where('over_limit_status',0)
	->where('active',1)
	->limit(1)
	->get();

	if($getIpStackAPIKEy->num_rows() > 0){
			//print_r($getIpStackAPIKEy->row());
		$api_key = $getIpStackAPIKEy->row()->api_key;

	}

	$query_recheck = "select *,device_addresses.id as addresses_id from device_addresses";
	$query_recheck .= " join devices on devices.id = device_addresses.devices_id";
	$query_recheck .= " where country_name = 'Thailand'";
	$query_recheck .= " and (device_addresses.province_region is null or device_addresses.province_region = '')";
	$query_recheck .= " and ip_address not like '192.168%'";

	$query = $this->db->query($query_recheck);

		// echo $this->db->last_query();exit;

		// if($query->get()->result());
	foreach ($query->result() as $key => $value) {
			# code...
			//print_r($value);
		$url = 'http://api.ipstack.com/'.$value->ip_address.'?access_key='.$api_key.'&format=1';

		$curl = curl_init();
		        // Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_TIMEOUT=>10000
		));
		    // Send the request & save response to $resp
		$resp = curl_exec($curl);

		    //echo $resp;
		    // Close request to clear up some resources
		curl_close($curl);

		$response = json_decode($resp);



		/* update device addresses record */
		$this->db->update('device_addresses',[
			'continent_code'=>$response->continent_code,
			'continent_name'=>$response->continent_name,
			'country_code'=>$response->country_code,
			'country_name'=>$response->country_name,
			'region_code'=>$response->region_code,
			'region_name'=>$response->region_name,
			'city'=>$response->city,
			'zip'=>$response->zip,
			'latitude'=>(!$value->latitude)?$response->latitude:$value->latitude,
			'longitude'=>(!$value->longitude)?$response->longitude:$value->longitude,
			'location'=>json_encode($response->location),
			'province_region'=>($response->country_name == 'Thailand')?$this->getProvinceRegionByZipCode(['zipcode'=>$response->zip]):NULL,
			'updated'=>date('Y-m-d H:i:s')
		],['id'=>$value->addresses_id]);

	}
		// print_r($query->result());
		// print_r($query->num_rows());exit;

}

public function testConnectS3Application(){
	$this->__connectS3Application();
}

public function runPreviousAudienceRegionSummary(){

	/* check over peak time 18.00 - 22.00 */
	$current_date = date('Y-m-d');
	$peak_time_start = new DateTime($current_date.' 18:00');
	$peak_time_end = new DateTime($current_date.' 22:00');

	$currentDateTime = new DateTime();



	$lastRecordQuery = $this->db->select('id,date,channels_id,province_region')
	->from('channel_audience_region_summary_new')
	->order_by('id','desc')
	->limit(1)
	->get();

	$rowLastRecord = $lastRecordQuery->row();

	$last_record_date = $rowLastRecord->date;

				// count last record by date
	$countDate = $this->db->select('count(id) as count_id')
	->from('channel_audience_region_summary_new')
	->where('date',$last_record_date)
	->get();

	$rowCountDate = $countDate->row();

				// print_r($rowCountDate);
	$getNextDate = new DateTime($last_record_date);
	$getNextDate->add(new DateInterval('P1D'));

	$currentDate = new DateTime();

	if($rowLastRecord->channels_id == 499 && $rowLastRecord->province_region == 'Western' && strtotime($getNextDate->format('Y-m-d')) < strtotime($currentDate->format('Y-m-d'))){

		$this->setChannelAudienceRegionSummary($getNextDate->format('Y-m-d'));
	}

	if($rowCountDate->count_id >= 1274 && strtotime($getNextDate->format('Y-m-d')) < strtotime($currentDate->format('Y-m-d'))){
					//print_r('ok run');

					// if(strtotime($getNextDate->format('Y-m-d')) >= )
		echo 'in case';


	}else{
		echo 'out of case';
	}


}

public function runPreviousAudienceRegionSummaryByDate(){
	$date_report = @$this->input->get('date_report');

	if(!$date_report){
		echo json_encode([
			'status'=>false,
			'result_desc'=>'Please enter date report'
		]);
		exit;
	}else{
		$this->setChannelAudienceRegionSummary($date_report);
	}
}

public function runPreviousAudienceRegionSummaryByDateAndChannel(){
	$date_report = @$this->input->get('date_report');
	$channel_id = @$this->input->get('channel_id');

	if(!$date_report || !$channel_id){
		echo json_encode([
			'status'=>false,
			'result_desc'=>'Please enter date report and channel_id'
		]);
		exit;
	}else{
		$this->setPreviousChannelAudienceRegionSummaryByDateAndChannel([
			'date_report'=>$date_report,
			'channel_id'=>$channel_id
		]);
	}
}

public function runCheckAccuracyOfDevicesHasConfirmLatitudeLongitude(){
	$query = $this->db->select('*')
	->from('device_addresses')
	->where('latitude <> ','0.000000')
	->where('longitude <> ','0.000000')
	->where('latitude <> ','13.75')
	->where('longitude <> ','100.4667')
	->where('latitude <> ','13.756329536438')
	->where('longitude <> ','100.50177001953')
	->where('latitude <> ','13.855560302734')
	->where('longitude <> ','100.50805664062')
	->where('latitude != ',NULL)
	->where('longitude != ',NULL)
	->where('update_from_api',1)
	->where('update_confirm_lat_lon',0)
	->where('country_name','Thailand')
		//->or_where('country_name',NULL)
	->order_by('id','desc')
	->limit(100)
	->get();

	if(@$this->input->get('test') == 'test'){
		echo $this->db->last_query();exit;
	}

	foreach ($query->result() as $key => $row) {
			# code...
		$arrDistance = $this->getAmphurDeviceDistance([
			'device_addresses'=>$row
		]);

		$keys = array_column($arrDistance, 'distance');

		array_multisort($keys, SORT_ASC, $arrDistance);

		$nearest = $arrDistance[0];

		$this->updateConfirmLatLonByAmphur([
			'arrNearest'=>$nearest,
			'device_addresses'=>$row
		]);




	}
}

public function runCreateTopTwentyJsonFile(){

	require_once(APPPATH. "libraries/api/S3RatingLibrary.php");
	$requestCriteria = new StdClass();






}

public function runCheckTransferRatingDataDailyDevicesMonthYears(){

		// get previous date
	$current_date = new DateTime();

	$previous_date = new DateTime();
	$previous_date->sub(new DateInterval('P1D')); 

	$max_id = $this->getTopChannelIdFromChannelAudienceRegionSummaryNew(); 

	if(isset($_GET['test']) && $_GET['test'] == 'test'){
		print_r($max_id);exit;
	}

	if(isset($_GET['previous_date']) && $_GET['previous_date'] != ''){
		$previous_date = new DateTime($_GET['previous_date']);
	}


		//print_r($previous_date);exit;
		// check table channel audience region summary new run finish
	$query = $this->db->select('*')
	->from('channel_audience_region_summary_new')
	->where('date',$previous_date->format('Y-m-d'))
		// ->where('channels_id',$max_id)
		// ->where('province_region','Western')
	->get();

	if($query->num_rows() > 0){

		$this->checkCreateBKRatingDataDailyDevicesMonthYears([
			'year'=>$previous_date->format('Y'),
			'month'=>$previous_date->format('n')
		]); 

		$daily_devices_table = 'rating_data_daily_devices_'.$previous_date->format('Y').'_'.$previous_date->format('n');

		$bk_daily_devices_table = 'bk_rating_data_daily_devices_'.$previous_date->format('Y').'_'.$previous_date->format('n');





		$queryBk = $this->db->select('*')
		->from($daily_devices_table)
		->where('created >=',$previous_date->format('Y-m-d 00:00'))
		->where('created < ',$current_date->format('Y-m-d 00:00'))
		->limit(70000)
		->get(); 



		if($queryBk->num_rows() > 0){


			foreach ($queryBk->result() as $key => $value) {
					# code...
					// insert data into bk daily devices table 

				$this->db->insert($bk_daily_devices_table,[
					'rating_data_daily_id'=>$value->rating_data_daily_id,
					'devices_id'=>$value->devices_id,
					'ship_code'=>$value->ship_code,
					'total_seconds'=>$value->total_seconds,
					'created'=>$value->created,
					'updated'=>$value->updated
				]);

					//echo $this->db->last_query();exit;

				$this->db->delete($daily_devices_table,[
					'id'=>$value->id
				]);
			}

		}	


	}

} 



private function cmp($a, $b){
	return strcmp($a->rating, $b->rating);
}


private function getChannelDailyRatingIDByChannelsIDAndDate($data = []){

	$query = $this->db->select('*')
	->from('channel_daily_rating')
	->where('channels_id',$data['channels_id'])
	->where('date',$data['date'])
	->limit(1)
	->get();

	if($query->num_rows() > 0){
		return $query->row()->id;
	}else{
		return 0;
	}




}

private function getAverageDailyRating($data = []){

	$queryLogs = $this->db->select('AVG(rating) as avg_rating')
	->from('channel_daily_rating_logs')
	->where('channel_daily_rating_id',$data['channel_daily_rating_id'])
	->group_by('channel_daily_rating_id')
	->get();

	if($queryLogs->num_rows() > 0){
		return $queryLogs->row()->avg_rating;
	}else{
		return 0;
	}

}

private function getRegionRating($data = array()){

	$queryAllDevice = $this->db->select('count(id) as all_devices')
	->from('devices')
	->where('CONVERT(char(10),created,126) <= ',$data['date_report'])
	->get();

        //echo $this->db->last_query();exit;

	$count_devices = $queryAllDevice->row()->all_devices;

	return number_format(($data['total_devices']/$count_devices)*100,3);

}

private function getRegionWatchTime($total_seconds){

	$hours = floor($total_seconds / 3600);
	$minutes = floor(($total_seconds / 60) % 60);
	$seconds = $total_seconds % 60;

	return $hours.' h '.$minutes.' m '.$seconds.' s';

}


private function getSchoolTypeRating($data = array()){
	//bak for production
	$queryAllDevice = $this->db->select('count(device_addresses.id) as all_devices')
	->from('devices')
	->join('device_addresses' , 'devices.id = device_addresses.devices_id')
	->where('CONVERT(char(10),device_addresses.created,126) <= ',$data['date_report'])
	->where('device_addresses.SchoolID IS NOT NULL')
	->get();

	//bak for testing
	// $queryAllDevice = $this->db->select('count(id) as all_devices')
	// ->from('devices')
	// ->get();

	$count_devices = $queryAllDevice->row()->all_devices;

	return number_format(($data['total_devices']/$count_devices)*100,3);

}
private function getSchoolTypeWatchTime($total_seconds){

	$hours = floor($total_seconds / 3600);
	$minutes = floor(($total_seconds / 60) % 60);
	$seconds = $total_seconds % 60;

	return $hours.' h '.$minutes.' m '.$seconds.' s';

}

private function getDeviceGroupRating($data = array()){
	//bak for production
	$queryAllDevice = $this->db->select('count(device_addresses.id) as all_devices')
	->from('devices')
	->join('device_addresses' , 'devices.id = device_addresses.devices_id')
	->where('CONVERT(char(10),device_addresses.created,126) <= ',$data['date_report'])
	->where('device_addresses.SchoolID IS NOT NULL')
	->get();
	//bak for testing
	// $queryAllDevice = $this->db->select('count(devices_group.id) as all_devices')
	// ->from('devices_group')
	// ->join('device_rel_group' , 'devices_group.id = device_rel_group.device_group_id' , 'left')
	// ->get();
	$count_devices = $queryAllDevice->row()->all_devices;

	return number_format(($data['total_devices']/$count_devices)*100,3);

}
private function getDeviceGroupWatchTime($total_seconds){

	$hours = floor($total_seconds / 3600);
	$minutes = floor(($total_seconds / 60) % 60);
	$seconds = $total_seconds % 60;

	return $hours.' h '.$minutes.' m '.$seconds.' s';

}

private function getProvinceRegionByZipCode($data = []){

	$query = $this->db->select('*')
	->from('zipcode')
	->join('provinces','zipcode.provinces_id = provinces.id')
	->where('zipcode.zipcode',$data['zipcode'])
	->order_by('provinces.id','asc')
	->limit(1)
	->get();

	if($query->num_rows() > 0){
		return $query->row()->province_region;
	}else{
		return null;
	}


}

private function getAnotherDeviceAddressesInfoByZipcode($data = []){

	$query = $this->db->select('*')
	->from('device_addresses')
	->where('zip',$data['zipcode'])
	->order_by('id','desc')
	->limit(1)
	->get();

	if($query->num_rows() > 0){
		return $query->row();
	}else{
		return false;
	}
}

private function getDeviceUserDataByChipCode($data = []){

	$url = "";
	switch (ENVIRONMENT) {
		case 'development':
    			# code...
		$url = 'http://s3remoteservice.development/welcome/getDeviceUserDataByChipCode';
		break;
		case 'testing':
		$url = 'http://sv-rating.dltv.ac.th/welcome/getDeviceUserDataByChipCode';
		break;
		case 'production':
		$url = 'http://sv-rating.dltv.ac.th/welcome/getDeviceUserDataByChipCode';
		break;

		default:
		$url = 'http://sv-rating.dltv.ac.th/welcome/getDeviceUserDataByChipCode';
		break;
	}

	$url .= "?chip_code=".$data['chip_code'];


	$curl = curl_init();
		        // Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $url,
		CURLOPT_TIMEOUT=>10000
	));
		    // Send the request & save response to $resp
	$resp = curl_exec($curl);

	curl_close($curl);

	return json_decode($resp);


}

private function __connectS3Application(){

	$this->load->config('api');
	$db_config = $this->config->item('s3_application_db');

	if(ENVIRONMENT == 'development'){

	    	$serverName = $db_config['development']['servername']; //serverName\instanceName
	    	$connectionInfo = array( "Database"=>$db_config['db_name'],"CharacterSet" => "UTF-8");
	    	$this->s3_application_conn = sqlsrv_connect( $serverName, $connectionInfo);


	    	if(!$this->s3_application_conn){
	    		echo "Connection could not be established.<br />";
	    		die( print_r( sqlsrv_errors(), true));
	    	}
	    }else if(ENVIRONMENT == 'testing'){
			$serverName = $db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$db_config['db_name'], "UID"=>$db_config['testing']['username'], "PWD"=>$db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->s3_application_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->s3_application_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$db_config['db_name'], "UID"=>$db_config['production']['username'], "PWD"=>$db_config['production']['password'],"CharacterSet" => "UTF-8");
			$this->s3_application_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->s3_application_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}



		}
	}


	private function getS3ApplicationUserDataByChipCode($data = []){

		$this->__connectS3Application();

		$result_row = new StdClass();
		$user_login_type = "";
		$has_user_device = false;
		/* check from guest device first */
		$queryGuest = "select TOP 1 guest_devices.guests_id,
		guest_devices.chip_code,
		guests.gender,
		guests.age,
		guests.firstname,
		guests.lastname,
		guests.gender,
		guests.age,
		guests.dob,
		guests.os,
		guests.os_version,
		guests.device_type,
		guests.id as users_id";
		$queryGuest .= " from guest_devices join guests on guest_devices.guests_id = guests.id";
		$queryGuest .= " where guest_devices.chip_code = '".$data['chip_code']."'";
		$queryGuest .= " order by guests.age desc";
		/* eof check from guest device */

    	//echo $queryGuest;exit;

		$stmt = sqlsrv_query($this->s3_application_conn, $queryGuest,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
		if(sqlsrv_num_rows($stmt) > 0){
			$has_user_device = true;
			$user_login_type = "guest";
			$result_row = sqlsrv_fetch_object($stmt);
			$result_row->{'type'} = 'guest';
		}else{
			$user_login_type = '';
		}




		/* check from member devices */

		$queryMember = "select TOP 1 member_devices.members_id,
		member_devices.chip_code,
		members.gender,
		members.age,
		members.firstname,
		members.lastname,
		members.gender,
		members.age,
		members.dob,
		members.os,
		members.os_version,
		members.device_type,
		members.id as users_id";
		$queryMember .= " from member_devices";
		$queryMember .= " join members on member_devices.members_id = members.id";
		$queryMember .= " where member_devices.chip_code = '".$data['chip_code']."'";
		$queryMember .= " order by members.age desc";
		/* eof check from member devices */

		$stmt2 = sqlsrv_query($this->s3_application_conn, $queryMember,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
		if(sqlsrv_num_rows($stmt2) > 0){
			$has_user_device = true;
			$user_login_type = "member";
			$result_row = sqlsrv_fetch_object($stmt2);
			$result_row->{'type'} = 'member';
		}else{
			$user_login_type = '';
		}

    	//return $result_row;
		return [
			'user_login_type'=>@$result_row->type,
			'has_user_device'=>$has_user_device,
			'result_row'=>$result_row
		];

	}

	private function createChannelDailyDevicesSummaryTable($data = []){

    	//print_r($data);exit;
		$table_name = 'channel_daily_devices_summary_'.$data['year'].'_'.$data['month'];

    	//echo $table_name;exit;
		$query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$table_name."'");

		if($query->num_rows() <= 0){
			/* create table for rating data */
			$strQuery = "CREATE TABLE ".$table_name." (
			id int IDENTITY(1,1) PRIMARY KEY,
			devices_id int,
			channels_id int,
			chip_code varchar(255),
			gender varchar(10),
			age int,
			os varchar(50),
			os_version varchar(50),                    
			device_type varchar(50),
			created datetime
		)";

		$this->db->query($strQuery);

	}


}

private function getDeviceUserDataByDevicesId($data = []){

	$query = $this->db->select('*')
	->from('device_users')
	->where('devices_id',$data['devices_id'])
	->get();

	if($query->num_rows() > 0){
		return $query->row();
	}else{
		return false;
	}
}

private function countAllDevicesByProvinceRegion($data = []){

	// $query = $this->db->select('count(device_addresses.id) as count_all_devices')
	// ->from('device_addresses')
	// ->where('province_region',$data['province_region'])
	// ->where('SchoolID !=','')
	// ->where('SchoolID IS NOT NULL')
	// ->get();
	$query = $this->db->select('count(device_addresses.id) as count_all_devices')
	->from('device_addresses')
	->where('province_region',$data['province_region'])
	->get();

	return $query->row()->count_all_devices;
}

private function countAllDevicesByProvinceRegionRV1($data = []){
	$query = $this->db->select('count(devices_addresses_tmp.id) as count_all_devices')
	->from('devices_addresses_tmp')
	->where('province_region',$data['province_region'])
	->get();

	return $query->row()->count_all_devices;
}
private function getChannelReachDevicesNewVersion($data = []){

	$rating_data_daily_devices_table = 'rating_data_daily_devices_'.date('Y').'_'.date('n');

	$current_datetime = new DateTime();

	$minusOneHour = $current_datetime->sub(new DateInterval('PT1H'));

	$query = $this->db->select('count('.$rating_data_daily_devices_table.'.devices_id) as count_devcies')
	->from($rating_data_daily_devices_table)
	->join('rating_data_daily','rating_data_daily.id = '.$rating_data_daily_devices_table.'.rating_data_daily_id')
	->where('rating_data_daily.channels_id',$data['channels_id'])
	->where($rating_data_daily_devices_table.'.updated >=',$current_datetime->format('Y-m-d H:i:s'))
	->group_by($rating_data_daily_devices_table.'.devices_id')
	->get();

        // echo $this->db->last_query();exit;

	return $query->num_rows();


}

public function testQuery(){
	$this->getChannelReachDevicesNewVersion([
		'channels_id'=>463
	]);
}

public function getAllChannelRating(){ 

	$arrReturn = [];

	$query = $this->db->select('*')
	->from('channels')
	->where('channel_extended_status',0)
	->where('active',1)
	->order_by('channel_name','asc')
	->get(); 

	foreach ($query->result() as $key => $value) {
    		# code...
		array_push($arrReturn, [
			'id'=>$value->id,
			'name'=>$value->channel_name
		]); 
	} 

	echo json_encode([
		'status'=>true,
		'response'=>$arrReturn
	]);


}

public function getAllDevicesAndInsertIntoMYSQL(){ 



}

public function testSendOcs(){ 

	$data = [
		'from'=>'PSIOCS',
		'to'=>'66802295577',
		'text'=>'test'
	];


	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'http://api.ants.co.th/sms/1/text/single',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS =>json_encode($data),
		CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Authorization: Basic  cHNpc21zOlBzSUAyNTYxI21TczE4"
		),
	));

	$response = curl_exec($curl);

	curl_close($curl); 

	$decode_result = json_decode($response); 
} 

public function testPostModule(){
	if($this->input->post('request') || file_get_contents('php://input')){

		$request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->input->post('request');

		print_r($request);

	}
}

public function setUpdateHitvarietyMonthFour(){

	$arrData = [];
    	// fetch all data from true select until 08.04.20 to current and save with hitvariety

    	// true select id equal 519

    	// hit variety id equal 521

    	// start date 08.04.20 to 20.04.20

	$queryFromCriteria = $this->db->select('*')
	->from('channel_daily_rating')
	->where('channels_id',519)
	->where('date >=','2020-04-08')
	->where('date <=','2020-04-20')
	->get();


	if($queryFromCriteria->num_rows() > 0){
		foreach ($queryFromCriteria->result() as $key => $value) {
    			# code...

    			// insert to channel_daily_rating with id equal 521
    			// $this->db->insert('channel_daily_rating',[
    			// 	'channels_id'=>521,
    			// 	'date'=>$value->date,
    			// 	'rating'=>$value->rating,
    			// 	'created'=>$value->created,
    			// 	'updated'=>$value->updated
    			// ]);

    			// get all channel_daily_rating_logs by id

			array_push($arrData,[
				'id'=>$value->id,
				'channels_id'=>521,
				'date'=>$value->date,
				'rating'=>$value->rating,
				'created'=>$value->created,
				'updated'=>$value->updated,
				'channel_daily_rating_logs'=>[]
			]);

			$queryLogs = $this->db->select('*')
			->from('channel_daily_rating_logs')
			->where('channel_daily_rating_id',$value->id)
			->get();

			if($queryLogs->num_rows() > 0){
				foreach ($queryLogs->result() as $k => $v) {
    					# code...
    					//print_r($v);
					array_push($arrData[$key]['channel_daily_rating_logs'], $v);
				}
			}
    			// print_r($value);
		}
	}


	foreach ($arrData as $key => $value) {
    		# code...
		$this->db->insert('channel_daily_rating',[
			'channels_id'=>$value['channels_id'],
			'date'=>$value['date'],
			'rating'=>$value['rating'],
			'created'=>$value['created'],
			'updated'=>$value['updated']
		]);

		$insert_id = $this->db->insert_id();

		foreach ($value['channel_daily_rating_logs'] as $k => $v) {
    			# code...
			$this->db->insert('channel_daily_rating_logs',[
				'channel_daily_rating_id'=>$insert_id,
				'rating'=>$v->rating,
				'reach_devices'=>$v->reach_devices,
				'log_message'=>$v->log_message,
				'created'=>$v->created
			]);
		}
	}


    	// query for check 
    	/* 

			select * from channel_daily_rating join channel_daily_rating_logs on channel_daily_rating.id = channel_daily_rating_logs.channel_daily_rating_id 
			where channel_daily_rating.channels_id = 521 
			and date >= '2020-04-08' 
			and date <= '2020-04-20'
    	*/






		}

		private function getRandomIpStackApiKey(){
			$query = $this->db->select('TOP (1) *')
			->from('ipstack_api')
			->where('over_limit_status',0)
			->where('active',1)
			->order_by('NEWID()')
			->get();

			if($query->num_rows() > 0){
				return $query->row()->api_key;
			}else{
				return false;
			}
		}

		private function getLastIpAddressByDevicesId($data = []){
			$table = 'rating_data_'.date('Y').'_'.date('n');

			$query = $this->db->select('TOP (1) devices_id,ip_address,created')
			->from($table)
			->where('devices_id',$data['devices_id'])
			->order_by('created','desc')
			->get();

			if($query->num_rows() > 0){
				return $query->row()->ip_address;
			}else{
				return false;
			}
		}

		private function getAmphurDeviceDistance($data = []){
			$this->load->helper([
				'our'
			]);
			$device_addresses = $data['device_addresses'];

			$queryAmphur = $this->db->select('id,amphur_name,amphur_name_eng,provinces_id,latitude,longitude')
			->from('amphurs')
			->get();

			$arrReturn = [];

			foreach ($queryAmphur->result() as $key => $value) {
    		# code...
				array_push($arrReturn, [
					'amphurs_id'=>$value->id,
					'amphur_name'=>$value->amphur_name,
					'amphur_name_eng'=>$value->amphur_name_eng,
					'provinces_id'=>$value->provinces_id,
					'distance'=>calculateDistance($value->latitude,$value->longitude,$device_addresses->latitude,$device_addresses->longitude,'K')
				]);

			}

			return $arrReturn;


		}

		private function updateConfirmLatLonByAmphur($data = []){
			$arrNearest = $data['arrNearest'];
			$device_addresses = $data['device_addresses'];
    	// print_r($data);
			$province = new M_provinces($data['arrNearest']['provinces_id']);

    	// print_r($province->to_array());


			$this->db->update('device_addresses',[
				'region_name'=>$province->ipstack_province_name,
				'city'=>$arrNearest['amphur_name_eng'],
				'province_region'=>$province->province_region,
				'update_confirm_lat_lon'=>1,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$device_addresses->id]);


		}


		private function getChannelDailyRatingIdByChannelsAndDate($data = []){

			$query = $this->db->select('*')
			->from('channel_daily_rating')
			->where('channels_id',$data['channels_id'])
			->where('date',$data['date'])
			->get();

			if($query->num_rows() > 0){
				return $query->row()->id;
			}else{
				return 0;
			}

		}

		private function checkCreateBKRatingDataDailyDevicesMonthYears($data = []){

    	//print_r($data);exit;
			$table_name = 'bk_rating_data_daily_devices_'.$data['year'].'_'.$data['month'];

    	//echo $table_name;exit;
			$query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$table_name."'");

			if($query->num_rows() <= 0){
				/* create table for rating data */
				$strQuery = "CREATE TABLE ".$table_name." (
				id int IDENTITY(1,1) PRIMARY KEY,
				rating_data_daily_id int,
				devices_id int,
				ship_code varchar(255),
				total_seconds int,
				created datetime,
				updated datetime
			)";

			$this->db->query($strQuery);

		}

	}

	private function connectS3WebserverMYSQLRating(){

		$rating_mysql_connect = $this->config->item('rating_mysql_connect');

    	//print_r($rating_mysql_connect);exit; 
		$connection_setting = $rating_mysql_connect[ENVIRONMENT];

    	//print_r($connection_setting);exit;

    	// Create connection
		$this->conn_mysql = new mysqli($connection_setting['servername'], $connection_setting['username'], $connection_setting['password'], $connection_setting['dbname']);
		$this->conn_mysql->set_charset("utf8");
        // Check connection
		if ($this->conn_mysql->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		} 

	} 

	private function getTopChannelIdFromChannelAudienceRegionSummaryNew(){
		$query = $this->db->select('MAX(channels_id) as max_id')
		->from('channel_audience_region_summary_new')
		->get();

		return $query->row()->max_id;
	}




	


}
