<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Schools extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();

        $str_query = "  select schools.* , provinces.province_name, school_type.SchoolType
                        from schools 
                        left join 
                        provinces on schools.ProvinceID = provinces.province_code
                        left join school_type  on schools.SchoolTypeID = school_type.id";
                    
        $exec_query = $this->db->query($str_query);

        // EOF
        // echo '<PRE>';

    	$data = [
    		'schools'=>$exec_query->result()
    	];

    	$this->template->content->view('admin/school/school_list',$data);
    	$this->template->publish();

    }

    public function editSchool($id){
    	$province = new M_schools($id);

    	if(!$province->id){
    		redirect('admin/'.$this->controller);
    	}

    	$this->__editSchool($id);
    }

    public function __editSchool($id = null){
		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js');
		
		$query_find_max =  $this->db->select('*')->from('schools')->order_by('id', 'DESC')->limit(1)->get();
		$max_SchoolID = 0;
		if($query_find_max->num_rows() > 0){
			$max_SchoolID = $query_find_max->row()->SchoolID + 1;
		
		}
		//$schools = new M_schools($id);
		if($id != null){
			$school_query = $this->db->select("schools.* , amphurs.amphur_code as amphurs_id")->from('schools')
					  ->join('districts','schools.DistrictID = districts.district_code')
					  ->join('amphurs' , 'districts.amphurs_id = amphurs.id')
					  ->where('schools.id',$id)
					  ->get();
			// $schools_result = $school_query->result();
			$schools = $school_query->row();
		
		}else{
			$schools = new M_schools($id);
		}
		$province_id = 0;
		if(isset($schools->id)){
			if(isset($schools->DistrictID)){
				$district_query = $this->db->select('district_name')->from('districts')->where('district_code', $schools->DistrictID)->get();
				$schools->DistrictName = $district_query->row()->district_name;

				$am_query = $this->db->select('amphur_name')->from('amphurs')->where('amphur_code', $schools->amphurs_id)->get();
				$schools->AmphurName = $am_query->row()->amphur_name;

				$pro_qu = $this->db->select('id')->from('provinces')->where('province_code', $schools->ProvinceID)->get();
				$province_id = $pro_qu->row()->id;
				
			}
		}
		
    	if($this->input->post(NULL,FALSE)){
			if($id == null){

					
					
			}else{

				$data = array(
					'SchoolName' => $this->input->post('SchoolName'),
					'DistrictID' => $this->input->post('DistrictID'),
					'ProvinceID' => $this->input->post('ProvinceID'),
					'SchoolTypeID' => $this->input->post('SchoolTypeID'),
					'zipcode' => $this->input->post('zip_code'),
					'Website' => $this->input->post('Website'),
					'Telephone' => $this->input->post('Telephone'),
					'Fax' => $this->input->post('Fax'),
					'Latitude' => $this->input->post('Latitude'),
					'Longitude' => $this->input->post('Longitude'),
					'distance_from_center' => $this->input->post('distance_from_center'),
					'distance_to_center_time' => $this->input->post('distance_to_center_time'),
					'Email' => $this->input->post('Email')
				);
				$this->db->where('id', $id);
				$this->db->update('schools', $data);
				## UPDATE DEVICE ADDRESS #########
				$device_rel_group = $this->db->select('device_rel_group.device_id ,devices_group.*')->from('devices_group')
				->join('device_rel_group','devices_group.id = device_rel_group.device_group_id')
				->where('devices_group.SchoolID', $schools->SchoolID)->get();
				
				if($device_rel_group->num_rows() > 0){
					foreach($device_rel_group->result() as $key => $val){
						# GET PROVINCE CODE 
						$provinces_query=  $this->db->select('*')->from('provinces')->where('province_code' , $this->input->post('ProvinceID'))->get();
						$provinces = $provinces_query->row();
						# CHANGE DEVICE ADDRESS 
						$device_addr_query = $this->db->select('id,devices_id')->from('device_addresses')
						->where('devices_id' , $val->device_id)
						->get();


						if($device_addr_query->num_rows() > 0){
							$device_address_id = $device_addr_query->row()->id;
							$data_update = array(
								'region_code'=>$provinces->province_code,
								'region_name' => $provinces->ipstack_province_name,
								'province_region' => $provinces->province_region,
								'zip' => $this->input->post('zip_code'),
								'SchoolID' => $schools->SchoolID,
								'updated' => date("Y-m-d H:i:s")
							);
							 $this->db->update('device_addresses',$data_update,array('id'=>$device_address_id));
						}

					}
				}
				
				## END OF UPDATE DEVICE ADDRESS ##
				$txtSuccess = __('Edit schools success');
				$this->msg->add($txtSuccess,'success');
				//$this->__editSchool(null);
				unset($_POST);
				redirect($this->uri->uri_string());

				// $this->msg->add($txtSuccess,'success');
				// redirect($this->uri->uri_string());

			}
    	}


    	$data = [
			'schools'=>$schools,
			'provinces' => $this->getSelectProvinceID(),
			'amphurs' => $this->getSelectAmphurs(null),
			'districts' => $this->getSelectDistrictID(null),
			'zipcode' => $this->getSelectZipcode(),
			'SchoolType' => $this->getSelectSchoolType(),

    	];

		if(!empty($province_id) && $province_id > 0){
			$data['amphurs_edit'] = $this->getSelectAmphurs($province_id);
			$data['district_edit'] = $this->getSelectDistrictID($province_id);
		}
		//  echo '<PRE>';
		//  print_r($data);exit();
		
	
		
		$this->template->content->view('admin/school/edit_school',$data);
		$this->template->javascript->add(base_url('assets/admin/js/schools/create_school.js'));
		$this->template->publish();
		
	
	}


    public function createSchool($id = null){
		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js');
		
		$query_find_max =  $this->db->select('*')->from('schools')->order_by('id', 'DESC')->limit(1)->get();
		$max_SchoolID = 0;
		if($query_find_max->num_rows() > 0){
			$max_SchoolID = $query_find_max->row()->SchoolID + 1;
		
		}
		//$schools = new M_schools($id);
		if($id != null){
			$school_query = $this->db->select("schools.* , amphurs.amphur_code as amphurs_id")->from('schools')
					  ->join('districts','schools.DistrictID = districts.district_code')
					  ->join('amphurs' , 'districts.amphurs_id = amphurs.id')
					  ->where('schools.id',$id)
					  ->get();
			// $schools_result = $school_query->result();
			$schools = $school_query->row();
		
		}else{
			$schools = new M_schools($id);
		}
		$province_id = 0;
		if(isset($schools->id)){
			if(isset($schools->DistrictID)){
				$district_query = $this->db->select('district_name')->from('districts')->where('district_code', $schools->DistrictID)->get();
				$schools->DistrictName = $district_query->row()->district_name;

				$am_query = $this->db->select('amphur_name')->from('amphurs')->where('amphur_code', $schools->amphurs_id)->get();
				$schools->AmphurName = $am_query->row()->amphur_name;

				$pro_qu = $this->db->select('id')->from('provinces')->where('province_code', $schools->ProvinceID)->get();
				$province_id = $pro_qu->row()->id;
				
			}
		}
		
    	if($this->input->post(NULL,FALSE)){
			if($id == null){

					if(!isset($schools->SchoolID)){
						$schools->SchoolID = $max_SchoolID;
					}
					
					$schools->SchoolName = $this->input->post('SchoolName');
					$schools->DistrictID = $this->input->post('DistrictID');
					$schools->ProvinceID = $this->input->post('ProvinceID');
					$schools->SchoolTypeID = $this->input->post('SchoolTypeID');
					$schools->zipcode = $this->input->post('zip_code');
					$schools->Website = $this->input->post('Website');
					$schools->Telephone = $this->input->post('Telephone');
					$schools->Fax = $this->input->post('Fax');
					$schools->Latitude = $this->input->post('Latitude');
					$schools->Longitude = $this->input->post('Longitude');
					$schools->distance_from_center = $this->input->post('distance_from_center');
					$schools->distance_to_center_time = $this->input->post('distance_to_center_time');
					$schools->Email = $this->input->post('Email');
					$schools->active = 1;

					if($schools->save()){
						
						$txtSuccess = ($id)?__('Edit schools success','admin/schools/createSchool'):__('Create schools success','admin/schools/editSchool');

						$this->msg->add($txtSuccess,'success');
						redirect($this->uri->uri_string());
					}
			}
    	}


    	$data = [
			'schools'=>$schools,
			'provinces' => $this->getSelectProvinceID(),
			'amphurs' => $this->getSelectAmphurs(null),
			'districts' => $this->getSelectDistrictID(null),
			'zipcode' => $this->getSelectZipcode(),
			'SchoolType' => $this->getSelectSchoolType(),

    	];

		if(!empty($province_id) && $province_id > 0){
			$data['amphurs_edit'] = $this->getSelectAmphurs($province_id);
			$data['district_edit'] = $this->getSelectDistrictID($province_id);
		}
		//  echo '<PRE>';
		//  print_r($data);exit();
	
		$this->template->content->view('admin/school/create_school',$data);
		$this->template->javascript->add(base_url('assets/admin/js/schools/create_school.js'));
    	$this->template->publish();
	}

	public function getSelectAmphurs($id){
		$arr_return = [];
	
		$query = $this->db->select('*')
		->from('amphurs');

		if($id != null){
			$query = $query->where("provinces_id",$id);
		}

		$result = $query->get(); 
		//echo $this->db->last_query();exit();
		if($result->num_rows() > 0){
			foreach ($result->result() as $key => $value) {
				# code...
				$arr_return[$value->amphur_code] = $value->amphur_name;
			}
		}
	
		return $arr_return;
	}

	public function getSelectProvinceID(){
		$arr_return = [];
		$arr_return[''] = __('Select Province','');
		$query = $this->db->select('*')
		->from('provinces')
		->get();
	
		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arr_return[$value->province_code] = $value->province_name;
			}
		}
	
		return $arr_return;
	}

	public function getSelectDistrictID($id){
		$arr_return = [];
		
		$query = $this->db->select('*')
		->from('districts');

		if($id != null){
			$query = $query->where("provinces_id",$id);
		}

		$result = $query->get();
	
		if($result->num_rows() > 0){
			foreach ($result->result() as $key => $value) {
				# code...
				$arr_return[$value->district_code] = $value->district_name;
			}
		}
	
		return $arr_return;
	}


	public function getSelectZipcode(){
		$arr_return = [];
		$arr_return[''] = __('Select Zipcode','');
		$query = $this->db->select('*')
		->from('amphurs')
		->get();
	
		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arr_return[$value->id] = $value->zip_code;
			}
		}
	
		return $arr_return;
	}



	public function getSelectSchoolType(){
		$arr_return = [];
		$arr_return[''] = __('Select School Affiliates','');
		$query = $this->db->select('*')
		->from('school_type')
		->get();
	
		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arr_return[$value->id] = $value->SchoolType;
			}
		}
	
		return $arr_return;
	}

	public function amphures($province_code)
    {
	  // $amphures = Amphure::where('province_id', $provice_id)->active()->get();
	  $province_query = $this->db->select('*')
	  ->from('provinces')
	  ->where('province_code',$province_code)
	  ->get();
	  $province_id = 0;
	  if($province_query->num_rows() > 0){
		   $province_id = $province_query->row()->id;
	  }
	   $amphures = $this->db->select('*')
		->from('amphurs')
		->where('provinces_id',$province_id)
		->get();
		
	  // return $amphures->result();
	    echo json_encode([
			'status'=>true,
			'post_data'=>$amphures->result(),
			'url'=>$url,
			'resp'=>$resp
		]);
    }

    public function districts($amphur_code)
    {
	   //$districts = District::where('amphure_id', $district_id)->active()->get();

	   $amphur_query = $this->db->select('*')
		->from('amphurs')
		->where('amphur_code',$amphur_code)
		->get();
		$amphur_id = 0;
		if($amphur_query->num_rows() > 0){
			 $amphur_id = $amphur_query->row()->id;
		}

		$districts = $this->db->select('districts.* , zipcode.zipcode as zip_code, zipcode.id as zip_code_id')
		->from('districts')
		->join('zipcode','districts.district_code = zipcode.districts_code')
		->where('districts.amphurs_id',$amphur_id)
		->get();
		// echo '<PRE>';
		// print_r($districts->result());exit();
		//return $districts->result();
		echo json_encode([
			'status'=>true,
			'post_data'=>$districts->result(),
			'url'=>$url,
			'resp'=>$resp
		]);
    }
	
	




	
}

