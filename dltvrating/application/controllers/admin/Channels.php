<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Channels extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $db_config = array();
	private $psirating_conn;
	public function __construct() {
                        parent::__construct();
                        $this->db_config = array(
                        	'db_name'=>'PsiRating',
                        	'production'=>array(
                        		'servername'=>'192.168.100.21',
                        		'username'=>'Dev',
                        		'password'=>'Psi@dev'
                        	)
                        );


    }

    public function index($page = null){
    	//print_r($this->session->userdata('user_data'));
    	// $this->loadDataTableStyle();
    	// $this->loadDataTableScript();

        $this->load->library([
            'pagination'
        ]);
        $this->page_num = 10;

        $this->template->javascript->add(base_url('assets/admin/js/channels/channel_list.js?'.time()));


        $channels = new M_channels();

        if(!empty($_GET)){

            if(isset($_GET['search_channel_name']) && $_GET['search_channel_name'] != ''){
                $channels->like('channel_name',$_GET['search_channel_name'],'both');
            }

            if(isset($_GET['search_frq']) && $_GET['search_frq'] != ''){
                $channels->where_related('channel_components','frq',$_GET['search_frq']);
            }

            if(isset($_GET['search_sym']) && $_GET['search_sym'] != ''){
                $channels->where_related('channel_components','sym',$_GET['search_sym']);
            }

            if(isset($_GET['search_pol']) && $_GET['search_pol'] != ''){
                $channels->where_related('channel_components','pol',$_GET['search_pol']);
            }

            if(isset($_GET['search_vdo_pid']) && $_GET['search_vdo_pid'] != ''){
                $channels->where_related('channel_components','vdo_pid',$_GET['search_vdo_pid']);
            }

            if(isset($_GET['search_ado_pid']) && $_GET['search_ado_pid'] != ''){
                $channels->where_related('channel_components','ado_pid',$_GET['search_ado_pid']);
            }

            if(isset($_GET['search_service_id']) && $_GET['search_service_id'] != ''){
                $channels->where_related('channel_components','service_id',$_GET['search_service_id']);
            }

            $this->page_num = 100;


        }


        $clone = $channels->get_clone();
        $channelsCount = $clone->count();
        $channels->order_by('channel_name','asc');

        $channels->limit($this->page_num,$page);
        $channels->get();

        //echo $channels->check_last_query();

        $this->setData('channels',$channels);
        $this->config_page($channelsCount,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url('admin/'.$this->controller.'/index/');
        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());
        $this->setData('result_count',$channelsCount);

        $data = $this->getData();



        //print_r($data);

    	// $query = $this->db->select('*')
    	// ->from('channels')
    	// ->order_by('id','asc')
    	// ->get();

    	// $data = array(
    	// 	'channels'=>$query->result()
    	// );	


    	$this->template->content->view('admin/channels/channel_list',$data);
    	$this->template->publish();
    }

    public function channel_categories(){
        $this->loadDataTableStyle();
        $this->loadDataTableScript();


        $query = $this->db->select('*')
        ->from('channel_categories')->get();

        $data = array(
            'channel_categories'=>$query->result()
        );

        $this->template->content->view('admin/channels/channel_category_lists',$data);
        $this->template->publish();
    }

    public function channel_ranking(){

        $this->template->stylesheet->add(base_url('assets/backend/css/switch/switch.css'));
        $this->loadDataTableStyle();
        $this->loadDataTableScript();

        $this->template->javascript->add(base_url('assets/admin/js/channels/channel_ranking.js'));

        $query = $this->db->select('*')
        ->from('channels')
        ->order_by('id','asc')
        ->get();

        $data = [
            'channels'=>$query->result()
        ];


        $this->template->content->view('admin/channels/channe_ranking',$data);
        $this->template->publish();
    }

    public function channel_program_chart(){

        



        $data = [

        ];

        $this->template->content->view('admin/channels/channel_program_chart',$data);
        $this->template->publish();
    }

    public function setChannelPrivilege($id){

        $this->loadDataTableStyle();
        $this->loadDataTableScript();

        $channels = new M_channels();
        $channels->where('id <> ',$id)->get();

        $data = [
            'channels'=>$channels
        ];

        $this->template->content->view('admin/channels/set_channel_privilege',$data);
        $this->template->publish();
    }

    public function setChannelProgram($channels_id,$page=null){

        $this->load->library([
            'pagination'
        ]);


        $this->loadValidator();
        // $this->loadDataTableStyle();
        $this->loadTempusDominusStyle();

        // $this->loadDataTableScript();
        $this->loadTempusDominusScript();
        $this->loadSweetAlert();


        $this->template->stylesheet->add(base_url('assets/admin/css/multiple-select-master/multiple-select.css'));

        $this->template->javascript->add(base_url('assets/admin/js/multiple-select-master/multiple-select.js'));

        $this->template->javascript->add(base_url('assets/admin/js/channels/set_channel_program.js?'.time()));

        if($this->input->post(NULL,FALSE)){
            $type = $this->input->post('type');

            switch ($type) {
                case 'action':
                    if($this->postActionEventForWeekData()){

                        if($this->input->post('action') == 'delete'){
                            $this->msg->add(__('Delete record success','default'),'success');
                            redirect($this->uri->uri_string().'?'.http_build_query(@$_GET));
                        }
                    }
                break;
                
                default:
                    # code...
                break;
            }
        }


        $channels = new M_channels($channels_id);

        if(!$channels->id){
            redirect('admin/'.$this->controller);
        }


        $channel_programs = new M_channel_programs();
        $channel_programs->where('channels_id',$channels_id);


        if(!empty($_GET)){

            if(isset($_GET['search_program_name']) && $_GET['search_program_name'] != ''){
                $channel_programs->like('name',$_GET['search_program_name'],'both');
            }

            if(isset($_GET['search_dow']) && $_GET['search_dow'] != ''){

                $channel_programs->where('date_of_week',$_GET['search_dow']);
            }


            $this->setData('criteria',$_GET);


        }



        $clone = $channel_programs->get_clone();
        $program_count = $clone->count();

        if(isset($_GET['sorting']) && $_GET['sorting'] != ''){
            if($_GET['sorting'] == 'time_asc'){
                $channel_programs->order_by('start_time','asc');
            }else if($_GET['sorting'] == 'time_desc'){
                $channel_programs->order_by('start_time','desc');
            }
            $this->setData('sorting',$_GET['sorting']);
        }else{
            $channel_programs->order_by('start_time','asc');
        }

        $channel_programs->order_by('id','desc');
        $channel_programs->limit(10,$page);
        $channel_programs->get();


        $this->setData('channel_programs',$channel_programs);
        $this->setData('program_count',$program_count);
        $clone->get();

        //echo $program_count.'<br>'.$page;exit;
        $page = (is_null($page))?1:$page;

        $this->config_page($program_count,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');

        //print_r($config_page);exit;


        $config_page['base_url'] = base_url('admin/channels/setChannelProgram/'.$channels_id.'/');


        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());


        $data = $this->getData();
        $data['channels_id'] = $channels_id;



        $this->template->content->view('admin/channels/set_channel_program',$data);
        $this->template->publish();

    }

    public function setChannelProgramBODate($channels_id=null,$page=null){

        $this->load->library([
            'pagination'
        ]);


        //echo $page;



        $this->loadValidator();
        //$this->loadDataTableStyle();
        $this->loadTempusDominusStyle();

        //$this->loadDataTableScript();
        $this->loadTempusDominusScript();
        $this->loadSweetAlert();

        $this->template->javascript->add(base_url('assets/backend/js/bootstrap-daterangepicker/moment.min.js'));

        $this->template->javascript->add(base_url('assets/admin/js/channels/set_channel_program_baseon_date.js?'.time()));



        if($this->input->post(NULL,FALSE)){
            $type = $this->input->post('type');

            switch ($type) {
                case 'action':
                    if($this->postActionEvent()){

                        if($this->input->post('action') == 'delete'){
                            $this->msg->add(__('Delete record success','default'),'success');
                            redirect($this->uri->uri_string().'?'.http_build_query(@$_GET));
                        }
                    }
                break;
                
                default:
                    # code...
                break;
            }
        }




        $channels = new M_channels($channels_id);

        if(!$channels->id){
            redirect('admin/'.$this->controller);
        }


        $channel_programs_baseon_date = new M_channel_programs_baseon_date();
        $channel_programs_baseon_date->where('channels_id',$channels_id);


        if(!empty($_GET)){

            if(isset($_GET['search_name']) && $_GET['search_name'] != ''){
                $channel_programs_baseon_date->like('name',$_GET['search_name'],'both');
            }

            if(isset($_GET['search_date']) && $_GET['search_date'] != ''){

                //print_r(urldecode($_GET['search_date']));exit;
                $exDateProgram = explode('/', $_GET['search_date']);

                $date_program = new DateTime($exDateProgram[2].'-'.$exDateProgram[1].'-'.$exDateProgram[0]);
                $channel_programs_baseon_date->where('date',$date_program->format('Y-m-d'));
            }


            $this->setData('criteria',$_GET);


        }
        


        $clone = $channel_programs_baseon_date->get_clone();
        $program_count = $clone->count();


        if(isset($_GET['sorting']) && $_GET['sorting'] != ''){
            if($_GET['sorting'] == 'time_asc'){
                $channel_programs_baseon_date->order_by('start_time','asc');
            }else if($_GET['sorting'] == 'time_desc'){
                $channel_programs_baseon_date->order_by('start_time','desc');
            }
            $this->setData('sorting',$_GET['sorting']);
        }else{
            $channel_programs_baseon_date->order_by('start_time','asc');
        }



        $channel_programs_baseon_date->order_by('id','desc');
        $channel_programs_baseon_date->limit(10,$page);
        $channel_programs_baseon_date->get();


        $this->setData('channel_programs',$channel_programs_baseon_date);
        $this->setData('program_count',$program_count);
        $clone->get();

        //echo $program_count.'<br>'.$page;exit;
        $page = (is_null($page))?1:$page;

        $this->config_page($program_count,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');

        //print_r($config_page);exit;


        $config_page['base_url'] = base_url('admin/channels/setChannelProgramBODate/'.$channels_id.'/');


        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());


        $data = $this->getData();
        $data['channels_id'] = $channels_id;





        $this->template->content->view('admin/channels/set_channel_program_baseon_date',$data);
        $this->template->publish();

    }

    public function createChannel(){
    	$this->__createChannel();
    }
    public function editChannel($channel_id){
    	$channels = new M_channels($channel_id);

    	if(!$channels->id){
    		redirect('admin/'.$this->controller);
    	}

    	$this->__createChannel($channel_id);
    }

    public function deleteChannel($channel_id){ 

        if(!in_array($this->admin_data['email'], ['jquery4me@gmail.com'])){
            redirect('admin/'.$this->controller);
        }

        
        $channels = new M_channels($channel_id);

        if(!$channels->id){
            redirect('admin/'.$this->controller);
        }


        

        /* delete from channel components*/
        $this->db->delete('channel_components',array(
            'channels_id'=>$channels_id
        ));
        /* eof delete from channel components*/


        /* delete from channel has categories */
        $this->db->delete('channel_has_categories',[
            'channels_id'=>$channels_id
        ]);
        /* eof delete from channel has categories */

        


        /* delete from channels*/
        if($this->db->delete('channels',array('id'=>$channel_id))){
            $this->msg->add(__('Delete channel success','admin/channels/channel_list'),'success');
            redirect('admin/'.$this->controller);
        }
    }
    public function ajaxUpdateShowOnRanking(){
        $post_data = $this->input->post();


        $channel = new M_channels();
        $channel->where('id',$post_data['channels_id'])->get();

        if($channel->id){
            switch ($post_data['checked_status']) {
                case 'true':
                   $channel->show_on_ranking = 1;
                break;

                case 'false':
                    $channel->show_on_ranking = 0;
                break;
                
                default:
                    # code...
                    $channel->show_on_ranking = 1;
                break;
            }

            $channel->save();

        }

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    public function ajaxSetChannelProgram(){
        $post_data = $this->input->post();

        

        if($post_data['action'] == 'create'){

            foreach ($post_data['date_of_week'] as $key => $value) {
                # code...
                // check already exist
                $check_exist = new M_channel_programs();
                $check_exist->where('channels_id',$post_data['hide_channels_id'])
                ->where('name',$post_data['name'])
                ->where('start_time',date('H:i:s',strtotime($post_data['start_time'])))
                ->where('end_time',date('H:i:s',strtotime($post_data['end_time'])))
                ->where('date_of_week',$value)
                ->get();

                if(!$check_exist->id){
                    $channel_promgrams = new M_channel_programs();
                    $channel_promgrams->channels_id = $post_data['hide_channels_id'];
                    $channel_promgrams->name = $post_data['name'];
                    $channel_promgrams->description = $post_data['description'];
                    $channel_promgrams->date_of_week = $value;
                    $channel_promgrams->start_time = date('H:i:s',strtotime($post_data['start_time']));
                    $channel_promgrams->end_time = date('H:i:s',strtotime($post_data['end_time']));
                    $channel_promgrams->active = $post_data['active'];
                    $channel_promgrams->save();

                }
            }

            

        }else{
            $channel_promgrams = new M_channel_programs($post_data['hide_channel_programs_id']);
            $channel_promgrams->name = $post_data['name'];
            $channel_promgrams->description = $post_data['description'];
            // $channel_promgrams->date_of_week = $post_data['date_of_week'];
            $channel_promgrams->start_time = date('H:i:s',strtotime($post_data['start_time']));
            $channel_promgrams->end_time = date('H:i:s',strtotime($post_data['end_time']));
            $channel_promgrams->active = $post_data['active'];
            $channel_promgrams->save();

        }
        





        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    public function ajaxSetChannelProgramBODate(){
        $post_data = $this->input->post();

        $exDateProgram = explode('/', $post_data['date']);

        $date_program = new DateTime($exDateProgram[2].'-'.$exDateProgram[1].'-'.$exDateProgram[0]);

        if($post_data['action'] == 'create'){
            $channel_programs_baseon_date = new M_channel_programs_baseon_date();
            $channel_programs_baseon_date->date = $date_program->format('Y-m-d H:i:s');
            $channel_programs_baseon_date->channels_id = $post_data['hide_channels_id'];
            $channel_programs_baseon_date->name = $post_data['name'];
            $channel_programs_baseon_date->description = $post_data['description'];
            $channel_programs_baseon_date->start_time = date('H:i:s',strtotime($post_data['start_time']));
            $channel_programs_baseon_date->end_time = date('H:i:s',strtotime($post_data['end_time']));
            $channel_programs_baseon_date->active = $post_data['active'];
            $channel_programs_baseon_date->save();



        }else{
            $channel_programs_baseon_date = new M_channel_programs_baseon_date($post_data['hide_channel_programs_id']);
            $channel_programs_baseon_date->date = $date_program->format('Y-m-d H:i:s');
            $channel_programs_baseon_date->name = $post_data['name'];
            $channel_programs_baseon_date->description = $post_data['description'];
            $channel_programs_baseon_date->start_time = date('H:i:s',strtotime($post_data['start_time']));
            $channel_programs_baseon_date->end_time = date('H:i:s',strtotime($post_data['end_time']));
            $channel_programs_baseon_date->active = $post_data['active'];
            $channel_programs_baseon_date->save();


        }

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    public function ajaxDeleteChannelProgram(){
        $post_data = $this->input->post();

        $channel_promgrams = new M_channel_programs($post_data['rowid']);

        if($channel_promgrams->id){
            $channel_promgrams->delete();
        }

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    public function ajaxAddChannelProgramRows(){
        $post_data = $this->input->post();


        $row_data = $this->load->view('admin/channels/ajaxAddChannelProgramRows',[],true);



        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data,
            'row_data'=>$row_data
        ]);
    }

    public function ajaxSetChannelProgramMultirows(){
        $post_data = $this->input->post();

        $exDateProgram = explode('/', $post_data['date']);

        $date_program = new DateTime($exDateProgram[2].'-'.$exDateProgram[1].'-'.$exDateProgram[0]);


        foreach ($post_data['multirows_name'] as $key => $value) {
            # code...
            $tv_program = new M_channel_programs_baseon_date();
            $tv_program->date  = $date_program->format('Y-m-d');
            $tv_program->channels_id = $post_data['hide_multirows_channels_id'];
            $tv_program->name = $value;
            $tv_program->start_time = $post_data['multirows_starttime'][$key];
            $tv_program->end_time = $post_data['multirows_endtime'][$key];
            $tv_program->created = date('Y-m-d H:i:s');
            $tv_program->active = 1;
            $tv_program->save();


        }

        echo json_encode([
            'status'=>true,
            'date_program'=>$date_program,
            'post_data'=>$post_data
        ]);
    }

    private function __createChannel($id=null){

    	$this->loadValidator();
        $this->loadSweetAlert();
    	$this->template->javascript->add(base_url('assets/admin/js/channels/create_channel.js?'.time()));

    	$channels = new M_channels($id);



    	if(isset($_POST) && count($_POST) > 0){

                

            

    		//print_r($this->input->post());exit;
    		$channels->channel_name = $this->input->post('channel_name');
    		$channels->channel_description = $this->input->post('channel_description');
    		$channels->corporate_id = $this->input->post('corporate');

    		// $channels->pol = $this->input->post('pol');
    		// $channels->frq = $this->input->post('frq');
    		// $channels->sym = $this->input->post('sym');
    		// $channels->vdo_pid = $this->input->post('vdo_pid');
    		// $channels->ado_pid = $this->input->post('ado_pid');
            /* change above into channel component table */



            $channels->active = $this->input->post('active');

    		$channels->channel_type = $this->input->post('channel_type');
    		$channels->band_type = $this->input->post('band_type');
    		$channels->dvb_type = $this->input->post('dvb_type');
    		$channels->dvb_def = $this->input->post('dvb_def');
    		$channels->service_id = $this->input->post('service_id');
    		$channels->encoded = ($this->input->post('encoded') == '1')?'T':'F';
            $channels->active = $this->input->post('status');
            $channels->channel_categories_id = $this->input->post('channel_categories_id');
            $channels->channel_extended_status = $this->input->post('channel_extended_status');
            $channels->enable_tv_program = $this->input->post('enable_tv_program');
            $channels->remark = $this->input->post('remark');
            

            if($this->input->post('merge_s3remote_id')){
                $channels->merge_s3remote_id = $this->input->post('merge_s3remote_id');
            }
            if($this->input->post('logo')){
                $channels->logo = $this->input->post('logo');
            }
            
            if($this->input->post('cband_ordinal')){
                $channels->cband_ordinal = $this->input->post('cband_ordinal');
            }

            if($this->input->post('kuband_ordinal')){
                $channels->kuband_ordinal = $this->input->post('kuband_ordinal');
            }

            /* set channel component update logs */
            if($id){
                $this->checkChannelComponentUpdate(array(
                    'channels_id'=>$channels->id,
                    'post_data'=>$this->input->post()
                ));
            }
            /* eof set channel component update logs */

            if($this->input->post('channel_extended_status') == '1'){
                $channels->channel_extended_from_channels_id = $this->input->post('channel_extended_from_channels_id');
            }else{
                $channels->channel_extended_from_channels_id = 0;
            }


    		if($channels->save()){

                /* insert data into channel component */
                if($this->insertChannelComponentData(array(
                    'channels_id'=>$channels->id,
                    'post_data'=>$this->input->post()
                ))){
                    $txt_message = ($id)?__('Edit channel success','admin/channels/create_channel'):__('Create channel success','admin/channels/create_channel');
                    $this->msg->add($txt_message,'success'); 
                }
                /* eof insert data into channel component */  

                unset($_POST);
                redirect($this->uri->uri_string());


    			
    		}

    	}

    	$data = array(
    		'channel_data'=>$channels,
    		'corporate'=>$this->getSelectCorporate(),
            'channel_components'=>$this->getChannelComponent(array(
                'channels_id'=>$channels->id
            )),
            'channel_components_update_logs'=>$this->getChannelComponentUpdateLogs([
                'channels_id'=>$channels->id
            ]),
            'channel_categories'=>$this->getSelectChannelCategories(),
            'select_channel_extended_from'=>$this->getSelectChannelExtendedFrom(array(
                'current_channels_id'=>$channels->id
            )),
            's3remote_select_channel'=>$this->getS3RemoteSelectChannel()
    	);

        //print_r($this->input->post());

        //print_r($data['channel_components']);
    	$this->template->content->view('admin/channels/create_channel',$data);
    	$this->template->publish();


    }

    public function createChannelCategory(){
        $this->__createChannelCategory();
    }

    public function editChannelCategory($id){
        $channel_categories = new M_channel_categories($id);

        if(!$channel_categories){
            redirect(base_url('admin/'.$this->controller));
        }

        $this->__createChannelCategory($id);
    }
    public function deleteChannelCategory($id){
        $channel_categories = new M_channel_categories($id);

        if(!$channel_categories){
            redirect(base_url('admin/'.$this->controller));
        }


    }

    private function __createChannelCategory($id=null){

        $this->loadValidator();
        $this->template->javascript->add(base_url('assets/admin/js/channels/create_channel_category.js'));

        $channel_categories = new M_channel_categories($id);

        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;
            $channel_categories->category_name = $this->input->post('category_name');
            $channel_categories->category_description = $this->input->post('category_description');
            $channel_categories->active = $this->input->post('active');

            if($channel_categories->save()){
                $txt_message = ($id)?__('Edit channel category success','admin/channels/create_channel_category'):__('Create channel category success','admin/channels/create_channel_category');
                $this->msg->add($txt_message,'success');
                redirect($this->uri->uri_string());
            }

        }

        $data = array(
            'channel_category'=>$channel_categories
        );

        $this->template->content->view('admin/channels/create_channel_category',$data);
        $this->template->publish();

    }
    public function initialChannels(){
    	$this->connectPsiRatingDB();
    	$query = "select * from Channel";

    	$stmt = sqlsrv_query( $this->psirating_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		while($result = sqlsrv_fetch_object($stmt)){
    			$this->db->insert('channels',array(
    				'channel_name'=>$result->ChannelName,
    				'frq'=>$result->FRQ,
    				'sym'=>$result->SYM,
    				'vdo_pid'=>$result->VDO_PID,
    				'ado_pid'=>$result->ADO_PID,
    				'channel_type'=>$result->ChannelType,
    				'logo'=>$result->Picture,
    				'encoded'=>$result->Encoded,
    				'band_type'=>$result->BandType,
    				'dvb_type'=>$result->DVBType,
    				'dvb_def'=>$result->DVBDef
    			));
    		}
    		//return $result;
    	}


    }

    public function updatePOL(){

    	$this->connectPsiRatingDB();
    	$query = "select * from Channel";

    	$stmt = sqlsrv_query( $this->psirating_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		while($result = sqlsrv_fetch_object($stmt)){
    			$this->db->update('channels',array(
    				'pol'=>$result->POL
    			),array('channel_name'=>$result->ChannelName));

    		}
    	}

    }

    public function updateOldVersionIntoChannelComponent(){
        /* */
        $channels = new M_channels();

        foreach ($channels->get() as $key => $value) {
            # code...
            //print_r($value->to_array());
            $channel_components = new M_channel_components();
            $channel_components->channels_id = $value->id;
            $channel_components->frq = $value->frq;
            $channel_components->sym = $value->sym;
            $channel_components->pol = $value->pol;
            $channel_components->vdo_pid = $value->vdo_pid;
            $channel_components->ado_pid = $value->ado_pid;
            $channel_components->service_id = $value->service_id;
            $channel_components->band_type = 'C';
            $channel_components->save();

        }
    }

    public function ajaxDeleteChannelProgramBODate(){
        $post_data = $this->input->post();


        $this->db->delete('channel_programs_baseon_date',[
            'id'=>$post_data['rowid']
        ]);

        


        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    public function ajaxAddDefaultProgramFromDow(){
        $post_data = $this->input->post();

        $exDateProgram = explode('/', $post_data['program_date']);

        $program_date = new DateTime($exDateProgram[2].'-'.$exDateProgram[1].'-'.$exDateProgram[0]);

        
        $queryDateOfWeekData = $this->db->select('*')
        ->from('channel_programs')
        ->where('channels_id',$post_data['channels_id'])
        ->where('date_of_week',$program_date->format('D'))
        ->order_by('start_time','asc')
        ->get();

        if($queryDateOfWeekData->num_rows() > 0){


            $view = $this->load->view('admin/channels/ajaxAddDefaultFromDow',[
                'result'=>$queryDateOfWeekData->result()
            ],true);

            //echo $view;exit;


            echo json_encode([
                'status'=>true,
                'post_data'=>$post_data,
                'row_data'=>$view
            ]); exit;

        }else{
            echo json_encode([
                'status'=>false
            ]);exit;

        }

        
    }
    private function connectPsiRatingDB(){

    	if(ENVIRONMENT == 'development'){
	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
			$this->psirating_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->psirating_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
		}else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->psirating_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->psirating_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['production']['username'], "PWD"=>$this->db_config['production']['password'],"CharacterSet" => "UTF-8");
			$this->psirating_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->psirating_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}



		}
    }

    private function getSelectCorporate(){
    	$arrReturn = array();
    	$arrReturn['0'] = __('Select Corporate','admin/channels/create_channel');
    	$query = $this->db->select('id,name')
    	->from('corporate')->where('active',1)->get();
    	
    	foreach ($query->result() as $key => $value) {
    		# code...
    		$arrReturn[$value->id] = $value->name;
    	}
    	return $arrReturn;

    }

    private function insertChannelComponentData($data = []){
        $channels_id = $data['channels_id'];
        $post_data = $data['post_data'];  

       // print_r($data);exit;


        if(trim($post_data['band_type']) == 'C'){ 

            /* delete kuband record if exist */
            $this->db->delete('channel_components',array(
                'channels_id'=>$channels_id,
                'band_type'=>'KU'
            )); 

            $query_check_exist = $this->db->select('*')
            ->from('channel_components')
            ->where('band_type',$post_data['band_type'])
            ->where('channels_id',$channels_id)
            ->get();

            if($query_check_exist->num_rows() <= 0){
                // insert new record 
                $this->db->insert('channel_components',[
                    'channels_id'=>$channels_id,
                    'frq'=>$post_data['cband_frq'],
                    'sym'=>$post_data['cband_sym'],
                    'pol'=>$post_data['cband_pol'],
                    'vdo_pid'=>$post_data['cband_vdo_pid'],
                    'ado_pid'=>$post_data['cband_ado_pid'],
                    'service_id'=>$post_data['cband_service_id'],
                    'band_type'=>$post_data['band_type'],
                    'updated_serviceid_by'=>'admin',
                    'created'=>date('Y-m-d H:i:s')
                ]);

            }else{
                $row = $query_check_exist->row();


                // edit record 
                $this->db->update('channel_components',[
                    'frq'=>$post_data['cband_frq'],
                    'sym'=>$post_data['cband_sym'],
                    'pol'=>$post_data['cband_pol'],
                    'vdo_pid'=>$post_data['cband_vdo_pid'],
                    'ado_pid'=>$post_data['cband_ado_pid'],
                    'service_id'=>$post_data['cband_service_id'],
                    'updated_serviceid_by'=>'admin',
                    'updated'=>date('Y-m-d H:i:s')
                ],['id'=>$row->id]);

            }

        }else if(trim($post_data['band_type']) == 'KU'){ 

            /* delete cband record if exist */
                $this->db->delete('channel_components',array(
                    'channels_id'=>$channels_id,
                    'band_type'=>'C'
                ));

                $query_check_exist = $this->db->select('*')
                ->from('channel_components')
                ->where('band_type',$post_data['band_type'])
                ->where('channels_id',$channels_id)
                ->get();

                if($query_check_exist->num_rows() <= 0){
                    // insert new record 
                    $this->db->insert('channel_components',[
                        'channels_id'=>$channels_id,
                        'frq'=>$post_data['kuband_frq'],
                        'sym'=>$post_data['kuband_sym'],
                        'pol'=>$post_data['kuband_pol'],
                        'vdo_pid'=>$post_data['kuband_vdo_pid'],
                        'ado_pid'=>$post_data['kuband_ado_pid'],
                        'service_id'=>$post_data['kuband_service_id'],
                        'band_type'=>$post_data['band_type'],
                        'updated_serviceid_by'=>'admin',
                        'created'=>date('Y-m-d H:i:s')
                    ]);

                }else{
                    $row = $query_check_exist->row();


                    // edit record 
                    $this->db->update('channel_components',[
                        'frq'=>$post_data['kuband_frq'],
                        'sym'=>$post_data['kuband_sym'],
                        'pol'=>$post_data['kuband_pol'],
                        'vdo_pid'=>$post_data['kuband_vdo_pid'],
                        'ado_pid'=>$post_data['kuband_ado_pid'],
                        'service_id'=>$post_data['kuband_service_id'],
                        'updated_serviceid_by'=>'admin',
                        'updated'=>date('Y-m-d H:i:s')
                    ],['id'=>$row->id]);

                }


        }else if(trim($post_data['band_type']) == 'ALL'){ 

            //print_r($data);exit;

            // check also cband and kuband

            $query_check_cband_exist = $this->db->select('*')
            ->from('channel_components')
            ->where('band_type','C')
            ->where('channels_id',$channels_id)
            ->get();

            if($query_check_cband_exist->num_rows() <= 0){
                // insert new record 
                $this->db->insert('channel_components',[
                    'channels_id'=>$channels_id,
                    'frq'=>$post_data['cband_frq'],
                    'sym'=>$post_data['cband_sym'],
                    'pol'=>$post_data['cband_pol'],
                    'vdo_pid'=>$post_data['cband_vdo_pid'],
                    'ado_pid'=>$post_data['cband_ado_pid'],
                    'service_id'=>$post_data['cband_service_id'],
                    'band_type'=>'C',
                    'updated_serviceid_by'=>'admin',
                    'created'=>date('Y-m-d H:i:s')
                ]);

            }else{ 

                // print_r($post_data);
                // update exist record 
                $row_cband = $query_check_cband_exist->row();


                // edit record 
                $this->db->update('channel_components',[
                    'frq'=>$post_data['cband_frq'],
                    'sym'=>$post_data['cband_sym'],
                    'pol'=>$post_data['cband_pol'],
                    'vdo_pid'=>$post_data['cband_vdo_pid'],
                    'ado_pid'=>$post_data['cband_ado_pid'],
                    'service_id'=>$post_data['cband_service_id'],
                    'updated_serviceid_by'=>'admin',
                    'updated'=>date('Y-m-d H:i:s')
                ],['band_type'=>'C','channels_id'=>$channels_id]); 

                // echo $this->db->last_query();exit;

            }



            ///////////////////////////////////////////////////
            // ================ CHECK KUBAND =============== //
            ///////////////////////////////////////////////////

            $query_check_kuband_exist = $this->db->select('*')
            ->from('channel_components')
            ->where('band_type','KU')
            ->where('channels_id',$channels_id)
            ->get();

            if($query_check_kuband_exist->num_rows() <= 0){  

                $this->db->insert('channel_components',[
                        'channels_id'=>$channels_id,
                        'frq'=>$post_data['kuband_frq'],
                        'sym'=>$post_data['kuband_sym'],
                        'pol'=>$post_data['kuband_pol'],
                        'vdo_pid'=>$post_data['kuband_vdo_pid'],
                        'ado_pid'=>$post_data['kuband_ado_pid'],
                        'service_id'=>$post_data['kuband_service_id'],
                        'band_type'=>'KU',
                        'updated_serviceid_by'=>'admin',
                        'created'=>date('Y-m-d H:i:s')
                ]);

                


            }else{ 
                $row_kuband = $query_check_kuband_exist->row();


                    // edit record 
                    $this->db->update('channel_components',[
                        'frq'=>$post_data['kuband_frq'],
                        'sym'=>$post_data['kuband_sym'],
                        'pol'=>$post_data['kuband_pol'],
                        'vdo_pid'=>$post_data['kuband_vdo_pid'],
                        'ado_pid'=>$post_data['kuband_ado_pid'],
                        'service_id'=>$post_data['kuband_service_id'],
                        'updated_serviceid_by'=>'admin',
                        'updated'=>date('Y-m-d H:i:s')
                    ],['band_type'=>'KU','channels_id'=>$channels_id]);

            }



            }

            return true;

    }

    private function getChannelComponent($data = array()){
        $arrReturn = array();
        $channels_id = $data['channels_id']; 

        $query_components = $this->db->select('*')
        ->from('channel_components')
        ->where('channels_id',$channels_id)
        ->get();



        // $channel_components = new M_channel_components();
        // $channel_components->where('channels_id',$channels_id)
        // ->get();

        if($query_components->num_rows() > 0){

            foreach ($query_components->result() as $key => $value) {
                # code...
                if(trim($value->band_type) == 'C'){
                    $arrReturn['CBAND'] = array(
                        'id'=>$value->id,
                        'channels_id'=>$value->channels_id,
                        'frq'=>$value->frq,
                        'sym'=>$value->sym,
                        'pol'=>$value->pol,
                        'vdo_pid'=>$value->vdo_pid,
                        'ado_pid'=>$value->ado_pid,
                        'service_id'=>$value->service_id
                    );
                }else{
                    $arrReturn['KUBAND'] = array(
                        'id'=>$value->id,
                        'channels_id'=>$value->channels_id,
                        'frq'=>$value->frq,
                        'sym'=>$value->sym,
                        'pol'=>$value->pol,
                        'vdo_pid'=>$value->vdo_pid,
                        'ado_pid'=>$value->ado_pid,
                        'service_id'=>$value->service_id
                    );
                }
            }
        }

        return $arrReturn;
    }

    private function getSelectChannelCategories(){
        $arrReturn = array();
        $arrReturn[""] = __('Select Category','admin/channels/create_channel');
        $query = $this->db->select('*')
        ->from('channel_categories')
        ->where('active',1)->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $arrReturn[$value->id] = $value->category_name;
            }
        }

        return $arrReturn;
    }

    private function checkChannelComponentUpdate($data = array()){
        $checkUpdateStatus = false;
        $channels_id = $data['channels_id'];
        $post_data = $data['post_data']; 

        //print_r($post_data);exit;

        $channel_components = $this->getChannelComponent(array('channels_id'=>$channels_id));

      

        if(array_key_exists('CBAND', $channel_components)){
            if($channel_components['CBAND']['frq'] != $post_data['cband_frq'] 
                || $channel_components['CBAND']['sym'] != $post_data['cband_sym'] 
                || $channel_components['CBAND']['vdo_pid'] != $post_data['cband_vdo_pid'] 
                || $channel_components['CBAND']['ado_pid'] != $post_data['cband_ado_pid']){

                /* write record into channel component update logs */
                $this->db->insert('channel_components_update_logs',array(
                    'channel_components_id'=>$channel_components['CBAND']['id'],
                    'channels_id'=>$channel_components['CBAND']['channels_id'],
                    'frq'=>$channel_components['CBAND']['frq'],
                    'sym'=>$channel_components['CBAND']['sym'],
                    'pol'=>$channel_components['CBAND']['pol'],
                    'vdo_pid'=>$channel_components['CBAND']['vdo_pid'],
                    'ado_pid'=>$channel_components['CBAND']['ado_pid'],
                    'service_id'=>$channel_components['CBAND']['service_id'],
                    'updated_to_message'=>json_encode(array(
                        'frq'=>$post_data['cband_frq'],
                        'sym'=>$post_data['cband_sym'],
                        'pol'=>$post_data['cband_pol'],
                        'vdo_pid'=>$post_data['cband_vdo_pid'],
                        'ado_pid'=>$post_data['cband_ado_pid'],
                        'service_id'=>$post_data['cband_service_id']
                    )),
                    'updated_by'=>$this->admin_data['email'],
                    'created'=>date('Y-m-d H:i:s')
                ));


            }
        }
        
        if(array_key_exists('KUBAND', $channel_components)){
            if($channel_components['KUBAND']['frq'] != $post_data['kuband_frq'] 
                || $channel_components['KUBAND']['sym'] != $post_data['kuband_sym'] 
                || $channel_components['KUBAND']['vdo_pid'] != $post_data['kuband_vdo_pid'] 
                || $channel_components['KUBAND']['ado_pid'] != $post_data['kuband_ado_pid']){

                /* write record into channel component update logs */
                $this->db->insert('channel_components_update_logs',array(
                    'channel_components_id'=>$channel_components['KUBAND']['id'],
                    'channels_id'=>$channel_components['KUBAND']['channels_id'],
                    'frq'=>$channel_components['KUBAND']['frq'],
                    'sym'=>$channel_components['KUBAND']['sym'],
                    'pol'=>$channel_components['KUBAND']['pol'],
                    'vdo_pid'=>$channel_components['KUBAND']['vdo_pid'],
                    'ado_pid'=>$channel_components['KUBAND']['ado_pid'],
                    'service_id'=>$channel_components['KUBAND']['service_id'],
                    'updated_to_message'=>json_encode(array(
                        'frq'=>$post_data['kuband_frq'],
                        'sym'=>$post_data['kuband_sym'],
                        'pol'=>$post_data['kuband_pol'],
                        'vdo_pid'=>$post_data['kuband_vdo_pid'],
                        'ado_pid'=>$post_data['kuband_ado_pid'],
                        'service_id'=>$post_data['kuband_service_id']
                    )),
                    'updated_by'=>$this->admin_data['email'],
                    'created'=>date('Y-m-d H:i:s')
                ));


            }


        }

        return true;

        

    }

    private function getSelectChannelExtendedFrom($data = array()){

        $arrReturn = array();
        $arrReturn["0"] = __('-- select channel extended from --','admin/channels/create_channel');

        $channels = new M_channels();
        $channels->where('channel_extended_status',0)
        ->where('id !=',$data['current_channels_id'])
        ->where('active',1)
        ->order_by('channel_name')
        ->get();

        if($channels->result_count() > 0){

            foreach ($channels as $key => $value) {
                # code...
                $arrReturn[$value->id] = $value->channel_name;
            }
        }

        return $arrReturn;
    }

    private function getS3RemoteSelectChannel(){
        $arrReturn = array();
        $arrReturn["0"] = "Select Channel";  
        $response = file_get_contents($this->getS3RemoteUrl().'welcome/getAllChannelList');

        $decode_response = json_decode($response);

        foreach ($decode_response as $key => $value) {
            # code...
            $arrReturn[$value->id] = $value->name;
        }

        return $arrReturn;
        //print_r($decode_response);exit;
    }


    private function getS3RemoteUrl(){
        switch (ENVIRONMENT) {
            case 'development':
                # code...
                return 'http://s3remoteservice.development/';
            break;
            case 'testing':

            break;

            case 'production':
                return 'http://sv-rating.dltv.ac.th/';
            break;
            
            default:
                # code...
                break;
        }
    }

    private function postActionEvent(){
        //print_r($this->input->post());exit;
        if($this->input->post('action') == 'delete'){
            $json_decode = json_decode($this->input->post('json_data'));

            foreach ($json_decode as $key => $value) {
                # code...
                $this->db->delete('channel_programs_baseon_date',[
                    'id'=>$value->program_id
                ]);
            }
        }


        return true;
    } 

    private function postActionEventForWeekData(){

        if($this->input->post('action') == 'delete'){
            $json_decode = json_decode($this->input->post('json_data'));

            foreach ($json_decode as $key => $value) {
                # code...
                $this->db->delete('channel_programs',[
                    'id'=>$value->program_id
                ]);
            }
        }


        return true;
    }

    private function getChannelComponentUpdateLogs($data = []){

        $channel_components_update_logs = new M_channel_components_update_logs();
        $channel_components_update_logs->where('channels_id',$data['channels_id'])
        ->order_by('created','desc')
        ->get();

        return $channel_components_update_logs;

    }

}