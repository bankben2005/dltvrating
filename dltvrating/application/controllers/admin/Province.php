<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Province extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();


    	$province = new M_provinces();

    	$data = [
    		'province'=>$province->get()
    	];

    	$this->template->content->view('admin/province/province_list',$data);
    	$this->template->publish();

    }

    public function editProvince($id){
    	$province = new M_provinces($id);

    	if(!$province->id){
    		redirect('admin/'.$this->controller);
    	}

    	$this->__createProvince($id);
    }


    private function __createProvince($id = null){


    	$province = new M_provinces($id);


    	if($this->input->post(NULL,FALSE)){
    		//print_r($this->input->post());exit;
    		$province->province_name = $this->input->post('province_name');
    		$province->province_region = $this->input->post('province_region');
    		$province->ipstack_province_name = $this->input->post('ipstack_province_name');
            $province->population = $this->input->post('population');
            $province->latitude = $this->input->post('latitude');
            $province->longitude = $this->input->post('longitude');

    		if($province->save()){

    			$txtSuccess = ($id)?__('Edit province success','admin/province/create_province'):__('Create province success','admin/province/create_province');

    			$this->msg->add($txtSuccess,'success');
    			redirect($this->uri->uri_string());
    		}
    	}


    	$data = [
    		'province'=>$province
    	];


    	$this->template->content->view('admin/province/create_province',$data);
    	$this->template->publish();
    }

}