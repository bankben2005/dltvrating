<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class District extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();

    	$districts = new M_districts();


    	$data = [
    		'districts'=>$districts
    	];

    	$this->template->content->view('admin/district/district_list',$data);
    	$this->template->publish();

    }
    public function createDistrict(){

    }
    public function editDistrict($id){
    	$district = new M_districts($id);

    	if(!$district->id){
    		redirect(base_url('admin/district'));
    	}

    	$this->__createDistrict($id);
    }

    public function __createDistrict($id = null){


    	$district = new M_districts($id);

        if($this->input->post(NULL,FALSE)){
            // print_r($this->input->post());exit;
            $district->district_name = $this->input->post('district_name');
            $district->latitude = $this->input->post('latitude');
            $district->longitude = $this->input->post('longitude');

            if($district->save()){
                $this->msg->add(__('Update district success','admin/district/create_district'),'success');
                redirect($this->uri->uri_string());
            }
        }

    	$data = [
    		'district'=>$district
    	];

    	$this->template->content->view('admin/district/create_district',$data);
    	$this->template->publish();

    }

}