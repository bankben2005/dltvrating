<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Class_room extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

    	$this->loadDataTableStyle();
        $this->loadDataTableScript();
      

            
     
        $str_query = "select devices_group.*,r1.ship_code,devices_group.id as device_group_id,Schools.SchoolName
        from devices_group 
        join schools
        on devices_group.SchoolID = schools.SchoolID
        join (
            SELECT device_rel_group.device_group_id,string_agg(concat('', '', devices.ship_code), ', ') as ship_code
            FROM devices
            join device_rel_group on devices.id = device_rel_group.device_id
            group by device_rel_group.device_group_id
        )r1
        on devices_group.id = r1.device_group_id
        where devices_group.SchoolID != '' and devices_group.SchoolID is not null
        order by devices_group.id desc";

                    
        $exec_query = $this->db->query($str_query);
       
       
        $data = [
            'class_room'=>$exec_query->result(),
           
        ];
       
        $this->template->content->view('admin/class_room/classroom_list',$data);
        $this->template->javascript->add(base_url('assets/js/classroom/classroom.js'));
        $this->template->publish();
    

    }

    public function editClassroom($id){
    	$class_room = new M_classroom($id);

    	if(!$class_room->id){
    		redirect('admin/'.$this->controller);
    	}

    	$this->__editClassroom($id);
    }
    public function __editClassroom($id = null){

        $this->loadValidator();
        $this->template->stylesheet->add(base_url('assets/css/bootstrap_select/bootstrap-select.css'));
        $this->template->stylesheet->add(base_url('assets/css/bootstrap_select/bootstrap-select.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/baguetteBox/baguetteBox.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/gallary/fluid/fluid-gallery.css'));
        $this->template->javascript->add(base_url('assets/js/baguetteBox/baguetteBox.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap_select/js/bootstrap-select.js'));
        $this->template->javascript->add(base_url('assets/js/ajax_bootstrap_select/ajax-bootstrap-select.js'));
        $this->template->javascript->add(base_url('assets/js/ajax_bootstrap_select/ajax-bootstrap-select.min.js'));
        $this->template->javascript->add(base_url('assets/js/classroom/edit_classroom.js'));
       
       // M_classroom
       $class_room = new M_classroom($id);
       $device_rel_group =  $this->db->select('device_rel_group.*,devices.ship_code')->from('device_rel_group')
       ->join('devices' , 'device_rel_group.device_id  = devices.id')
        ->where('device_rel_group.device_group_id',$id)
        ->where('device_rel_group.active',1)->get();

        //->where('device_rel_group.is_move_to_groupid',null)
        $resultfromClass = $device_rel_group->result();
        $returnDataClass = array();
        if($device_rel_group->num_rows() > 0){
            $returnDataClass = $resultfromClass;
        }

        // class room log
        

    	if($this->input->post(NULL,FALSE)){
           
         
    	}


    	$data = [
            'class_room' => $class_room,
            'devices'=>$this->getSelectDevices(),
            'schools' => $this->getSelectSchool(),
            'devices_rel_class' => $returnDataClass,
            'class_room_logs' => $this->getClassRoomLog($id),
            'class_id' => $id
        ];
       
    

    
    	$this->template->content->view('admin/class_room/edit_classroom',$data);
    	$this->template->publish();
    }

    public function getClassRoomLog($class_id){
        $arrReturn = array();
        //$arrReturn["0"] = __('-- select school from --','admin/class_room/createClassroom');

        $str_query = "   SELECT device_update_logs.*,devices.ship_code,administrator.firstname as admin_name
        ,(select group_name from dbo.devices_group where id =  device_update_logs.is_device_move_to_classid ) as move_to_class
        ,(select group_name from dbo.devices_group where id =  device_update_logs.is_device_move_from_classid ) as move_from_class
        FROM device_update_logs 
        inner join devices on device_update_logs.device_id = devices.id
        inner join administrator on device_update_logs.updated_by = administrator.id
        where class_id = {$class_id} ";
        //or is_device_move_to_classid = {$class_id}

        $exec_query = $this->db->query($str_query);

        if($exec_query->num_rows() > 0){
            $arrReturn = $exec_query->result_array();

        }
        
        return $arrReturn;

    }
    public function getactivedevices(){
   
        $str_query = "SELECT top 1000 * FROM devices where active = 1 and (status is null or status = 1) and ship_code like '".$_POST['q']."%' order by created desc ";
        $deviceQuery = $this->db->query($str_query);
        //echo $this->db->last_query();
        $returnData = array("returndata" => array());
            if($deviceQuery->num_rows() > 0){
            $returnData['returndata'] = $deviceQuery->result();
        }
        
        echo json_encode($returnData);
        //echo '5';exit();
    }
    public function checkdevicemove(){
        // echo '<PRE>';
        // print_R($_POST);exit();
  
        $status['status'] = ""; 
        $status['result_code'] = "";
        $status['result_message'] = "";
        $status['return_data'] = array();
        //echo json_encode($_POST['post_data']['devices']);exit();
        if(isset($_POST['post_data']['devices'])){
            //echo '5';
            if(count($_POST['post_data']['devices']) > 0){
                    $str_query = $this->db->select('device_rel_group.*,devices_group.group_name,devices.ship_code')->from('device_rel_group')
                    ->join('devices_group' ,'device_rel_group.device_group_id = devices_group.id')
                    ->join('devices','device_rel_group.device_id = devices.id')
                    ->where_in('device_rel_group.device_id',$_POST['post_data']['devices'])
                    ->where('device_rel_group.device_group_id !=', $_POST['post_data']['class_id'])
                    ->get();
                    //echo $this->db->last_query();
                    if($str_query->num_rows() > 0){
                        $status['return_data'] = $str_query->result();
                        $chipArr = array();
                        foreach($str_query->result() as $k => $v){
                            $chipArr[] = $v->ship_code.'('.$v->group_name.')';
                        }
                        $status['status'] = false;
                        $status['result_code'] = "-002";
                        $status['result_message'] = "This Chip ID ".implode(",",$chipArr)." Already Exist On Another Classroom , Do you want to move on this class ?";
                    }else{
                        $status['status'] = true;
                        $status['result_code'] = "000";
                        $status['result_message'] = "success";
                    }
            }else{
                $status['status'] = true;
                $status['result_code'] = "000";
                $status['result_message'] = "success";
            }
        }

        echo json_encode($status);
    }

    public function ajaxAlreadyExistOnSameClass(){
        $status['status'] = ""; 
        $status['result_code'] = "";
        $status['result_message'] = "";
        $status['return_data'] = array();

        if(isset($_POST['post_data']['devices'])){
            //echo '5';
            if(count($_POST['post_data']['devices']) > 0){
                    $str_query = $this->db->select('device_rel_group.*,devices_group.group_name,devices.ship_code')->from('device_rel_group')
                    ->join('devices_group' ,'device_rel_group.device_group_id = devices_group.id')
                    ->join('devices','device_rel_group.device_id = devices.id')
                    ->where_in('devices.id',$_POST['post_data']['devices'])
                    ->where('device_rel_group.device_group_id =', $_POST['post_data']['class_id'])
                    ->get();
                    //check ว่ามี device นี้อยู่แล้วไหม และใน array ที่ส่งมามีไหม
                    $deviceGroups = array();
                    if($str_query->num_rows() > 0){
                        $deviceGroups = $str_query->result();
                    }
                    $checkAlreadyHaveForSure = $this->checkAlreadyHaveForSure($_POST['post_data']['devices'] , $deviceGroups);
                    //print_r($_POST['post_data']['devices']);exit();
                    if($checkAlreadyHaveForSure == true){
                        if($str_query->num_rows() > 0){
                            $status['return_data'] = $str_query->result();
                            $chipArr = array();
                            foreach($str_query->result() as $k => $v){
                                $chipArr[] = $v->ship_code.'('.$v->group_name.')';
                            }
                            $status['status'] = false;
                            $status['result_code'] = "-002";
                            $status['result_message'] = "This Chip ID ".implode(",",$chipArr)." Already Exist On This Classroom , Do you want to keep doing this action ?";
                        }else{
                            $status['status'] = true;
                            $status['result_code'] = "000";
                            $status['result_message'] = "success";
                        }

                    }else{
                        $status['status'] = true;
                        $status['result_code'] = "000";
                        $status['result_message'] = "success";
                    }
                    //echo $this->db->last_query();
                
            }else{
                $status['status'] = true;
                $status['result_code'] = "000";
                $status['result_message'] = "success";
            }
        }
        //echo json_encode(array());
        echo json_encode($status);
      
    }
    public function checkAlreadyHaveForSure($device_key , $deviceGroups){
      
        foreach($deviceGroups as $k => $v){
            if(in_array($v->device_id , $device_key)){
                return true;
            }
        }
        return false;

    }
    public function deviceActionLog(){
        $status['status'] = ""; 
        $status['result_code'] = "";
        $status['result_message'] = "";
        $status['return_data'] = array();
        /**
         * DEVICE STATUS : 1=active , 2=BROKE
         */
        //echo json_encode($_POST['post_data']);
        if(isset($_POST['post_data']['devices'])){
            //echo '5';
            if(count($_POST['post_data']['devices']) > 0){
                //- select เช็คก่อน insert ว่ามี device นี้ไหมในห้อง (device_rel_group)
                foreach($_POST['post_data']['devices'] as $dvk => $dvv){
                        $str_query = $this->db->select('device_rel_group.*,devices_group.group_name,devices.ship_code')->from('device_rel_group')
                        ->join('devices_group' ,'device_rel_group.device_group_id = devices_group.id')
                        ->join('devices','device_rel_group.device_id = devices.id')
                        ->where('device_rel_group.device_group_id =', $_POST['post_data']['class_id'])
                        ->where('devices.id',$dvv)
                        ->get();
                        //echo $this->db->last_query();
                        if($str_query->num_rows() > 0){
                            //device status  1 = active = 2 broke
                            //1.1 ) check ว่า device นี้ล่าสุดเคยชำรุดไหม ถ้าเคยให้ insert และ return success มา
                            $check_log = $this->db->select('*')->from('device_update_logs')->where('device_id',$dvv)
                            ->where('class_id', $_POST['post_data']['class_id'])
                            ->order_by('created' , 'desc')
                            ->limit(1)->get();
                            //echo $this->db->last_query();exit();


                            // check ว่า device นี้มีอยู่ในห้องอื่นไหม ถ้ามี เข้า 1.2 ถ้าไม่มี เข้า 1.1
                            if($check_log->num_rows() > 0){
                                $check_duplicate_on_thisclass = $this->db->select('*')->from('device_update_logs')
                                ->where('device_id',$dvv)
                                ->order_by('created', 'desc')
                                ->limit(1)
                                ->get();
                                //echo $this->db->last_query();exit();
                                if($check_duplicate_on_thisclass->num_rows() > 0){
                                    $_duplicateClassID = $check_duplicate_on_thisclass->row()->class_id;
                                   // echo $_duplicateClassID.' |||| ' . $_POST['post_data']['class_id'];exit();
                                    if($_duplicateClassID ==  $_POST['post_data']['class_id']){
                                        // #code .. (1.2  update log ถ้าห้องนี้มีอยู่แล้วต้องการย้ายกลับมา)
                                           //$this->case_12($dvv,$_POST['post_data']['class_id'],$_POST);
                                            $status['status'] = true;
                                            $status['result_code'] = "-002";
                                            $status['result_message'] = "This Device Already Active , Please Try Again.";
                                            echo json_decode($status);
                                    }else{
                                        // #code .. (1.1  update log ในห้องอื่นว่าย้ายมาห้องนี้ และ insert ลง device_rel_group)
                                        $this->case_11($dvv,$_duplicateClassID,$_POST);

                                        $status['status'] = true;
                                        $status['result_code'] = "000";
                                        $status['result_message'] = "success";

                                    }
                                }else{
                                    // ไม่เข้ากรณีอะไรให้ insert ตามปกติ
                                    $device_status = $check_log->row()->device_status;
                                    $device_update_log =new M_device_update_logs();
                                    $device_update_log->device_id = $dvv;
                                    $device_update_log->class_id = $_POST['post_data']['class_id'];
                                    $device_update_log->device_status = 1;
                                    $device_update_log->updated_by = $_POST['post_data']['user_id'];
                                    $device_update_log->created = date('Y-m-d H:i:s');
                                    $device_update_log->save();

                                    $status['status'] = true;
                                    $status['result_code'] = "000";
                                    $status['result_message'] = "success";
                                    echo json_decode($status);
                                }
                                // if($device_status == 2){
                                //     $device_update_log =new M_device_update_logs();
                                //     $device_update_log->device_id = $dvv;
                                //     $device_update_log->class_id = $_POST['post_data']['class_id'];
                                //     $device_update_log->device_status = 1;
                                //     $device_update_log->updated_by = $_POST['post_data']['user_id'];
                                //     $device_update_log->created = date('Y-m-d H:i:s');
                                //     $device_update_log->save();

                                //     $status['status'] = true;
                                //     $status['result_code'] = "000";
                                //     $status['result_message'] = "success";
                                //     //echo json_decode($status);
                                // }else{
                                // // 1.2 ) check ว่า device นี้ล่าสุดเคยชำรุดไหม ถ้าไม่เคยให้  return กลับไปว่า device คุณกำลังเพิ่ม device ซ้ำลงระบบ
                                // // เนื่องจาก device นี้ไม่เคยชำรุด
                                //     $ship_code = $str_query->row()->ship_code;
                                //     $status['status'] = true;
                                //     $status['result_code'] = "-002";
                                //     $status['result_message'] = "This Device ".$ship_code."Already Active , Please Try Again.";
                                //     echo json_decode($status);
                                // }
                            }else{
                                $getActual_data = $_POST;
                                unset($getActual_data['post_data']['devices']);
                                //CHECK IS IT FIRST DEVICE
                                $checkIsFirstDevice = $this->db->select('*')->from('device_update_logs')->where('device_id',$dvv)
                                ->where('class_id', $_POST['post_data']['class_id'])
                                ->order_by('created' , 'desc')
                                ->limit(1)->get();
                                if($checkIsFirstDevice->num_rows() > 0){

                                     //SET ONLY DEVICE
                                    $getActual_data['post_data']['devices'] = $dvv;
                                    $boolean_ = $this->checkDeviceActiveOnAnotherClassRoom($getActual_data);
                                    if(empty($boolean_) || $boolean_ == false){
                                        $status['status'] = false;
                                        $status['result_code'] = "-002";
                                        $status['result_message'] = "Something wen wrong on our server , please try again later.";
                                        echo json_decode($status); 
                                    }else{
                                        $status['status'] = true;
                                        $status['result_code'] = "000";
                                        $status['result_message'] = "success";

                                    }
                                }else{

                                    $status['status'] = true;
                                    $status['result_code'] = "-002";
                                    $status['result_message'] = "Device Already Active , Please Check Your Input.";
                                    echo json_decode($status);
                                }
                            }
                         

                        }else{
                            $getActual_data = $_POST;
                            unset($getActual_data['post_data']['devices']);
                            //SET ONLY DEVICE
                            $getActual_data['post_data']['devices'] = $dvv;
                            $boolean_ = $this->checkDeviceActiveOnAnotherClassRoom($getActual_data);
                            if(empty($boolean_) || $boolean_ == false){
                                $status['status'] = false;
                                $status['result_code'] = "-002";
                                $status['result_message'] = "Something wen wrong on our server , please try again later.";
                                echo json_decode($status);
                            }else{
                                $status['status'] = true;
                                $status['result_code'] = "000";
                                $status['result_message'] = "success";
                            }
                        }
                }
            }
        }
        //echo json_encode(array());
        echo json_encode($status);
    }

    public function  checkDeviceActiveOnAnotherClassRoom($data){
        // (2)- select เช็คก่อน insert ว่ามี device นี้ไหมในห้องอื่น (device_rel_group)
        $getdata = $data;
        $devices = array($data['post_data']['devices']);
        foreach($devices as $deviceKey => $deviceValue){
            //echo '5555555';exit();
            $str_query = $this->db->select('device_rel_group.*,devices_group.group_name,devices_group.SchoolID,devices.ship_code')->from('device_rel_group')
            ->join('devices_group' ,'device_rel_group.device_group_id = devices_group.id')
            ->join('devices','device_rel_group.device_id = devices.id')
            ->where('device_rel_group.device_id',$deviceValue)
            ->where('device_rel_group.device_group_id !=', $data['post_data']['class_id'])
            ->get();
           // echo $this->db->last_query();exit();
            if($str_query->num_rows() > 0){
               
                //1.1) update log ในห้องอื่นว่าย้ายมาห้องนี้ และ insert ลง device_rel_group
                foreach($str_query->result() as $dvk => $dvv){
                        $class_id = $dvv->device_group_id;
                        $device_id= $dvv->device_id;
                       //CHECK ALREADY EXIST NOT ALERT DUPLICATE
                        // #code .. (1.1  update log ในห้องอื่นว่าย้ายมาห้องนี้ และ insert ลง device_rel_group)
                        //$this->case_11($device_id,$class_id,$data);
                        $check_duplicate_on_thisclass = $this->db->select('*')->from('device_update_logs')
                        ->where('device_id',$device_id)
                        ->order_by('created', 'desc')
                        ->limit(1)
                        ->get();

                        if($check_duplicate_on_thisclass->num_rows() > 0){
                            $_duplicateClassID = $check_duplicate_on_thisclass->row()->class_id;
                           // echo $_duplicateClassID.' |||| ' . $_POST['post_data']['class_id'];exit();
                            if($_duplicateClassID ==  $data['post_data']['class_id']){
                                // #code .. (1.2  update log ถ้าห้องนี้มีอยู่แล้วต้องการย้ายกลับมา)
                                   //$this->case_12($dvv,$_POST['post_data']['class_id'],$_POST);
                                    $status['status'] = true;
                                    $status['result_code'] = "-002";
                                    $status['result_message'] = "This Device Already Active , Please Try Again.";
                                    echo json_decode($status);
                            }else{
                                // #code .. (1.1  update log ในห้องอื่นว่าย้ายมาห้องนี้ และ insert ลง device_rel_group)
                                $this->case_11($device_id,$class_id,$data);

                                $status['status'] = true;
                                $status['result_code'] = "000";
                                $status['result_message'] = "success";

                            }
                        }
                        /** ======================================================================================
                         * EOF case:1.1
                         * ======================================================================================
                         */

                       //CHECK ALREADY EXIST NOT ALERT DUPLICATE
                       $check_duplicate = $this->db->select('*')->from('device_rel_group')
                       ->where('device_id',$device_id)
                       ->where('device_group_id =', $data['post_data']['class_id'])
                       ->get();

                       if($check_duplicate->num_rows() == 0){

                            $device_rel_group  = new M_class_rel_group();
                            $device_rel_group->device_id = $device_id;
                            $device_rel_group->device_group_id = $data['post_data']['class_id'];
                            $device_rel_group->active = 1;
                            $device_rel_group->save();
                        }
                        // UPDATE SCHOOL
                        $this->update_SchoolID_devicesAdrress($dvv->SchoolID, $device_id);

                        
                }
            }else{

              
                $device_rel_group  = new M_class_rel_group();
                $device_rel_group->device_id = $deviceValue;
                $device_rel_group->device_group_id = $data['post_data']['class_id'];
                $device_rel_group->active = 1;
        
                $device_rel_group->save();

                $schools = $this->db->select('SchoolID')->from('devices_group')->where('id',$data['post_data']['class_id'])->get();
                $SchoolID = $schools->row()->SchoolID;
                // UPDATE SCHOOL
                $this->update_SchoolID_devicesAdrress($SchoolID, $deviceValue);
            
            }
        }

        return true;

    }

 /** =================================================================================
  * #code .. (1.1  update log ในห้องอื่นว่าย้ายมาห้องนี้ และ insert ลง device_rel_group)
  * ==================================================================================
  */
    public function case_11($device_id,$class_id,$data){
          sleep(1);
        // #code .. (1.1  update log ในห้องอื่นว่าย้ายมาห้องนี้ และ insert ลง device_rel_group)
          $this->case_11_1($device_id,$class_id,$data);
          //delay 1 second (purpose: for summary data report )
          sleep(1);
          $this->case_11_2($device_id,$class_id,$data);

    }
    public function case_11_1($device_id,$class_id,$data){
                  // MOVE FROM CLASS (A)  -> TO CLASS (B)
            $device_update_log = new M_device_update_logs();
            $device_update_log->device_id = $device_id;
            $device_update_log->class_id = $class_id;
            $device_update_log->device_status = 1;
            $device_update_log->updated_by = $data['post_data']['user_id'];
            $device_update_log->created = date('Y-m-d H:i:s');
            $device_update_log->is_device_move_from_classid = $class_id;
            $device_update_log->is_device_move_to_classid = $data['post_data']['class_id'];
            $device_update_log->is_device_move_to_classdate = date('Y-m-d H:i:s');
            $device_update_log->save();
    }
    public function case_11_2($device_id,$class_id,$data){
            // INSERT LOG THIS CLASS (B)
            $device_update_log = new M_device_update_logs();
            $device_update_log->device_id = $device_id;
            $device_update_log->class_id = $data['post_data']['class_id'];
            $device_update_log->device_status = 1;
            $device_update_log->updated_by = $data['post_data']['user_id'];
            $device_update_log->created = date('Y-m-d H:i:s');
            $device_update_log->is_device_move_from_classid = $class_id;
            $device_update_log->is_device_move_to_classdate = date('Y-m-d H:i:s');
            $device_update_log->save();
    }
    /** ===========================================================================
     * EOF
     * =============================================================================
     */

    public function case_12($device_id,$class_id,$data){
             // MOVE FROM CLASS (B)  -> TO CLASS (A)
             $device_update_log = new M_device_update_logs();
             $device_update_log->device_id = $device_id;
             $device_update_log->class_id = $data['post_data']['class_id'];
             $device_update_log->device_status = 1;
             $device_update_log->updated_by = $data['post_data']['user_id'];
             $device_update_log->created = date('Y-m-d H:i:s');
             $device_update_log->is_device_move_from_classid = $data['post_data']['class_id'];
             $device_update_log->is_device_move_to_classid = $class_id;
             $device_update_log->is_device_move_to_classdate = date('Y-m-d H:i:s');
             $device_update_log->save();
             sleep(1);
             // INSERT LOG THIS CLASS (A)
             $device_update_log = new M_device_update_logs();
             $device_update_log->device_id = $device_id;
             $device_update_log->class_id = $class_id;
             $device_update_log->device_status = 1;
             $device_update_log->updated_by = $data['post_data']['user_id'];
             $device_update_log->created = date('Y-m-d H:i:s');
             $device_update_log->is_device_move_from_classid = $class_id;
             $device_update_log->is_device_move_to_classdate = date('Y-m-d H:i:s');
             $device_update_log->save();
    }


    public function update_SchoolID_devicesAdrress($SchoolID , $devices_id){
         // UPDATE ADDRESS IN SCHOOL
         $str_query = " update device_addresses set SchoolID = '{$SchoolID}' where devices_id = '{$devices_id}'";
         $this->db->query($str_query);
    }
    public function createClassroom($id = null){

        $this->loadValidator();
        $this->template->stylesheet->add(base_url('assets/css/bootstrap_select/bootstrap-select.css'));
        $this->template->stylesheet->add(base_url('assets/css/bootstrap_select/bootstrap-select.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/baguetteBox/baguetteBox.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/gallary/fluid/fluid-gallery.css'));
        $this->template->javascript->add(base_url('assets/js/baguetteBox/baguetteBox.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap_select/js/bootstrap-select.js'));
        $this->template->javascript->add(base_url('assets/js/ajax_bootstrap_select/ajax-bootstrap-select.js'));
        $this->template->javascript->add(base_url('assets/js/ajax_bootstrap_select/ajax-bootstrap-select.min.js'));
        $this->template->javascript->add(base_url('assets/js/classroom/create_classroom.js'));
       
       // M_classroom
       $class_room = new M_classroom($id);
    	if($this->input->post(NULL,FALSE)){
           
            $class_room->group_name = $this->input->post('class_name');
            $class_room->SchoolID = $this->input->post('select_school');
            $class_room->active = 1;
    		if($class_room->save()){
                $class_id = $this->db->insert_id();

                // M_classroom
                $class_rel_device = new M_class_rel_group();
                $class_rel_device->device_id = $this->input->post('select-deivces');
                $class_rel_device->device_group_id = $class_id;
                $class_rel_device->active = 1;
                if($class_rel_device->save()){
                    // UPDATE ADDRESS IN SCHOOL
                    $str_query = " update device_addresses set SchoolID = '{$this->input->post('select_school')}' where devices_id = '{$this->input->post('select-deivces')}'";
                    $this->db->query($str_query);

                    $txtSuccess = __('Create Classroom success','admin/class_room/createClassroom');

                    $this->msg->add($txtSuccess,'success');
                    redirect($this->uri->uri_string());
                }
            }
    	}


    	$data = [
            'devices'=>$this->getSelectDevices(),
            'schools' => $this->getSelectSchool()
        ];
        // echo '<PRE>';
        // print_r($data);
    

    
    	$this->template->content->view('admin/class_room/create_classroom',$data);
    	$this->template->publish();
    }

    /**
     * SCHOOL JOIN -> province , district 
     */
    // select schools.* , provinces.province_name,provinces.province_code 
    // ,provinces.province_region,provinces.latitude as province_lat 
    // , provinces.longitude as province_long
    // ,districts.district_name,districts.district_name_eng
    // from schools 
    // inner join provinces
    // on schools.ProvinceID = provinces.province_code
    // inner join districts
    // on schools.DistrictID = districts.district_code;

    public function getSelectSchool($data = array()){

        $arrReturn = array();
        //$arrReturn["0"] = __('-- select school from --','admin/class_room/createClassroom');

        $str_query = "   SELECT * FROM schools where active = 1";

        $exec_query = $this->db->query($str_query);

        if($exec_query->num_rows() > 0){
          

            foreach ($exec_query->result()  as $key => $value) {
                # code...
                $arrReturn[$value->SchoolID] = $value->SchoolName;
            }
        }
        
        return $arrReturn;
    }


    private function getSelectDevices($data = array()){

        $arrReturn = array();
        //$arrReturn["0"] = __('-- select device from --','admin/class_room/createClassroom');

        $str_query = " select top 2000 * from devices order by created desc";

        $exec_query = $this->db->query($str_query);

        if($exec_query->num_rows() > 0){
          

            foreach ($exec_query->result()  as $key => $value) {
                # code...
                // $arrReturn[$value->id] = $value->ship_code.'('.$value->created.')';
                $arrReturn[$value->id] = $value->ship_code;
            }
        }

        return $arrReturn;
    }


    public function setdevicestatus(){
        $status['status'] = ""; 
        $status['result_code'] = "";
        $status['result_message'] = "";
        $status['return_data'] = array();
       // echo '<PRE>';
        //print_r($_POST);exit();
        if(isset($_POST)){
                $dd_status = $_POST['device_status'];
                $device_id = $_POST['device_id'];
                $user_id = $_POST['user_id'];

                //select all group that have this device and update then 
                $findgroup = $this->db->select('class_id')->from('device_update_logs')->where('device_id',$device_id)->get();
                if($findgroup->num_rows() > 0){
                    foreach($findgroup->result() as $groupKey => $groupvalue){
                        try{
                            $class_id = $groupvalue->class_id;
                            // INSERT NEW LOGS
                            $device_update_log =new M_device_update_logs();
                            $device_update_log->device_id = $device_id;
                            $device_update_log->class_id = $class_id;
                            $device_update_log->device_status = $dd_status;
                            $device_update_log->updated_by = $user_id;
                            $device_update_log->created = date('Y-m-d H:i:s');
                            $device_update_log->save();
                            //UPDATE STATUS device
                            $devices =new M_devices($device_id);
                            $devices->status = $dd_status;
                            $devices->save();

                            $status['status'] = true; 
                            $status['result_code'] = "000";
                            $status['result_message'] = "success";
                        }
                        catch (Exception $e) {
                            // handing error
                            $status['status'] = false; 
                            $status['result_code'] = "-002";
                            $status['result_message'] = "Something went wrong on our server.";
                        }
                   

                    }
                }else{
                         //UPDATE STATUS device
                         $devices =new M_devices($device_id);
                         $devices->status = $dd_status;
                         $devices->save();

                         $status['status'] = true; 
                         $status['result_code'] = "000";
                         $status['result_message'] = "success";


                }
        }

        echo json_encode($status);
    }

   
}