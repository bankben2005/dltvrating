<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Corporate extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

        $this->loadDataTableStyle();
        $this->loadDataTableScript();
        
    	$corporate = new M_corporate();


    	$data = array(
    		'corporate'=>$corporate->get()
    	);

    	$this->template->content->view('admin/corporate/corporate_lists',$data);
    	$this->template->publish();



    }

    public function createCorporate(){
    	$this->__createCorporate();
    }
    public function editCorporate($id){
    	$corporate  = new M_corporate($id);

    	if(!$corporate){
    		redirect('admin/'.$this->controller);
    	}

    	$this->__createCorporate($id);

    }
    public function deleteCorporate($id){
    	$corporate  = new M_corporate($id);

    	if(!$corporate){
    		redirect('admin/'.$this->controller);
    	}


    }

    private function __createCorporate($id=null){
    	$this->loadValidator();
    	$this->template->javascript->add(base_url('assets/admin/js/corporate/create_corporate.js'));

    	$corporate = new M_corporate($id);

    	if($this->input->post(NULL,FALSE)){
    		
    		$corporate->name = $this->input->post('name');
    		$corporate->description = $this->input->post('description');
    		$corporate->active = $this->input->post('active');

    		if($corporate->save()){
    			$txt_success = ($id)?__('Edit corporate success','admin/corporate/create_corporate'):__('Create corporate success','admin/corporate/create_corporate');

    			$this->msg->add($txt_success,'success');
    			redirect($this->uri->uri_string());
    		}
    	}

    	$data = array(
    		'corporate'=>$corporate
    	);

    	$this->template->content->view('admin/corporate/create_corporate',$data);
    	$this->template->publish();


    }
}