<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class School_type extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();


		$str_query = "  select school_type.* , school_group.school_group_name from school_type 
						inner join school_group 
						on school_type.parent_id = school_group.id";
    
        $exec_query = $this->db->query($str_query);

        // EOF
        //  echo '<PRE>';
		// print_r($exec_query->result());exit();
        $data = [
         'school_type'=>$exec_query->result()
        ];

    	$this->template->content->view('admin/school_type/schooltype_list',$data);
    	$this->template->publish();

    }

    public function editAffiliate($id){
    	$M_school_type_ = new M_school_type($id);

    	if(!$M_school_type_->id){
    		redirect('admin/'.$this->controller);
    	}

    	$this->createAffiliate($id);
    }

	
    public function createAffiliate($id = null){


    	$M_school_type_ = new M_school_type($id);


    	if($this->input->post(NULL,FALSE)){
    		//print_r($this->input->post());exit;
    		$M_school_type_->SchoolType = $this->input->post('school_type');
    		$M_school_type_->parent_id = $this->input->post('school_group');
			//SET DEFAULT ACTIVE VALUE IS 1
			$M_school_type_->active = 1;

    		if($M_school_type_->save()){

    			$txtSuccess = ($id)?__('Edit school_type success','admin/school_type/editAffiliate'):__('Create school_type success','admin/school_type/createAffiliate');

    			$this->msg->add($txtSuccess,'success');
    			redirect($this->uri->uri_string());
    		}
    	}


    	$data = [
			'M_school_type_'=>$M_school_type_,
			'school_type' => $this->getSelectSchoolType(),
			'school_group' => $this->getSelectSchoolGroup()
    	];


    	$this->template->content->view('admin/school_type/create_schooltype',$data);
    	$this->template->publish();
	}
	
	public function getSelectSchoolType(){
		$arr_return = [];
		$arr_return[''] = __('Select Affiliate','');
		$query = $this->db->select('*')
		->from('school_type')
		->get();
	
		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arr_return[$value->id] = $value->SchoolType;
			}
		}
	
		return $arr_return;
	}


	public function getSelectSchoolGroup(){
		$arr_return = [];
		//$arr_return[''] = __('Select Parent Group','');
		$query = $this->db->select('*')
		->from('school_group')
		->get();
	
		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arr_return[$value->id] = $value->school_group_name;
			}
		}
	
		return $arr_return;
	}

}