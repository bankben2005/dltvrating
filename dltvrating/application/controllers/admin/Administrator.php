<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Administrator extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

        $this->loadDataTableStyle();
        $this->loadDataTableScript();


    	$administrator = new M_administrator();
    	$administrator->where('id !=',$this->admin_data['id']);

    	if($this->admin_data['type'] == 'admin'){    		
			//DLTV USER CAN VIEW ALL ACCESS DEVICE
			//$administrator->where('created_by',$this->admin_data['id']);
    	}

		$administrator->get();
	

    	$data = array(
    		'administrator'=>$administrator
    	);


    	$this->template->content->view('admin/administrator/administrator_list',$data);
    	$this->template->publish();

    }

    public function createAdministrator(){
    	$this->__createAdministrator();
    }

    public function editAdministrator($admin_id){

    	/* check admin id and check permission for edit */
    	$administrator = new M_administrator();
    	$administrator->where('id',$admin_id);

    	if($this->admin_data['type'] == 'admin'){
			//DLTV USER CAN VIEW ALL ACCESS DEVICE
    		//$administrator->where('created_by',$this->admin_data['id']);
    	}

    	$administrator->get();

    	if(!$administrator->id){
    		redirect('admin/'.$this->controller);
    	}

    	$this->__createAdministrator($admin_id);

    }

    public function deleteAdministrator($admin_id){

    }

    public function ajaxCheckEmailExist(){
    	$get_data = $this->input->get();

    	$administrator = new M_administrator();
    	$administrator->where('email',$get_data['email'])->get();

    	if($administrator->id){
    		echo json_encode(array(
    			'valid'=>false
    		));
    	}else{
    		echo json_encode(array(
    			'valid'=>true
    		));	
    	}

    }

    public function ajaxSetResetPassword(){
    	$post_data = $this->input->post();

    	$administrator = new M_administrator($post_data['administrator_id']);

    	if($administrator->id){
    		$administrator->password = sha1($post_data['new_password']);
    		$administrator->save();
    	}

    	echo json_encode(array(
    		'status'=>true,
    		'post_data'=>$post_data
    	));
    }

    private function __createAdministrator($id = null){
    	$this->loadValidator();
    	$this->loadSweetAlert();

    	if($id){
    		$this->template->javascript->add(base_url('assets/admin/js/administrator/edit_administrator.js'));
    	}else{
    		$this->template->javascript->add(base_url('assets/admin/js/administrator/create_administrator.js'));
    	}

    	$administrator = new M_administrator($id);
	

    	if($this->input->post(NULL,FALSE)){

    		$administrator->type = $this->input->post('type');
    		$administrator->firstname = $this->input->post('firstname');
    		$administrator->lastname = $this->input->post('lastname');

    		if(!$id){
    			$administrator->email = $this->input->post('email');
    			$administrator->password = sha1($this->input->post('password'));
    			$administrator->created_by = $this->admin_data['id'];
			}
			// check if type is staff then can view only 
			if($this->input->post('type') == 'staff'){
				$administrator->for_review_devices = 1;
				$administrator->can_action_button = 0;
			}else{
				if($this->input->post('type') == 'admin'){
					// IF ADMIN THEN CAN VIEW AND CAN't EDIT
					$administrator->for_review_devices = $this->input->post('for_review_devices');
					$administrator->can_action_button = 0;
				}else{
					// IF SUPER ADMIN CAN DO EVERYTHING 
					$administrator->for_review_devices = $this->input->post('for_review_devices');
					$administrator->can_action_button = 1;
				}
			}
			$administrator->telephone = $this->input->post('telephone');
    		$administrator->active = $this->input->post('active');

    		if($administrator->save()){
    			$txt_success = ($id)?__('Edit administrator success','admin/administrator/create_administrator'):__('Create administrator success','admin/administrator/create_administrator');

    			$this->msg->add($txt_success,'success');

    			redirect($this->uri->uri_string());

    		}
    		


    	}





    	$data = array(
    		'administrator'=>$administrator
		);
		
		// echo '<PRE>';
		// print_r($administrator);exit();

    	$this->template->content->view('admin/administrator/create_administrator',$data);
    	$this->template->publish();


    }

}