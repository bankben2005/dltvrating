<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Shopping extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }

    public function advertisement_campaign(){

        $this->loadDataTableStyle();
        $this->loadDataTableScript();

        $ads_campaign = new M_advertisement_campaigns();


    	$data = array(
            'ads_campaign' => $ads_campaign
    	);

    	$this->template->content->view('admin/shopping/advertisement_campaign_list',$data);
    	$this->template->publish();
    }

    public function advertisement(){

    	$data = array(

    	);

    	$this->template->content->view('admin/shopping/advertisement_list',$data);
    	$this->template->publish();

    }

    public function createAdvertisementCampaign(){
    	$this->__createAdvertisementCampaign();
    }
    public function editAdvertisementCampaign($id){

        /* check this advertisement campaign */
        $advertisement_campaigns = new M_advertisement_campaigns($id);

        if(!$advertisement_campaigns->id){
            redirect('admin/'.$this->controller.'/advertisement');
        }
        /* eof check this advertisement campaign */


        $this->__createAdvertisementCampaign($id);
    }

    public function setAdvertisementImageAndVideo($ads_id = 0){

        $this->loadImageUploadStyle();        
        $this->loadDateRangePickerStyle();

        $this->loadSweetAlert();
        $this->loadValidator();
        $this->loadDateRangePickerScript();
        $this->loadImageUploadScript();

        $this->template->javascript->add(base_url('assets/admin/js/shopping/set_advertisement_imageandvideo.js'));

        /* check this advertisement campaign */
        $advertisement_campaigns = new M_advertisement_campaigns($ads_id);

        if(!$advertisement_campaigns->id){
            redirect('admin/'.$this->controller.'/advertisement');
        }
        /* eof check this advertisement campaign */


        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;

            $advertisement_campaign_images = new M_advertisement_campaign_images();
            $advertisement_campaign_images->where('advertisement_campaigns_id',$this->input->post('advertisement_campaigns_id'))
            ->where('position',$this->input->post('hide_position'))->get();

            $exDuration = explode(' - ', $this->input->post('duration'));
            $start_datetime = new DateTime($exDuration[0]);
            $end_datetime = new DateTime($exDuration[1]);

            if($advertisement_campaign_images->id){
                /* if already has record*/
                $advertisement_campaign_images->start_datetime = $start_datetime->format('Y-m-d H:i:s');
                $advertisement_campaign_images->end_datetime = $end_datetime->format('Y-m-d H:i:s');
                
                if($advertisement_campaign_images->save()){
                    if($_FILES['ads_image']['error'] == 0){
                        $this->uploadAdsImage($advertisement_campaign_images);
                    }
                }

                $this->msg->add(__('Edit advertisement image success','admin/shopping/set_advertisement_imageandvideo'),'success');
                redirect($this->uri->uri_string());

                

            }else{
                /* create new record */
                $created_ads_images = new M_advertisement_campaign_images();
                $created_ads_images->advertisement_campaigns_id = $this->input->post('advertisement_campaigns_id');
                $created_ads_images->position = $this->input->post('hide_position');
                $created_ads_images->start_datetime = $start_datetime->format('Y-m-d H:i:s');
                $created_ads_images->end_datetime = $end_datetime->format('Y-m-d H:i:s');

                if($created_ads_images->save()){
                    if($_FILES['ads_image']['error'] == 0){
                        $this->uploadAdsImage($created_ads_images);
                    }

                }
                $this->msg->add(__('Create advertisement image success','admin/shopping/set_advertisement_imageandvideo'),'success');
                redirect($this->uri->uri_string());

            }



        }


        $data = array(
            'ads_campaign'=>$advertisement_campaigns,
            'ads_images'=>$this->getArrAdvertisementImages($advertisement_campaigns)
        );

        //print_r($data['ads_images']);exit;
        //$key = array_search('1', array_column($data['ads_images'], 'position'));

        // echo $key;exit;

        $this->template->content->view('admin/shopping/set_advertisement_imageandvideo',$data);
        $this->template->publish();

    }

    public function ajaxSetAdsVideo(){
        $post_data = $this->input->post();

        $start_datetime = new DateTime($post_data['start_datetime']);

        $advertisement_campaign_videos = new M_advertisement_campaign_videos($post_data['ads_campaign_videos_id']);
        $advertisement_campaign_videos->advertisement_campaigns_id = $post_data['ads_campaign_id'];
        $advertisement_campaign_videos->video_url = $post_data['youtube_url'];
        $advertisement_campaign_videos->start_datetime = $start_datetime->format('Y-m-d H:i:s');
        $advertisement_campaign_videos->save();


        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data
        ));
    }

    public function ajaxUnsetAdsVideo(){
        $post_data = $this->input->post();
        $advertisement_campaign_videos = new M_advertisement_campaign_videos($post_data['ads_campaign_videos_id']);

        $advertisement_campaign_videos->delete();

        //print_r($advertisement_campaign_videos->to_array());


        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data
        ));
    }

    public function delete_image($ads_campaign_image_id){

        $advertisement_campaign_images = new M_advertisement_campaign_images($ads_campaign_image_id);


        if(!$advertisement_campaign_images->id){
            redirect('admin/'.$this->controller.'/setAdvertisementImageAndVideo');
        }

        $ads_campaign_id = $advertisement_campaign_images->advertisement_campaigns_id;


        /* delete image and folder image first */
        $folder_image = base_url('uploaded/advertisement/campaign_images/'.$advertisement_campaign_images->id);
        $image_path = $folder_image.'/'.$advertisement_campaign_images->image;
        // echo $folder_image."<br>";
        // echo $image_path;
        if(file_exists($image_path)){
            /* unlink image and remove folder */
            unlink($image_path);
            if(is_dir($folder_image)){
                rmdir($folder_image);
            }
            
        }

        if($advertisement_campaign_images->delete()){
            $this->msg->add(__('Delete advertisement image success','admin/shopping/set_advertisement_imageandvideo'),'success');
            redirect(base_url('admin/'.$this->controller.'/setAdvertisementImageAndVideo/'.$ads_campaign_id));
        }
        //echo $image_path;
        /* eof delete image and folder image */
    }

    public function postEditAdsImage(){

        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;
            $advertisement_campaign_images = new M_advertisement_campaign_images($this->input->post('hide_ads_image_id'));

            if($advertisement_campaign_images->id){
                $exDuration = explode(' - ', $this->input->post('edit_duration'));
                $start_datetime = new DateTime($exDuration[0]);
                $end_datetime = new DateTime($exDuration[1]);

                $advertisement_campaign_images->start_datetime = $start_datetime->format('Y-m-d H:i');
                $advertisement_campaign_images->end_datetime = $end_datetime->format('Y-m-d H:i');

                if($advertisement_campaign_images->save()){
                    if($_FILES['ads_image']['error'] == 0){
                        $this->uploadEditAdsImage($advertisement_campaign_images);
                    }

                    $this->msg->add(__('Edit advertisement image success','admin/shopping/set_advertisement_imageandvideo'),'success');
                    redirect(base_url('admin/'.$this->controller.'/setAdvertisementImageAndVideo/'.$advertisement_campaign_images->advertisement_campaigns_id));
                    //redirect($this->uri->uri_string());

                }
            }

        }


    }

    public function deleteAdsPopupImage($advertisement_campaigns_id){
        
    }

    private function __createAdvertisementCampaign($id=null){

        // $this->loadDataTableStyle();
        $this->loadDateRangePickerStyle();
        $this->loadImageUploadStyle();

        // $this->loadDataTableScript();
        $this->loadDateRangePickerScript();
        $this->loadImageUploadScript();


        $this->loadValidator();

        if($id){
            $this->template->javascript->add(base_url('assets/admin/js/shopping/edit_advertisement_campaign.js'));
        }else{
            $this->template->javascript->add(base_url('assets/admin/js/shopping/create_advertisement_campaign.js'));
        }

    	$advertisement_campaigns = new M_advertisement_campaigns($id);


        if($this->input->post(NULL,FALSE)){
            // print_r($this->input->post());exit;

            /* ex duration */
            $exDuration = explode(' - ', $this->input->post('duration'));
            $start_datetime = new DateTime($exDuration[0]);
            $end_datetime = new DateTime($exDuration[1]);
            /* eof ex duration */

            $advertisement_campaigns->name = $this->input->post('name');
            $advertisement_campaigns->description = $this->input->post('description');
            $advertisement_campaigns->start_datetime = $start_datetime->format('Y-m-d H:i:s');
            $advertisement_campaigns->end_datetime = $end_datetime->format('Y-m-d H:i:s');            
            $advertisement_campaigns->active = $this->input->post('active');
            $advertisement_campaigns->channels_id = $this->input->post('choose_channel');
            $advertisement_campaigns->popup_image_url = $this->input->post('popup_image_url');


            if($advertisement_campaigns->save()){

                /* check upload image */
                if($_FILES['popup_image']['error'] == 0){
                         $this->uploadAdvertisementCampaignImage(array(
                            'advertisement_campaigns'=>$advertisement_campaigns,
                            'segment_id'=>$id
                        ));
                }
               
                /* eof check upload image */


                $txt_success = ($id)?__('Edit advertisement campaign success','admin/shopping/create_advertisement_campaign'):__('Create advertisement campaign success','admin/shopping/create_advertisement_campaign');

                $this->msg->add($txt_success,'success');
                redirect($this->uri->uri_string());
            }

           

        }


    	$data = array(
    		'advertisement_campaigns'=>$advertisement_campaigns,
            'channels'=>$this->getAllChannels()
    	);

    	$this->template->content->view('admin/shopping/create_advertisement_campaign',$data);
    	$this->template->publish();

    }

    private function getAllChannels(){
        $channels = new M_channels();
        $channels->where('active',1)->order_by('channel_name')->get();

        return $channels;
    }

    private function uploadAdsImage($ads_image){
            if(!is_dir('uploaded/advertisement/campaign_images/'.$ads_image->getId().'')){
                    mkdir('uploaded/advertisement/campaign_images/'.$ads_image->getId().'',0777,true);
            }
            clearDirectory('uploaded/advertisement/campaign_images/'.$ads_image->getId().'/');
                                                     
            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
            $config['upload_path'] = 'uploaded/advertisement/campaign_images/'.$ads_image->getId().'/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2000';
            $this->load->library('upload',$config);
                if(!$this->upload->do_upload('ads_image')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->msg->add($error['error'], 'error');
                    if(file_exists('uploaded/advertisement/campaign_images/'.$ads_image->getId())){
                            rmdir('uploaded/advertisement/campaign_images/'.$ads_image->getId());
                    }
                    $ads_image->delete();

                    redirect('admin/'.$this->controller.'/set_advertisement_imageandvideo');
            }else{
                        $data_upload = array('upload_data' => $this->upload->data());

                        $ads_update = new M_advertisement_campaign_images($ads_image->id);
                        $ads_update->image = $data_upload['upload_data']['file_name'];
                        $ads_update->save();
                        // $employee_update = new M_employee($ads_image->getId());
                        // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                        // $employee_update->save();
            }
    }

    private function uploadEditAdsImage($ads_image){

            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
            $config['upload_path'] = 'uploaded/advertisement/campaign_images/'.$ads_image->getId().'/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2000';
            $this->load->library('upload',$config);
                if(!$this->upload->do_upload('ads_image')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->msg->add($error['error'], 'error');
                    if(file_exists('uploaded/advertisement/campaign_images/'.$ads_image->getId())){
                            rmdir('uploaded/advertisement/campaign_images/'.$ads_image->getId());
                    }
                    $ads_image->delete();

                    redirect('admin/'.$this->controller.'/set_advertisement_imageandvideo');
            }else{

                        /* unlink older file */
                        $image_path = base_url('uploaded/advertisement/campaign_images/'.$ads_image->id.'/'.$ads_image->image);
                        if(file_exists($image_path)){
                            unlink($image_path);
                        }
                        /* eof unlink older file */

                        $data_upload = array('upload_data' => $this->upload->data());

                        $ads_update = new M_advertisement_campaign_images($ads_image->id);
                        $ads_update->image = $data_upload['upload_data']['file_name'];
                        $ads_update->save();
                        // $employee_update = new M_employee($ads_image->getId());
                        // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                        // $employee_update->save();
            }

    }

    private function getArrAdvertisementImages($ads_campaign){

        $arrReturn = array();
        $arrPosition = array();
        //array_push($arrReturn, array());

        foreach ($ads_campaign->advertisement_campaign_images->get() as $key => $value) {
            # code...
            $duration = "";
            $start_datetime = new DateTime($value->start_datetime);
            $end_datetime = new DateTime($value->end_datetime);
            //print_r($value->to_array());
            array_push($arrReturn, array(
                'id'=>$value->id,
                'position'=>$value->position,
                'image'=>$value->image,
                'duration'=>$start_datetime->format('d-m-Y H:i').' - '.$end_datetime->format('d-m-Y H:i')
            ));

            //array_push($arrPosition, $value->position);
        }
        return $arrReturn;

    }

    private function uploadAdvertisementCampaignImage($data = array()){
        $advertisement_campaigns = $data['advertisement_campaigns'];
        $id = $data['segment_id'];


            if(!is_dir('uploaded/advertisement/popup_images/'.$advertisement_campaigns->getId().'')){
                    mkdir('uploaded/advertisement/popup_images/'.$advertisement_campaigns->getId().'',0777,true);
            }

            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
            $config['upload_path'] = 'uploaded/advertisement/popup_images/'.$advertisement_campaigns->getId().'/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000';
            $this->load->library('upload',$config);
                if(!$this->upload->do_upload('popup_image')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->msg->add($error['error'], 'error');

                    if(file_exists('uploaded/advertisement/campaign_images/'.$advertisement_campaigns->getId())){
                            rmdir('uploaded/advertisement/campaign_images/'.$advertisement_campaigns->getId());
                    }
                    $advertisement_campaigns->delete();

                    if($id){
                        redirect('admin/'.$this->controller.'/editAdvertisementCampaign/'.$id);
                    }else{
                        redirect('admin/'.$this->controller.'/createAdvertisementCampaign');
                    }
            }else{

                        /* unlink older file */
                        $image_path = base_url('uploaded/advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image);
                        if(file_exists($image_path)){
                            unlink($image_path);
                        }
                        /* eof unlink older file */

                        $data_upload = array('upload_data' => $this->upload->data());

                        $ads_update = new M_advertisement_campaigns($advertisement_campaigns->id);
                        $ads_update->popup_image = $data_upload['upload_data']['file_name'];
                        $ads_update->save();
                        // $employee_update = new M_employee($ads_image->getId());
                        // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                        // $employee_update->save();
            }





    }

}