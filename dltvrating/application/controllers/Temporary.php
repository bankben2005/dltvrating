<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Temporary extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $rating_conn;
	
	public function __construct(){
		parent::__construct();
		$this->connectRatingRealServer();

		print_r($this->rating_conn);

	}
	public function index(){
		$this->load->view('welcome_message');
	}

	public function syncCreateDevicesTmp(){



		set_time_limit(0);



		if(ENVIRONMENT == 'production'){

				$querySelect = $this->db->select('*')
				->from('devices')
				->order_by('id','asc')
				->limit(5000)
				->get();

				if(@$_GET['test'] == 'test'){
					print_r($this->db->last_query());exit;
				}

				foreach ($querySelect->result() as $key => $value) {
					# code...
					$this->db->insert('devices_tmp',[
						'id'=>$value->id,
						'ship_code'=>$value->ship_code,
						'ip_address'=>$value->ip_address,
						'has_user_status'=>$value->has_user_status,
						'created'=>$value->created,
						'updated'=>$value->updated,
						'active'=>$value->active
					]);

					/* insert into device_addresses_tmp */
					//$insert_id = $this->db->insert_id();
					$queryDeviceAddresses = $this->db->select('*')
					->from('device_addresses')
					->where('devices_id',$value->id)
					->get();

					$rowDeviceAddresses = $queryDeviceAddresses->row();

					$this->db->insert('devices_addresses_tmp',[
							'id'=>$rowDeviceAddresses->id,
							'devices_tmp_id'=>$value->id,
							'continent_code'=>$rowDeviceAddresses->continent_code,
							'continent_name'=>$rowDeviceAddresses->continent_name,
							'country_code'=>$rowDeviceAddresses->country_code,
							'country_name'=>$rowDeviceAddresses->country_name,
							'region_code'=>$rowDeviceAddresses->region_code,
							'region_name'=>$rowDeviceAddresses->region_name,
							'city'=>$rowDeviceAddresses->city,
							'zip'=>$rowDeviceAddresses->zip,
							'latitude'=>$rowDeviceAddresses->latitude,
							'longitude'=>$rowDeviceAddresses->longitude,
							'location'=>$rowDeviceAddresses->location,
							'tmp_latitude'=>$rowDeviceAddresses->tmp_latitude,
							'tmp_longitude'=>$rowDeviceAddresses->tmp_longitude,
							'update_from_api'=>$rowDeviceAddresses->update_from_api,
							'province_region'=>$rowDeviceAddresses->province_region,
							'tmp_latlon_status'=>$rowDeviceAddresses->tmp_latlon_status,
							'created'=>$rowDeviceAddresses->created->date,
							'updated'=>$rowDeviceAddresses->updated->date,
							'active'=>$rowDeviceAddresses->active
					]);
				}



		}else{

				$query = "select TOP 5000 * from devices order by id asc";

				$stmt = sqlsrv_query( $this->rating_conn, $query,array(),array( "Scrollable" => 'static' ));
		    	// echo ENVIRONMENT;exit;
				if(sqlsrv_num_rows($stmt) > 0){
					while($result = sqlsrv_fetch_object($stmt)){
		    			//print_r($result);

						$device_address_data = $this->getDeviceAddressesData([
							'devices_id'=>$result->id
						]);

						//print_r($device_address_data);exit;

						
						$this->db->insert('devices_tmp',[
							'id'=>$result->id,
							'ship_code'=>$result->ship_code,
							'ip_address'=>$result->ip_address,
							'has_user_status'=>$result->has_user_status,
							'created'=>$result->created->date,
							'updated'=>$result->created->date,
							'active'=>$result->active
						]);
						//exit;

						//$insert_id = $this->db->insert_id();
						/* after that insert into device addresses tmp */
						$this->db->insert('devices_addresses_tmp',[
							'id'=>$device_address_data->id,
							'devices_tmp_id'=>$result->id,
							'continent_code'=>$device_address_data->continent_code,
							'continent_name'=>$device_address_data->continent_name,
							'country_code'=>$device_address_data->country_code,
							'country_name'=>$device_address_data->country_name,
							'region_code'=>$device_address_data->region_code,
							'region_name'=>$device_address_data->region_name,
							'city'=>$device_address_data->city,
							'zip'=>$device_address_data->zip,
							'latitude'=>$device_address_data->latitude,
							'longitude'=>$device_address_data->longitude,
							'location'=>$device_address_data->location,
							'tmp_latitude'=>$device_address_data->tmp_latitude,
							'tmp_longitude'=>$device_address_data->tmp_longitude,
							'update_from_api'=>$device_address_data->update_from_api,
							'province_region'=>$device_address_data->province_region,
							'tmp_latlon_status'=>$device_address_data->tmp_latlon_status,
							'created'=>$device_address_data->created->date,
							'updated'=>$device_address_data->updated->date,
							'active'=>$device_address_data->active
						]);


					}

				}
		}







	}

	public function updateDeviceTmpProvinceRegionBangkok(){
		$this->db->update('devices_addresses_tmp',[
			'province_region'=>'Bangkok'
		],['region_name'=>'Bangkok']);

	}

	private function connectRatingRealServer(){


		$db_config = [
			'servername'=>'192.168.100.21',
			'db_name'=>'S3Rating',
			'username'=>'Dev',
			'password'=>'Psi@dev'
		];

		if(ENVIRONMENT == 'development'){


	    	$serverName = $db_config['servername']; //serverName\instanceName
	    	$connectionInfo = array( "Database"=>$db_config['db_name'], "UID"=>$db_config['username'], "PWD"=>$db_config['password'],"CharacterSet" => "UTF-8");

	    	$this->rating_conn = sqlsrv_connect( $serverName, $connectionInfo);


	    	if(!$this->rating_conn){
	    		echo "Connection could not be established.<br />";
	    		die( print_r( sqlsrv_errors(), true));
	    	}
	    }else if(ENVIRONMENT == 'testing'){
			$serverName = $db_config['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$db_config['db_name'], "UID"=>$db_config['username'], "PWD"=>$db_config['password'],"CharacterSet" => "UTF-8");

			$this->rating_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->rating_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $db_config['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$db_config['db_name'], "UID"=>$db_config['username'], "PWD"=>$db_config['password'],"CharacterSet" => "UTF-8");
			$this->rating_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->rating_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}



		}
	}



	public function createDevicesTmpTable(){
		$sql_str = 'CREATE TABLE [dbo].[devices_tmp](
	[id] [int] NOT NULL,
	[ship_code] [varchar](100) NULL,
	[ip_address] [varchar](100) NULL,
	[has_user_status] [tinyint] NULL,
	[created] [datetime] NULL,
	[updated] [datetime] NULL,
	[active] [tinyint] NULL,
 CONSTRAINT [PK_devices_tmp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]';

	$this->db->query($sql_str);


}
public function createDeviceAddressesTmpTable(){

	$sql_str = 'CREATE TABLE [dbo].[devices_addresses_tmp](
	[id] [int] NOT NULL,
	[devices_tmp_id] [int] NULL,
	[continent_code] [varchar](2) NULL,
	[continent_name] [varchar](50) NULL,
	[country_code] [varchar](2) NULL,
	[country_name] [varchar](50) NULL,
	[region_code] [varchar](50) NULL,
	[region_name] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[zip] [varchar](50) NULL,
	[latitude] [varchar](100) NULL,
	[longitude] [varchar](100) NULL,
	[location] [text] NULL,
	[tmp_latitude] [varchar](50) NULL,
	[tmp_longitude] [varchar](50) NULL,
	[update_from_api] [tinyint] NULL,
	[province_region] [varchar](50) NULL,
	[tmp_latlon_status] [tinyint] NULL,
	[created] [datetime] NULL,
	[updated] [datetime] NULL,
	[active] [tinyint] NULL,
 CONSTRAINT [PK_devices_addresses_tmp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]';

$this->db->query($sql_str);


}


private function getDeviceAddressesData($data = []){
	$query = "select * from device_addresses where devices_id = '".$data['devices_id']."'";

	$stmt = sqlsrv_query( $this->rating_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
	if(sqlsrv_num_rows($stmt) > 0){
		$result = sqlsrv_fetch_object($stmt);
		return $result;
	}


}

}