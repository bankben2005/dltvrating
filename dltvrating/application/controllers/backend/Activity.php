<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/BackendLibrary.php';
class Activity extends BackendLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){

    }

    public function change_password(){

    	$this->loadValidator();
    	$this->template->javascript->add(base_url('assets/backend/js/activity/change_password.js'));
    	//echo '<center><h1>Coming Soon</h1></cetner>';


    	if($this->input->post(NULL,FALSE)){
    		//print_r($this->input->post());exit;
    		$this->db->update('user',array(
    			'password'=>sha1($this->input->post('new_password'))
    		),array('id'=>$this->user_data['id']));

    		$this->msg->add(__('Change password complete','backend/activity/change_password'),'success');
    		redirect($this->uri->uri_string());
    	}
    	$data = array(

    	);

    	$this->template->content->view('backend/activity/change_password',$data);
    	$this->template->publish();
    }

    public function ajaxCheckOldPassword(){
    	$get_data = $this->input->get();

    	$queryCheck = $this->db->select('id,password')
    	->from('user')
    	->where('id',$this->user_data['id'])
    	->where('password',sha1($get_data['old_password']))
    	->get();

    	if($queryCheck->num_rows() > 0){
    		echo json_encode(array(
    			'valid'=>true
    		));
    	}else{
    		echo json_encode(array(
    			'valid'=>false
    		));
    	}


    }

}
