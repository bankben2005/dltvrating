<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/BackendLibrary.php';
class Devices extends BackendLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){
        $this->checkMethodAvailable();

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();

    	//print_r($this->session->userdata('user_data'));
    	$devices = $this->db->select('*,devices.id as devices_id')
    	->from('devices')
    	->join('device_addresses','device_addresses.devices_id = devices.id')
    	->where('devices.active',1)->get();
    	$data = array(
    		'devices'=>$devices
    	);
    	$this->template->content->view('backend/devices/device_lists',$data);
    	$this->template->publish();
    }

    public function view_device($device_id){

    	$this->template->javascript->add(base_url('assets/backend/js/devices/view_device.js'));

    	$query = $this->db->select('*')
    	->from('devices')
    	->join('device_addresses','devices.id = device_addresses.devices_id')
    	->where('devices.id',$device_id)->get();

    	//echo $this->db->last_query();exit;

    	if($query->num_rows() <= 0){
    		redirect(base_url('backend/'.$this->controller));
    	}

    	$data = array(
    		'device_data'=>$query->row()
    	);

    	$this->template->content->view('backend/devices/view_device',$data);
    	$this->template->publish();


    }

}