<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/BackendLibrary.php';
class Rating extends BackendLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    private $current_rating_data_table;
    private $current_rating_data_daily_devices;

	public function __construct() {
                        parent::__construct();
                        $this->setCurrentDataTable();
    }

    public function index(){

        
        $this->checkMethodAvailable();

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();

    	$rating = $this->db->select('*')
    	->from($this->current_rating_data_table)
    	->order_by('created','desc')
    	->limit(100)
    	->get();

    	$data = array(
    		'rating_data'=>$rating
    	);

    	$this->template->content->view('backend/rating/rating_list',$data);
    	$this->template->publish();

    }

    public function rating_by_device(){
        //$this->checkMethodAvailable();

        $this->loadDataTableStyle();
        $this->loadDataTableScript();


        $rating_data = new StdClass();

        if($this->input->get(NULL,FALSE)){

            /* check chip code */   
            $query_check_chipcode = $this->db->select('id')
            ->from('devices')
            ->where('ship_code',$this->input->get('chipcode'))
            ->order_by('id','desc')
            ->limit(1)
            ->get();

            if($query_check_chipcode->num_rows() > 0){
                //print_r($this->input->get());exit;
                $rating_data = $this->db->select('*')
                ->from($this->current_rating_data_table)
                ->where('ship_code',$this->input->get('chipcode'))
                ->order_by('created','desc')
                ->limit(200)
                ->get();

            }else{


            }
            /* eof check chip code*/
            
        }

        //print_r($rating_data);exit;

        $data = array(
            'rating_data'=>$rating_data
        );

        $this->template->content->view('backend/rating/rating_by_device',$data);
        $this->template->publish();
    }

    public function rating_summary(){
        $this->checkMethodAvailable();


    	$data = array(

    	);

    	$this->template->content->view('backend/rating/rating_summary',$data);
    	$this->template->publish();
    }

    private function setCurrentDataTable(){
        $this->current_rating_data_table = 'rating_data_'.date('Y').'_'.date('n');
        $this->current_rating_data_daily_devices = 'rating_data_daily_devices_'.date('Y').'_'.date('n');
    }

}