<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_advertisement_campaigns extends DataMapper {

    //put your code here
    var $table = 'advertisement_campaigns';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'channels' => array(
             'class' => 'M_channels',
               'other_field' => 'advertisement_campaigns',
               'join_other_as' => 'channels',
               'join_table' => 'channels'
           )
   );
    
    var $has_many = array(
       'advertisement_campaign_videos' => array(
           'class' => 'M_advertisement_campaign_videos',
           'other_field' => 'advertisement_campaigns',
           'join_self_as' => 'advertisement_campaigns',
           'join_other_as' => 'advertisement_campaigns',
           'join_table' => 'advertisement_campaign_videos'
        ),
       'advertisement_campaign_images' => array(
           'class' => 'M_advertisement_campaign_images',
           'other_field' => 'advertisement_campaigns',
           'join_self_as' => 'advertisement_campaigns',
           'join_other_as' => 'advertisement_campaigns',
           'join_table' => 'advertisement_campaign_images'
        )

   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}