<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_devices extends DataMapper {

    //put your code here
    var $table = 'devices';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   // var $has_one = array(
   //         'product' => array(
   //           'class' => 'M_product',
   //             'other_field' => 'product_image',
   //             'join_other_as' => 'product',
   //             'join_table' => 'product'
   //         )
   // );
    
    var $has_many = array(
       'device_addresses' => array(
           'class' => 'M_device_addresses',
           'other_field' => 'devices',
           'join_self_as' => 'devices',
           'join_other_as' => 'devices',
           'join_table' => 'device_addresses')
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}