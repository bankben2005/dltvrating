<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_channel_components_update_logs extends DataMapper {

    //put your code here
    var $table = 'channel_components_update_logs';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'channel_components' => array(
             'class' => 'M_channel_components',
               'other_field' => 'channel_components_update_logs',
               'join_other_as' => 'channel_components',
               'join_table' => 'channel_components'
           )
   );
    
   //  var $has_many = array(
   //     'systemuser' => array(
   //         'class' => 'M_systemuser',
   //         'other_field' => 'branch',
   //         'join_self_as' => 'BranchCode',
   //         'join_other_as' => 'BranchCode',
   //         'join_table' => 'SystemUser')
   // );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}