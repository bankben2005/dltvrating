<div class="row mb-3">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header"><i class="fa fa-search"></i> Search</div>
			<div class="card-body">
				<?php echo form_open('',['name'=>'search-box-form','method'=>'get'])?>
					<div class="row">
						<div class="col-lg-4">
							<?php echo form_input([
								'name'=>'search_program_name',
								'class'=>'form-control',
								'placeholder'=>'Program name...',
								'value'=>@$_GET['search_program_name']
							])?>
						</div>

						<div class="col-lg-4">
							<?php echo form_dropdown('search_dow',[
		                      ''=>'-- select date of week --',
		                      'Sun'=>'Sunday',
		                      'Mon'=>'Monday',
		                      'Tue'=>'Tueday',
		                      'Wed'=>'Wednesday',
		                      'Thu'=>'Thursday',
		                      'Fri'=>'Friday',
		                      'Sat'=>'Saturday'
		                    ],@$_GET['search_dow'],['class'=>'form-control'])?>
						</div>

						<div class="col-lg-4">
							<?php echo form_button([
								'type'=>'submit',
								'class'=>'btn btn-success',
								'content'=>__('Search','default')
							])?>

							<?php if(!empty($_GET)){
								echo anchor($this->uri->uri_string(),__('Reset','default'),[
									'class'=>'btn btn-danger'
								]);
							} ?>
						</div>	



					</div>	

				<?php echo form_close()?>

			</div>
		</div>
	</div>
</div>