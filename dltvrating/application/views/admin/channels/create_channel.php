<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo ($channel_data->id)?__('Edit Channel'):__('Create Channel')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Channel List')?></a></li>
          <li class="breadcrumb-item active"><?php echo ($channel_data->id)?__('Edit Channel'):__('Create Channel')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($channel_data->id)?__('Edit Channel Form').' ('.$channel_data->channel_name.')':__('Create Channel Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-channel-form'))?>
                  <div class="row">
                  <div class="col-lg-4">

                      <div class="form-group">
                        <label><strong><?php echo __('Channel Name')?> : </strong></label>
                        <?php echo form_input(array(
                          'name'=>'channel_name',
                          'class'=>'form-control',
                          'value'=>@$channel_data->channel_name
                        ))?>
                      </div>

                      <div class="form-group">
                        <label><strong><?php echo __('Channel Description')?> : </strong></label>
                        <?php echo form_textarea(array(
                          'name'=>'channel_description',
                          'class'=>'form-control',
                          'value'=>@$channel_data->channel_description,
                          'rows'=>3
                        ))?>
                      </div>

                      <div class="form-group">
                        <label><strong><?php echo __('Corporate')?> <?php echo __('(Optional)')?> : </strong></label>
                        <?php echo form_dropdown('corporate',$corporate,@$channel_data->corporate_id,'class="form-control"')?>
                      </div>

                      <div class="form-group">
                        <label><strong><?php echo __('Channel Extended')?> : </strong></label>
                        <?php echo form_dropdown('channel_extended_status',array(
                          '0'=>__('No'),
                          '1'=>__('Yes')
                        ),@$channel_data->channel_extended_status,'class="form-control"')?>
                      </div>

                      <div class="form-group" id="channel-extended-from" style="display: none;">
                        <label><strong><?php echo __('Channel Extended From')?> : </strong></label>
                        <?php echo form_dropdown('channel_extended_from_channels_id',@$select_channel_extended_from,@$channel_data->channel_extended_from_channels_id,'class="form-control"')?>
                      </div>


                      <div class="form-group">
                        <label><strong><?php echo __('Status')?> : </strong></label>
                        <?php echo form_dropdown('status',array(
                          '0'=>__('Unactive'),
                          '1'=>__('Active')
                        ),@$channel_data->active,'class="form-control"')?>
                      </div>

                      <?php //print_r($this->session->userdata())?>
                      <?php if($this->session->userdata('admin_data')['email'] == 'jquery4me@gmail.com'){?>
                      <div class="form-group">
                        <label><strong>C-Band Ordinal :</strong></label>
                        <?php echo form_input(array(
                          'name'=>'cband_ordinal',
                          'class'=>'form-control',
                          'value'=>@$channel_data->cband_ordinal
                        ))?>
                      </div>
                      <?php }?>

                      <?php if($this->session->userdata('admin_data')['email'] == 'jquery4me@gmail.com'){?>

                      <div class="form-group">
                        <label><strong>KU-Band Ordinal : </strong></label>
                        <?php echo form_input(array(
                          'name'=>'kuband_ordinal',
                          'class'=>'form-control',
                          'value'=>@$channel_data->kuband_ordinal
                        ))?>
                      </div>
                      <?php }?>

                     
                      <div class="form-group">
                        <label><strong>Merge S3remote Channel : </strong></label>
                        <?php echo form_dropdown('merge_s3remote_id',@$s3remote_select_channel,@$channel_data->merge_s3remote_id,'class="form-control"')?>
                      </div>

                      <!-- <div class="form-group">
                        <label><strong>Logo : </strong></label>
                        <?php echo form_input(array(
                          'name'=>'logo',
                          'class'=>'form-control',
                          'value'=>@$channel_data->logo
                        ))?>
                      </div> -->

                      <div class="form-group">
                        <label><strong><?php echo __('Enable tv program')?> : </strong></label>
                        <?php echo form_dropdown('enable_tv_program',[
                          '1'=>__('Enable'),
                          '0'=>__('Disable')
                        ],@$channel_data->enable_tv_program,'class="form-control"')?>
                      </div>

                      <div class="form-group">
                        <label><strong><?php echo __('Remark')?> : </strong> (Such as channel ordinal...)</label>
                        <?php echo form_textarea([
                          'name'=>'remark',
                          'class'=>'form-control',
                          'rows'=>5,
                          'value'=>$channel_data->remark
                        ])?>
                      </div>






                  </div>
                  <div class="col-lg-8">

                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label><strong><?php echo __('Channel Type')?> : </strong></label>
                          <?php echo form_dropdown('channel_type',array(
                            'S'=>__('Satellite'),
                            'F'=>__('Digital TV')
                          ),@$channel_data->channel_type,'class="form-control"')?>
                        </div>
                      </div>
                      <div class="col-lg-4">

                        <div class="form-group">
                          <label><strong><?php echo __('DVB TYPE')?></strong></label>
                          <?php echo form_dropdown('dvb_type',array(
                            'S1'=>__('S1'),
                            'S2'=>__('S2'),
                            'S3'=>__('S3')
                          ),@$channel_data->dvb_type,'class="form-control"')?>
                        </div>


                      </div>
                      <div class="col-lg-4">
                          <div class="form-group">
                            <label><strong><?php echo __('DVB Def')?> : </strong></label>
                            <?php echo form_dropdown('dvb_def',array(
                              'SD'=>__('SD'),
                              'HD'=>__('HD')
                            ),@$channel_data->dvb_def,'class="form-control"')?>
                          </div>

                        </div>

                    </div>

                    <div class="row">
                      <div class="col-lg-4">

                        <div class="form-group">
                            <label><strong><?php echo __('Band Type')?> : </strong></label>
                            <?php echo form_dropdown('band_type',array(
                              'ALL'=>__('ALL'),
                              'C'=>__('C-Band'),
                              'KU'=>__('KU-Band')
                              
                            ),@$channel_data->band_type,'class="form-control"')?>
                        </div>

                      </div>
                      <div class="col-lg-8">
                            <label><strong><?php echo __('Channel Category')?> : </strong></label>
                            <?php echo form_dropdown('channel_categories_id',@$channel_categories,@$channel_data->channel_categories_id,'class="form-control"')?>
                      </div>
                      
                    </div>

                    <div class="row">
                      <div class="col-lg-12">

                        <div class="alert alert-warning">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="cband-tab" data-toggle="tab" href="#cband" role="tab" aria-controls="cband" aria-selected="true">C-Band</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="kuband-tab" data-toggle="tab" href="#kuband" role="tab" aria-controls="kuband" aria-selected="false">KU-Band</a>
                          </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="cband" role="tabpanel" aria-labelledby="cband-tab">
                              <div class="row mt-2">
                                <div class="col-lg-6">

                                  <div class="form-group">
                                    <label><strong><?php echo __('FRQ')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'cband_frq',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['CBAND']['frq']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('VDO PID')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'cband_vdo_pid',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['CBAND']['vdo_pid']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('SERVICE ID')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'cband_service_id',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['CBAND']['service_id']
                                    ))?>
                                  </div>  




                                </div>
                                <div class="col-lg-6">

                                  <div class="form-group">
                                    <label><strong><?php echo __('SYM')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'cband_sym',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['CBAND']['sym']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('ADO PID')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'cband_ado_pid',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['CBAND']['ado_pid']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('POL')?> : </strong></label>
                                    <?php echo form_dropdown('cband_pol',array(
                                      'H'=>'H','V'=>'V'
                                    ),@$channel_components['CBAND']['pol'],'class="form-control"')?>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="tab-pane fade" id="kuband" role="tabpanel" aria-labelledby="kuband-tab">
                            <div class="row mt-2">
                                <div class="col-lg-6">

                                  <div class="form-group">
                                    <label><strong><?php echo __('FRQ')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'kuband_frq',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['KUBAND']['frq']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('VDO PID')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'kuband_vdo_pid',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['KUBAND']['vdo_pid']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('SERVICE ID')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'kuband_service_id',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['KUBAND']['service_id']
                                    ))?>
                                  </div>  




                                </div>
                                <div class="col-lg-6">

                                  <div class="form-group">
                                    <label><strong><?php echo __('SYM')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'kuband_sym',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['KUBAND']['sym']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('ADO PID')?> : </strong></label>
                                    <?php echo form_input(array(
                                      'name'=>'kuband_ado_pid',
                                      'class'=>'form-control',
                                      'value'=>@$channel_components['KUBAND']['ado_pid']
                                    ))?>
                                  </div>

                                  <div class="form-group">
                                    <label><strong><?php echo __('POL')?> : </strong></label>
                                    <?php echo form_dropdown('kuband_pol',array(
                                      'H'=>'H','V'=>'V'
                                    ),@$channel_components['KUBAND']['pol'],'class="form-control"')?>
                                  </div>





                                </div>
                              </div>


                          </div>
                        </div>

                      </div>

                      </div>
                    </div>


                  <?php if(@$channel_data->id){?>                     
                    <div class="alert alert-info">
                      <h5 class="text-center"><?php echo __('Channel components update log')?></h5>
                      <table class="table" style="font-size: 9px;">
                        <thead>
                          <tr>
                            <th><?php echo __('FRQ/SYM/POL')?></th>
                            <th><?php echo __('VDO PID')?></th>
                            <th><?php echo __('ADO PID')?></th>
                            <th><?php echo __('SERVICE ID')?></th>
                            <th><?php echo __('BAND TYPE')?></th>
                            <th><?php echo __('UPDATED BY')?></th>
                            <th><?php echo __('FREQUENCY END DATE')?></th>

                          </tr>
                        </thead>

                        <tbody>

                          <?php 
                          if($channel_components_update_logs->result_count() > 0){ 
                           foreach($channel_components_update_logs as $key => $row){?>
                            <tr>
                              <td><?php echo $row->frq.' / '.$row->sym.' / '.$row->pol?></td>
                              <td><?php echo $row->vdo_pid?></td>
                              <td><?php echo $row->ado_pid?></td>
                              <td><?php echo $row->service_id?></td>
                              <td><?php echo $row->channel_components->get()->band_type?></td>
                              <td><?php echo $row->updated_by?></td>
                              <td><?php echo date('d/m/Y H:i',strtotime($row->created))?></td>
                            </tr>
                          <?php } }else{?>
                            <tr>
                              <td colspan="7" align="center"><span style="color:red;"><?php echo __('Not found component update log')?></span></td>
                            </tr>
                          <?php }?>
                        </tbody>
                        
                      </table>
                    </div>
                  <?php }?>

                      

                      

                      
                  </div>

                  <div class="col-lg-12">
                    

                      

                      

                      

                      
                      
                      
                  </div>

                  <div class="col-lg-12">
                    <?php echo form_button(array(
                      'type'=>'submit',
                      'class'=>'btn btn-success float-right',
                      'content'=>__('Submit','default')
                    ))?>
                  </div>  
                </div>
                  <?php echo form_close()?>
            </div>
            
            
        </div>
</div>