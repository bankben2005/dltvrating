<style type="text/css">
    .modal-medium{
        max-width: 70%;
    }
</style>
<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Device Detail')?> (<?php echo $device_data->ship_code?>)</h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Device List')?></a></li>
          <li class="breadcrumb-item active"><?php echo __('Devices Detail')?></li>
        </ul>
</div>
<div class="tile mb-4">
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Device Detail')?></h2>
            </div>
          </div>
        </div>
        <div class="row">
        	<div class="col-lg-12 mb-3">
        		<div id="map-canvas" style="width:100%; min-height:300px;">

	        	</div>
        	</div>
        	<div class="col-lg-6">
	        	<div class="card">
	        		<div class="card-header text-center"><?php echo __('Device Information')?></div>
	        		<div class="card-body">
	        			<p><strong><?php echo __('Ship Code')?> : </strong><?php echo $device_data->ship_code?></p>
	        			<p><strong><?php echo __('IP Address')?> : </strong> <?php echo ($latest_ip_address)?$latest_ip_address:$device_data->ip_address?></p>
	        			<p><strong><?php echo __('Created')?> : </strong> <?php echo $device_data->created?></p>

                        <p><strong><?php echo __('Updated')?> : </strong> <?php echo $device_data->updated?></p>

                        
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="javascript:void(0)" class="btn btn-success" onclick="updateDeviceAddressByIpAddress(this)" data-ip="<?php echo ($latest_ip_address)?$latest_ip_address:$device_data->ip_address?>" data-deviceaddressesid="<?php echo $device_data->id?>">
                                    <i class="fa fa-refresh"></i> <?php echo __('Update data by ip address')?>
                                </a>
                            </div>
                        </div>

	        			
	        		</div>
	        	</div>
        	</div>

        	<div class="col-lg-6">
        		<div class="card">
        			<div class="card-header text-center"><?php echo __('Address Information')?></div>
        			<div class="card-body">
        				<p><strong><?php echo __('Continent')?> : </strong><?php echo $device_data->continent_name?></p>
        				<p><strong><?php echo __('Country')?> : </strong><?php echo $device_data->country_name.' ('.$device_data->country_code.')'?></p>

                        <div class="row">
                            <!-- <div class="col-lg-3">
                                <p><strong><?php echo __('Region name')?> : </strong></p>
                            </div> -->
                            <div class="col-lg-12">
                                <div class="form-group">
                                <label><strong><?php echo __('Region Name')?> : </strong></label>
                                <a href="javascript:void(0)" data-toggle="modal"  data-target="#allProvince"><?php echo __('All Province')?></a>
                                <?php echo form_dropdown('region_name',@$select_region,@$device_data->region_name,'class="form-control" onchange="changeSelectRegion(this)" data-deviceaddressesid="'.$device_data->id.'"')?>
                                </div>
                            </div><!-- 
                            <div class="col-lg-6"></div> -->
                        </div>
                        

        				<p><strong><?php echo __('City')?> : </strong><?php echo $device_data->city?></p>
        				<p><strong><?php echo __('Zipcode')?> : </strong><?php echo $device_data->zip?></p>
                        <p><strong><?php echo __('Latitude')?> : </strong><?php echo $device_data->latitude?></p>
                        <p><strong><?php echo __('Longitude')?> : </strong><?php echo $device_data->longitude?></p>

                        <div class="form-group">
                            <label><strong><?php echo __('Province Region')?> : </strong>


                            </label>
                            <?php echo form_dropdown('province_region',[
                                ''=>__('Select Province Region'),
                                'Bangkok'=>'Bangkok',
                                'Northern'=>'Northern',
                                'NorthEastern'=>'NorthEastern',
                                'Central'=>'Central',
                                'Eastern'=>'Eastern',
                                'Southern'=>'Southern',
                                'Western'=>'Western'
                            ],@$device_data->province_region,'class="form-control" onchange="changeSelectProvinceRegion(this)" data-deviceaddressesid="'.$device_data->id.'"')?>
                        </div>

                        <p><strong><?php echo __('Confirm latitude longitude from api')?> : </strong>
                            <?php if($device_data->update_from_api){?>
                                <span class="badge badge-success"><?php echo __('Confirm')?></span>
                            <?php }else{?>
                                <span class="badge badge-secondary"><?php echo __('Unconfirm')?></span>
                            <?php }?>
                        </p>
                        

                        <?php if($device_data->zip && !$device_data->province_region){?>
                            <a href="javascript:void(0)" class="btn btn-secondary" onclick="updateProvinceRegionByZipcode(this)" data-zipcode="<?php echo $device_data->zip?>" data-deviceaddressesid="<?php echo $device_data->id?>">
                                <i class="fa fa-refresh"></i> <?php echo __('Update province region by zipcode')?>
                            </a>
                        <?php }?>

        				<!-- <p><strong><?php echo __('Location')?> : </strong></p> -->
        				<?php echo form_input(array(
        					'type'=>'hidden',
        					'name'=>'device_latitude',
        					'value'=>@$device_data->latitude
        				))?>
        				<?php echo form_input(array(
        					'type'=>'hidden',
        					'name'=>'device_longitude',
        					'value'=>@$device_data->longitude
        				))?>
        				<?php echo form_input(array(
        					'type'=>'hidden',
        					'name'=>'device_address',
        					'value'=>@$device_data->city
        				))?>
	        			
        			</div>
        		</div>
        	</div>
        </div>

        <div class="row mt-2">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-center"><?php echo __('Location')?></div>
                    <div class="card-body">
                        <pre>
                        <?php print_r(json_decode($device_data->location))?>
                        </pre>
                    </div>
                </div>
            </div>
        </div>
</div>

<!-- Modal -->
<div class="modal fade" id="allProvince" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-medium" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('All Province')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Province')?></th>
                    <th><?php echo __('Province Region')?></th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($all_province as $key => $row){?>
                        <tr>
                            <td><?php echo $row->province_name?></td>
                            <td><?php echo $row->province_region?></td>
                        </tr>
                    <?php }?>
                </tbody>
        </table>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASMadJM_c7vFjnWLWLrM5u2rQeZYGOLKM&callback=initialize_map&libraries=geometry,places&time=<?php echo strtotime(date('Y-m-d H:i:s'))?>">
</script>