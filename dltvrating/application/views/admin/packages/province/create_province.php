    <div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo ($province->id)?__('Edit Province'):__('Create Province')?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Province List')?></a></li>
              <li class="breadcrumb-item active"><?php echo ($province->id)?__('Edit Province'):__('Create Province')?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($province->id)?__('Edit Province Form').' ('.$province->province_name.')':__('Create Province Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-province-form'))?>
                      <div class="row">
                        <div class="col-lg-4">

                          <div class="form-group">
                            <label><strong><?php echo __('Province Name')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'province_name',
                              'class'=>'form-control',
                              'value'=>@$province->province_name
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Province Region')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'province_region',
                              'class'=>'form-control',
                              'value'=>@$province->province_region
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('IP Stack province name')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'ipstack_province_name',
                              'class'=>'form-control',
                              'value'=>@$province->ipstack_province_name
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Population')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'population',
                              'class'=>'form-control',
                              'value'=>@$province->population
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Latitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'latitude',
                              'class'=>'form-control',
                              'value'=>@$province->latitude
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Longitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'longitude',
                              'class'=>'form-control',
                              'value'=>@$province->longitude
                            ])?>
                          </div>

                          <div class="form-group">
                            <?php echo form_button([
                              'type'=>'submit',
                              'class'=>'btn btn-success float-right',
                              'content'=>__('Submit','default')
                            ])?>
                          </div>

                        </div>

                        <div class="col-lg-4">

                        </div>

                        <div class="col-lg-4">

                        </div>
                      </div>

                  <?php echo form_close()?>

            </div>
        </div>
    </div>