    <div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo ($district->id)?__('Edit district'):__('Create district')?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('District List')?></a></li>
              <li class="breadcrumb-item active"><?php echo ($district->id)?__('Edit district'):__('Create district')?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($district->id)?__('Edit District Form').' ('.$district->district_name.')':__('Create District Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-district-form'))?>
                      <div class="row">
                        <div class="col-lg-4">

                          <div class="form-group">
                            <label><strong><?php echo __('District Name')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'district_name',
                              'class'=>'form-control',
                              'value'=>@$district->district_name
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Latitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'latitude',
                              'class'=>'form-control',
                              'value'=>@$district->latitude
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Longitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'longitude',
                              'class'=>'form-control',
                              'value'=>@$district->longitude
                            ])?>
                          </div>

                          <div class="form-group">
                            <?php echo form_button([
                              'type'=>'submit',
                              'class'=>"btn btn-success float-right",
                              'content'=>__('Submit','default')
                            ])?>
                          </div>

                        </div>
                      </div>
                    <?php echo form_close()?>
            </div>
        </div>
      </div>
