<div class="row mb-3">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header"><i class="fa fa-search"></i> <?php echo __('Search')?></div>
			<div class="card-body">
				<?php echo form_open('',['name'=>'channel-list-search-form','method'=>'get','onsubmit'=>'checkSubmitChannelSearchForm(event)'])?>
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<?php echo form_input([
									'name'=>'search_channel_name',
									'class'=>'form-control',
									'value'=>@$_GET['search_channel_name'],
									'placeholder'=>__('Channel name...')
								])?>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="form-group">
								<?php echo form_input([
									'name'=>'search_frq',
									'class'=>'form-control',
									'value'=>@$_GET['search_frq'],
									'placeholder'=>__('Frq..')
								])?>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="form-group">
								<?php echo form_input([
									'name'=>'search_sym',
									'class'=>'form-control',
									'value'=>@$_GET['search_sym'],
									'placeholder'=>__('Sym..')
								])?>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="form-group">
								<?php echo form_dropdown('search_pol',[
									''=>__('-- Select POL --'),
									'V'=>'V',
									'H'=>'H'
								],@$_GET['pol'],'class="form-control"')?>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<?php echo form_input([
									'name'=>'search_vdo_pid',
									'class'=>'form-control',
									'value'=>@$_GET['search_vdo_pid'],
									'placeholder'=>__('VDO PID..')
								])?>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="form-group">
								<?php echo form_input([
									'name'=>'search_ado_pid',
									'class'=>'form-control',
									'value'=>@$_GET['search_ado_pid'],
									'placeholder'=>__('ADO PID..')
								])?>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="form-group">
								<?php echo form_input([
									'name'=>'search_service_id',
									'class'=>'form-control',
									'value'=>@$_GET['search_service_id'],
									'placeholder'=>__('SERVICE ID..')
								])?>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-lg-12">
							<?php if(!empty($_GET)){?>
								<a href="<?php echo base_url($this->uri->uri_string())?>" class="btn btn-danger float-right ml-2"><?php echo __('Reset','default')?></a>
							<?php }?>

							<?php echo form_button([
								'type'=>'submit',
								'class'=>'btn btn-success float-right',
								'content'=>'<i class="fa fa-search"></i> '.__('Search')
							])?>


						</div>
					</div>	

				<?php echo form_close()?>

			</div>
		</div>
	</div>
</div>