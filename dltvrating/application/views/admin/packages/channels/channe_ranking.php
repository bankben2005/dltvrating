      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Channels Ranking Control')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Channels Ranking Control')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Channel Name')?></th>
                    <th><?php echo __('Show on ranking')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($channels as $key => $row){?>
                    <tr>
                      <td><?php echo $row->channel_name?></td>
                      <td>
                        <label class="switch">
                              <?php echo form_checkbox(array(
                                    'name'=>'show_on_ranking',
                                    'value'=>$row->id,
                                    'checked'=>($row->show_on_ranking)?TRUE:FALSE,
                                    'onclick'=>"clickEnableDisableShowOnRanking(this)"
                              ))?>
                              <span class="slider"></span>
                        </label>
                      </td>
                      <td></td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
