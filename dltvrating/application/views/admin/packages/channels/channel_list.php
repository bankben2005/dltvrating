      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Channels')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Channels')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/channels/createChannel')?>" class="btn btn-success float-right <?php echo ($this->admin_data['can_action_button'] > 0) ? "" : "d-none";?>"><?php echo __('Create Channel')?></a>
              <div class="clearfix mb-3"></div>


              <!-- for search form -->
              <?php $this->load->view('admin/channels/searchbox/channel_list_search')?>
              <!-- eof for search form -->

              <?php if(!empty($_GET)){?>
              <div class="row">
                <div class="col-lg-12">
                  <div class="alert alert-success">
                    <?php echo __('Result')?> <?php echo @$result_count?> <?php echo __('Record(s)')?>
                  </div>
                </div>
              </div>
              <?php }?>

              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Channel Name')?></th>
                    <th><?php echo __('Frequency Component')?></th>
                    <!-- <th><?php echo __('FRQ')?></th>
                    <th><?php echo __('SYM')?></th>
                    <th><?php echo __('VDO PID')?></th>
                    <th><?php echo __('ADO PID')?></th>
                    <th><?php echo __('Service ID')?></th> -->
                    <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                    <?php if($this->admin_data['can_action_button'] > 0){ ?>
                        <th></th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($channels as $key => $row){?>
                    <tr>
                      <td><?php echo $row->channel_name?></td>
                      <td>
                          <?php 

                            $channels_id = ($row->channels_id)?$row->channels_id:$row->id;
                            $ch_component = new M_channel_components();
                            $ch_component->where('channels_id',$channels_id)
                            ->get();
                          ?>

                          <?php foreach($ch_component as $k => $v){ 
                            $frequency_html = 'Frq : '.$v->frq.'<br>';
                            $frequency_html .= 'Sym : '.$v->sym.'<br>';
                            $frequency_html .= 'Pol : '.$v->pol.'<br>';
                            $frequency_html .= 'Vdo pid : '.$v->vdo_pid.'<br>';
                            $frequency_html .= 'Ado pid : '.$v->ado_pid.'<br>';                   
                            $frequency_html .= 'Service id : '.$v->service_id.'<br>';
                          ?>
                          <button class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php echo $frequency_html?>"><?php echo $v->band_type?> Band</button>

                          <?php }?>
                      </td>
                     <!--  <td><?php echo $row->frq?></td>
                      <td><?php echo $row->sym?></td>
                      <td><?php echo $row->vdo_pid?></td>
                      <td><?php echo $row->ado_pid?></td>
                      <td><?php echo $row->service_id?></td> -->
                      <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                      <?php if($this->admin_data['can_action_button'] > 0){ ?>
                          <td>
                            <a href="<?php echo base_url('admin/channels/editChannel/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>

                            <a href="<?php echo base_url('admin/channels/setChannelProgramBODate/'.$row->id)?>" class="btn btn-success btn-sm">
                              <i class="fa fa-bar-chart"></i> <?php echo __('Set channel program by date')?>
                            </a>

                            <a href="<?php echo base_url('admin/channels/setChannelProgram/'.$row->id)?>" class="btn btn-info btn-sm"><i class="fa fa-bar-chart"></i> <?php echo __('Set default channel program')?></a>
                            <!-- <a href="<?php echo base_url('admin/channels/setChannelPrivilege/'.$row->id)?>" class="btn btn-info btn-sm"><i class="fa fa-key"></i> <?php echo __('Set channel privilege')?></a> -->

                            <?php if($this->admin_data['email'] == 'jquery4me@gmail.com'){?>
                            <a href="javascript:void(0)" onclick="if(confirm('ต้องการลบช่องรายการนี้ใช่หรือไม่') == true){window.location.href='<?php echo base_url('admin/channels/deleteChannel/'.$row->id)?>'}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                            <?php }?>
                          </td>
                      <?php } ?>
                    </tr>
                  <?php }?>
                </tbody>
              </table>

              <!-- for pagination -->
              <div class="row">
                <div class="col-lg-12">
                  <?php echo $pages?>
                </div>
              </div>
              <!-- eof for pagination -->
            </div>
          </div>
        </div>
      </div>