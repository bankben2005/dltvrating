    <div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo ($amphur->id)?__('Edit amphur'):__('Create amphur')?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('amphur List')?></a></li>
              <li class="breadcrumb-item active"><?php echo ($amphur->id)?__('Edit amphur'):__('Create amphur')?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($amphur->id)?__('Edit Amphur Form').' ('.$amphur->amphur_name.')':__('Create Amphur Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-amphur-form'))?>
                      <div class="row">
                        <div class="col-lg-4">

                          <div class="form-group">
                            <label><strong><?php echo __('Province')?> : </strong></label>
                            <?php echo form_dropdown('provinces_id',@$select_province,@$amphur->provinces_id,'class="form-control"')?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Amphur Name')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'amphur_name',
                              'class'=>'form-control',
                              'value'=>@$amphur->amphur_name
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Latitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'latitude',
                              'class'=>'form-control',
                              'value'=>@$amphur->latitude
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Longitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'longitude',
                              'class'=>'form-control',
                              'value'=>@$amphur->longitude
                            ])?>
                          </div>

                          <div class="form-group">
                            <?php echo form_button([
                              'type'=>'submit',
                              'class'=>"btn btn-success float-right",
                              'content'=>__('Submit','default')
                            ])?>
                          </div>

                        </div>
                      </div>
                    <?php echo form_close()?>
            </div>
        </div>
      </div>
