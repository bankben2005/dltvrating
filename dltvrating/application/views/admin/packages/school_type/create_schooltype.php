<div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo ($M_school_type_->id)?__('Edit Affiliate'):__('Create Affiliate')?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Affiliate List')?></a></li>
              <li class="breadcrumb-item active"><?php echo ($M_school_type_->id)?__('Edit Affiliate'):__('Create Affiliate')?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($M_school_type_->id)?__('Edit Affiliate Form').' ('.$M_school_type_->SchoolType.')':__('Create Affiliate Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-schooltype-form'))?>
                      <div class="row">
                        <div class="col-lg-4">
                            
                        <div class="form-group">
                          <label for=""><?php echo __('Parent Affiliate')?></label>
                          <?php echo form_dropdown('school_group',$school_group,$M_school_type_->parent_id,'class="form-control" id="school_group"')?>
                        </div>

                          <div class="form-group">
                            <label><strong><?php echo __('School Affiliate')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'school_type',
                              'class'=>'form-control',
                              'value'=>$M_school_type_->SchoolType
                            ])?>
                          </div>

                         

                          <div class="form-group">
                            <?php echo form_button([
                              'type'=>'submit',
                              'class'=>'btn btn-success float-right',
                              'content'=>__('Submit','default')
                            ])?>
                          </div>

                        </div>

                        <div class="col-lg-4">

                        </div>

                        <div class="col-lg-4">

                        </div>
                      </div>

                  <?php echo form_close()?>

            </div>
        </div>
    </div>