<div class="form-group">
	<label> 
		<span><?php echo form_checkbox([
			'name'=>'show_hide_channels_ranking',
			'value'=>1,
			'checked'=>($rating_packages->show_hide_channels_ranking)?TRUE:FALSE
		])?>

	</span>
	<strong> <?php echo __('Show hide channels ranking (Shopping)')?></strong> 

</label>
</div>



<!-- compare channel  -->

<p>
	<a href="#collapseCompareChannelMenu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseCompareChannelMenu" onclick="clickCollapseMenu(this)">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'compare_channel_menu_status',
					'value'=>1,
					'checked'=>($rating_packages->compare_channel_menu_status)?TRUE:FALSE
				])?>
			</span> 
			<strong> <?php echo __('Compare Channel menu')?></strong>
		</label>

	</a>
</p>

<div class="collapse" id="collapseCompareChannelMenu">
	
</div>
<!-- compare channel  -->


<!-- top twenty channel -->

<p>
	<a href="#collapseTopTwentyMenu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseTopTwentyMenu" onclick="clickCollapseMenu(this)">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'top_twenty_channels_menu_status',
					'value'=>1,
					'checked'=>($rating_packages->top_twenty_channels_menu_status)?TRUE:FALSE
				])?>
			</span> 
			<strong> <?php echo __('Top twenty menu')?></strong>
		</label>

	</a>
</p>


<div class="collapse" id="collapseTopTwentyMenu">
	
</div>



<p>
	<a href="#collapseTVProgramMenu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseTVProgramMenu" onclick="clickCollapseMenu(this)">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'show_tv_program_status',
					'value'=>1,
					'checked'=>($rating_packages->show_tv_program_status)?TRUE:FALSE
				])?>
			</span>
			<strong> <?php echo __('TV program menu')?></strong>
		</label>
	</a>
</p>
<div class="collapse" id="collapseTVProgramMenu">
	
</div>


<p>
	<a href="#collapseCompareChannelByRegionMenu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseCompareChannelByRegionMenu" onclick="clickCollapseMenu(this)">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'compare_channel_by_region_menu_status',
					'value'=>1,
					'checked'=>($rating_packages->compare_channel_by_region_menu_status)?TRUE:FALSE
				])?>
			</span> 
			<strong> <?php echo __('Compare channel by region menu')?></strong>
		</label>

	</a>
</p>
<div class="collapse" id="collapseCompareChannelByRegionMenu">
	
</div> 

<p>
	<a href="#collapseCompareChannelExportMenu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseCompareChannelExportMenu" onclick="clickCollapseMenu(this)">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'compare_channel_export_status',
					'value'=>1,
					'checked'=>($rating_packages->compare_channel_export_status)?TRUE:FALSE
				])?>
			</span> 
			<strong> <?php echo __('Compare channel export excel')?></strong>
		</label>

	</a>
</p>
<div class="collapse" id="collapseCompareChannelExportMenu">
	
</div>


<p>
	<a href="#collapseRealtimeViewersMenu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseRealtimeViewersMenu" onclick="clickCollapseMenu(this)">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'realtime_viewers_menu_status',
					'value'=>1,
					'checked'=>($rating_packages->realtime_viewers_menu_status)?TRUE:FALSE
				])?>
			</span> 
			<strong> <?php echo __('Realtime viewers menu')?></strong>
		</label>
	</a>
</p>

<div class="collapse" id="collapseRealtimeViewersMenu">

	<div class="card">

	<div class="card-body">
	<div class="form-group">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'show_total_device_status',
					'value'=>1,
					'checked'=>($rating_packages->show_total_device_status)?TRUE:FALSE
				])?>
			</span>
			<strong> <?php echo __('Show total devices')?></strong>
		</label>
	</div>





	<div class="form-group">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'show_population_and_devices_status',
					'value'=>1,
					'checked'=>($rating_packages->show_population_and_devices_status)?TRUE:FALSE
				])?>
			</span>
			<strong> <?php echo __('Show population and devices')?></strong>
		</label>
	</div>

	<div class="form-group">
		<label>
			<span>
				<?php echo form_checkbox([
					'name'=>'show_latlon_map_status',
					'value'=>1,
					'checked'=>($rating_packages->show_latlon_map_status)?TRUE:FALSE
				])?>
			</span>	
			<strong> <?php echo __('Show latitude/longitude map')?></strong>
		</label>
	</div>

</div>
</div>

	
</div>


