<div class="app-title">
  <div>
    <h1><i class="fa fa-th-list"></i> <?php echo ($rating_packages->id)?__('Edit Package'):__('Create Package')?></h1>
    <!-- <p>Table to display analytical data effectively</p> -->
  </div>
  <ul class="app-breadcrumb breadcrumb side">
    <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Package List')?></a></li>
    <li class="breadcrumb-item active"><?php echo ($rating_packages->id)?__('Edit Package'):__('Create Package')?></li>
  </ul>
</div>
<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="tile mb-4">


  <div class="page-header">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="mb-3 line-head" id="buttons"><?php echo ($rating_packages->id)?__('Edit Package').' ('.$rating_packages->name.')':__('Create Package')?> </h2>
      </div>
    </div>
  </div>
  <div class="row">

    <div class="col-lg-12">
    <?php echo form_open('',['name'=>'create-package-form'])?>

      <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label><strong><?php echo __('Package Name')?> : </strong></label>

          <?php echo form_input([
            'name'=>'name',
            'class'=>'form-control',
            'value'=>@$rating_packages->name,
            'required'=>'required'
          ])?>
        </div>

        <div class="form-group">
          <label><strong><?php echo __('Package Description')?> : </strong></label>
          <?php echo form_textarea([
            'name'=>'description',
            'class'=>'form-control',
            'value'=>@$rating_packages->description,
            'rows'=>3
          ])?>

        </div>

        <div class="form-group">
          <label><strong><?php echo __('Price/Month')?> : </strong></label>
          <?php echo form_input([
            'type'=>'number',
            'name'=>'price_per_month',
            'class'=>'form-control',
            'value'=>@$rating_packages->price_per_month,
            'requird'=>'requird'
          ])?>
        </div>

        <div class="form-group">
          <label><strong><?php echo __('Status')?> : </strong></label>
          <?php echo form_dropdown('active',[
            '1'=>__('Active','default'),
            '0'=>__('Unactive','default')
          ],@$rating_packages->active,'class="form-control"')?>
        </div>
      </div>
      <div class="col-lg-6">
        <?php $this->load->view('admin/packages/components/package_abilities')?>
      </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="form-group">
            <?php echo form_button([
              'type'=>'submit',
              'class'=>'btn btn-success float-right',
              'content'=>__('Save','default')
            ])?>
          </div>
        </div>
      </div>  

    <?php echo form_close()?>
    </div>

  </div>
</div>