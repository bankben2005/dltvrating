<style type="text/css">
  table#table-images > tbody > tr > td{
    vertical-align: middle;
    width: 200px;
  }
  table#table-images > tbody > tr > td > img{
    /*vertical-align: middle;*/
    /*width: 200px;
    height: 138px;*/

  }
  #show_screen{
        width: 99%;
    margin: 0 auto;
    margin-top: 20px;
  }
  #show_screen img{
    max-height: 116px;
    max-width: 168.13px;
    opacity: 1;
  }
  #show_screen .col-lg-2{
    display: table-cell;
    text-align: center;
    border:1px dashed #ddd;
    padding: 0;
    /*min-height: 139px;*/
    border-width: thin;
    vertical-align: middle;
    width: 200px;
    height: 116px;
  }
  #show_screen .col-lg-2 a{
    position: relative;
    top: 45px;
  }
  #show_screen .col-lg-2:hover{
    opacity: 0.7;
  }

  .view {
    border: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    position: relative;
    text-align: center;
    -webkit-box-shadow: 1px 1px 2px #e6e6e6;
    -moz-box-shadow: 1px 1px 2px #e6e6e6;
    box-shadow: 1px 1px 2px #e6e6e6;
    cursor: default;
    background: #fff url(../images/bgimg.jpg) no-repeat center center;
  }
  .view img{
    display:initial;
  }
  .view .mask, .view .content {
    width: 100%;
    height: 100%;
  }

  @media (min-width: 1367px) and (max-width: 1440px){
    #show_screen .col-lg-2{
      height: 127px;
    }
    #show_screen img{
     max-height: 127px;
    }
  }

  @media (min-width: 769px) and (max-width: 1024px){
    #show_screen .col-lg-2{
      height: 79px;
    }
    #show_screen img{
     max-height: 79px;
    }
    #show_screen .col-lg-2 a {
      position: relative;
      top: 19px;
    }
  }
</style>
<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Set advertisment images and video')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller.'/advertisement_campaign')?>"><?php echo __('Channel Campaign List')?></a></li>
          <li class="breadcrumb-item active"><?php echo __('Set advertisment images and video')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Set advertisment images and video')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">

                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'hide_ads_campaign_id',
                    'value'=>$ads_campaign->id
                  ))?>

                  <?php 
                  $hide_start_date = new DateTime();
                  $hide_start_date->add(new DateInterval('P1D'));
                  echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'hide_start_date',
                    'value'=>$hide_start_date->format('d-m-Y')
                  ))?>
                
                  <?php echo form_open('',array('name'=>'set-advertisement-imagevideo-form'))?>

                    


                    <div class="row">
                      <div class="col-lg-12" style="background-image: url('<?php echo base_url('assets/images/tv-background.png')?>');background-size: 84%;height:700px;background-repeat: no-repeat;">


                        <div class="col-lg-12" id="show_screen">
                        <?php 
                          $position = 1;
                          for($i=1;$i<=4;$i++){

                        ?>

                          <div class="row">
                             <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="col-lg-2 text-center">

                                <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo base_url('uploaded/advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image'])?>" class="img-responsive">
                                    <div class="mask">
                                                        <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                        <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>

                                                        <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('admin/').$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>
                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="col-lg-2">
                                <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo base_url('uploaded/advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image'])?>" class="img-responsive">
                                    <div class="mask">
                                            <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>
                                             
                                             <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                              
                                              <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('admin/').$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>

                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="col-lg-2">
                                <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo base_url('uploaded/advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image'])?>" class="img-responsive">
                                    <div class="mask">
                                                <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                           
                                                <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('admin/').$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>

                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="col-lg-2">
                                 <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo base_url('uploaded/advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image'])?>" class="img-responsive">
                                    <div class="mask">
                                                <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                           
                                                <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('admin/').$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>

                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="col-lg-2">
                                 <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo base_url('uploaded/advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image'])?>" class="img-responsive">
                                    <div class="mask">
                                                <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                           
                                                <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('admin/').$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>




                          </div>

                        <?php }?>
                        </div>

                        <table class="table mt-4 ml-2 pd-3 table-bordered" style="width: 98.5%;height: 79%; text-align: 
                        center;vertical-align: middle;display: none;" id="table-images">
                          <tbody>

                            <?php 

                            //print_r($ads_images);
                            $position =1;
                            for($i=1;$i<=4;$i++){
                            ?>

                            <tr>
                              <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                              <td style="text-align: center;vertical-align:middle;">
                                <?php if(@$arr_image['id']){?>
                                  <img src="<?php echo base_url('uploaded/advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image'])?>">
                                <?php }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal">

                                <?php }?>
                                

                              </td>
                              <td><a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a></td>
                              <td><a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a></td>
                              <td><a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a></td>
                              <td><a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a></td>
                            </tr>

                            <?php }?>
                            
                            
                          </tbody>
                        </table>

                      </div>
                    </div>
                    <?php echo form_close()?>
                    <hr>
                    <div class="row mb-3">
                      <div class="col-lg-12 mb-3 text-right">

                          <a href="javascript:void(0)" class="btn btn-info btn-sm" data-toggle="modal" data-target="#setYoutubeUrl"><i class="fa fa-plus"></i> <?php echo __('Click for add youtube url')?></a>
                          
                      </div>

                      <div class="col-lg-12">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><?php echo __('Video Url')?></th>
                              <th><?php echo __('Start Time')?></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php foreach($ads_campaign->advertisement_campaign_videos->get() as $key => $row){?>
                                <tr>
                                  <td><?php echo $row->video_url?></td>
                                  <td><?php echo $row->start_datetime?></td>
                                  <td>
                                    <a href="javascript:void(0)" class="btn btn-secondary btn-sm" data-startdatetime="<?php echo date('d-m-Y H:i',strtotime($row->start_datetime))?>" data-youtubeurl="<?php echo $row->video_url?>" data-adcvideoid="<?php echo $row->id?>" onclick="editAdsCampaignVideos(this)"><i class="fa fa-pencil"></i></a>

                                    <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteAdsCampaignVideos(this)" data-adcvideoid="<?php echo $row->id?>"><i class="fa fa-trash"></i></a>

                                  </td>
                                </tr>
                              <?php }?>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    


            </div>
        </div>
</div>

<!-- Modal -->
<div class="modal fade" id="uploadImageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Upload image and set duration')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <?php echo form_open_multipart('',array('name'=>'upload-image-form'))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'hide_position'
      ))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'advertisement_campaigns_id'
      ))?>
      <div class="modal-body">
          <div class="form-group">
            <label><strong><?php echo __('Duration')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'duration',
              'class'=>'form-control',
              'autocomplete'=>'off',
              'value'=>$hide_start_date->format('d-m-Y').' 00:00'.' - '.$hide_start_date->format('d-m-Y').' 00:00'
            ))?>
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Image')?> (jpg|png|gif)</strong></label>

            <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('ads_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                        </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
         <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Save','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="editAdsCampaignImageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Edit image and set duration')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open_multipart(base_url('admin/'.$this->controller.'/postEditAdsImage'),array('name'=>'edit-adsimage-form'))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'hide_ads_image_id',
        'value'=>''
      ))?>
      <div class="modal-body">
        

          <div class="form-group">
            <label><strong><?php echo __('Duration')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'edit_duration',
              'class'=>'form-control',
              'autocomplete'=>'off',
              'value'=>''
            ))?>
          </div>

          <div class="form-group">
            <img src="" class="img-fluid">
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Image')?> (jpg|png|gif)</strong></label>

            <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('ads_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                        </div>
          </div>


        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Save','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="setYoutubeUrl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Add/Edit youtube url and duration')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',array('name'=>'add-youtube-url'))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'ads_campaign_videos_id',
        'value'=>'0'
      ))?>
      <div class="modal-body">
        
          <div class="form-group">
            <label><strong><?php echo __('Start')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'start_datetime',
              'class'=>'form-control',
              'autocomplete'=>'off'
            ))?>
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Youtube URL')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'youtube_url',
              'class'=>'form-control',
            ))?>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Save','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>




<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo __('Image Preview')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <img src="" id="imagepreview" class="img-fluid" >
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>