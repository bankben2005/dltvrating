<div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo ($province->id)?__('Edit Classroom'):__('Create Classroom')?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Classroom List')?></a></li>
              <li class="breadcrumb-item active"><?php echo __('Create Classroom');?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">
   
        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($province->id)?__('Edit Classroom Form').' ('.$province->province_name.')':__('Create Classroom Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-classroom-form'))?>
                      

                      <div class="row">
                        <div class="col-lg-5">

                          <div class="form-group row">
                              <div class="col-lg-10">
                                <label><strong><?php echo __('Classroom')?> : </strong></label>
                                <?php echo form_input([
                                  'name'=>'class_name',
                                  'class'=>'form-control',
                                  'value'=>''
                                ])?>
                              </div>
                              <div class="col-lg-2"> 
                                      &nbsp;
                                </div>
                          </div>

                         <!-- dropdown : school -->
                        
                       <?php
                                         
                       ?>
                          <label><strong><?php echo __('School')?> : </strong></label>
                          <div class="form-group ">
                              <select id="select_school" name="select_school" class="selectpicker" data-live-search="true" title="Please select school">
                                 <?php foreach($schools as $schk =>$schv){ ?>
                                      <?php
                                        //print_r($schools);exit();
                                        ?>
                                       <option value="<?php echo $schk;?>"><?php echo $schv;?></option>
                             
                                  <?php } ?>
                              </select>
                          </div>
                       



                          <!-- dropdown : devices -->
                     
                  
                          <label><strong><?php echo __('Device')?> (Search By Chip ID): </strong></label>
                          <div class="form-group ">
                              <select id="select-deivces"  name="select-deivces" class="selectpicker placeholder" data-live-search="true" title="Please select device">
                                  <!-- <?php foreach($devices as $dvk =>$dvv){ ?>
                                      <?php
                                        //print_r($schools);exit();
                                        ?>
                                       <option value="<?php echo $dvk;?>"><?php echo $dvv;?></option>
                             
                                  <?php } ?> -->
                              </select>
                          </div>
                       

                          <!-- <div class="form-group">
                            <label><?php echo __('Status','backend/default')?> : </label>
                            <?php echo form_dropdown('active',array(
                              
                              '1'=>__('Active','backend/default'),
                              '0'=>__('Unactive','backend/default')
                            ),'','class="form-control"')?>
                          </div> -->

                          

                          <div class="form-group">
                             <?php echo form_button([
                              'type'=>'submit',
                              'class'=>'btn btn-success float-right',
                              'content'=>__('Submit','default')
                            ])?>
                            <!-- <span class="btn btn-success float-right" onclick ="alreadyUsed()">Submit</span> -->
                          </div>

                        </div>

                        <div class="col-lg-4">
                         
                        </div>

                        <div class="col-lg-4">
                      
                               
                       
                        </div>
                        
                      </div>

                  <?php echo form_close()?>
                              
            </div>
        </div>
    </div>
 
  <script>
        function alreadyUsed(){
          confirm("กล่องchip id 06eaa9da00001515 นี้ถูกเพิ่มอยู่ในห้องอื่นแล้ว ท่านต้องการย้ายกล่องมายังห้องนี้หรือไม่?");
        }


        
    </script>


    