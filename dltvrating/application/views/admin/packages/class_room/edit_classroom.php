<div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo __('Edit Classroom');?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Classroom List')?></a></li>
              <li class="breadcrumb-item active"><?php echo __('Edit Classroom');?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">
   
        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Edit Classroom Form');?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
  <!-- div form group -->
            <div class="col-lg-6">
                
                  <?php echo form_open('',array('name'=>'create-classroom-form'))?>
                      
  <?php

  ?>
                    <input type="hidden" name="SchoolID" value="<?php echo $class_room->SchoolID;?>">
                    <input type="hidden" name="class_id" value="<?php echo $class_room->id;?>">
                    <input type="hidden" id="user_id" name="user_id" value="<?php echo $this->admin_data['id'];?>">
                      <div class="row">
                      
                        <div class="col-lg-12">

                          <div class="form-group row">
                              <div class="col-lg-10">
                                <label><strong><?php echo __('Classroom')?> : </strong></label>
                                <?php echo form_input([
                                  'id'=>'class_name',
                                  'class'=>'form-control',
                                  'value'=>@$class_room->group_name,
                                  ($this->admin_data['can_action_button'] == 0) ? "readonly": "attr_"  => 1,
                                  'title' => ($this->admin_data['can_action_button'] == 0) ? " you don't have permission to edit this input.":"Classname"
                                ])?>
                              </div>
                              <div class="col-lg-2"> 
                                      &nbsp;
                                </div>
                          </div>

                         <!-- dropdown : school -->
                        
                       <?php
                        if($this->admin_data['can_action_button'] == 1){         
                       ?>
                          <label><strong><?php echo __('School')?> : </strong></label>
                          <div class="form-group " >
                              <select id="select_school" name="select_school" class="selectpicker" data-live-search="true" title="Please select school">
                                 <?php foreach($schools as $schk =>$schv){ ?>
                                      <?php
                                        //print_r($schools);exit();
                                        ?>
                                       <option value="<?php echo $schk;?>" <?php echo ($schk == $class_room->SchoolID) ? "selected" : "";?>><?php echo $schv;?></option>
                             
                                  <?php } ?>
                              </select>
                          </div>
                        <?php }else{?>
                        <?php
                        ?>
                            <div class="form-group row">
                                <div class="col-lg-10">
                                    <label><strong><?php echo __('School')?> : </strong></label>
                                    <?php echo form_input([
                                    'id'=>'class_name',
                                    'class'=>'form-control',
                                    'value'=>$schools[$class_room->SchoolID],
                                    ($this->admin_data['can_action_button'] == 0) ? "readonly": "attr_"  => 1,
                                    'title' => ($this->admin_data['can_action_button'] == 0) ? " you don't have permission to edit this input.":"Classname"
                                  ])?>
                                </div>
                                <div class="col-lg-2"> 
                                        &nbsp;
                                    </div>
                            </div>

                        <?php }?>
                       
                            <?php
                            //echo '<PRE>';
                            //print_r($devices_rel_class);exit();
                            if(!empty($devices_rel_class)){
                                $firstDeviceKey = key($devices_rel_class);
                            }
                           
                            ?>
                          <!-- dropdown : devices -->     
                          <label><strong><?php echo __('Device')?> (Search By Chip ID): </strong></label>
                      
                                <?php 
                             
                                if($this->admin_data['type'] == 'superadmin'){
                                    $superadmin_count = 0;
                                    foreach($devices_rel_class as $dvkx => $dvxk) {
                                      ++ $superadmin_count;
                                ?>
                                        <div class="form-group row">
                                            <div class="col-lg-9">
                                                      <select id="select-deivces"  name="select-deivces" class="selectpicker placeholder" data-live-search="true" title="Please select device">
                                                      <?php
                                                      if(!empty($devices_rel_class)){
                                                      ?>
                                                      <option value="<?php echo $devices_rel_class[$dvkx]->device_id;?>" selected><?php echo $devices_rel_class[$dvkx]->ship_code;?></option> 
                                                    <?php }?>
                                                      </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <?php if($superadmin_count == 1) {?>
                                                    <span class="btn btn-success " onclick ="createDD_devicelist()">+</span> 
                                                <?php } ?>
                                            </div>
                                        </div>
                                      
                                    <?php }?>
                                    </div>
                                <?php } ?>

                                <?php 
                                //echo ($this->admin_data['type']);
                                if($this->admin_data['type'] != 'superadmin' ){
                                      if(!empty($devices_rel_class)){
                                        $count = 0;
                                          foreach($devices_rel_class as $dvk => $dvv){
                                            ++ $count;
                                      ?>
                                              <div class="form-group row">
                                                  <div class="col-lg-9">
                                                
                                                        <?php echo form_input([
                                                          'id'=>'device_group_' .$devices_rel_class[$dvk]->device_id,
                                                          'class'=>'form-control',
                                                          'value'=>@$devices_rel_class[$dvk]->ship_code,
                                                          ($this->admin_data['type'] != 'superadmin') ? "readonly": "attr_"  => 1,
                                                          'title' =>  ($this->admin_data['type'] != 'superadmin') ? " you don't have permission to edit this input.":"device_group"
                                                        ])?>
                                                    </div>
                                                    <div class="col-lg-3">
                                                      <?php if($count == 1){ ?>
                                                          <span class="btn btn-success " onclick ="createDD_devicelist()">+</span> 
                                                      <?php } ?>
                                                  </div>
                                              </div>
                                              
                                          <?php }?>
                                    <?php }?> 
                                    </div>      

                                  <!-- if empty group-->
                                  <?php if(empty($devices_rel_class)){ ?>
                                    <div class="row">
                                        <div class="col-lg-9">
                                              <select id="select-deivces"  name="select-deivces[]" class="selectpicker placeholder" data-live-search="true" title="Please select device">
                                          
                                              </select>
                                          </div>
                                          <div class="col-lg-3">
                                            <span class="btn btn-success " onclick ="createDD_devicelist()">+</span> 
                                          </div>
                                      </div>
                                    
                                  <?php } ?>

                              <?php } ?>
                    <?php
                      
                    ?>
                    <!-- device list-->
                    <div class="form-group  col-lg-12" id="device_list">


                    </div>

                    <div class="form-group col-lg-11">
                      <?php echo form_button([
                        'type'=>'submit',
                        'class'=>'btn btn-success float-right mt-2',
                        'content'=>__('Submit','default')
                      ])?>
                    
                    </div>
                    <!-- end of device list -->
                
                                    

                  <?php echo form_close()?>
            </div>
        </div>
                          <!-- DIV: Table -->
                              <div class="col-lg-6">
                                     <div class="alert alert-info">
                                        <h5 class="text-center">Classroom Log</h5>
                                        <table class="table" style="font-size: 9px;">
                                          <thead>
                                              <tr>
                                                  <th>Device</th>
                                                  <th>Status</th>
                                                  <th>Created</th>
                                                  <th>Updated By</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                                 <?php if(!empty($class_room_logs)) {?>
                                                 <?php
                                               
                                                  
                                                  ?>
                                                      <?php foreach($class_room_logs as $clk => $clv) {?>
                                                      <?php
                                                        $status_text = "";
                                                        $status_arr = array("1" => "กำลังใช้งาน"
                                                         ,"2" =>  "ชำรุด" 
                                                         ,"3" => "พร้อมใช้งาน" 
                                                        );
                                                        if(isset($clv['device_status'])){
                                                          $status_text = $status_arr[$clv['device_status']];
                                                        }
                                                        if($clv['is_device_move_from_classid'] > 0){
                                                          $status_text = "ย้ายมาจาก " . $clv['move_from_class'] . '(' . $clv['is_device_move_to_classdate'] . ')';
                                                       
                                                        }
                                                        if($clv['is_device_move_to_classid'] ){
                                                          $status_text = "ย้ายไปห้อง " . $clv['move_to_class'] . '(' . $clv['is_device_move_to_classdate'] . ')';
                                                        }
       
                                           
                                                          // if($clv['is_device_move_from_classid'] > 0){
                                                          //   $status_text = "ย้ายมาจาก " . $clv['move_from_class'] . '(' . $clv['is_device_move_to_classdate'] . ')';
                                                         
                                                          // }
                                                          
                                                          // else{
                                                          //   $status_text = "ย้ายไปห้อง " . $clv['move_to_class'] . '(' . $clv['is_device_move_to_classdate'] . ')';
                                                          // }
                                                          
                                                        
                                                      ?>
                                                        <tr>
                                                            <td><?php echo $clv['ship_code'];?></td>
                                                            <td><?php echo $status_text;?></td>
                                                            <td><?php echo $clv['created'];?></td>
                                                            <td><?php echo $clv['admin_name'];?></td>
                                                        </tr>
                                                      <?php }?>
                                                  <?php } ?>
                                          </tbody>
                                        </table>
                                      </div>
                            </div>
                          <!-- END (TABLE) -->
    </div>
 
  <script type="text/javascript">
       
        function createDD_devicelist(){
          
          var randNum = Math.floor(Math.random() * 1000000000) + 1; 
          var html = "";
          html += '<div class="row mt-2" id="'+randNum+'">';
          html += '   <div class="col-lg-9">';
          html += '      <select id="select-deivces-'+randNum+'"  name="select-deivces[]" class="selectpicker placeholder" data-live-search="true" title="Please select device">';
          html += '      </select>';
          html += '   </div>';
          html += '   <div class="col-lg-3">'
          html += '       <span class="btn btn-danger " onclick ="deleteDD_deviceList('+randNum+')">X</span>'; 
          html += '   </div>';
          html += '</div>';
          // append html
          $('#device_list').append(html);

          //call dropdown event
          setDevicesAjax("select-deivces-" + randNum +"");
        }


        function deleteDD_deviceList(randNumID){
            $('#' + randNumID).remove();
        }
    </script>


    