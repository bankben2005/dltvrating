      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Devices')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Devices')?></li>
        </ul>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="tile">
            <div class="tile-body">
              <div class="card">
                        <h5 class="card-header">
                          <a data-toggle="collapse" href="#search-content" aria-expanded="true" aria-controls="search-content" id="heading-search" class="d-block">
                              <i class="fa fa-chevron-down pull-right"></i>
                              <i class="fa fa-search"></i> <?php echo __('SEARCH')?>
                          </a>
                            
                        </h5>
         <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->admin_data['id'];?>">
                        <div id="search-content" class="collapse show" aria-labelledby="heading-detail">
                          <div class="card-body">
                            <?php echo form_open('',['name'=>'search-form'])?>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                      <?php echo form_input([
                                        'name'=>'chip_code',
                                        'class'=>'form-control',
                                        'value'=>@$search_criteria['chip_code'],
                                        'placeholder'=>'Chip Code..'
                                      ])?>
                                    </div>
                                </div>

                                  
                                <div class="col-lg-3">
                                      <?php echo form_dropdown('dropdown_status',[
                                        '0'=>__('-- Select Status--'),
                                        '1'=>'กำลังใช้งาน',
                                        '2'=>'ชำรุด'
                                      ],@$search_criteria['dropdown_status'],'class="form-control"')?>

                                </div>


                            <!--      <div class="col-lg-3">
                                   <div class="form-group">
                                      <?php echo form_dropdown('country_name',@$select_country_name,@$search_criteria['country_name'],'class="form-control"')?>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                      <?php echo form_dropdown('region_name',@$select_region_name,@$search_criteria['region_name'],'class="form-control"')?>

                                </div>
                                
                                <div class="col-lg-3">
                                      <?php echo form_dropdown('province_region',[
                                        '0'=>__('-- Select Province Region --'),
                                        'Bangkok'=>'Bangkok',
                                        'Northern'=>'Northern',
                                        'NorthEastern'=>'NorthEastern',
                                        'Central'=>'Central',
                                        'Eastern'=>'Eastern',
                                        'Southern'=>'Southern',
                                        'Western'=>'Western',
                                        'Undefine'=>'*Undefine'
                                      ],@$search_criteria['province_region'],'class="form-control"')?>

                                </div>
                            </div>

                            <div class="row">
                              <div class="col-lg-3">

                                <div class="form-group">
                                <?php echo form_input([
                                  'name'=>'search_latitude',
                                  'class'=>'form-control',
                                  'value'=>@$search_criteria['search_latitude'],
                                  'placeholder'=>"Latitude..."
                                ])?>
                                </div>


                              </div>

                              <div class="col-lg-3">
                                <div class="form-group">
                                  <?php echo form_input([
                                    'name'=>'search_longitude',
                                    'class'=>'form-control',
                                    'value'=>@$search_criteria['search_longitude'],
                                    'placeholder'=>"Longitude..."
                                  ])?>
                                </div>
                              </div>

                              <div class="col-lg-3">
                                <div class="form-group">
                                  <label>
                                    <?php echo form_checkbox([
                                      'name'=>'update_from_api',
                                      'value'=>1,
                                      'checked'=>(@$search_criteria['update_from_api'])?TRUE:FALSE
                                    ])?>
                                     <span>Confirm latitude longtitude</span>
                                  </label>
                                </div>
                              </div> -->
                            </div>

                            <div class="row">
                              <div class="col-lg-12">
                                <?php echo form_button([
                                  'type'=>'button',
                                  'class'=>'btn btn-success float-right',
                                  'content'=>__('Search'),
                                  'onclick'=>'searchDevice()'
                                ])?>
                              </div>
                            </div>
                            <?php echo form_close()?>
                          </div>
                        </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <div class="clearfix mb-3"></div>
              <?php if($result_count){?>
              <div class="alert alert-success">
                <strong><?php echo __('Totals')?> : </strong> <?php echo number_format($result_count)?> <?php echo __('devices')?>
              </div>
              <?php }?>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th width="15%"><?php echo __('Chip Code')?></th>
                    <th width="15%"><?php echo __('IP Address')?></th>
                    <!-- <th width="15%"><?php echo __('Country Name')?></th> -->
                    <!-- <th><?php echo __('Region Name')?></th> -->
                    <!--  <th width="15%"><?php echo __('Address')?></th>
                    <th width="5%"><?php echo __('Distance from center (KM)')?></th> -->
                    <th width="10%"><?php echo __('Created')?></th>
                    <!-- <th><?php echo __('Province Region')?></th> -->
                    <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                    <?php if(isset($administrator['can_action_button'])) {?>
                    <?php if($administrator['can_action_button'] > 0 || $administrator['type'] =='admin'){ ?>
                        <th width="15%"></th>
                    <?php } ?>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($devices as $key => $row){

                    //renew parent id 
                    if(isset($row->devices->parent['id'])){
                      $get_where_id = $this->db->select('id')->from('device_addresses')
                      ->where('devices_id',$row->devices->parent['id'])->get();
                      $new_parent_id = $get_where_id->row()->id;
                      // change
                      $row->devices->parent['id'] = $new_parent_id;
                    }
                    //eof renew parent id 

                    $device_data = $row->devices
                    ->select('*,device_addresses_device_addresses.devices_id as dev_id')->get();
                    
                   //echo $this->db->last_query();exit();
                    // $province_data = new M_provinces();
                    // $province_data->where('ipstack_province_name',$row->region_name)
                    // ->get();

                    // print_r($row->latitude);
                    // print_r($row->longitude);
                  ?>
                    <tr>
                      <td><?php echo $device_data->ship_code?></td>
                      <td>
                        <a href="http://api.ipstack.com/<?php echo $row->ip_address?>?access_key=<?php echo @$ipstack_key?>" target="_blank">
                        <?php echo $device_data->ip_address?>
                        </a>                          
                      </td>
                   
                      <td><?php echo datetime_show($row->created)?></td>
                 
                      <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                      <?php if(isset($administrator['can_action_button'])) {?>
                        <?php if($administrator['can_action_button'] > 0 || $administrator['type'] =='admin'){ ?>
                          <td>
                            <div class="form-group">
                              <?php
                              $status = $device_data->status > 0 ?  $device_data->status : 0;
                              ?>
                                <?php echo form_dropdown('active',array(
                                  '0'=>__('-- Select Status --','backend/default'),
                                  '1'=>__('กำลังใช้งาน','backend/default'),
                                  '2'=>__('ชำรุด','backend/default'),
                                ),$status,'class="form-control" id="sel_idx_'.$device_data->id.'" onchange="onchangeDeviceStatus('.$status.','.$device_data->id.' , this.selectedIndex)" ')?>
                              </div>
                          </td>
                        <?php } ?>
                      <?php } ?>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
             

              <div class="row">
                <div class="col-lg-12">
                  <?php echo $pages?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

     <script>
        function onchangeDeviceStatus(old_index , device_id ,index){
       
            var status = index;
           
            //alert(old_index);
            if(confirm("คุณต้องการแก้ไขสถานะของอุปกรณ์นี้ใช่หรือไม่ ?")){
                //set status and update log
                set_device_status_and_update_logs(status,device_id);
            }else{
                // change selected index 
                $('#sel_idx_' + device_id + ' option:eq(' + old_index +')').prop('selected', true);
                
            }
        }

       

        function set_device_status_and_update_logs(status,device_id){
          var base_url = $('input[name="base_url"]').val();
          var user_id = $('#user_id').val();
         //alert(user_id);
          if(status > 0){ 
            $.ajax({
                          url: base_url + 'api/classroom/setdevicestatus',
                          type: "post",
                          data: {
                              'device_status':status,
                              'device_id':device_id,
                              'user_id':user_id
                          },
                          async:true,
                          dataType:'json',
                          success: function (response) {
                              console.log('==response==');
                              console.log(response);
                              if(response.status == false){
                                          alert(response.result_message);
                              }else{
                                          alert(response.result_message);
                              }
                              // you will get response from your php page (what you echo or print)                 

                          },
                          error: function (request, status, error) {
                              console.log(request.responseText);
                          }
                      });
          }
      }


      </script>