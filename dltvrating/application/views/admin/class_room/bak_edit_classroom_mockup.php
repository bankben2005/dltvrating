<div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo __('Edit Classroom');?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Classroom List')?></a></li>
              <li class="breadcrumb-item active"><?php echo __('Edit Classroom');?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">
   
        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Edit Classroom Form');?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
  <!-- div form group -->
            <div class="col-lg-6">
                
                  <?php echo form_open('',array('name'=>'create-classroom-form'))?>
                      
    <?php 

    ?>
                      <div class="row">
                      
                        <div class="col-lg-12">

                          <div class="form-group row">
                              <div class="col-lg-10">
                                <label><strong><?php echo __('Classroom')?> : </strong></label>
                                <?php echo form_input([
                                  'id'=>'class_name',
                                  'class'=>'form-control',
                                  'value'=>@$class_room->group_name,
                                  ($this->admin_data['can_action_button'] == 0) ? "readonly": "attr_"  => 1,
                                  'title' => ($this->admin_data['can_action_button'] == 0) ? " you don't have permission to edit this input.":"Classname"
                                ])?>
                              </div>
                              <div class="col-lg-2"> 
                                      &nbsp;
                                </div>
                          </div>

                         <!-- dropdown : school -->
                        
                       <?php
                        if($this->admin_data['can_action_button'] == 1){         
                       ?>
                          <label><strong><?php echo __('School')?> : </strong></label>
                          <div class="form-group " >
                              <select id="select_school" name="select_school" class="selectpicker" data-live-search="true" title="Please select school">
                                 <?php foreach($schools as $schk =>$schv){ ?>
                                      <?php
                                        //print_r($schools);exit();
                                        ?>
                                       <option value="<?php echo $schk;?>" <?php echo ($schk == $class_room->SchoolID) ? "selected" : "";?>><?php echo $schv;?></option>
                             
                                  <?php } ?>
                              </select>
                          </div>
                        <?php }else{?>
                        <?php
                        ?>
                            <div class="form-group row">
                                <div class="col-lg-10">
                                    <label><strong><?php echo __('School')?> : </strong></label>
                                    <?php echo form_input([
                                    'id'=>'class_name',
                                    'class'=>'form-control',
                                    'value'=>$schools[$class_room->SchoolID],
                                    ($this->admin_data['can_action_button'] == 0) ? "readonly": "attr_"  => 1,
                                    'title' => ($this->admin_data['can_action_button'] == 0) ? " you don't have permission to edit this input.":"Classname"
                                  ])?>
                                </div>
                                <div class="col-lg-2"> 
                                        &nbsp;
                                    </div>
                            </div>

                        <?php }?>
                       
                            <?php
                            //echo '<PRE>';
                            //print_r($devices_rel_class);exit();
                            if(!empty($devices_rel_class)){
                                $firstDeviceKey = key($devices_rel_class);
                            }
                           
                            ?>
                          <!-- dropdown : devices -->     
                          <label><strong><?php echo __('Device')?> (Search By Chip ID): </strong></label>
                          <div class="form-group " id="device_group">
                                <?php 
                             
                                if($this->admin_data['type'] == 'superadmin'){
                                ?>
                                  <div class="row">
                                    <div class="col-lg-9">
                                          <select id="select-deivces"  name="select-deivces" class="selectpicker placeholder" data-live-search="true" title="Please select device">
                                          <?php
                                          if(!empty($devices_rel_class)){
                                          ?>
                                          <option value="<?php echo $devices_rel_class[$firstDeviceKey]->device_id;?>" selected><?php echo $devices_rel_class[$firstDeviceKey]->ship_code;?></option> 
                                        <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-lg-3">
                                        <span class="btn btn-success " onclick ="alreadyUsed()">+</span> 
                                      </div>
                                  </div>
                                <?php } ?>

                                <?php 
                                //echo ($this->admin_data['type']);
                                if($this->admin_data['type'] != 'superadmin' ){
                                if(!empty($devices_rel_class)){
                                 
                                ?>
                                    <div class="row">
                                          <div class="col-lg-9">
                                          
                                            <?php echo form_input([
                                              'id'=>'device_group_' .$devices_rel_class[$firstDeviceKey]->device_id,
                                              'class'=>'form-control',
                                              'value'=>@$devices_rel_class[$firstDeviceKey]->ship_code,
                                              ($this->admin_data['type'] != 'superadmin') ? "readonly": "attr_"  => 1,
                                              'title' =>  ($this->admin_data['type'] != 'superadmin') ? " you don't have permission to edit this input.":"device_group"
                                            ])?>
                                        </div>
                                        <div class="col-lg-3">
                                        
                                          <span class="btn btn-success " onclick ="alreadyUsed()">+</span> 
                                      </div>
                                        </div>
                                    </div>


                                              <div class="row">
                                              <div class="col-lg-9">
                                          <select id="select-deivces"  name="select-deivces" class="selectpicker placeholder" data-live-search="true" title="Please select device">
                                          <?php
                                          if(!empty($devices_rel_class)){
                                          ?>
                                          <option value="<?php echo $devices_rel_class[$firstDeviceKey]->device_id;?>" selected><?php echo $devices_rel_class[$firstDeviceKey]->ship_code;?></option> 
                                        <?php }?>
                                          </select>
                                      </div>
                                                  <div class="col-lg-3">
                                                  
                                                    <span class="btn btn-danger " onclick ="alreadyUsed()">X</span> 
                                                </div>
                                                  </div>
                                                      
                                                  <div class="form-group">
                                                    <?php echo form_button([
                                                      'type'=>'submit',
                                                      'class'=>'btn btn-success float-right mt-2',
                                                      'content'=>__('Submit','default')
                                                    ])?>
                                                  
                                                  </div>
                                    </div>

                                    
                               <?php }?> 
                              <?php if(empty($devices_rel_class)){ ?>
                                <div class="row">
                                    <div class="col-lg-9">
                                          <select id="select-deivces"  name="select-deivces" class="selectpicker placeholder" data-live-search="true" title="Please select device">
                                          <?php
                                          if(!empty($devices_rel_class)){
                                          ?>
                                          <option value="<?php echo $devices_rel_class[$firstDeviceKey]->device_id;?>" selected><?php echo $devices_rel_class[$firstDeviceKey]->ship_code;?></option> 
                                        <?php }?>
                                          </select>
                                      </div>
                                      <div class="col-lg-3">
                                        <span class="btn btn-success " onclick ="alreadyUsed()">+</span> 
                                      </div>
                                  </div>
                                 
                              <?php } }?>
                           
                              <!-- <div class="form-group " id="device_list">


                            </div>
                                                                         -->
                          

                       
                  <?php echo form_close()?>
            </div>
        </div>
                          <!-- DIV: Table -->
                              <div class="col-lg-6">
                                     <div class="alert alert-info">
                                        <h5 class="text-center">Classroom Log</h5>
                                        <table class="table" style="font-size: 9px;">
                                          <thead>
                                            <tr>
                                              <th>Device</th>
                                              <th>Status</th>
                                              <th>Created</th>
                                              <th>Updated</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                                    <tr>
                                                        <td>00e649ea-00004a1c</td>
                                                        <td>ชำรุด</td>
                                                        <td>2021-01-19 10:17:00</td>
                                                        <td>2021-01-29 09:10:00</td>
                                                    </tr>
                                          </tbody>
                                          
                                        </table>
                                      </div>
                            </div>
                          <!-- END (TABLE) -->
    </div>
 
  <script>
        function alreadyUsed(){
          confirm("กล่องchip id 06eaa9da00001515 นี้ถูกเพิ่มอยู่ในห้องอื่นแล้ว ท่านต้องการย้ายกล่องมายังห้องนี้หรือไม่?");
        }


        
    </script>


    