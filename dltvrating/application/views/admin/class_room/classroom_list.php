<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Classroom')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Classroom')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
    </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
            <a    href="<?php echo base_url('admin/class_room/createClassroom')?>" class="btn btn-success float-right <?php echo ($this->admin_data['can_action_button'] == 0 && $this->admin_data['type'] == 'staff') ?   'd-none' : ""?>"><?php echo __('Create Classroom')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="classroomTable">
                <thead>
                  <tr>
                    
                    <th><?php echo __('Class Room')?></th>
                    <th><?php echo __('SchoolName')?></th>
                    <th><?php echo __('Devices')?></th>
                    <th><?php echo __('Created');?></th>
                     <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                     <?php if($this->admin_data['can_action_button'] > 0 || $this->admin_data['type'] == 'admin'){ ?>
                         <th></th>
                     <?php }?>
                  </tr>
                </thead>
                <tbody>
                <?php 
                function search($array, $key, $value)
                {
                    $results = array();
                
                    if (is_array($array)) {
                        if (isset($array[$key]) && $array[$key] == $value) {
                            $results[] = $array;
                        }
                
                        foreach ($array as $subarray) {
                            $results = array_merge($results, search($subarray, $key, $value));
                        }
                    }
                
                    return $results;
                }
                  //echo '<PRE>';
                  //print_r($class_room);exit();
                ?>
                
                  <?php foreach($class_room as $key => $row){?>
                    <tr>
                      <td><?php echo $row->group_name?></td>
                      <td><?php echo $row->SchoolName?></td>
                      <td><?php echo $row->ship_code?></td>
                      <td><?php echo $row->created?></td>
                       <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                       <?php if($this->admin_data['can_action_button'] > 0 || $this->admin_data['type'] == 'admin'){ ?>
                      <td>
                        <a href="<?php echo base_url('admin/'.$this->controller.'/editClassroom/'.$row->id)?>" class="btn btn-secondary btn-sm" target="_blank"><i class="fa fa-pencil"></i></a>
                      </td>
                      <?php } ?>
                    </tr>

                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

