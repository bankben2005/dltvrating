<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Schools')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Schools')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
    </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
           
              <a href="<?php echo base_url('admin/'.$this->controller.'/createSchool')?>" class="btn btn-success float-right <?php echo ($this->admin_data['can_action_button'] == 0) ?   'd-none' : ""?> "><?php echo __('Create School')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('School Name')?></th>
                    <th><?php echo __('Province')?></th>
                    <th><?php echo __('School Affiliate ')?></th>
                    <th><?php echo __('Latitude/Longitude')?></th>
                    <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                    <?php if($this->admin_data['can_action_button'] > 0){ ?>
                      <th class="<?php echo ($this->admin_data['can_action_button'] == 0) ?   'd-none' : ""?> "></th>
                    <?php } ?>
                    <!-- EOF -->
                  </tr>
                </thead>
                <tbody>
              
                  <?php foreach($schools as $key => $row){?>
                    <tr>
                      <td><?php echo $row->SchoolName?></td>
                      <td><?php echo $row->province_name?></td>
                      <td><?php echo $row->SchoolType?></td>
                      <?php
                      $lat_long = "";
                      if(!empty($row->Latitude) && !empty($row->Longitude)){
                        $lat_long = $row->Latitude.'/'.$row->Longitude;
                      }
                      ?>
                      <td><?php echo $lat_long;?></td>

                      <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                      <?php if($this->admin_data['can_action_button'] > 0){ ?>
                        <td>
                          <a href="<?php echo base_url('admin/'.$this->controller.'/editSchool/'.$row->id)?>" class="btn btn-secondary btn-sm" target="_blank"><i class="fa fa-pencil"></i></a>
                        </td>
                      <?php } ?>
                      <!-- EOF -->
                    
                    </tr>

                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>