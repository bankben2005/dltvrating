<div class="app-title">
            <div>
              <h1><i class="fa fa-th-list"></i> <?php echo ($schools->id)?__('Edit School'):__('Create School')?></h1>
              <!-- <p>Table to display analytical data effectively</p> -->
            </div>
            <ul class="app-breadcrumb breadcrumb side">
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('School List')?></a></li>
              <li class="breadcrumb-item active"><?php echo ($schools->id)?__('Edit School'):__('Create School')?></li>
            </ul>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <?php echo message_warning($this)?>
              </div>
    </div>
    <div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12 "  >
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($schools->id)?__('Edit School Form').'':__('Create School Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            
        <!-- $schools->SchoolID = $max_SchoolID;
            $schools->SchoolName = $this->input->post('SchoolName');
            $schools->DistrictID = $this->input->post('DistrictID');
            $schools->AmphurID = $this->input->post('AmphurID');
            $schools->ProvinceID = $this->input->post('ProvinceID');
            $schools->SchoolTypeID = $this->input->post('SchoolTypeID');
            $schools->zipcode = $this->input->post('zipcode');
            $schools->Website = $this->input->post('Website');
            $schools->Telephone = $this->input->post('Telephone');
            $schools->Fax = $this->input->post('Fax');
            $schools->Latitude = $this->input->post('Latitude');
            $schools->Longitude = $this->input->post('Longitude');
            $schools->distance_from_center = $this->input->post('distance_from_center');
            $schools->distance_to_center_time = $this->input->post('distance_to_center_time');
            $schools->Email = $this->input->post('Email'); -->

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-school-form'))?>
                      <div class="row">
                        <div class="col-lg-4">

                          <div class="form-group">
                            <label><strong><?php echo __('School Name')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'SchoolName',
                              'class'=>'form-control',
                              'value'=>@$schools->SchoolName
                            ])?>
                          </div>
                          
                          <div class="form-group">
                            <label><strong><?php echo __('School affiliates')?> : </strong></label>
                            <?php echo form_dropdown('SchoolTypeID',@$SchoolType,@$schools->SchoolTypeID,'class="form-control" id="SchoolTypeID"')?>
                          </div>
                          


                          <div class="form-group">
                            <label><strong><?php echo __('Provinces')?> : </strong></label>
                            <?php echo form_dropdown('ProvinceID',@$provinces,@$schools->ProvinceID,'class="form-control" id="province"')?>
                          </div>
                          
                          <!-- <div class="form-group">
                            <label><strong><?php echo __('Amphur')?> : </strong></label>
                            <?php echo form_dropdown('AmphurID',@$amphurs,'','class="form-control" id="amphure" ')?>
                          </div>
                          
                          <div class="form-group">
                            <label><strong><?php echo __('District')?> : </strong></label>
                            <?php echo form_dropdown('DistrictID',@$districts,'','class="form-control" id="district" ')?>
                          </div> -->

                          <div class="form-group ">
                          <label for="">อำเภอ/เขต</label>
                          <select name="AmphurID" id="amphure" class="form-control">
                            <option value="" disabled selected>กรุณาเลือกจังหวัดก่อน</option>
                            <?php if (isset($schools->amphurs_id)){
                              
                              foreach($amphurs_edit as $ak => $av){
                              ?>
                                <option value="<?php echo $ak?>" <?php echo $ak == $schools->amphurs_id? "selected": "";?>><?php echo $av;?> </option>
                              <?php
                              }
                            } ?>
                         </select>
                        </div>
                        <div class="form-group ">
                          <label for="">ตำบล/แขวง</label>
                          <select name="DistrictID" id="district" class="form-control">
                            <option value="" disabled selected>กรุณาเลือกอำเภอก่อน</option>
                          <?php if (isset($schools->DistrictID)){
                           
                            foreach($district_edit as $kd => $kv){
                            ?>
                              <option value="<?php echo $kd?>" <?php echo $kd == $schools->DistrictID? "selected": "";?>><?php echo $kv;?> </option>
                            <?php
                            }
                          } ?>
                          </select>
                        </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Zipcode')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'zip_code',
                              'id'=>'zip_code',
                              'class'=>'form-control',
                              'value'=>@$schools->zipcode
                            ])?>
                          </div>
                    

                          <div class="form-group">
                            <label><strong><?php echo __('Latitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'Latitude',
                              'id'=>'Latitude',
                              'class'=>'form-control',
                              'value'=>@$schools->Latitude
                            ])?>
                          </div>

                     

                        </div>

                        <div class="col-lg-4">
                     
                          
                          <div class="form-group">
                            <label><strong><?php echo __('Longitude')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'Longitude',
                              'id'=>'Longitude',
                              'class'=>'form-control',
                              'value'=>@$schools->Longitude
                            ])?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Distance from center (KM)')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'distance_from_center',
                              'id'=>'distance_from_center',
                              'class'=>'form-control',
                              'value'=>@$schools->distance_from_center
                            ])?>
                          </div>
                          

                          <div class="form-group">
                            <label><strong><?php echo __('How many hour does it take to the city')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'distance_to_center_time',
                              'id'=>'distance_to_center_time',
                              'class'=>'form-control',
                              'value'=>@$schools->distance_to_center_time
                            ])?>
                          </div>
                          <div class="form-group">
                            <label><strong><?php echo __('Email')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'Email',
                              'id'=>'Email',
                              'class'=>'form-control',
                              'value'=>@$schools->Email
                            ])?>
                          </div>
                                    

                          <div class="form-group">
                            <label><strong><?php echo __('Website')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'Website',
                              'id'=>'Website',
                              'class'=>'form-control',
                              'value'=>@$schools->Website
                            ])?>
                          </div>
                          <div class="form-group">
                            <label><strong><?php echo __('Telephone')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'Telephone',
                              'id'=>'Telephone',
                              'class'=>'form-control',
                              'value'=>@$schools->Telephone
                            ])?>
                          </div>
                          <div class="form-group">
                            <label><strong><?php echo __('Fax')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'Fax',
                              'id'=>'Fax',
                              'class'=>'form-control',
                              'value'=>@$schools->Fax
                            ])?>
                          </div>
                          
                          

                          <div class="form-group">
                            <?php echo form_button([
                              'type'=>'submit',
                              'class'=>'btn btn-success float-right',
                              'content'=>__('Submit','default')
                            ])?>
                          </div>


                        </div>

                        <div class="col-lg-4">
                                
                        </div>
                      </div>

                  <?php echo form_close()?>

            </div>
        </div>
    </div>

    <script type="text/javascript">
      var province_ids = <?php echo isset($schools->ProvinceID) ? $schools->ProvinceID : 0?>;
      var amphure_ids = <?php echo isset($schools->amphurs_id) ?$schools->amphurs_id : 0?>;
      var district_ids= <?php echo isset($schools->DistrictID) ? $schools->DistrictID: 0?>;

    </script>