<aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?php echo getAdminPictureProfile()?>" alt="User Image" width="48" height="48">
        <div>
          <p class="app-sidebar__user-name"><?php echo $this->admin_data['firstname'].' '.mb_substr($this->admin_data['lastname'],0,1,'utf-8').'.';?></p>
          <p class="app-sidebar__user-designation"></p>
        </div>
      </div>
      <ul class="app-menu">

  <?php 
      //echo '<PRE>';
      //print_r($this->admin_data);exit();
  
  ?>
        <?php if(!$this->admin_data['for_review_devices']){?>
        <li><a class="app-menu__item" href="<?php echo base_url('admin/dashboard')?>"><i class="app-menu__icon <?php echo ($this->controller == 'dashboard')?'active':''?> fa fa-dashboard"></i><span class="app-menu__label"><?php echo __('Dashboard')?></span></a></li>
        <?php }?>

        <!-- <li><a class="app-menu__item" href="<?php echo base_url('admin/channels')?>"><i class="app-menu__icon <?php echo ($this->controller == 'channels')?'active':''?> fa fa-desktop"></i><span class="app-menu__label"><?php echo __('Channels')?></span></a></li>
 -->
        <?php if(!$this->admin_data['for_review_devices']){?>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-desktop"></i><span class="app-menu__label"><?php echo __('Channels')?></span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="<?php echo base_url('admin/channels')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Channel Lists')?></a></li>
            <li><a class="treeview-item" href="<?php echo base_url('admin/channels/channel_categories')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Channel Categories')?></a></li>
            <?php if($this->admin_data['type'] == 'superadmin') {?>
              <li><a class="treeview-item" href="<?php echo base_url('admin/channels/channel_ranking')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Channel Ranking')?></a></li>
            <?php } ?>
            <!-- <li><a class="treeview-item" href="<?php echo base_url('admin/channels/channel_program_chart')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Channel Program Chart')?></a></li> -->
            
          </ul>
        </li>
        <?php }?>

        <?php if(!$this->admin_data['for_review_devices']){?>
        <li><a class="app-menu__item" href="<?php echo base_url('admin/corporate')?>"><i class="app-menu__icon <?php echo ($this->controller == 'dashboard')?'active':''?> fa fa-building"></i><span class="app-menu__label"><?php echo __('Corporates')?></span></a></li>
        <?php }?>
        
        <li><a class="app-menu__item" href="<?php echo base_url('admin/schools')?>"><i class="app-menu__icon <?php echo ($this->controller == 'schools')?'active':''?> fa fa-cube"></i><span class="app-menu__label"><?php echo __('Schools')?></span></a></li>

        <li><a class="app-menu__item" href="<?php echo base_url('admin/school_type')?>"><i class="app-menu__icon <?php echo ($this->controller == 'school_aff')?'active':''?> fa fa-cube"></i><span class="app-menu__label"><?php echo __('School Affiliates')?></span></a></li>
        

        <li><a class="app-menu__item" href="<?php echo base_url('admin/class_room')?>"><i class="app-menu__icon <?php echo ($this->controller == 'class_room')?'active':''?> fa fa-cube"></i><span class="app-menu__label"><?php echo __('Classroom')?></span></a></li>
        
        <li><a class="app-menu__item" href="<?php echo base_url('admin/device')?>"><i class="app-menu__icon <?php echo ($this->controller == 'device')?'active':''?> fa fa-cube"></i><span class="app-menu__label"><?php echo __('Devices')?></span></a></li>

        <?php if(!$this->admin_data['for_review_devices']){?>
        <li><a class="app-menu__item" href="<?php echo base_url('admin/province')?>"><i class="app-menu__icon <?php echo ($this->controller == 'province')?'active':''?> fa fa-cube"></i><span class="app-menu__label"><?php echo __('Province')?></span></a></li>

        <?php if(!$this->admin_data['for_review_devices']){?>
        <li><a class="app-menu__item" href="<?php echo base_url('admin/amphur')?>"><i class="app-menu__icon <?php echo ($this->controller == 'amphur')?'active':''?> fa fa-cube"></i><span class="app-menu__label"><?php echo __('Amphur')?></span></a></li>
        <?php }?>

<!-- 
        <?php if(!$this->admin_data['for_review_devices']){?>
        <li><a class="app-menu__item" href="<?php echo base_url('admin/packages')?>"><i class="app-menu__icon <?php echo ($this->controller == 'packages')?'active':''?> fa fa-tags"></i><span class="app-menu__label"><?php echo __('Packages')?></span></a></li>
        
        
        <?php }?> -->

        <?php if(!$this->admin_data['for_review_devices']){?>
        <li><a class="app-menu__item" href="<?php echo base_url('admin/district')?>"><i class="app-menu__icon <?php echo ($this->controller == 'district')?'active':''?> fa fa-cube"></i><span class="app-menu__label"><?php echo __('District')?></span></a></li>
        
        <?php }?>

        

        <!-- <li><a class="app-menu__item" href="<?php echo base_url('admin/user')?>"><i class="app-menu__icon <?php echo ($this->controller == 'dashboard')?'active':''?> fa fa-user"></i><span class="app-menu__label"><?php echo __('Users')?></span></a></li> -->
        <?php if($this->admin_data['type'] == 'superadmin') {?>
          <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label"><?php echo __('Users')?></span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
              <li><a class="treeview-item" href="<?php echo base_url('admin/user')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Users')?></a></li>
              <li><a class="treeview-item" href="<?php echo base_url('admin/user/user_menu')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('User Menu')?></a></li>
            </ul>
          </li>
        <?php } ?>
        <?php if($this->admin_data['type'] == 'superadmin') {?>
          <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label"><?php echo __('Administrator')?></span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
              <li><a class="treeview-item" href="<?php echo base_url('admin/administrator')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Administrator List')?></a></li>
            </ul>
          </li>
        <?php } ?>

        <!-- <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-shopping-cart"></i><span class="app-menu__label"><?php echo __('Shopping')?></span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="<?php echo base_url('admin/shopping/advertisement_campaign')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Advertisement Campaign')?></a></li>
          </ul>
        </li> -->

        <?php }?>
        <!-- <li class="treeview is-expanded"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-file-text"></i><span class="app-menu__label">Pages</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item active" href="blank-page.html"><i class="icon fa fa-circle-o"></i> Blank Page</a></li>
            <li><a class="treeview-item" href="page-login.html"><i class="icon fa fa-circle-o"></i> Login Page</a></li>
            <li><a class="treeview-item" href="page-lockscreen.html"><i class="icon fa fa-circle-o"></i> Lockscreen Page</a></li>
            <li><a class="treeview-item" href="page-user.html"><i class="icon fa fa-circle-o"></i> User Page</a></li>
            <li><a class="treeview-item" href="page-invoice.html"><i class="icon fa fa-circle-o"></i> Invoice Page</a></li>
            <li><a class="treeview-item" href="page-calendar.html"><i class="icon fa fa-circle-o"></i> Calendar Page</a></li>
            <li><a class="treeview-item" href="page-mailbox.html"><i class="icon fa fa-circle-o"></i> Mailbox</a></li>
            <li><a class="treeview-item" href="page-error.html"><i class="icon fa fa-circle-o"></i> Error Page</a></li>
          </ul>
        </li> -->
      </ul>
    </aside>