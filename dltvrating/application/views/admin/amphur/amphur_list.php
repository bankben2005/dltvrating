      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Amphurs')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Amphurs')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
    </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/amphur/createAmphur')?>" class="btn btn-success float-right <?php echo ($this->admin_data['can_action_button'] > 0) ? "" : "d-none";?>"><?php echo __('Amphur Province')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Amphur Name')?></th>
                    <th><?php echo __('Amphur Nmae (EN)')?></th>
                    <th><?php echo __('Province')?></th>
                    <th><?php echo __('Latitude/Longitude')?></th>
                    <?php if($this->admin_data['can_action_button'] > 0){?>
                      <th></th>
                    <?php }?>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($amphur->get() as $key => $row){?>
                    <tr>
                      <td><?php echo $row->amphur_name?></td>
                      <td><?php echo $row->amphur_name_eng?></td>
                      <td><?php echo $row->provinces->get()->province_name?></td>
                      <td><?php echo $row->latitude.' / '.$row->longitude?></td>
                      <?php if($this->admin_data['can_action_button'] > 0){?>
                        <td>
                          <a href="<?php echo base_url('admin/'.$this->controller.'/editAmphur/'.$row->id)?>" class="btn btn-secondary btn-sm" target="_blank"><i class="fa fa-pencil"></i></a>
                        </td>
                      <?php } ?>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
