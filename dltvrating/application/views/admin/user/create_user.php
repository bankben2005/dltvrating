<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo ($user->id)?__('Edit User'):__('Create User')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('User List')?></a></li>
          <li class="breadcrumb-item active"><?php echo ($user->id)?__('Edit User'):__('Create User')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($user->id)?__('Edit User').' ('.$user->firstname.' '.$user->lastname.')':__('Create User')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'email_exist',
                    'value'=>__('Email has been used.Please enter other email address')
                  ))?>
                  <?php echo form_open('',array('name'=>'create-user-form'))?>
                  <div class="row">
                      <div class="col-lg-5">
                            <div class="form-group">
                                <label><strong><?php echo __('Accesstype')?> : </strong></label>
                                <?php echo form_dropdown('user_accesstype_id',@$user_accesstype,@$user->user_accesstype_id,'class="form-control"')?>
                              </div>
                          <div class="row">
                            <div class="col-lg-6">


                              <div class="form-group">
                                <label><strong><?php echo __('Firstname')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'firstname',
                                  'class'=>'form-control',
                                  'value'=>@$user->firstname
                                ))?>
                              </div>
                            </div>
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label><strong><?php echo __('Lastname')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'lastname',
                                  'class'=>'form-control',
                                  'value'=>@$user->lastname
                                ))?>
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Email / Username')?> : </strong></label>
                            <?php echo form_input(array(
                              'name'=>'email',
                              'class'=>'form-control',
                              'value'=>@$user->email
                            ))?>
                          </div>

                          <?php if(!$user->id){?>
                          <div class="form-group">
                            <label><strong><?php echo __('Password')?> : </strong></label>
                            <?php echo form_input(array(
                              'type'=>'password',
                              'name'=>'password',
                              'class'=>'form-control'
                            ))?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Confirm Password')?> : </strong></label>
                            <?php echo form_input(array(
                              'type'=>'password',
                              'name'=>'confirm_password',
                              'class'=>'form-control'
                            ))?>
                          </div>  

                          <?php }else{?>
                          <div class="form-group">
                            <?php echo form_button(array(
                              'type'=>'button',
                              'class'=>'btn btn-primary btn-block',
                              'content'=>'<i class="fa fa-key"></i> '.__('Reset Password'),
                              'data-toggle'=>'modal',
                              'data-target'=>'#resetPasswordModal'
                            ))?>
                          </div>
                          <?php }?>

                          <div class="form-group">
                            <label><strong><?php echo __('Telephone')?> : </strong></label>
                            <?php echo form_input(array(
                              'type'=>'telephone',
                              'name'=>'telephone',
                              'class'=>'form-control',
                              'value'=>@$user->telephone
                            ))?>
                          </div>

                          <!-- <div class="form-group">
                            <label><strong><?php echo __('Rating Package')?> : </strong></label>
                            <?php echo form_dropdown('rating_packages_id',@$select_rating_packages,@$user->rating_packages_id,'class="form-control"')?>
                          </div>
                           -->

                           <div class="form-group" style="display:none;">
                            <label><strong><?php echo __('Rating Package')?> : </strong></label>
                            <?php echo form_dropdown('rating_packages_id',@$select_rating_packages,5,'class="form-control"')?>
                          </div>

                          <!-- <div class="form-group">
                            <label><strong><?php echo __('All realtime viewers')?> : </strong></label>
                            <?php echo form_dropdown('admin_status',[
                              '0'=>__('No','default'),
                              '1'=>__('Yes','default')
                            ],@$user->admin_status,'class="form-control" onchange="checkToggleShowTotalDevice(this)"')?>
                          </div>

                          <div class="form-group" style="display: none;" id="show_total_device">
                            <label><strong><?php echo __('Show total devices')?> : </strong></label>
                            <?php echo form_dropdown('show_total_device_status',[
                              '1'=>__('Show','default'),
                              '0'=>__('Not Show','default')
                            ],@$user->show_total_device_status,'class="form-control"')?>
                          </div> -->

                          <div class="form-group d-none">
                            <label><strong><?php echo __('Show hide channels ranking (Shopping Channels)')?> : </strong></label>
                            <?php echo form_dropdown('show_hide_channels_ranking',[
                              '1'=>__('Show','default'),
                              '0'=>__('Not Show','default')
                            ],@$user->show_hide_channels_ranking,'class="form-control"')?>
                          </div> 

                          <!-- <div class="form-group">
                            <label><strong><?php echo __('Show tv program menu')?> : </strong></label>
                            <?php echo form_dropdown('show_tv_program_status',[
                              '0'=>__('No Show','default'),
                              '1'=>__('Show','default')
                            ],@$user->show_tv_program_status,'class="form-control"')?>
                          </div>   

                          <div class="form-group">
                            <label><strong><?php echo __('Show population and devices')?> : </strong></label>
                            <?php echo form_dropdown('show_population_and_devices_status',[
                              '0'=>__('No Show','default'),
                              '1'=>__('Show','default')
                            ],@$user->show_population_and_devices_status,'class="form-control"')?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Show latitude longitude map')?> : </strong></label>
                            <?php echo form_dropdown('show_latlon_map_status',[
                              '0'=>__('No Show','default'),
                              '1'=>__('Show','default')
                            ],@$user->show_latlon_map_status,'class="form-control"')?>
                          </div> -->
                          

                          <div class="form-group">
                            <label><strong><?php echo __('Status','default')?> : </strong></label>
                            <?php echo form_dropdown('active',array(
                              '1'=>__('Active','default'),
                              '0'=>__('Unactive','default')
                            ),@$user->active,'class="form-control"')?>
                          </div>
                      </div>

                      <div class="col-lg-7">
                        <div class="form-group">
                          <label><strong><?php echo __('Owner Channel?')?>:</strong></label>
                          <br>
                          <label>
                            <?php echo form_radio(array(
                              'name'=>'owner_channel_status',
                              'value'=>'no',
                              'checked'=>(count($user_owner_channels) <= 0)?TRUE:FALSE
                            ))?>
                            <span><?php echo __('No')?></span>
                          </label>

                          <label>
                            <?php echo form_radio(array(
                              'name'=>'owner_channel_status',
                              'value'=>'yes',
                              'checked'=>(count($user_owner_channels) > 0)?TRUE:FALSE
                            ))?>
                            <span><?php echo __('Yes')?></span>
                          </label>
                        </div>

                        <div class="form-group" style="display: none;" id="panel_owner_status">
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                  <tr>
                                    <th><?php echo __('Channel Name')?></th>
                                    <th><?php echo __('Owner Status')?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($channels as $key => $row){?>
                                    <tr>
                                      <td><a href="<?php echo base_url('admin/channels/editChannel/'.$row->id)?>" target="_blank"><?php echo $row->channel_name?></a></td>
                                      <td>
                                        <label class="switch">
                                          <?php echo form_checkbox(array(
                                            'name'=>'user_owner_channels[]',
                                            'value'=>$row->id,
                                            'checked'=>(in_array($row->id, $user_owner_channels))?TRUE:FALSE
                                          ))?>
                                          <span class="slider"></span>
                                        </label>
                                      </td>
                                    </tr>
                                  <?php }?>
                                </tbody>
                            </table>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="form-group">
                          <?php echo form_button(array(
                            'type'=>'submit',
                            'class'=>'btn btn-success float-right',
                            'content'=>__('Submit','default')
                          ))?>
                        </div>
                      </div>
                  </div>
                  <?php echo form_close();?>
            </div>
        </div>
</div>

<!-- Modal -->
<div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Reset password form')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'user_id',
        'value'=>@$user->id
      ))?>
      <?php echo form_open('',array('name'=>'reset-password-form'))?>
      <div class="modal-body">
        
          <div class="form-group">
            <label><strong><?php echo __('New Password')?> : </strong></label>
            <?php echo form_input(array(
              'type'=>'password',
              'class'=>'form-control',
              'name'=>'new_password'
            ))?>
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Confirm new password')?> : </strong></label>
            <?php echo form_input(array(
              'type'=>'password',
              'class'=>'form-control',
              'name'=>'confirm_new_password'
            ))?>
          </div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Submit','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>