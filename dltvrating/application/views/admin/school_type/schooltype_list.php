<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Affiliate')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Affiliate')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
    </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a   href="<?php echo base_url('admin/school_type/createAffiliate')?>" class="btn btn-success float-right <?php echo ($this->admin_data['can_action_button'] == 0) ?   'd-none' : ""?>"><?php echo __('Create Affiliate')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    
                    <th><?php echo __('School Affiliate')?></th>
                    <th><?php echo __('Parent Affiliate')?></th>
               
                    <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                    <?php if($this->admin_data['can_action_button'] > 0){ ?>
                        <th></th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($school_type as $key => $row){?>
                    <tr>
                      <td><?php echo $row->SchoolType?></td>
                      <td><?php echo $row->school_group_name?></td>
                      <!-- <td>
                          <?php if($row->active){?>
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td> -->
                          <!-- CHECK IF ACCESS TYPE IS STAFF THEN CAN'T EDIT BUTTON -->
                          <?php if($this->admin_data['can_action_button'] > 0){ ?>
                              <td>
                                <a href="<?php echo base_url('admin/'.$this->controller.'/editAffiliate/'.$row->id)?>" class="btn btn-secondary btn-sm" target="_blank"><i class="fa fa-pencil"></i></a>
                              </td>
                          <?php } ?>
                    </tr>

                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>