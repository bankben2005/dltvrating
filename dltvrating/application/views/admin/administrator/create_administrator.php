<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo ($administrator->id)?__('Edit Administrator'):__('Create Administrator')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('Administrator List')?></a></li>
          <li class="breadcrumb-item active"><?php echo ($administrator->id)?__('Edit Administrator'):__('Create Edit Administrator')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        <?php 
        //echo '<PRE>';
        //print_r($administrator);exit();

        ?>
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($administrator->id)?__('Edit Administrator').' ('.$administrator->firstname.' '.$administrator->lastname.')':__('Create Administrator')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'email_exist',
                    'value'=>__('Email has been used.Please enter other email address')
                  ))?>
                  <?php echo form_open('',array('name'=>'create-administrator-form'))?>
                  <div class="row">
                      <div class="col-lg-5">

                        <?php if($this->admin_data['type'] == 'superadmin'){?>
                          <div class="form-group">
                                <label><strong><?php echo __('Access Type')?> : </strong></label>
                                <?php echo form_dropdown('type',array(
                                 
                                  'superadmin'=>'SuperAdmin',
                                  'admin'=>'Admin',
                                  'staff'=>'Staff'
                                ),@$administrator->type,'class="form-control" id="sel_access_type" ')?>
                          </div>
                        <?php }?>

                          <div class="row">
                            <div class="col-lg-6">


                              <div class="form-group">
                                <label><strong><?php echo __('Firstname')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'firstname',
                                  'class'=>'form-control',
                                  'value'=>@$administrator->firstname
                                ))?>
                              </div>
                            </div>
                            <div class="col-lg-6">
                              <div class="form-group">
                                <label><strong><?php echo __('Lastname')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'lastname',
                                  'class'=>'form-control',
                                  'value'=>@$administrator->lastname
                                ))?>
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Email')?> : </strong></label>
                            <?php echo form_input(array(
                              'type'=>'email',
                              'name'=>'email',
                              'class'=>'form-control',
                              'value'=>@$administrator->email
                            ))?>
                          </div>

                          <?php if(!$administrator->id){?>
                          <div class="form-group">
                            <label><strong><?php echo __('Password')?> : </strong></label>
                            <?php echo form_input(array(
                              'type'=>'password',
                              'name'=>'password',
                              'class'=>'form-control'
                            ))?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Confirm Password')?> : </strong></label>
                            <?php echo form_input(array(
                              'type'=>'password',
                              'name'=>'confirm_password',
                              'class'=>'form-control'
                            ))?>
                          </div>

                            

                          <?php }else{?>
                          <div class="form-group">
                            <?php echo form_button(array(
                              'type'=>'button',
                              'class'=>'btn btn-primary btn-block',
                              'content'=>'<i class="fa fa-key"></i> '.__('Reset Password'),
                              'data-toggle'=>'modal',
                              'data-target'=>'#resetPasswordModal'
                            ))?>
                          </div>
                          <?php }?>
                          <?php
                          if(!isset($administrator->type)){
                              if($administrator->type != 'staff'){
                          
                          ?>
                          <div class="form-group" id ="div_for_review_deivce">
                            <label><strong><?php echo __('For review device') ?> : </strong></label>
                            <label>
                              <?php echo form_radio([
                                'name'=>'for_review_devices',
                                'value'=>0,
                                'checked'=>TRUE
                              ])?>
                            </label> <span><?php echo __('No','default')?></span>

                            <label>
                              <?php echo form_radio([
                                'name'=>'for_review_devices',
                                'value'=>1,
                                'checked'=>(@$administrator->for_review_devices)?TRUE:FALSE
                              ])?> <span><?php echo __('Yes','default')?></span>
                            </label>
                          </div>
                          <?php 
                              }
                          } 
                          ?>
                          <div class="form-group">
                            <label><strong><?php echo __('Telephone')?> : </strong></label>
                            <?php echo form_input(array(
                              'type'=>'telephone',
                              'name'=>'telephone',
                              'class'=>'form-control',
                              'value'=>@$administrator->telephone
                            ))?>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Status','default')?> : </strong></label>
                            <?php echo form_dropdown('active',array(
                              '1'=>__('Active','default'),
                              '0'=>__('Unactive','default')
                            ),@$administrator->active,'class="form-control"')?>
                          </div>

                          <div class="form-group">
                            <?php echo form_button(array(
                              'type'=>'submit',
                              'class'=>'btn btn-success pull-right',
                              'content'=>__('Submit','default')
                            ))?>
                          </div>
                      </div>
                  </div>
                  <?php echo form_close()?>
            </div>
        </div>
</div>

<!-- Modal -->
<div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Reset password form')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'administrator_id',
        'value'=>@$administrator->id
      ))?>
      <?php echo form_open('',array('name'=>'reset-password-form'))?>
      <div class="modal-body">
        
          <div class="form-group">
            <label><strong><?php echo __('New Password')?> : </strong></label>
            <?php echo form_input(array(
              'type'=>'password',
              'class'=>'form-control',
              'name'=>'new_password'
            ))?>
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Confirm new password')?> : </strong></label>
            <?php echo form_input(array(
              'type'=>'password',
              'class'=>'form-control',
              'name'=>'confirm_new_password'
            ))?>
          </div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Submit','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>

