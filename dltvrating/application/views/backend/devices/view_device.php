<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Device Detail')?> (<?php echo $device_data->ship_code?>)</h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('backend')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('backend/'.$this->controller)?>"><?php echo __('Device List')?></a></li>
          <li class="breadcrumb-item active"><a href="#"><?php echo __('Devices Detail')?></a></li>
        </ul>
</div>
<div class="tile mb-4">
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Device Detail')?></h2>
            </div>
          </div>
        </div>
        <div class="row">
        	<div class="col-lg-12 mb-3">
        		<div id="map-canvas" style="width:100%; min-height:300px;">

	        	</div>
        	</div>
        	<div class="col-lg-6">
	        	<div class="card">
	        		<div class="card-header text-center"><?php echo __('Device Information')?></div>
	        		<div class="card-body">
	        			<p><strong><?php echo __('Ship Code')?> : </strong><?php echo $device_data->ship_code?></p>
	        			<p><strong><?php echo __('IP Address')?> : </strong> <?php echo $device_data->ip_address?></p>
	        			<p><strong><?php echo __('Created')?> : </strong> <?php echo yearMonthDayFormat($device_data->created)?></p>

	        			
	        		</div>
	        	</div>
        	</div>

        	<div class="col-lg-6">
        		<div class="card">
        			<div class="card-header text-center"><?php echo __('Address Information')?></div>
        			<div class="card-body">
        				<p><strong><?php echo __('Continent')?> : </strong><?php echo $device_data->continent_name?></p>
        				<p><strong><?php echo __('Country')?> : </strong><?php echo $device_data->country_name.' ('.$device_data->country_code.')'?></p>
        				<p><strong><?php echo __('City')?> : </strong><?php echo $device_data->city?></p>
        				<p><strong><?php echo __('Zipcode')?> : </strong><?php echo $device_data->zip?></p>

        				<!-- <p><strong><?php echo __('Location')?> : </strong></p> -->
        				<?php echo form_input(array(
        					'type'=>'hidden',
        					'name'=>'device_latitude',
        					'value'=>@$device_data->latitude
        				))?>
        				<?php echo form_input(array(
        					'type'=>'hidden',
        					'name'=>'device_longitude',
        					'value'=>@$device_data->longitude
        				))?>
        				<?php echo form_input(array(
        					'type'=>'hidden',
        					'name'=>'device_address',
        					'value'=>@$device_data->city
        				))?>
	        			
        			</div>
        		</div>
        	</div>
        </div>
</div>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASMadJM_c7vFjnWLWLrM5u2rQeZYGOLKM&callback=initialize_map&libraries=geometry,places&time=<?php echo strtotime(date('Y-m-d H:i:s'))?>">
    </script>