      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Change Password')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('backend')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><a href="#"><?php echo __('Change Password')?></a></li>
        </ul>
      </div>

      <div class="tile mb-4">
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Change password form')?></h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4">
              <?php echo message_warning($this)?>
              <?php echo form_open('',array('name'=>'change-password-form'))?>
                <div class="form-group">
                  <label><strong><?php echo __('Old Password')?> : </strong></label>
                  <?php echo form_input(array(
                    'type'=>'password',
                    'name'=>'old_password',
                    'class'=>'form-control'
                  ))?>
                </div>

                <div class="form-group">
                  <label><strong><?php echo __('New Password')?> : </strong></label>
                  <?php echo form_input(array(
                    'type'=>'password',
                    'name'=>'new_password',
                    'class'=>'form-control'
                  ))?>
                </div>

                <div class="form-group">
                  <label><strong><?php echo __('Confirm New Password')?> : </strong></label>
                  <?php echo form_input(array(
                    'type'=>'password',
                    'name'=>'confirm_new_password',
                    'class'=>'form-control'
                  ))?>
                </div>


                <div class="form-group">
                  <?php echo form_button(array(
                    'type'=>'submit',
                    'class'=>'btn btn-success float-right',
                    'content'=>__('Submit','default')
                  ))?>
                </div>
              <?php echo form_close()?>
          </div>  
        </div>
      </div>