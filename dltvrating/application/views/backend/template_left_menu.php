<aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?php echo getPictureProfile()?>" alt="User Image" width="48" height="48">
        <div>
          <p class="app-sidebar__user-name"><?php echo $this->user_data['firstname'].' '.mb_substr($this->user_data['lastname'],0,1,'utf-8').'.';?></p>
          <p class="app-sidebar__user-designation"></p>
        </div>
      </div>
      <ul class="app-menu">

        <?php if(checkControllerAvailable('dashboard')){?>
        <li><a class="app-menu__item" href="<?php echo base_url('backend/dashboard')?>"><i class="app-menu__icon <?php echo ($this->controller == 'dashboard')?'active':''?> fa fa-dashboard"></i><span class="app-menu__label"><?php echo __('Dashboard')?></span></a></li>
        <?php }?>

        <?php if(checkControllerAvailable('rating')){?>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label"><?php echo __('Rating')?></span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">

            <?php if(checkMethodAvailable(array('controller'=>'rating','method'=>'index'))){?>
            <li><a class="treeview-item" href="<?php echo base_url('backend/rating')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Rating List')?></a></li>
            <?php }?>

            <?php //if(checkMethodAvailable(array('controller'=>'rating','method'=>'rating_by_device'))){?>
              <li><a class="treeview-item" href="<?php echo base_url('backend/dltvrating/rating_by_device')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Rating by chipcode')?></a></li>
            <?php //}?>

            <?php if(checkMethodAvailable(array('controller'=>'rating','method'=>'rating_summary'))){?>
            <li><a class="treeview-item" href="<?php echo base_url('backend/dltvrating/rating_summary')?>"><i class="icon fa fa-circle-o"></i> <?php echo __('Rating Summary')?></a></li>
            <?php }?>
            
          </ul>
        </li>
        <?php }?>


        <?php if(checkControllerAvailable('devices')){?>
        <li><a class="app-menu__item" href="<?php echo base_url('backend/devices')?>"><i class="app-menu__icon <?php echo ($this->controller == 'devices')?'active':''?> fa fa-address-book"></i><span class="app-menu__label"><?php echo __('Devices')?></span></a></li>
        <?php }?>


        <!-- <li style="display: none;"><a class="app-menu__item" href="charts.html"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Charts</span></a></li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Forms</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="form-components.html"><i class="icon fa fa-circle-o"></i> Form Components</a></li>
            <li><a class="treeview-item" href="form-custom.html"><i class="icon fa fa-circle-o"></i> Custom Components</a></li>
            <li><a class="treeview-item" href="form-samples.html"><i class="icon fa fa-circle-o"></i> Form Samples</a></li>
            <li><a class="treeview-item" href="form-notifications.html"><i class="icon fa fa-circle-o"></i> Form Notifications</a></li>
          </ul>
        </li> -->
        <!-- <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview" style="display: none;"><i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">Tables</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="table-basic.html"><i class="icon fa fa-circle-o"></i> Basic Tables</a></li>
            <li><a class="treeview-item" href="table-data-table.html"><i class="icon fa fa-circle-o"></i> Data Tables</a></li>
          </ul>
        </li> -->
        <!-- <li class="treeview is-expanded"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-file-text"></i><span class="app-menu__label">Pages</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item active" href="blank-page.html"><i class="icon fa fa-circle-o"></i> Blank Page</a></li>
            <li><a class="treeview-item" href="page-login.html"><i class="icon fa fa-circle-o"></i> Login Page</a></li>
            <li><a class="treeview-item" href="page-lockscreen.html"><i class="icon fa fa-circle-o"></i> Lockscreen Page</a></li>
            <li><a class="treeview-item" href="page-user.html"><i class="icon fa fa-circle-o"></i> User Page</a></li>
            <li><a class="treeview-item" href="page-invoice.html"><i class="icon fa fa-circle-o"></i> Invoice Page</a></li>
            <li><a class="treeview-item" href="page-calendar.html"><i class="icon fa fa-circle-o"></i> Calendar Page</a></li>
            <li><a class="treeview-item" href="page-mailbox.html"><i class="icon fa fa-circle-o"></i> Mailbox</a></li>
            <li><a class="treeview-item" href="page-error.html"><i class="icon fa fa-circle-o"></i> Error Page</a></li>
          </ul>
        </li> -->
      </ul>
    </aside>