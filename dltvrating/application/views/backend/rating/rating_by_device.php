      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Search rating by chipcode')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('backend')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><a href="#"><?php echo __('Ratings')?></a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">

              
              <?php echo form_open('',array('name'=>'search-rating-logs','method'=>'get'))?>
              <div class="row">
                <div class="col-lg-4 offset-lg-4">
                <div class="form-group">
                  <label><strong><?php echo __('Chip Code')?> : </strong></label>
                  <?php echo form_input(array(
                    'name'=>'chipcode',
                    'class'=>'form-control',
                    'value'=>@$this->input->get('chipcode'),
                    'required'=>'required'
                  ))?>
                </div>

                <div class="form-group">
                  <?php echo form_button(array(
                    'type'=>'submit',
                    'class'=>'btn btn-primary btn-block',
                    'content'=>__('Search')
                  ))?>
                </div>
                </div>
              </div>

              <?php echo form_close()?>

              <?php if(empty((array)$rating_data)){?>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="alert alert-danger text-center">
                        <?php echo __('Not found rating data')?>
                      </div>
                    </div>
                  </div>
              <?php }else{?>
              <div class="table-result">

                <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('ChipCode')?></th>
                    <th><?php echo __('Channel')?></th>
                    <th><?php echo __('FRQ')?></th>
                    <th><?php echo __('SYM')?></th>
                    <th><?php echo __('POL')?></th>
                    <th><?php echo __('Service Id')?></th>
                    <th><?php echo __('VDO PID')?></th>
                    <th><?php echo __('ADO PID')?></th>
                    <th><?php echo __('Start View')?></th>                    
                    <th><?php echo __('Duration (Seconds)')?></th>
                    <th><?php echo __('Created')?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($rating_data->result() as $key => $row){?>
                    <tr>
                      <td><a href="<?php echo base_url('backend/devices/view_device/'.$row->devices_id)?>" target="_blank"><?php echo $row->ship_code?></a></td>
                      <td><?php echo ($row->channels_id)?getChannelNameById($row->channels_id):'-'?></td>
                      <td><?php echo $row->frq?></td>
                      <td><?php echo $row->sym?></td>
                      <td><?php echo $row->pol?></td>
                      <td><?php echo $row->server_id?></td>
                      <td><?php echo $row->vdo_pid?></td>
                      <td><?php echo $row->ado_pid?></td>
                      <!-- <td><?php echo yearMonthDayFormat($row->startview_datetime)?></td> -->
                      <td><?php echo date('d-m-Y H:i:s',strtotime($row->startview_datetime))?></td>
                      <td><?php echo $row->view_seconds?></td>
                      <!-- <td><?php echo yearMonthDayFormat($row->created)?></td> -->
                      <td><?php echo date('d-m-Y H:i:s',strtotime($row->created))?></td>

                    </tr>
                  <?php }?>
                </tbody>
              </table>

              </div>
              <?php }?>


            </div>
          </div>
        </div>
      </div>