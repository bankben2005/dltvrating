<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_districts extends DataMapper {

    //put your code here
    var $table = 'districts';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'amphurs' => array(
             'class' => 'M_amphurs',
               'other_field' => 'districts',
               'join_other_as' => 'amphurs',
               'join_table' => 'amphurs'
           )
   );
    
   //  var $has_many = array(
   //     'device_addresses' => array(
   //         'class' => 'M_device_addresses',
   //         'other_field' => 'devices',
   //         'join_self_as' => 'devices',
   //         'join_other_as' => 'devices',
   //         'join_table' => 'device_addresses')
   // );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}