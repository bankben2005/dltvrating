<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_amphurs extends DataMapper {

    //put your code here
    var $table = 'amphurs';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'provinces' => array(
             'class' => 'M_provinces',
               'other_field' => 'amphurs',
               'join_other_as' => 'provinces',
               'join_table' => 'provinces'
           )
   );
    
    var $has_many = array(
       'districts' => array(
           'class' => 'M_districts',
           'other_field' => 'amphurs',
           'join_self_as' => 'amphurs',
           'join_other_as' => 'amphurs',
           'join_table' => 'districts')
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}