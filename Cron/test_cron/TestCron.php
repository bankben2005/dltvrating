<?php
date_default_timezone_set('Asia/Bangkok');
class TestCron{
// Create connection

	function __construct(){
		
	}

	public function testWriteTextToFile(){

		$log_file_path = $this->createLogFilePath('testCrontabWriteLogFile');
        $file_content = date("Y-m-d H:i:s") . "\n";
        file_put_contents($log_file_path, $file_content, FILE_APPEND);
        unset($file_content);

		
	}
	private function createLogFilePath($filename = '') {
        $log_path = './logs/test_logs';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

	
}

$cron = new TestCron();
$cron->testWriteTextToFile();
//$cron->setSubscriptionSchedule();
//$cron->checkTransactionSubscription();
//$cron->connClose();

?>